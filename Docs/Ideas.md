#### Structures

##### Management
- Office (staff limit and boon modules)
- Library - "Food for thought" (attracts researchers)

##### Accessibilty
Attract people with a bad bus connection.

###### B
- Bike rack
- Parking lot

##### Toilets
###### S  
- Portable toilet - "Good enough... sometimes"

###### B  
- Urinoirium

##### Other seats

###### B
Bleachers/stands - requires slope

##### Trashcans
###### S
- Bin

###### B
- Humpty Dumpster

### Neighbours
Some land cannot be bought per square, but only as a full lot, house and all.
Private lots will devalue when there's too much noise nearby. This may sound good, but the residents may sue you if you don't make them a reasonable offer, or lessen the noise.

* S: Subgrid, B: Non-subgrid
