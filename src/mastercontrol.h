/* LucKey Park
// Copyright (C) 2024 LucKey Productions (https://luckey.games)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef MASTERCONTROL_H
#define MASTERCONTROL_H

#include <Dry/DryAll.h>
#include <Dry/Engine/Application.h>

/// Opaque zomp color.
const Color COLOR_ZOMP{ .222f, .652f, .551f, 1.f };

enum Layer{ LAYER_GROUND = 0, LAYER_CHARACTER, LAYER_STRUCTURE, LAYER_DECO, LAYER_UI };
#define LAYER(x) (1 << x)
#define RES(x, y) GetSubsystem<ResourceCache>()->GetResource<x>(y)
#define RES_TEMP(x, y) GetSubsystem<ResourceCache>()->GetTempResource<x>(y)
#define MC GetSubsystem<MasterControl>()

#define TAG_CAMTARGET "CamTarget"
#define FLAT(v) IntVector2{ v.x_, v.z_ }

template <class T> inline T Random(const Vector<T> items)
{
    if (items.IsEmpty())
        return T{};

    return items.At(Random(static_cast<int>(items.Size())));
}

template <class T> inline T Random(const PODVector<T> items)
{
    if (items.IsEmpty())
        return T{};

    return items.At(Random(static_cast<int>(items.Size())));
}

class MasterControl: public Application
{
    DRY_OBJECT(MasterControl, Application)

public:
    MasterControl(Context* context);

    void Setup() override;
    void Start() override;
    void Stop() override;

    String GetSettingsPath()  const { return storagePath_ + "/Settings.json"; }
    String GetSaveGamePath(const String& filename = "") const
    {
        if (filename.StartsWith("/"))
            return storagePath_ + filename;
        else
            return storagePath_ + "/Save/" + filename;
    }

private:
    void FindResourcePath();
    void SaveSettings() const;

    void HandleUpdate(StringHash eventType, VariantMap& eventData);
    void HandleSank(StringHash /*eventType*/, VariantMap& /*eventData*/)
    { Log::Write(LOG_INFO, "You sank it!" ); }

    String storagePath_;
    void ConfigureDefaultZoneAndRenderPath();
};

DRY_EVENT(E_SANK, Sank) {};

static PODVector<IntVector2> Neighbors(const IntVector2& coords, bool diagonal = false)
{
    if (diagonal)
    {
        return { IntVector2{ -1, -1 } + coords, IntVector2{ 1, -1 } + coords,
                 IntVector2{ -1,  1 } + coords, IntVector2{ 1,  1 } + coords};
    }
    else
    {
        return { IntVector2{ -1,  0 } + coords, IntVector2{ 1, 0 } + coords,
                 IntVector2{  0, -1 } + coords, IntVector2{ 0, 1 } + coords};
    }
}

static PODVector<IntVector2> AllNeighbors(const IntVector2& coords)
{
    return { Neighbors(coords, true) + Neighbors(coords, false) };
}

#endif // MASTERCONTROL_H
