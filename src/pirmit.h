/* LucKey Park
// Copyright (C) 2024 LucKey Productions (https://luckey.games)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef PIRMIT_H
#define PIRMIT_H

#include <Dry/Container/Str.h>
#include <Dry/Math/MathDefs.h>
#include <Dry/Core/StringUtils.h>

using namespace Dry;

const String octN{ "012345678" };
const String octA{ "abcdefghi" };

static String EncodeOctal(int num, bool alpha)
{
    String res{};
    bool neg{ num < 0 };
    if (neg)
        num = Abs(num);

    while (num || res.IsEmpty())
    {
        if (alpha)
            res = String{ octA.At(num % 010) } + res;
        else
            res = String{ octN.At(num % 010) } + res;
        num /= 010;
    }

    if (neg)
        res.Append((alpha ? octA.Back() : octN.Back()));

    return res;
}

static String EncodeOctal(const int* const data, unsigned dataSize, bool startAlpha = false)
{
    String coded{};
    unsigned i{ 0u };
    while (i < dataSize)
    {
        int digit{ data[i] };
        coded.Append(EncodeOctal(digit, ((i + startAlpha) % 2) != 0));
        ++i;
    }

    return coded;
}

static PODVector<int> DecodeOctal(const String& coded)
{
    PODVector<int> res{};
    int r{};
    bool alpha{ IsAlpha(coded.Front()) };
    for (char c: coded)
    {
        bool cAlpha{ IsAlpha(c) };
        if (alpha != cAlpha)
        {
            res.Push(r);
            r = 0;
            alpha = cAlpha;
        }

        unsigned i{};
        if (alpha)
            i = octA.Find(c);
        else
            i = octN.Find(c);

        if (i < 8u)
        {
            r *= 010;
            r += i;
        }
        else
        {
            r *= -1;
        }
    }

    res.Push(r);
    return res;
}

static PODVector<IntVector2> DecodeOctal2(const String& coded)
{
    const PODVector<int> integers{ DecodeOctal(coded) };
    PODVector<IntVector2> res{};
    for (unsigned i{ 0u }; i < integers.Size() - 1u; i += 2u)
        res.Push({ integers.At(i), integers.At(i + 1u) });

    return res;
}

static PODVector<IntVector3> DecodeOctal3(const String& coded)
{
    const PODVector<int> integers{ DecodeOctal(coded) };
    PODVector<IntVector3> res{};
    for (unsigned i{ 0u }; i < integers.Size() - 2u; i += 3u)
        res.Push({ integers.At(i), integers.At(i + 1u), integers.At(i + 2u) });

    return res;
}

#endif // PIRMIT_H
