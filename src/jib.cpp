/* LucKey Park
// Copyright (C) 2024 LucKey Productions (https://luckey.games)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "jib.h"

#include "game.h"
#include "world.h"
#include "utility.h"
#include "ui/gui.h"

Jib::Jib(Context* context): Component(context),
    activeCamera_{ nullptr },
    following_{ nullptr },
    lastTargetPos_{ Vector3::ONE * M_INFINITY },
    lastTargetVelocity_{ Vector3::ZERO },
    pivot_{ Vector3::ZERO },
    jump_{ Vector3::ZERO },
    overlook_{ 5.f },
    lastPan_{},
    lastRot_{},
    middleMouseDown_{ false }
{
    SubscribeToEvent(E_POSTUPDATE, DRY_HANDLER(Jib, HandlePostUpdate));
    SubscribeToEvent(E_MOUSEBUTTONDOWN, DRY_HANDLER(Jib, HandleMouseButtonDown));
    SubscribeToEvent(E_MOUSEBUTTONUP, DRY_HANDLER(Jib, HandleMouseButtonUp));
}

void Jib::OnNodeSet(Node* node)
{
    if (!node)
        return;

    activeCamera_ = CreateCamera(node_);
    activeCamera_->SetFarClip(340.f);

    Viewport* viewport{ new Viewport{ context_, GetScene(), activeCamera_ } };
    GetSubsystem<Renderer>()->SetViewport(0, viewport);

//    SubscribeToEvent(E_POSTRENDERUPDATE, DRY_HANDLER(Jib, JibDebug));
}

void Jib::HandleMouseButtonDown(StringHash /*eventType*/, VariantMap& eventData)
{
    if (eventData[MouseButtonDown::P_BUTTON].GetInt() == MOUSEB_MIDDLE
      && GetSubsystem<UI>()->GetFocusElement() == nullptr)
    {
        middleMouseDown_ = true;

        Input* input{ GetSubsystem<Input>() };
        UI* ui{ GetSubsystem<UI>() };
        input->SetMouseMode(MM_WRAP);

        if (following_)
            return;

        Ray ray{ GetScreenRay(VectorClamp(ui->GetCursorPosition(), { 0, GUI::sizes_.screen_.y_ / 4 }, { M_MAX_INT, GUI::sizes_.screen_.y_ * 3/4 })) };

        Octree* octree{ GetScene()->GetComponent<Octree>() };
        PODVector<RayQueryResult> result{};
        unsigned viewMask{ LAYER(LAYER_GROUND) };
        RayOctreeQuery query{ result, ray, RAY_TRIANGLE, M_INFINITY, DRAWABLE_GEOMETRY, viewMask };
        octree->RaycastSingle(query);

        if (query.result_.Size())
        {
            Vector3 pivot{ result.Front().position_.ProjectOntoPlane(node_->GetWorldRight(), node_->GetWorldPosition()) };

            const Vector3 flatDirection{ node_->GetDirection().ProjectOntoPlane(Vector3::UP).Normalized() };
            const float ahead{ flatDirection.DotProduct(pivot - node_->GetWorldPosition()) - 2.f  };
            const float maxDist{ 100.f };

            if (ahead < 0.f)
                pivot -= flatDirection * ahead;
            else if (ahead > maxDist)
                pivot = node_->GetWorldPosition() + (pivot - node_->GetWorldPosition()).Normalized() * maxDist;

            SetPivot(pivot);
        }
    }
}

void Jib::HandleMouseButtonUp(StringHash /*eventType*/, VariantMap& eventData)
{
    if (eventData[MouseButtonDown::P_BUTTON].GetInt() == MOUSEB_MIDDLE)
    {
        middleMouseDown_ = false;

        Input* input{ GetSubsystem<Input>() };
        input->SetMouseMode(MM_ABSOLUTE);
    }
}

bool Jib::SaveJSON(JSONValue& dest) const
{
    JSONValue info{};
    info.Set("pivot", pivot_.ToString());
    info.Set("overlook", overlook_);
    info.Set("position", node_->GetWorldPosition().ToString());
    dest.Set("camera", info);

    return true;
}

bool Jib::LoadJSON(const JSONValue& source)
{
    JSONValue info{ source.Get("camera") };
    SetPivot(FromString<Vector3>(info.Get("pivot").GetString()), false);
    overlook_ = info.Get("overlook").GetFloat();
    SetPosition(FromString<Vector3>(info.Get("position").GetString()));

    return true;
}

void Jib::JibDebug(StringHash /*eventType*/, VariantMap& /*eventData*/)
{
    DrawDebugGeometry(GetScene()->GetOrCreateComponent<DebugRenderer>(), true);
}

void Jib::DrawDebugGeometry(DebugRenderer* debug, bool depthTest)
{
//    activeCamera_->DrawDebugGeometry(debug, depthTest);
    debug->AddCross(pivot_, 2.f, Color::ORANGE, depthTest);
}

void Jib::Restore()
{
    Unfollow();

    lastPan_ = jump_ = Vector3::ZERO;
    lastRot_ = Quaternion::IDENTITY;
    overlook_ = 5.f;

    World* world{ GetSubsystem<World>() };
    const float edge{ (-world->FlatSize().y_ * .5f - 4.f) * GRID };
    SetPivot(Vector3::FORWARD * (edge + 42.f), false);
    SetPosition({ 0.f, 17.f, edge - 23.f });
}

void Jib::SetPivot(const Vector3& pivot, bool correctOverlook)
{
    pivot_ = pivot;

    if (correctOverlook)
    {
        const Ray pivotRay{ pivot_, Vector3::UP };
        const Vector3 closest{ pivotRay.ClosestPoint(activeCamera_->GetScreenRay({ .5f, .5f })) };

        overlook_ = closest.y_ - pivot_.y_;
    }
}

void Jib::UpdatePivot()
{
    Octree* octree{ GetScene()->GetComponent<Octree>() };
    PODVector<RayQueryResult> result{};
    Ray ray{ (following_ ? activeCamera_->GetScreenRay(activeCamera_->WorldToScreenPos(pivot_))
                         : activeCamera_->GetScreenRay({ .5f, .5f })) };

    unsigned viewMask{ LAYER(LAYER_GROUND) };
    RayOctreeQuery query{ result, ray, RAY_TRIANGLE, M_INFINITY, DRAWABLE_GEOMETRY, viewMask };
    octree->RaycastSingle(query);

    if (query.result_.Size())
        SetPivot(query.result_.Front().position_, true);
}

void Jib::Jump(const Vector3& target)
{
    UpdatePivot();

    jump_ = target - pivot_;
}

void Jib::Follow(Node* target)
{
    if (following_ == target)
        return;

    if (following_ && target)
    {
        Jump(target->GetWorldPosition() - following_->GetWorldPosition());
    }
    else
    {
        if (target)
            SetPivot(target->GetWorldPosition().ProjectOntoPlane(node_->GetWorldRight(), node_->GetWorldPosition()));
    }

    following_ = target;
    lastTargetPos_ = Vector3::ONE * M_INFINITY;
}

void Jib::Unfollow()
{
    Follow(nullptr);
}

void Jib::SetPosition(const Vector3& pos)
{
    node_->SetWorldPosition(pos);
    node_->LookAt(pivot_ + Vector3::UP * overlook_);

    SendMouseMoveEvent();
}

void Jib::HandlePostUpdate(StringHash /*eventType*/, VariantMap& eventData)
{
    const GameStatus gs{ GetSubsystem<Game>()->GetStatus() };
    if (gs == GS_MAIN || gs == GS_MODAL)
        return;

    Input* input{ GetSubsystem<Input>() };
    if (input->GetKeyPress(KEY_HOME))
    {
        Restore();
        return;
    }

    UI* ui{ GetSubsystem<UI>() };
    const Vector3 camPos{ node_->GetWorldPosition() };
    const IntVector2 cursorPos{ ui->GetCursorPosition() };
    const bool hover{ ui->GetElementAt(cursorPos) != nullptr };
    const int mouseWheel{ (hover && !middleMouseDown_ ? 0 : input->GetMouseMoveWheel()) };
    const IntVector2 mouseMove{ input->GetMouseMove() };
    const IntVector2 screenSize{ GetSubsystem<Graphics>()->GetSize() };

    Vector2 panEdge{};
    if (!middleMouseDown_ && input->HasFocus() && GetSubsystem<Graphics>()->GetFullscreen())
    {
        const int margin{ 1 + GUI::sizes_.margin_ * (hover ? 3 : 16) };
        const Vector2 edge{ margin * Vector2::ONE };
        const IntRect fromEdge{ { Clamp(cursorPos.x_, 0, margin), Clamp(cursorPos.y_, 0, margin) },
                                { Clamp(screenSize.x_ - cursorPos.x_, 0, margin), Clamp(screenSize.y_ - cursorPos.y_, 0, margin) } };

        panEdge = Vector2{ (VectorMax(Vector2::ZERO, edge - fromEdge.Max())
                            - VectorMax(Vector2::ZERO, edge - fromEdge.Min()))
                  / edge };
    }

    InputMaster* im{ GetSubsystem<InputMaster>() };
    const float timeStep{ eventData[Update::P_TIMESTEP].GetFloat() };
    const bool fast{ input->GetQualifierDown(QUAL_SHIFT) || im->GetAction(IA_FAST) };
    const Vector3 right{ node_->GetWorldRight() };

    IntVector3 panKey{ input->GetKeyDown(KEY_D) - input->GetKeyDown(KEY_A),
                       (input->GetKeyDown(KEY_KP_MINUS) || input->GetKeyDown(KEY_E)) - (input->GetKeyDown(KEY_KP_PLUS) || input->GetKeyDown(KEY_Q)),
                       input->GetKeyDown(KEY_W) - input->GetKeyDown(KEY_S)};

    Vector3 panJoy{ 0.f, im->GetTriggerPosition(), 0.f };
    bool joystickRotate{ im->GetAction(IA_INVERT) };
    if (!joystickRotate)
    {
        panJoy += { im->GetStickPosition(true).x_,
                   0.f,
                   -im->GetStickPosition(true).y_ };

        if (panJoy.LengthSquared() > .1f)
            UpdatePivot();

        panJoy *= Vector3{ 2.3f, .8f, 2.3f };
    }

    const Vector3 forward{ node_->GetWorldDirection().ProjectOntoPlane(Vector3::UP).Normalized() };
    const float angleScalar{ M_1_SQRT2 };
    const Vector3 up{ Quaternion{ (90.f + Asin(node_->GetWorldDirection().y_)) * angleScalar, -node_->GetWorldRight()} * Vector3::UP };
    const Vector3 panInput{ panKey + panJoy };
    Vector3 translate { panInput.x_ * right +
                        panInput.y_ * up * 2.3f +
                        panInput.z_ * forward };

    translate  *= 23.f * (.333f + 3.f * fast) * timeStep;

    const float heightFactor{ .125f + .25f * Sqrt(Abs(node_->GetWorldPosition().y_)) };

    translate  +=   panEdge.x_ * heightFactor * right
           - mouseWheel * heightFactor * 2.3f * up
           - panEdge.y_ * heightFactor * forward;

    translate  = lastPan_.Lerp(translate , Min(1.f, timeStep * 5.f));

    Vector2 rotInput{ input->GetKeyDown(KEY_L)    - input->GetKeyDown(KEY_J) +
                      input->GetKeyDown(KEY_KP_4) - input->GetKeyDown(KEY_KP_6) * 1.f,
                      input->GetKeyDown(KEY_I)    - input->GetKeyDown(KEY_K) +
                      input->GetKeyDown(KEY_KP_8) - input->GetKeyDown(KEY_KP_2) * 1.f };

    if (following_)
    {

        Vector3 targetPos{ following_->GetWorldPosition() };
        Vector3 targetVelocity{};
        if (lastTargetPos_.x_ != M_INFINITY)
            targetVelocity = timeStep == 0.f ? Vector3::ZERO : Lerp(lastTargetVelocity_, (targetPos - lastTargetPos_) / timeStep, Min(1.f, timeStep * 9.f));

        const Vector3 camToTarget{ targetPos - camPos };
        const float targetDist{ camToTarget.Length() };
        translate = camToTarget.Normalized() * Min(translate .DotProduct(-up), targetDist - GRID * M_1_SQRT2);
        Jump(targetPos + targetVelocity * SUBGRID * M_1_SQRT2);
        overlook_ = Lerp(overlook_, SUBGRID * (1.f + .23f * targetDist / GRID), Min(1.f, timeStep * 5.f));

        lastTargetPos_ = targetPos;
        lastTargetVelocity_ = targetVelocity;
        rotInput.x_ += panInput.x_;
    }

    const Vector3 jump{ (Equals(jump_.Length(), 0.f, SUBGRID * .1f) ? Vector3::ZERO : jump_ * Min(1.f, timeStep * 3.f)) };
    jump_ -= jump;

    node_->Translate(translate  + jump, TS_WORLD);
    pivot_ += translate  + jump;
    lastPan_ = translate;

    const Vector2 rightStick{ im->GetStickPosition(true) };
    if (joystickRotate)
        rotInput -= 5.5f * Vector2{ PowN(rightStick.x_, 3), PowN(rightStick.y_, 3) };

    rotInput.NormalizedOrDefault();
    rotInput *= 5.f * (3.f + 20.f * fast) * timeStep;

    Vector2 rotMouse{};
    if (middleMouseDown_)
        rotMouse = { 180.f * mouseMove.x_ / screenSize.x_,
                      90.f * mouseMove.y_ / screenSize.y_ };
    else if (following_)
        rotMouse = { -panEdge.x_, 0.f };

    float rotY{ (rotInput.y_ + rotMouse.y_) * Sign(node_->GetWorldUp().y_)};
    const float pitchBrake{ (node_->GetWorldDirection().Angle(Vector3::UP) - 90.f) / 90.f };
    if (Sign(rotY) == Sign(pitchBrake))
        rotY *= 1.f - Abs(pitchBrake);

    Quaternion rot{ Quaternion{ rotInput.x_ - rotMouse.x_, Vector3::DOWN }
                  * Quaternion{ rotY, right } };
    rot = lastRot_.Slerp(rot, Min(1.f, timeStep * 12.f));

    node_->RotateAround(pivot_, rot, TS_WORLD);
    node_->LookAt(pivot_ + Vector3::UP * overlook_);

    lastRot_ = rot;

    if ((translate  + jump).Length() > .01f || Abs(rot.Angle()) > .1f)
        SendMouseMoveEvent();
}

void Jib::SendMouseMoveEvent()
{
    VariantMap data{};

    Input* input{ GetSubsystem<Input>() };
    UI* ui{ GetSubsystem<UI>() };
    const IntVector2 cursorPos{ ui->GetCursorPosition() };

    data[MouseMove::P_X] = cursorPos.x_;
    data[MouseMove::P_Y] = cursorPos.y_;
    data[MouseMove::P_DX] = 0;
    data[MouseMove::P_DY] = 0;
    data[MouseMove::P_BUTTONS] = MOUSEB_NONE;
    data[MouseMove::P_QUALIFIERS] = static_cast<unsigned>(input->GetQualifiers());

    SendEvent(E_MOUSEMOVE, data);
}
