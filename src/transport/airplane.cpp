/* LucKey Park
// Copyright (C) 2024 LucKey Productions (https://luckey.games)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "../world.h"

#include "airplane.h"

Airplane::Airplane(Context* context): Vehicle(context)
{
}


void Airplane::OnNodeSet(Node* node)
{
    if (!node)
        return;

    const int size{ Random(4) };
    capacity_ = size * 32 + 8;

    SharedPtr<Material> paint{ GetSubsystem<Materials>()->Paint(Shade(Random(64), DiceRoll(2, 3), DiceRoll(2, 3) + 2)) };
    SharedPtr<Material> glass{ RES(Material, "Materials/Metal.xml") };
    SharedPtr<Material> tires{ GetSubsystem<Materials>()->Paint({ 4, 1, 1 }) };

    Node* cockpitNode{ node_->CreateChild("Cockpit")};
    cockpitNode->Translate(Vector3::FORWARD * GRID);
    StaticModel* cockpitModel{ cockpitNode->CreateComponent<StaticModel>() };
    cockpitModel->SetModel(RES(Model, "Models/Plane/Cockpit.mdl"));
    cockpitModel->SetMaterial(0, paint);
    cockpitModel->SetMaterial(1, glass);
    cockpitModel->SetMaterial(2, tires);
    cockpitModel->SetCastShadows(true);
    cockpitModel->SetViewMask(LAYER(LAYER_STRUCTURE));

    StaticModelGroup* sectionGroup{ node_->CreateComponent<StaticModelGroup>() };
    sectionGroup->SetModel(RES(Model, "Models/Plane/Section.mdl"));
    sectionGroup->SetMaterial(0, paint);
    sectionGroup->SetMaterial(1, glass);
    sectionGroup->SetCastShadows(true);
    sectionGroup->SetViewMask(LAYER(LAYER_STRUCTURE));

    for (int i{ 0 }; i < size; ++i)
    {
        Node* sectionNode{ node_->CreateChild("Section") };
        sectionNode->Translate(Vector3::BACK * i * GRID);
        sectionGroup->AddInstanceNode(sectionNode);
    }

    Node* tailNode{ node_->CreateChild("Tail") };
    tailNode->Translate(Vector3::BACK * GRID * size);
    AnimatedModel* tailModel{ tailNode->CreateComponent<AnimatedModel>() };
    tailModel->SetModel(RES(Model, "Models/Plane/Tail.mdl"));
    tailModel->SetMaterial(0, paint);
    tailModel->SetMaterial(1, glass);
    tailModel->SetCastShadows(true);
    tailModel->SetMorphWeight(0, (size - 1) * .5f);
    tailModel->SetViewMask(LAYER(LAYER_STRUCTURE));

    Node* wingNode{ node_->CreateChild("Wings") };
    wingNode->Translate(Vector3::BACK * (size - 1) * 1.25f);
    wingNode->Scale({ 1.f, 1.f, .9f + size * 0.1f });
    AnimatedModel* wingModel{ wingNode->CreateComponent<AnimatedModel>() };
    wingModel->SetModel(RES(Model, "Models/Plane/Wings.mdl"));
    wingModel->SetMaterial(0, paint);
    wingModel->SetMaterial(1, glass);
    wingModel->SetMaterial(2, tires);
    wingModel->SetCastShadows(true);
    wingModel->SetMorphWeight(0, (size - 1) * .5f);
    wingModel->SetViewMask(LAYER(LAYER_STRUCTURE));
}
