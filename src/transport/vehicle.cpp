/* LucKey Park
// Copyright (C) 2024 LucKey Productions (https://luckey.games)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "car.h"
#include "airplane.h"
#include "../materials.h"
#include "../world.h"

#include "vehicle.h"

void Vehicle::RegisterObject(Context* context)
{
    context->RegisterFactory<Car>();
    context->RegisterFactory<Airplane>();
}

Vehicle::Vehicle(Context* context): Component(context),
    capacity_{ 1u },
    suspension_{ 0.f },
    seats_{}
{
}

void Vehicle::OnSetEnabled() { Component::OnSetEnabled(); }
void Vehicle::OnNodeSet(Node* node)
{
    if (!node)
        return;
}

void Vehicle::OnSceneSet(Scene* scene) { Component::OnSceneSet(scene); }
void Vehicle::OnMarkedDirty(Node* node) {}
void Vehicle::OnNodeSetEnabled(Node* node) {}
bool Vehicle::Save(Serializer& dest) const { return Component::Save(dest); }
bool Vehicle::SaveXML(XMLElement& dest) const { return Component::SaveXML(dest); }
bool Vehicle::SaveJSON(JSONValue& dest) const { return Component::SaveJSON(dest); }
void Vehicle::MarkNetworkUpdate() { Component::MarkNetworkUpdate(); }
void Vehicle::GetDependencyNodes(PODVector<Node*>& dest) {}
void Vehicle::DrawDebugGeometry(DebugRenderer* debug, bool depthTest) {}
