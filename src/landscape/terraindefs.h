/* LucKey Park
// Copyright (C) 2024 LucKey Productions (https://luckey.games)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef TERRAINDEFS_H
#define TERRAINDEFS_H

#include "../mastercontrol.h"

struct HeightProfile
{
    using Elevation = Vector<int>;
/*
    0,1 B   C 1,1
          X
    0,0 A   D 1,0
*/
    static IntVector2 CornerOffset(char index)
    {
        IntVector2 offset{ 0, 0 };

        switch (index) {
        default: break;
        case 1: ++offset.y_; break;
        case 2: ++offset.x_; ++offset.y_; break;
        case 3: ++offset.x_; break;
        }

        return offset;
    }

    HeightProfile(): HeightProfile(0, 0, 0, 0) {}
    HeightProfile(int h): HeightProfile(h, h, h, h) {}
    HeightProfile(const Elevation& profile): profile_{ profile }
    {}

    HeightProfile(int a, int b, int c, int d):
        profile_{ { a, b, c, d } }
    {}

    unsigned ToHash() const
    {
        unsigned hash = 37;
        hash = 37 * hash + profile_[0];
        hash = 37 * hash + profile_[1];
        hash = 37 * hash + profile_[2];
        hash = 37 * hash + profile_[3];

        return hash;
    }

    int Reduce();

    int Max() const
    {
        int res{ profile_.At(0) };
        for (unsigned i{ 1 }; i < profile_.Size(); ++i)
            if (profile_.At(i) > res)
                res = profile_.At(i);

        return res;
    }

    int Min() const
    {
        int res{ profile_.At(0) };
        for (unsigned i{ 1 }; i < profile_.Size(); ++i)
            if (profile_.At(i) < res)
                res = profile_.At(i);

        return res;
    }

    Elevation GetProfile() const { return profile_; }

    bool operator ==(const HeightProfile& rhs) const
    {
        return rhs.GetProfile() == profile_;
    }

    bool operator !=(const HeightProfile& rhs) const
    {
        return rhs.GetProfile() != profile_;
    }

    bool IsFlat() const;
    bool IsSlope() const;

private:
    Elevation profile_;
};

class HeightMap: protected HashMap<IntVector2, int>
{
public:
    void Generate();

    HeightMap(const IntVector2& size): HashMap<IntVector2, int>(),
        width_{ size.x_ },
        depth_{ size.y_ }
    {
        for (int x{ 0 }; x < width_; ++x)
        for (int y{ 0 }; y < depth_; ++y)
            Insert({ { x, y }, 0 });
    }

    void Erode(int steps = 2, bool walk = false);

    IntVector2 GetSize() const { return { width_, depth_ }; }
    int GetWidth() const { return width_; }
    int GetDepth() const { return depth_; }

    int GetHeightAt(const IntVector2& coords) const { return GetHeightAt(coords.x_, coords.y_); }
    int GetHeightAt(int x, int y) const {
        if (x >= 0 && x < width_ && y >= 0 && y < depth_)
        {
            int elevation{};
            if (TryGetValue({ x, y }, elevation))
                return elevation;
        }

        return M_MAX_INT;
    }

    void SetHeightAt(const IntVector2& coords, int height) { SetHeightAt(coords.x_, coords.y_, height); }
    void SetHeightAt(int x, int y, int height)
    {
        if (WithinBounds({ x, y }))
            operator []({ x, y }) = height;
    }

    void Raise(const IntVector2& coords, int up)
    {
        SetHeightAt(coords.x_, coords.y_, GetHeightAt(coords) + up);
    }

    HeightProfile GetHeightProfileAt(const IntVector2& coords) const
    {
        return GetHeightProfileAt(coords.x_, coords.y_);
    }

    HeightProfile GetHeightProfileAt(int x, int y) const
    {
        HeightProfile::Elevation height{};

        for (unsigned c{ 0 }; c < 4; ++c)
        {
            IntVector2 coords{ x, y };

            coords += HeightProfile::CornerOffset(c);

            coords = VectorClamp(coords, IntVector2::ZERO, GetSize() - IntVector2::ONE );
            height.Push(GetHeightAt(coords));
        }

        HeightProfile profile{ height };

        return profile;
    }

    bool WithinBounds(const IntVector2& coords, int margin = 0) const
    {
        return (coords.x_ >= margin && coords.x_ < width_ - margin && coords.y_ >= margin && coords.y_ < depth_ - margin);
    }

private:
    int width_;
    int depth_;
};


class Tile
{
public:
    Tile(): Tile(nullptr) {}
    Tile(Node* node, const HeightProfile& profile = { M_MAX_INT }):
        node_{ node },
        profile_{ profile }
    {
        if (node)
        {
            node_->SetTemporary(true);

            tileModel_ = node_->CreateComponent<StaticModel>();
            tileModel_->SetCastShadows(true);
            tileModel_->SetViewMask(LAYER(LAYER_GROUND));
        }
    }

    void Set(const HeightProfile& profile, char rotation = 0)
    {

        node_->SetRotation(Quaternion{ 90.f * rotation, Vector3::UP });
        HashMap<HeightProfile, SharedPtr<Model> > tileSet{ GetTileset() };

        Model* model{ nullptr };
        if (tileSet.Contains(profile))
            model = tileSet[profile].Get();

        tileModel_->SetModel(model);
        Material* mat{ node_->GetSubsystem<ResourceCache>()->GetResource<Material>("Materials/Ground.xml") };
        tileModel_->SetMaterial(mat);

        profile_ = profile;
    }

    Node* GetNode() const { return node_; }

private:
    HashMap<HeightProfile, SharedPtr<Model> > GetTileset() const;

    Node* node_;
    StaticModel* tileModel_;
    HeightProfile profile_;

};

#endif // TERRAINDEFS_H
