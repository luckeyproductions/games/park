/* LucKey Park
// Copyright (C) 2024 LucKey Productions (https://luckey.games)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "../world.h"
#include "ground.h"

#include "terraindefs.h"


HashMap<HeightProfile, SharedPtr<Model> > Tile::GetTileset() const
{
    return node_->GetSubsystem<World>()->GetGround()->GetTileset();
}

void HeightMap::Generate()
{
    IntVector2 peaks{ DiceRoll(2, 3), DiceRoll(2, 3)};
    int scale{ Max(width_, depth_) / 10 };

    for (int x{ 0 }; x < width_; ++x)
        for (int y{ 0 }; y < depth_; ++y)
        {
            const int max{ Min(Max(0, y - 8 - scale) / scale, (Abs(x - width_ / 2) + (x > width_ / 2) - scale / 2) / scale + Max(0, (y - depth_ / 2 - scale) / scale)) };

            const int elevation{
                Min(Min(42, max * max), DiceRoll(max, max + 5) - max )
                        //                Min(Max(0, Abs(x - width_ / 2) - DiceRoll(2, 3)), maxElevation)
            };

            const bool peak{ (x < peaks.x_ || x > width_ - peaks.y_) && y == depth_ - 2 };

            Insert({ { x, y }, elevation + DiceRoll(7, peaks.x_ + peaks.y_) * peak });
        }


    //        Raise({ width_ * peaks.x_/(peaks.y_ * 2) + DiceRoll(peaks.x_, peaks.y_), depth_ - peaks.x_ - peaks.y_ - Random(5)}, 23);
    //        Raise({ width_ * peaks.x_/(peaks.y_ * 2) + Random(5), depth_ - peaks.x_ - peaks.y_ - DiceRoll(peaks.y_, peaks.x_)}, 23);

    Erode(4, true);
    Erode();
}

void HeightMap::Erode(int steps, bool walk)
{
    for (int i{ 0 }; i < steps; ++i)
    {
        for (int x{ 0 }; x < width_; ++x)
            for (int y{ depth_ - 1 }; y >= 0; --y)
            {
                const bool edge{ x == 0 || y == 0 || x == width_ - 1 || y == depth_ - 1 };
                int difference{ 1 + !walk };
                Vector<IntVector2> lower{};

                for (int dx{ -1 }; dx <= 1; ++dx)
                    for (int dy{ -1 }; dy <= 1; ++dy)
                    {
                        if (dx == 0 && dy == 0)
                            continue;

                        const IntVector2 coords{ x + dx, y + dy };

                        const int d{ GetHeightAt(x, y) - GetHeightAt(coords) };

                        if (d >= difference)
                        {
                            if (!walk && d > difference)
                            {
                                lower.Clear();
                                difference = d;
                            }

                            lower.Push(coords);
                        }
                    }

                if (lower.Size())
                {
                    const int change{ (walk ? 1 : (difference / 2)) };

                    SetHeightAt(x, y, GetHeightAt(x, y) - change + edge);
                    IntVector2 low{ lower.At(Random(static_cast<int>(lower.Size()))) };
                    SetHeightAt(low, GetHeightAt(low) + change);
                }
                else if (walk && Random(Abs(x - width_ / 2) / 4) == 0)
                {
                    SetHeightAt(x, y, GetHeightAt(x, y) / 2);
                }
            }
    }
}

int HeightProfile::Reduce()
{
    int low{ Min() };
    for (int& e: profile_)
        e -= low;

    HeightProfile least{ profile_ };
    int shift{ 0 };

    for (int i{ 0 }; i < 3; ++i)
    {
        Elevation p{ profile_[(i + 1) % 4],
                    profile_[(i + 2) % 4],
                    profile_[(i + 3) % 4],
                    profile_[(i + 4) % 4] };
        HeightProfile hp{ p };

        if (hp.ToHash() < least.ToHash())
        {
            least = hp;
            shift = i + 1;
        }
    }

    profile_ = least.GetProfile();

    return shift;
}

bool HeightProfile::IsFlat() const
{
    return Min() == Max();
}

bool HeightProfile::IsSlope() const
{
    if (IsFlat() || Max() - Min() > 1)
        return false;

    // Find a low side
    HashSet<unsigned> low{};
    for (unsigned e{ 0 }; e < profile_.Size(); ++e)
    {
        const int h{ profile_.At(e) };
        if (low.IsEmpty())
        {
            if (h == Min())
                low.Insert(e);
        }
        else if (h == profile_.At(low.Front()) &&
                 Equals((CornerOffset(e) - CornerOffset(low.Front())).Length(), 1.f))
        {
            low.Insert(e);
        }

        if (low.Size() == 2u)
            break;
    }

    if (low.Size() < 2u)
        return false;

    // Check remaining corners for being at the top
    for (unsigned e{ 0 }; e < profile_.Size(); ++e)
    {
        if (low.Contains(e))
            continue;

        if (profile_.At(e) != Max())
            return false;
    }

    return true;
}
