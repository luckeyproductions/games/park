/* LucKey Park
// Copyright (C) 2024 LucKey Productions (https://luckey.games)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "../world.h"
#include "tiledecalset.h"
#include "../pirmit.h"

#include "ground.h"

Ground::Ground(Context* context): Component(context),
    heightMap_{ context->GetSubsystem<World>()->FlatSize() + IntVector2::ONE },
    tiles_{},
    tileset_{},
    path_{ nullptr },
    pathPreview_{ nullptr }
{
    context_->RegisterFactory<TileDecalSet>();
}

void Ground::OnSetEnabled() { Component::OnSetEnabled(); }

void Ground::OnNodeSet(Node* node)
{
    if (!node)
        return;

    Node* pathNode{ node_->CreateChild("Path") };
    pathNode->Translate({ -GRID / 4, 0, -GRID / 4});
    path_ = pathNode->CreateComponent<TileDecalSet>();
    path_->SetMaterial(RES(Material, "Materials/Path.xml"));
    path_->SetViewMask(LAYER(LAYER_STRUCTURE));
    path_->SetTileSize({ GRID / 2, GRID / 2 });
    path_->SetTileSetSize({ 4, 4 });
    path_->SetRotationOffsets({ 2, 1, 2, 1,
                                3, 1, 3, 1,
                                2, 1, 2, 1 });
    path_->SetTargetMask(LAYER(LAYER_GROUND));
    path_->SetMaxVertices(65536);
    path_->SetMaxIndices(path_->GetMaxVertices() * 2u);

    Node* previewNode{ node_->CreateChild("Path") };
    previewNode->Translate({ -GRID / 4, 0, -GRID / 4});
    pathPreview_ = previewNode->CreateChild("Preview")->CreateComponent<TileDecalSet>();
    pathPreview_->SetMaterial(RES(Material, "Materials/PathPreview.xml"));
    pathPreview_->SetViewMask(LAYER(LAYER_UI));
    pathPreview_->SetTileSize       (path_->GetTileSize());
    pathPreview_->SetTileSetSize    (path_->GetTileSetSize());
    pathPreview_->SetRotationOffsets(path_->GetRotationOffsets());
    pathPreview_->SetTargetMask     (path_->GetTargetMask());
    pathPreview_->SetMaxVertices(65536);
    pathPreview_->SetMaxIndices(pathPreview_->GetMaxVertices() * 2u);
}

void Ground::GenerateTerrain()
{
    heightMap_.Generate();

    for (Tile& tile: tiles_.Values())
        tile.GetNode()->Remove();
    tiles_.Clear();

    World* world{ GetSubsystem<World>() };

    for (int x{ 0 }; x < heightMap_.GetWidth() - 1; ++x)
    for (int y{ 0 }; y < heightMap_.GetDepth() - 1; ++y)
    {
        Node* tileNode{ node_->CreateChild("Tile") };
        tileNode->SetPosition(world->CoordinatesToPosition({ x, 0, y }));

        tiles_[{ x, y }] = Tile{ tileNode, { 0 } };
    }

    UpdateTiles();
}

void Ground::UpdateTiles(const IntVector2& min, const IntVector2& max)
{
    Vector<IntVector2> coords{};

    for (int x{ min.x_ }; x <= max.x_; ++x)
    for (int y{ min.y_ }; y <= max.y_; ++y)
        coords.Push({ x, y });

    UpdateTiles(coords);
}

void Ground::UpdatePath(const IntVector2& coords)
{
    if (!tiles_.Contains(coords))
        return;

    World* world{ GetSubsystem<World>() };
    const IntVector2 tileOrigin{ 2 * coords };
    const IntVector2 min{ tileOrigin - IntVector2::ONE };
    const IntVector2 max{ tileOrigin + IntVector2::ONE * 2 };

    for (int x{ min.x_ }; x <= max.x_; ++x)
    for (int y{ min.y_ }; y <= max.y_; ++y)
    {
        const IntVector2 tileCoords{ x, y };
        if (tileCoords.y_ < 0)
            continue;

        bool empty{ !world->IsPath(tileCoords / 2) };

        if (empty)
        {
            path_->RemoveTile(tileCoords);
        }
        else
        {
            int i{ ((y % 2) == 0 ? (1 - x % 2)
                                 : (2 + x % 2)) };

            PODVector<IntVector2> neighbours{};
            switch (i)
            {
            default: break;
            case 0: neighbours = { {  1,  0 }, {  1, -1 }, {  0, -1 } }; break;
            case 1: neighbours = { {  0, -1 }, { -1, -1 }, { -1,  0 } }; break;
            case 2: neighbours = { { -1,  0 }, { -1,  1 }, {  0,  1 } }; break;
            case 3: neighbours = { {  0,  1 }, {  1,  1 }, {  1,  0 } }; break;
            }

            int profile{ 0 };
            for (unsigned s{ 0 }; s < neighbours.Size(); ++s)
            {
                const IntVector2 shift{ neighbours.At(s) };
                if (world->IsPath(tileCoords / 2 + shift))
                    profile += PowN(2, s);
            }

            if (((profile & 2) != 0) && profile != 7)
                profile -= 2;

            const int tile{ static_cast<int>(CountSetBits(profile)) };
            int rot{ i };
            if (profile > 0)
            {
                while ((profile & 1) == 0)
                {
                    profile >>= 1;

                    if (profile == 2)
                        rot -= 1;
                }
            }

            if (!path_->HasTile(tileCoords) ||
                path_->GetTileIndex(tileCoords) / 4 != tile)
            {
                path_->AddTile(tileCoords, rot, Random(4) + tile * 4);
            }
        }
    }
}

void Ground::PreviewPath(const IntVector2& coords)
{
    World* world{ GetSubsystem<World>() };
    ClearPathPreview();

    if (!tiles_.Contains(coords) || world->IsPath(coords))
        return;

    pathPreview_->SetEnabled(true);
    const IntVector2 tileOrigin{ 2 * coords };
    const IntVector2 min{ tileOrigin - IntVector2::ONE };
    const IntVector2 max{ tileOrigin + IntVector2::ONE * 2 };

    for (int x{ min.x_ }; x <= max.x_; ++x)
    for (int y{ min.y_ }; y <= max.y_; ++y)
    {
        const IntVector2 tileCoords{ x, y };
        if (tileCoords.y_ < 0)
            continue;

        bool irrelevant{ !path_->HasTile(tileCoords) &&
                         coords != tileCoords / 2 };

        if (irrelevant)
        {
            continue;
        }
        else
        {
            int i{ ((y % 2) == 0 ? (1 - x % 2)
                                 : (2 + x % 2)) };

            PODVector<IntVector2> neighbours{};
            switch (i)
            {
            default: break;
            case 0: neighbours = { {  1,  0 }, {  1, -1 }, {  0, -1 } }; break;
            case 1: neighbours = { {  0, -1 }, { -1, -1 }, { -1,  0 } }; break;
            case 2: neighbours = { { -1,  0 }, { -1,  1 }, {  0,  1 } }; break;
            case 3: neighbours = { {  0,  1 }, {  1,  1 }, {  1,  0 } }; break;
            }

            int profile{ 0 };
            for (unsigned s{ 0 }; s < neighbours.Size(); ++s)
            {
                const IntVector2 shift{ neighbours.At(s) };
                if (coords == tileCoords / 2 + shift
                    || world->IsPath(tileCoords / 2 + shift))
                    profile += PowN(2, s);
            }

            if (((profile & 2) != 0) && profile != 7)
                profile -= 2;

            const int tile{ static_cast<int>(CountSetBits(profile)) };
            int rot{ i };
            if (profile > 0)
            {
                while ((profile & 1) == 0)
                {
                    profile >>= 1;

                    if (profile == 2)
                        rot -= 1;
                }
            }

            if (!path_->HasTile(tileCoords) ||
                path_->GetTileIndex(tileCoords) / 4 != tile)
            {
                pathPreview_->AddTile(tileCoords, rot, Random(4) + tile * 4);
            }
        }
    }
}

void Ground::ClearPathPreview()
{
    pathPreview_->RemoveAllTiles();
    pathPreview_->SetEnabled(false);
}

void Ground::UpdateTiles(const Vector<IntVector2>& coords)
{
    for (const IntVector2& c: coords)
    {
        const HeightProfile profile{ heightMap_.GetHeightProfileAt({ c.x_, c.y_ }) };
        SetTile({ c.x_, c.y_ }, profile);
    }
}

void Ground::SetTile(const IntVector2& coords, HeightProfile profile)
{
    if (tiles_.Contains(coords))
    {
        const int elevation{ profile.Min() };
        const int shift{ profile.Reduce() };

        if (!tileset_.Contains(profile))
            GenerateTile(profile);

        Tile& tile{ tiles_[coords] };
        tile.Set(profile, shift);

        Node* tileNode{ tile.GetNode() };
        Vector3 pos{ tileNode->GetPosition() };
        pos.y_ = elevation * HEIGHTSTEP;
        tileNode->SetPosition(pos);
    }
}

void Ground::GenerateTile(const HeightProfile& profile)
{
    const Vector2 halfSize{ GRID * .5f, GRID * .5f };
    const bool flat{ profile.IsFlat() };

    Witch::Spell rect{ {}, Witch::Spell::QUAD };
    Vector3 normal{ Vector3::UP };

    for (unsigned c{ 0 }; c < 4; ++c)
    {
        const int e{ profile.GetProfile().At(c) - profile.Min()  };
        const IntVector2 offset{ HeightProfile::CornerOffset(c) * 2 - IntVector2::ONE };
        const Vector3 pos{ halfSize.x_ * offset.x_, e * HEIGHTSTEP, halfSize.y_ * offset.y_ };

        rect.Push(Witch::Rune{ { pos }, { normal.Length() == 0.0f ? pos.Normalized() : normal } });
    }

    SharedPtr<Model> tileModel{ context_->CreateObject<Model>() };

    if (flat)
    {
        tileModel = Grimoire::Summon({ rect }, 0);
    }
    else
    {
        tileModel->SetNumGeometries(1);
        tileModel->SetBoundingBox({ { -halfSize.x_,                        0.f, -halfSize.y_ },
                                    {  halfSize.x_, profile.Max() * HEIGHTSTEP,  halfSize.y_ } });

        const int lods{ 5 };
        tileModel->SetNumGeometryLodLevels(0, lods);

        for (int lod{ 0 }; lod < lods; ++lod)
        {
            tileModel->SetGeometry(0, lod, rect.Cast(Max(1, 1 << (lods - lod - 1))));
            tileModel->GetGeometry(0, lod)->SetLodDistance(lod * 23.5f);
        }
    }

    tileset_.Insert({ profile, tileModel });
}

void Ground::OnSceneSet(Scene* scene) { Component::OnSceneSet(scene); }
void Ground::OnMarkedDirty(Node* node) {}
void Ground::OnNodeSetEnabled(Node* node) {}
bool Ground::Save(Serializer& dest) const { return Component::Save(dest); }
bool Ground::SaveXML(XMLElement& dest) const { return Component::SaveXML(dest); }
bool Ground::SaveJSON(JSONValue& dest) const
{
    JSONArray height{};
    for (int y{ 0 }; y < heightMap_.GetDepth(); ++y)
    {
        String rowString{};
        for (int x{ 0 }; x < heightMap_.GetWidth(); ++x)
            rowString.Append(EncodeOctal(heightMap_.GetHeightAt(x, y), (x % 2) != 0));
        height.Push(JSONValue{ rowString });
    }

    dest.Set("heightmap", height);
    return true;
}

bool Ground::LoadJSON(const JSONValue& source)
{
    const JSONArray& height{ source.Get("heightmap").GetArray() };
    if (height.IsEmpty())
        return false;

    const unsigned d{ height.Size() };
    const unsigned w{ DecodeOctal(height.Front().GetString()).Size() };

    for (unsigned y{ 0u }; y < d; ++y)
    {
        const PODVector<int> row{ DecodeOctal(height.At(y).GetString()) };
        for (unsigned x{ 0u }; x < w; ++x)
            heightMap_.SetHeightAt(x, y, row.At(x));
    }

    UpdateTiles();
    return true;
}

void Ground::MarkNetworkUpdate() { Component::MarkNetworkUpdate(); }
void Ground::GetDependencyNodes(PODVector<Node*>& dest) {}
void Ground::DrawDebugGeometry(DebugRenderer* debug, bool depthTest) {}
