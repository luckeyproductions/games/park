/* LucKey Park
// Copyright (C) 2024 LucKey Productions (https://luckey.games)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef TILEDECALSET_H
#define TILEDECALSET_H

#include "luckey.h"

class TileDecalSet: public DecalSet
{
    DRY_OBJECT(TileDecalSet, DecalSet);

public:
    TileDecalSet(Context* context);

    /// Set tile size.
    void SetTileSize(const Vector2& size) { tileSize_ = size; }
    /// Set number of columns and rows in tile set.
    void SetTileSetSize(const IntVector2& size) { tileSetSize_ = size; }
    /// Set view mask for picking decal target.
    void SetTargetMask(unsigned mask) { targetMask_ = mask; }
    /// Set tile rotation offsets.
    void SetRotationOffsets(const PODVector<int>& offsets) { offsets_ = offsets; }

    /// Return tile size.
    Vector2 GetTileSize() const { return tileSize_; }
    /// Return number of columns and rows in tile set.
    IntVector2 GetTileSetSize() const { return tileSetSize_; }
    /// Return view mask for picking decal target.
    unsigned GetTargetMask() const { return targetMask_; }
    /// Return tile rotation offsets.
    PODVector<int> GetRotationOffsets() const { return offsets_; }

    /// Add a projected tile.
    void AddTile(const IntVector2& coordinates, int rotation, unsigned index);
    /// Remove a tile.
    void RemoveTile(const IntVector2& coordinates);

    /// Remove all tiles.
    void RemoveAllTiles();

    /// Check if tile exists.
    bool HasTile(const IntVector2& coords);
    /// Derive tile index from UVs.
    int GetTileIndex(const IntVector2& coords);

private:
    Vector2 tileSize_;
    IntVector2 tileSetSize_;
    PODVector<int> offsets_;
    HashMap<IntVector2, Decal*> tiles_;
    unsigned targetMask_;
};

#endif // TILEDECALSET_H
