/* LucKey Park
// Copyright (C) 2024 LucKey Productions (https://luckey.games)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef GROUND_H
#define GROUND_H

#include "cyberplasm.h"
#include "terraindefs.h"

class TileDecalSet;

class Ground: public Component
{
    DRY_OBJECT(Ground, Component);

public:
    Ground(Context* context);
    void OnSetEnabled() override;
    void GenerateTerrain();

    HeightMap& GetHeightMap() { return heightMap_; }
    const HashMap<HeightProfile, SharedPtr<Model> >& GetTileset() const { return tileset_; }
    Node* GetTileNode(const IntVector2& coords) const
    {
        if (tiles_.Contains(coords))
            return tiles_[coords]->GetNode();
        else
            return nullptr;
    }

    bool Save(Serializer& dest) const override;
    bool SaveXML(XMLElement& dest) const override;
    bool SaveJSON(JSONValue& dest) const override;
    bool LoadJSON(const JSONValue& source) override;
    void MarkNetworkUpdate() override;
    void GetDependencyNodes(PODVector<Node*>& dest) override;
    void DrawDebugGeometry(DebugRenderer* debug, bool depthTest) override;

    void UpdateTiles(const Vector<IntVector2>& coords);
    void UpdateTiles(const IntVector2& min, const IntVector2& max);
    void UpdateTiles() { UpdateTiles({ 0, 0 }, { heightMap_.GetWidth() - 2, heightMap_.GetDepth() - 2 }); }

    void UpdatePath(const IntVector2& coords);
    void PreviewPath(const IntVector2& coords);
    void ClearPathPreview();

protected:
    void OnNodeSet(Node* node) override;
    void OnSceneSet(Scene* scene) override;
    void OnMarkedDirty(Node* node) override;
    void OnNodeSetEnabled(Node* node) override;

private:
    void SetTile(const IntVector2& coords, HeightProfile profile);
    void GenerateTile(const HeightProfile& profile);

    HeightMap heightMap_;
    HashMap<IntVector2, Tile> tiles_;
    HashMap<HeightProfile, SharedPtr<Model> > tileset_;
    TileDecalSet* path_;
    TileDecalSet* pathPreview_;
};

#endif // GROUND_H
