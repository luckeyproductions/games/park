/* LucKey Park
// Copyright (C) 2024 LucKey Productions (https://luckey.games)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "tiledecalset.h"

TileDecalSet::TileDecalSet(Context* context): DecalSet(context),
    tileSize_{ Vector2::ONE },
    tileSetSize_{ IntVector2::ONE },
    offsets_{},
    tiles_{},
    targetMask_{ M_MAX_UNSIGNED }
{
}

void TileDecalSet::AddTile(const IntVector2& coordinates, int rotation, unsigned index)
{
    Octree* octree{ GetScene()->GetComponent<Octree>() };
    if (!octree)
        return;

    Drawable* target{ nullptr };
    Vector3 position{ coordinates.x_ * tileSize_.x_, 100.f, coordinates.y_ * tileSize_.y_ };
    Ray tileRay{ position + node_->GetWorldPosition(), Vector3::DOWN };
    PODVector<RayQueryResult> results{};
    RayOctreeQuery query{ results, tileRay, RAY_TRIANGLE, 200.f, DRAWABLE_GEOMETRY, targetMask_ };
    octree->RaycastSingle(query);

    if (results.IsEmpty())
        return;

    const RayQueryResult& result{ results.Front() };
    target = result.drawable_;
    position = result.position_;

    if (!target)
        return;

    int rotationOffset{ (index < offsets_.Size() ? offsets_.At(index) : 0) };
    Quaternion worldRotation{ Quaternion{ (rotation + rotationOffset) * 90.f, Vector3::UP } * Quaternion{ 90.f, Vector3::RIGHT }};
    float ratio{ tileSize_.x_ / tileSize_.y_ };
    const Vector2 tileUVsize{ Vector2::ONE / tileSetSize_ };
    const unsigned row{ index / tileSetSize_.x_ };
    const unsigned column{ index % tileSetSize_.x_ };
    const Vector2 topLeft{ column * tileUVsize.x_, row * tileUVsize.y_ };
    const Vector2 bottomRight{ (column + 1u) * tileUVsize.x_, (row + 1u) * tileUVsize.y_ };

    if (tiles_.Contains(coordinates))
        RemoveTile(coordinates);

    if (AddDecal(target,
                 position,
                 worldRotation,
                 tileSize_.x_, ratio,
                 10.f,
                 topLeft, bottomRight ))
    {
        tiles_[coordinates] = &decals_.Back();
    }
}

void TileDecalSet::RemoveTile(const IntVector2& coordinates)
{
    if (!tiles_.Contains(coordinates))
        return;

    for (List<Decal>::Iterator d{ decals_.Begin() }; d != decals_.End(); ++d)
    {
        if (d.operator->() == tiles_[coordinates])
        {
            tiles_.Erase(coordinates);
            RemoveDecal(d);
            return;
        }
    }
}

void TileDecalSet::RemoveAllTiles()
{
    RemoveAllDecals();
    tiles_.Clear();
}

bool TileDecalSet::HasTile(const IntVector2& coords)
{
    return tiles_.Contains(coords);
}

int TileDecalSet::GetTileIndex(const IntVector2& coords)
{
    if (!HasTile(coords))
        return M_MAX_UNSIGNED;

    Decal* decal{ tiles_[coords] };
    if (!decal)
        return M_MAX_UNSIGNED;

    Vector2 minTexCoord{ Vector2::ONE * M_INFINITY };
    for (const DecalVertex& vert: decal->vertices_)
        minTexCoord = VectorMin(minTexCoord, vert.texCoord_);

    const IntVector2 tileCoord{ VectorMod(VectorRoundToInt(minTexCoord * tileSetSize_), tileSetSize_) };
    return tileCoord.y_ * tileSetSize_.x_ + tileCoord.x_;
}

