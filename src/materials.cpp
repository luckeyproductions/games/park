/* LucKey Park
// Copyright (C) 2024 LucKey Productions (https://luckey.games)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "materials.h"

const Shade Shade::BLACK{ 0, 0, 0 };
const Shade Shade::WHITE{ 0, 0, 010 };
const Shade Shade::VCOL{ -1, -1, -1 };

Material* Materials::Cloth(const Shade& shade)
{
    if (!cloth_.Contains(shade))
    {
        SharedPtr<Material> cloth{ RES(Material, "Materials/Cloth.xml")->Clone() };

        Color diff{ shade.ToColor() };
        diff.FromHSV(diff.Hue(), diff.SaturationHSV() * .9f, diff.Value());
        Color spec{};
        spec.FromHSV(diff.Hue() + .5f,
                     diff.SaturationHSV() * .75f,
                     Lerp(diff.Value(), .25f, .75f),
                     2.3f);

        cloth->SetShaderParameter("MatDiffColor", diff);
        cloth->SetShaderParameter("MatSpecColor", spec);
        cloth_.Insert({ shade, cloth });
    }

    return cloth_[shade];
}

Material* Materials::Cloth(const Tone& tone)
{
    SharedPtr<Material> underwear{ Cloth() };
    while (ColorDistance(underwear->GetShaderParameter("MatDiffColor").GetColor(),
                         Skin(tone)->GetShaderParameter("MatDiffColor").GetColor()) < .1f)
        underwear = Cloth();

    return underwear;
}

Material* Materials::Paint(const Shade& shade, int gloss)
{
    const Sheen sheen{ shade, gloss };
    if (!paint_.Contains(sheen))
    {

        SharedPtr<Material> paint{ RES(Material, "Materials/Paint.xml")->Clone() };
        if (shade.IsVCol())
            paint->SetTechnique(0, RES(Technique, "Techniques/NoTextureVCol.xml"));

        Color spec{};
        spec.FromHSV(.0f, .0f, Min(1.f, .7f + .1f * gloss), 5.f + 7.f * gloss * gloss);

        paint->SetShaderParameter("MatDiffColor", shade.ToColor());
        paint->SetShaderParameter("MatSpecColor", spec);
        paint_.Insert({ sheen, paint });
    }

    return paint_[sheen];
}

Material* Materials::Skin(const Tone& tone)
{
    if (!skin_.Contains(tone))
    {
        SharedPtr<Material> skin{ RES(Material, "Materials/Skin.xml")->Clone() };

        skin->SetShaderParameter("MatDiffColor", tone.Diffuse());
        skin->SetShaderParameter("MatSpecColor", tone.Specular());
        skin_.Insert({ tone, skin });
    }

    return skin_[tone];
}

Material* Materials::Hair(const Tint& tint)
{
    if (!hair_.Contains(tint))
    {
        SharedPtr<Material> hair{ RES(Material, "Materials/Hair.xml")->Clone() };

        hair->SetShaderParameter("MatDiffColor", tint.Diffuse());
        hair->SetShaderParameter("MatSpecColor", tint.Specular());
        hair_.Insert({ tint, hair });
    }

    return hair_[tint];
}
