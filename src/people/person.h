/* LucKey Park
// Copyright (C) 2024 LucKey Productions (https://luckey.games)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef PERSON_H
#define PERSON_H

#include "character.h"

class Person: public Object
{
    DRY_OBJECT(Person, Object);

public:
    static void RegisterObject(Context* context);
    static void ResetNextID() { nextId_ = 1u; }

    Person(Context* context);

    void SetID(unsigned id);
    bool SaveJSON(JSONValue& dest) const;
    bool LoadJSON(const JSONValue& source);

    Personalia GetPersonalia() const { return personalia_; }
    Character* GetCharacter() const { return character_; }
    bool LivesLeft() const { return personalia_.home_.y_ < 180; }
    bool LivesNorth() const { return personalia_.home_.y_ < 90 || personalia_.home_.y_ > 270; }

    bool IsPresent() const { return present_; }
    void Appear(const Vector3& position = Vector3::ONE * M_INFINITY);

    bool WorksHere() const;

private:
    void HandleCharacterAway(StringHash eventType, VariantMap& eventData);

    static unsigned nextId_;

    Personalia personalia_;
    Character* character_;
    bool present_;
    bool passed_;
    bool visited_;
};

class Personnel: public Object
{
    DRY_OBJECT(Personnel, Object);
public:
    Personnel(Context* context): Object(context) {}

    bool IsEmployed(Person* person) const { return staff_.Contains(person); }
    void Clear() { staff_.Clear(); }

private:
    PODVector<Person*> staff_;
};


#endif // PERSON_H
