/* LucKey Park
// Copyright (C) 2024 LucKey Productions (https://luckey.games)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "../world.h"
#include "person.h"
#include "../pirmit.h"

#include "character.h"

Character::Character(Context* context): LogicComponent(context),
    person_{ nullptr },
    pathFinder_{ nullptr },
    route_{},
    velocity_{},
    mobile_{ 1.f },
    leap_{ SUBGRID },
    graphicsNode_{ nullptr },
    models_{},
    animCtrl_{ nullptr },
    animatron_{ nullptr },
    blink_{ Random() },
    ahead_{},
    randomizer_{ Random() },
    been_{}
{
}

void Character::OnNodeSet(Node* node)
{
    if (!node)
        return;

    node_->SetTemporary(true);
    node_->AddTag("CamTarget");

    graphicsNode_ = node->CreateChild("Graphics");

    models_[MOD_BODY] = graphicsNode_->CreateComponent<AnimatedModel>();
    models_[MOD_BODY]->SetCastShadows(true);

    pathFinder_ = node_->CreateComponent<PathFinder>();
    animCtrl_ = graphicsNode_->CreateComponent<AnimationController>();
    animatron_ = graphicsNode_->CreateComponent<Animatron>();
}

void Character::Initialize(Person* person)
{
    person_ = person;

    const Personalia& personalia{ person->GetPersonalia() };
    bool male{ personalia.gender_ == G_MALE };
    models_[MOD_BODY]->SetModel(RES(Model, String{ "Models/" } + (male ? "Male" : "Female") + ".mdl"));

    for (unsigned m{ 0u }; m < models_[MOD_BODY]->GetNumMorphs(); ++m)
        models_[MOD_BODY]->SetMorphWeight(m, Random(-.5f, 1.f));

    Materials* materials{ GetSubsystem<Materials>() };

    AssignBodyMaterials();

    models_[MOD_BODY]->SetMaterial(MAT_UNDERPANTS, materials->Cloth(personalia.skinTone_));

    for (int s{ MOD_HAIR }; s < MOD_ALL; ++s)
    {
        // Hair
        if (s == MOD_HAIR && RandomBool())
        {
            const Vector<String> styles{ "Mohawk", "Curtain", "Afro", "Ponytail" };

            models_[s] = graphicsNode_->CreateComponent<AnimatedModel>();
            models_[s]->SetModel(RES(Model, String{ "Models/Hair/" } + Random(styles) + ".mdl"));
            models_[s]->SetMaterial(models_[MOD_BODY]->GetMaterial(MAT_HAIR));
            models_[s]->SetCastShadows(true);

            if (models_[s]->GetNumGeometries() > 1)
                models_[s]->SetMaterial(1, materials->Cloth());

            for (unsigned m{ 0u }; m < models_[s]->GetNumMorphs(); ++m)
                models_[s]->SetMorphWeight(m, Clamp(RandomNormal(0.f, .25f + !male * .5f), -.5f, 1.f));
        }

        if (s == MOD_HAT && !models_.Contains(MOD_HAIR) && Random(34 - 23 * male) == 0)
        {
            Vector<String> hats{ "Fedora", "Boater" };
            bool boater{ RandomBool() };

            models_[s] = graphicsNode_->CreateComponent<AnimatedModel>();
            models_[s]->SetModel(RES(Model, "Models/Hats/" + hats.At(boater) + ".mdl"));
            models_[s]->SetCastShadows(true);

            models_[s]->SetMaterial(0u, materials->Cloth());
            models_[s]->SetMaterial(1u, materials->Hair(Tint{ 2 * boater + Random(3), -1 * boater }));
        }

        // Beard
        if (s == MOD_FACE && RandomBool())
        {
            if (male)
            {
                const Vector<String> beards{ "Beard", "Fumanchu", "Bandito", "Walrus" };

                models_[s] = graphicsNode_->CreateComponent<AnimatedModel>();
                models_[s]->SetModel(RES(Model, String{ "Models/Hair/" } + Random(beards) + ".mdl"));
                models_[s]->SetMaterial(models_[MOD_BODY]->GetMaterial(MAT_HAIR));

                for (unsigned m{ 0u }; m < models_[s]->GetNumMorphs(); ++m)
                    models_[s]->SetMorphWeight(m, Clamp(RandomNormal(-.25f, .25f), -.5f, 1.f));
            }
        }

        // Glasses
        if (s == MOD_GLASSES && Random(12) == 0)
        {
            const Vector<String> glasses{ "Lennon", "Holly" };

            models_[s] = graphicsNode_->CreateComponent<AnimatedModel>();
            models_[s]->SetModel(RES(Model, String{ "Models/Glasses/" } + Random(glasses) + ".mdl"));
            models_[s]->SetMaterial(0u, (RandomBool() ? models_[MOD_BODY]->GetMaterial(MAT_CORNEA) : RES(Material, "Materials/Shade.xml")));
            models_[s]->SetMaterial(1u, materials->Paint());
        }

        // Shirt
        if (s == MOD_SHIRT)
        {
            models_[s] = graphicsNode_->CreateComponent<AnimatedModel>();
            models_[s]->SetModel(RES(Model, String{ "Models/Clothes/" } + (male ? "MaleShirt" : "Reshirt") + ".mdl"));
            models_[s]->SetMaterial(models_[MOD_BODY]->GetMaterial(MAT_UNDERPANTS));
        }
        // Coat
        if (s == MOD_COAT)
        {
            if (male && Random(5))
            {
                const Vector<String> coats{ "MaleVest", "MaleHoodie" };

                models_[s] = graphicsNode_->CreateComponent<AnimatedModel>();
                models_[s]->SetModel(RES(Model, String{ "Models/Clothes/" } + Random(coats) + ".mdl"));
                models_[s]->SetMaterial(0u, materials->Cloth(personalia.skinTone_));
                models_[s]->SetCastShadows(true);

                if (models_[s]->GetNumGeometries() > 1)
                    models_[s]->SetMaterial(1u, RES(Material, "Materials/Metal.xml"));
            }
        }

        // Pants
        if (s == MOD_PANTS)
        {
            const Vector<String> pants{ (male ? "MaleLoosePants" : "LoosePants"),
                                        (male ? "MaleTightPants" : "TightPants"),
                                        "Shorts", "Skirt" };

            models_[s] = graphicsNode_->CreateComponent<AnimatedModel>();
            models_[s]->SetModel(RES(Model, String{ "Models/Clothes/" } + pants.At(Random(static_cast<int>(pants.Size() - male))) + ".mdl"));
            models_[s]->SetMaterial(0u, materials->Cloth(personalia.skinTone_));
        }
        // Shoes
        if (s == MOD_SHOES && Random(6) != 0)
        {
            const Vector<String> shoes{ "FlipFlops", "Boots" };
            const String shoe{ Random(shoes) };

            models_[s] = graphicsNode_->CreateComponent<AnimatedModel>();
            models_[s]->SetModel(RES(Model, String{ "Models/Clothes/" } + shoe + ".mdl"));

            Shade color{ Random(0100), DiceRoll(3, 3) - 3, DiceRoll(3, 3) - 2 };
            if (shoe == "FlipFlops")
            {
                models_[s]->SetMaterial(0u, materials->Cloth(color));
                color.SetHue(color.Hue() + RandomOffCenter(2, 8));
                color.SetSat(color.Sat() + 2);
                color.SetVal((color.Val() + 6) / 2);
                models_[s]->SetMaterial(1u, materials->Paint(color));
            }
            else
            {
                const int lightness{ Random(3) };
                models_[s]->SetMaterial(0u, materials->Hair(Tint{ lightness, Random(-2, 3) }));
                models_[s]->SetMaterial(1u, materials->Hair(Tint{ Abs(lightness + RandomSign()), Random(-2, 3) }));
            }
        }
    }

    // Baldness
    if (male && Random(17) == 0)
        models_[MOD_BODY]->SetMaterial(MAT_HAIR, models_[MOD_BODY]->GetMaterial(MAT_SKIN));

    for (const ModelMorph& bodyMorph: models_[MOD_BODY]->GetMorphs())
    for (AnimatedModel* model: models_.Values())
    {
        if (model == models_[MOD_BODY])
            continue;

        for (const ModelMorph& morph: model->GetMorphs())
            if (morph.nameHash_ == bodyMorph.nameHash_)
            {
                float weight{ bodyMorph.weight_ }; ///
//                if (models_.Contains(MOD_COAT) && model == models_[MOD_COAT])
//                    weight = Max(0.f, weight + models_.Contains(MOD_SHIRT) * .1f); ///

                model->SetMorphWeight(morph.nameHash_, weight);
            }
            else if (morph.name_ == "Male")
                model->SetMorphWeight(morph.nameHash_, male);
            else if (morph.name_ == "Female")
                model->SetMorphWeight(morph.nameHash_, !male);
    }

    float sideScale{ (1.f + 2.f * personalia.finalLength_) / 300 };
    graphicsNode_->SetScale({ sideScale, personalia.finalLength_ * .01f, sideScale });

    for (SharedPtr<Component> c: graphicsNode_->GetComponents())
    {
        if (c->IsInstanceOf<Drawable>())
            c->Cast<Drawable>()->SetViewMask(LAYER(LAYER_CHARACTER));
    }

    node_->SetDeepEnabled(false);

    SubscribeToEvent(E_POSTRENDERUPDATE, DRY_HANDLER(Character, HandlePostRenderUpdate));
    SubscribeToEvent(E_PATHREMOVED, DRY_HANDLER(Character, HandlePathRemoved));
}

void Character::AssignBodyMaterials()
{
    Materials* materials{ GetSubsystem<Materials>() };
    const Personalia& personalia{ person_->GetPersonalia() };

    models_[MOD_BODY]->SetMaterial(MAT_SKIN, materials->Skin(personalia.skinTone_));
    models_[MOD_BODY]->SetMaterial(MAT_LIPS, materials->Skin(personalia.skinTone_.Lips()));
    models_[MOD_BODY]->SetMaterial(MAT_TEETH, materials->Paint(personalia.teethColor_));
    models_[MOD_BODY]->SetMaterial(MAT_GUMS, materials->Skin(personalia.skinTone_.Gums()));
    models_[MOD_BODY]->SetMaterial(MAT_HAIR, materials->Hair(personalia.hairTint_));
    models_[MOD_BODY]->SetMaterial(MAT_EYEWHITE, materials->Paint(Shade::WHITE, 4));
    models_[MOD_BODY]->SetMaterial(MAT_IRIS, materials->Cloth(personalia.eyeColor_));
    models_[MOD_BODY]->SetMaterial(MAT_PUPIL, materials->Paint(Shade::BLACK, -5));
    models_[MOD_BODY]->SetMaterial(MAT_CORNEA, RES(Material, "Materials/Cornea.xml"));
    if (personalia.gender_ == G_MALE)
        models_[MOD_BODY]->SetMaterial(MAT_BROW, materials->Hair(personalia.hairTint_));
}

void Character::OnSetEnabled() { LogicComponent::OnSetEnabled(); }
void Character::Start() {}
void Character::DelayedStart() {}
void Character::Stop() {}
void Character::GoTo(const IntVector2& goal)
{
    route_ = pathFinder_->FindPathDiagonal(goal);

    World* world{ GetSubsystem<World>() };
    // Exit to the left
    if (route_.Front().x_ < GRID_SUBS)
        while (route_.Front().x_ >= 0)
            route_.Insert(0, route_.Front() + IntVector2::LEFT);
    // Exit to the right
    else if (route_.Front().x_ > (world->GetWidth() - 1) * GRID_SUBS)
        while (route_.Front().x_ < (world->GetWidth() * GRID_SUBS))
            route_.Insert(0, route_.Front() + IntVector2::RIGHT);

    Releap();
}

void Character::HandlePathRemoved(StringHash /*eventType*/, VariantMap& eventData)
{
    HashSet<IntVector2> coords{};
    VectorBuffer buffer{ eventData[PathRemoved::P_COORDINATES].GetBuffer() };
    while (!buffer.IsEof())
        coords.Insert(buffer.ReadIntVector2());

    for (const IntVector2& subCoords: route_)
    {
        if (coords.Contains(subCoords / GRID_SUBS))
        {
            GoTo(route_.Front());
            return;
        }
    }
}

void Character::Update(float timeStep)
{
    if (!node_ || !node_->IsEnabled())
        return;

    blink_ -= timeStep;

    if (mobile_ < 1.f)
        mobile_ = Min(1.f, mobile_ + timeStep * (mobile_ < 0.f ? 1.f : 4.f));

    const Vector3 flatNodePos{ node_->GetWorldPosition().ProjectOntoPlane(Vector3::UP) };
    const bool moving{ Moving() };

    World* world{ GetSubsystem<World>() };

    if (moving && route_.Size())
    {
        const IntVector2& coords{ route_.Back() };
        const Vector3 position{ world->CoordinatesToWorldPosition(coords, true) };
        Vector3 delta{ position.ProjectOntoPlane(Vector3::UP) - flatNodePos };
        const float dot{ delta.DotProduct(node_->GetWorldDirection()) };

        const bool last{ route_.Size() == 1 };
        const float dist{ SUBGRID * (last ? .25f : .75f) };
        const bool near{ delta.Length() < dist };
        const float till{ delta.Length() };

        if (dot > 0.f && till < leap_ && route_.Size() > 1)
        {
            const IntVector2& nextCoords{ route_.At(route_.Size() - 2)};
            const Vector3 nextPosition{ world->CoordinatesToWorldPosition(nextCoords, true) };
            const Vector3 nextDelta{ nextPosition.ProjectOntoPlane(Vector3::UP) - flatNodePos };
            const float advance{ Pow(Leapt(), M_1_SQRT2) };

            delta = delta.Lerp(nextDelta, advance);
        }
        const Vector3 v{ velocity_.Length() * delta.Normalized() };

        velocity_ = { v.x_, v.z_ };
        const bool passed{ dot < velocity_.Length() * timeStep * 1.25f };

        if ((!last || passed) && near)
        {
            route_.Pop();

            if (last)
                mobile_ = Random(-7.f, -2.3f);
            else
                Releap();
        }
    }

    Vector3 v{ velocity_.x_, 0.f, velocity_.y_ };

    if (route_.IsEmpty() || !moving)
        v *= 0;
    else
        v *= mobile_;


    if (!Equals(v.Length(), 0.f))
    {
        node_->Translate(timeStep * v, TS_WORLD);
        const float dirDot{ v.Normalized().DotProduct(node_->GetWorldDirection()) };
        const float directionFactor{ (Sign(dirDot) == dirDot ? dirDot : Cbrt(dirDot)) };
        const bool wasWalking{ animCtrl_->IsPlaying("Animations/Walk.ani") };
        animCtrl_->Play("Animations/Walk.ani", 0, true, (animCtrl_->IsPlaying(0) ? .3f : 0.f));
        if (!wasWalking)
            animCtrl_->SetTime("Animations/Walk.ani", Random(2) * .5f * animCtrl_->GetLength("Animations/Walk.ani"));
        animCtrl_->FadeOthers("Animations/Walk.ani", 0.f, .7f);
        animCtrl_->SetSpeed("Animations/Walk.ani", directionFactor * .666f * v.Length() / node_->GetScale().z_);

        float a{ v.Angle(node_->GetWorldDirection()) };
        if (a > 178.f)
            node_->Yaw(RandomSign() * 5.f);

        const float yaw{ Min(1.f, timeStep * v.Length() * 4.f) * Asin(node_->GetWorldDirection().CrossProduct(v.Normalized()).y_) };
        node_->Yaw(yaw);
        ahead_ -= yaw / M_PHI;

        PutOnGround(v, timeStep);
    }
    else
    {
        const bool wasIdle{ animCtrl_->IsPlaying("Animations/Idle.ani") };
        animCtrl_->Play("Animations/Idle.ani", 0, true, (animCtrl_->IsPlaying(0) ? .23f : 0.f));
        if (!wasIdle)
        {
            animCtrl_->SetTime("Animations/Idle.ani", Random(animCtrl_->GetLength("Animations/Idle.ani")));
            animCtrl_->SetSpeed("Animations/Idle.ani", .75f + .5f * Random(randomizer_));

        }
        animCtrl_->FadeOthers("Animations/Idle.ani", 0.f, .6f);
    }

//    been_.Push(node_->GetWorldPosition());
}

void Character::PostUpdate(float /*timeStep*/)
{
    if (!node_ || !node_->IsEnabled())
        return;

    World* world{ GetSubsystem<World>() };
    const float xPos{ node_->GetWorldPosition().x_ };
    if (velocity_.x_ * xPos < 0.f)
        return;

    const float halfWidth{ .5f * world->GetWidth() * GRID };
    if (xPos < -halfWidth || xPos > halfWidth)
    {
        VariantMap data{ { CharacterAway::P_CHARACTER, this },
                         { CharacterAway::P_TOLEFT, xPos < 0 } };

        SendEvent(E_CHARACTERAWAY, data);

        node_->SetDeepEnabled(false);
    }
}

void Character::FixedUpdate(float /*timeStep*/)
{
    if (route_.IsEmpty() && Moving())
        GoTo(Random(GetSubsystem<World>()->GetWalkable(false, true)));
}

void Character::HandlePostRenderUpdate(StringHash /*eventType*/, VariantMap& eventData)
{
//    DrawDebugGeometry(GetScene()->GetOrCreateComponent<DebugRenderer>(), false);

    AnimatedModel* bodyModel{ models_[MOD_BODY] };

    // No need to animate when out of sight
    if (!bodyModel || !bodyModel->IsInView())
        return;

    Node* headNode{ node_->GetChild("Head", true) };
    if (headNode)
        headNode->SetScale(graphicsNode_->GetTransform().Inverse().Scale() * 4/3.f);
    for (String side: { ".L", ".R" })
    {
        Node* shoulder{ node_->GetChild(String{ "Shoulder" + side }, true) };
        if (shoulder)
            shoulder->SetScale({ 1.f, 1.f + .125f * randomizer_, 1.f });
    }

    /// ANIMATRON
    if (headNode)
    {
        const float timeStep{ eventData[PostRenderUpdate::P_TIMESTEP].GetFloat() };
        const float scanning{ (1.f - randomizer_ * .75f) * (1.f - Abs(ahead_ / 45.f)) };

        if (!route_.IsEmpty())
        {
            const unsigned squares{ static_cast<unsigned>(RoundToInt(velocity_.Length() * 8.f)) };
            const Vector3 posA{ GetSubsystem<World>()->CoordinatesToWorldPosition(route_.At(Min(route_.Size() - 1u, route_.Size() - Min(route_.Size(), Max(0, squares - 1)))), true) };
            const Vector3 posB{ GetSubsystem<World>()->CoordinatesToWorldPosition(route_.At(Min(route_.Size() - 1u, route_.Size() - Min(route_.Size(), squares))), true) };
            const Vector3 downRoute{ posA.Lerp(posB, PowN(Leapt(), 2)) };
            const Vector3 deltaNorm{ (downRoute - node_->GetWorldPosition()).Normalized() };
            const bool fore{ headNode->GetWorldDirection().DotProduct(deltaNorm) < 0.f };
            const float sideDot{ headNode->GetWorldRight().DotProduct(deltaNorm) };

            const float t{ Min(1.f, timeStep * (7.f + 2.3f * randomizer_)) };
            if (!Equals(sideDot, 0.f))
            {
                const float angle{ Clamp(Asin((fore ? (Equals(sideDot, Sign(sideDot)) ? sideDot : Cbrt(sideDot))
                                                    : PowN(sideDot * 1.1f, 3)))
                                         * Min(route_.Size(), 4) * .25f, -42.f, 42.f) };

                ahead_ = Lerp(ahead_, angle, t);
            }
            else
            {
                ahead_ = Lerp(ahead_, 0.f, t) ;
            }
        }
        else
        {
            ahead_ = Lerp(ahead_, 0.f, timeStep) ;
        }

        const float scan{ scanning * scanning * -Polynomial({ 0.f, 10.f, -5.f, .5f, 1.f }, { randomizer_, Lerp(.125f, .5f, randomizer_)}, PT_HARMONIC_SIN).Solve(GetScene()->GetElapsedTime()) };

        headNode->Pitch(12.f * randomizer_ - 3.f);

        // Turn head, neck and back
        const float hexYaw{ (scan + ahead_) * .0625f };
        if (!Equals(hexYaw, 0.f))
        {
            const float pitch{ 2.3f * Abs(hexYaw) };
            headNode->Yaw(hexYaw * 11.f);
            headNode->Pitch(pitch);
            Node* neckNode{ headNode->GetParent() };
            neckNode->Yaw(hexYaw * 3.f);
            neckNode->Pitch(-pitch);
            Node* backNode{ headNode->GetParent()->GetParent() };
            backNode->Yaw(hexYaw * 2.f);
            backNode->Roll(-.25f * hexYaw);
        }
//        headNode->Yaw(scan + ahead_);

        // Skip animating face when not visible enough
//        const float viewAngle{ GetSubsystem<Jib>()->GetNode()->GetWorldDirection().Angle(-headNode->GetWorldDirection()) };
//        if (viewAngle > 150.f - bodyModel->GetLodDistance() * 1.7f)
//            return;

        // Smiling
        const float smile{ person_->GetPersonalia().stats_.mood_ * (1.f - PowN(Sin(GetScene()->GetElapsedTime() * 42.f + 23.f * randomizer_), 2) * .1f) };
        const float open{ 2.f * Max(0.f, smile - .5f) };

        headNode->GetChild("UpperLip", true)->Translate(Vector3::BACK * .01f * open, TS_LOCAL);
        headNode->GetChild("LowerLip", true)->Translate(Vector3::FORWARD * .0125f * open * open, TS_LOCAL);
        for (String side: { ".L", ".R" })
        {
            headNode->GetChild(String{ "MouthCorner" + side }, true)->Translate(Vector3::BACK * (.01f * (smile - open * .23f) - .0042f), TS_LOCAL);
        }

        Node* jawNode{ headNode->GetChild("Jaw", true) };
        if (jawNode)
        {
//            jawNode->Pitch(-2.3f * open * open);
        }

        for (String bone: { "Mouth"/*, "LowerLip"*/ })
            headNode->GetChild(bone, true)->Translate(Vector3::FORWARD * (.00023f * open + .005f * smile - .005f), TS_LOCAL);

        // Eyes
        for (const String& side: StringVector{ ".L", ".R" })
        {
            Node* eyeNode{ headNode->GetChild("Eye" + side, true) };
            eyeNode->Roll(scanning * Polynomial({ 0.f, 23.f, 5.f, 2.f, 10.f }, { randomizer_, Lerp(.125f, .5f, randomizer_)}, PT_HARMONIC_SIN).Solve(GetScene()->GetElapsedTime())
                          - M_TAU * Cbrt(ahead_));

            if (blink_ < .125f)
            {
                for (bool lower: { false, true })
                {
                    Node* lidNode{ node_->GetChild(String(lower ? "Lower" : "Upper") + "EyeLid" + side, true) };
                    lidNode->Pitch(lower ? -22.f : 11.f);
                }

                if (blink_ < 0.f)
                    blink_ = Random(.3f, 2.f);
            }
        }
    }

}

void Character::PutOnGround(Vector3 v, float timeStep)
{
    Octree* octree{ GetScene()->GetComponent<Octree>() };
    PODVector<RayQueryResult> result{};
    Ray ray{ FrontFoot() + Vector3::UP * HEIGHTSTEP * 1.5f, Vector3::DOWN };
    unsigned viewMask{ LAYER(LAYER_GROUND) };
    RayOctreeQuery query{ result, ray, RAY_TRIANGLE, M_INFINITY, DRAWABLE_GEOMETRY, viewMask };
    octree->RaycastSingle(query);

    if (query.result_.Size() && node_->GetWorldPosition().y_ != query.result_.Front().position_.y_)
        node_->Translate(Vector3::UP * (query.result_.Front().position_.y_ - node_->GetWorldPosition().y_) *
                                    Min(1.f, v.Length() * (PowN(Step(), 2) + .25f) * 5.5f * timeStep));
}

void Character::Releap()
{
    World* world{ GetSubsystem<World>() };
    const Vector3 flatNodePos{ node_->GetWorldPosition().ProjectOntoPlane(Vector3::UP) };
    const float newLeap{ (world->CoordinatesToWorldPosition(route_.Back(), true).ProjectOntoPlane(Vector3::UP) - flatNodePos).Length() };

    leap_ = newLeap;
}

float Character::Leapt() const
{
    if (route_.IsEmpty())
        return 1.f;

    World* world{ GetSubsystem<World>() };
    const Vector3 flatNodePos{ node_->GetWorldPosition().ProjectOntoPlane(Vector3::UP) };
    const Vector3 delta{ world->CoordinatesToWorldPosition(route_.Back(), true).ProjectOntoPlane(Vector3::UP) - flatNodePos };

    return Clamp((leap_ - delta.Length()) / leap_, 0.f, 1.f);
}

float Character::Step() const
{
    const float step{ (animCtrl_->IsPlaying("Animations/Walk.ani") ? Abs(Sin(animCtrl_->GetTime("Animations/Walk.ani") / animCtrl_->GetLength("Animations/Walk.ani") * 360.f - 15.f)) * animCtrl_->GetWeight("Animations/Walk.ani") : 0.f) };

    return step;
}

Vector3 Character::FrontFoot() const
{
    const Vector3 foot{ node_->GetWorldPosition() + node_->GetWorldDirection() * SUBGRID * (.2f + .3f * Step()) };

    return foot;
}

void Character::DrawDebugGeometry(DebugRenderer* debug, bool depthTest)
{
//    debug->AddCross(FrontFoot(), .5f, Color::YELLOW, depthTest);
    World* world{ GetSubsystem<World>() };

    if (route_.Size() > 1)
    {
        for (unsigned i{ 0 }; i < route_.Size() - 1; ++i)
        {
            debug->AddLine(world->CoordinatesToWorldPosition(route_.At(i),     true),
                           world->CoordinatesToWorldPosition(route_.At(i + 1), true),
                           Color::CYAN, depthTest);

            if (route_.Size() - i < 5)
                debug->AddQuad(world->CoordinatesToWorldPosition(route_.At(i + 1), true), SUBGRID, SUBGRID, Color::ORANGE.Lerp(Color::YELLOW, (route_.Size() - i - 1) / 3.f ), depthTest);
        }
    }

    debug->AddQuad(world->CoordinatesToWorldPosition(world->WorldPositionToCoordinates2D(node_->GetWorldPosition(), true), true), SUBGRID, SUBGRID, Color::RED, depthTest);

    if (been_.Size() > 1)
    {
        for (unsigned i{ 0 }; i < been_.Size() - 2; ++i)
            debug->AddLine(been_.At(i), been_.At(i + 1),
                           Color::GREEN, depthTest);
    }
}

void Character::FixedPostUpdate(float timeStep) {}
void Character::OnSceneSet(Scene* scene) { LogicComponent::OnSceneSet(scene); }
void Character::OnMarkedDirty(Node* node) {}
void Character::OnNodeSetEnabled(Node* node) {}

bool Character::SaveJSON(JSONValue& dest) const
{
    dest.Set("randomizer", randomizer_);

    String bodyMorphString{};
    AnimatedModel* bodyModel{ *models_[MOD_BODY] };
    for (unsigned m{ 0u }; m < bodyModel->GetNumMorphs(); ++m)
        bodyMorphString.Append(EncodeOctal(MorphToInt(bodyModel->GetMorphWeight(m)),
                                           !bodyMorphString.IsEmpty() && IsDigit(bodyMorphString.Back())));
    dest.Set("bodymorphs", bodyMorphString);

    if (bodyModel->GetMaterial(MAT_HAIR) == bodyModel->GetMaterial(MAT_SKIN))
        dest.Set("bald", 1);

    JSONValue attire{};
    for (int s{ MOD_HAIR }; s < MOD_ALL; ++s)
    {
        AnimatedModel* model{};
        if (models_.TryGetValue(s, model))
        {
            const String modelName{ model->GetModel()->GetName() };
            const String trimmedModelName{ modelName.Substring(modelName.FindLast('/') + 1u).Split('.').Front()};
            String colorString{};
            for (unsigned g{ 0u }; g < model->GetNumGeometries(); ++g)
            {
                PODVector<int> vals{ GetSubsystem<Materials>()->Values(model->GetMaterial(g)) };
                if (s == MOD_GLASSES && g == 0)
                    vals = { model->GetMaterial(g) == bodyModel->GetMaterial(MAT_CORNEA) };
                colorString.Append(EncodeOctal(vals.Buffer(), vals.Size(),
                                               !colorString.IsEmpty() && IsDigit(colorString.Back())));
            }
            String modelString{ trimmedModelName + " " + colorString };
            String morphString{};
            for (unsigned m{ 0u }; m < model->GetNumMorphs(); ++m)
                morphString.Append(EncodeOctal(MorphToInt(model->GetMorphWeight(m)),
                                               !morphString.IsEmpty() && IsDigit(morphString.Back())));
            if (!morphString.IsEmpty())
                modelString += " " + morphString;

            attire.Set(String{ s }, modelString);
        }
    }
    dest.Set("attire", attire);

    if (person_->IsPresent())
    {
        dest.Set("position", node_->GetWorldPosition().ToString());
        dest.Set("yaw", node_->GetWorldRotation().YawAngle());
        dest.Set("ahead", ahead_);
        dest.Set("mobile", mobile_);
        dest.Set("velocity", velocity_.ToString());
        if (!route_.IsEmpty())
            dest.Set("goal", route_.Front().ToString());
    }

    // Underwear
    String bodycolors{ dest.Get("bodycolors").GetString() };
    PODVector<int> vals{ GetSubsystem<Materials>()->Values((*models_[MOD_BODY])->GetMaterial(MAT_UNDERPANTS)) };
    bodycolors.Append(EncodeOctal(vals.Buffer(), vals.Size(),
                                  !bodycolors.IsEmpty() && IsDigit(bodycolors.Back())));
    dest.Set("bodycolors", bodycolors);

    return true;
}

bool Character::LoadJSON(const JSONValue& source)
{
    JSONObject person{ source.GetObject() };
    bool present{ person.Contains("position") };
    randomizer_ = person["randomizer"].GetFloat();

    const Personalia& personalia{ person_->GetPersonalia() };
    bool male{ personalia.gender_ == G_MALE };
    models_[MOD_BODY]->SetModel(RES(Model, String{ "Models/" } + (male ? "Male" : "Female") + ".mdl"));

    PODVector<int> bodyMorphInts{ DecodeOctal(person["bodymorphs"].GetString()) };
    for (unsigned m{ 0u }; m < models_[MOD_BODY]->GetNumMorphs(); ++m)
        models_[MOD_BODY]->SetMorphWeight(m, MorphToFloat(bodyMorphInts.At(m)));

    AssignBodyMaterials();
    if (person.Keys().Contains("bald"))
        models_[MOD_BODY]->SetMaterial(MAT_HAIR, models_[MOD_BODY]->GetMaterial(MAT_SKIN));

    const PODVector<int> bodyColVals{ DecodeOctal(source.Get("bodycolors").GetString()) };
    const unsigned underwearStart{ bodyColVals.Size() - 3u };
    const Shade underwear{ bodyColVals.At(underwearStart), bodyColVals.At(underwearStart + 1u), bodyColVals.At(underwearStart + 2u) };
    Materials* materials{ GetSubsystem<Materials>() };
    models_[MOD_BODY]->SetMaterial(MAT_UNDERPANTS, materials->Cloth(underwear));

    node_->SetDeepEnabled(present);
    if (present)
    {
        node_->SetPosition(FromString<Vector3>(person["position"].GetString()));
        node_->Yaw(person["yaw"].GetFloat());
        mobile_ = person["mobile"].GetFloat();
        if (mobile_ > 0.f)
        {
            animCtrl_->Play("Animations/Walk.ani", 0, true);
        }
        ahead_ = person["ahead"].GetFloat();
        velocity_ = FromString<Vector2>(person["velocity"].GetString());

        if (person.Contains("goal"))
            GoTo(FromString<IntVector2>(person["goal"].GetString()));
    }

    JSONObject attire{ source.Get("attire").GetObject() };
    for (const String& key: attire.Keys())
    {
        ModelSlot s{ static_cast<ModelSlot>(FromString<int>(key)) };
        models_[s] = graphicsNode_->CreateComponent<AnimatedModel>();

        PODVector<int> materialType{};

        String modelName{ "Models/" };
        switch (s)
        {
        default: continue;
        case MOD_HAIR: case MOD_FACE:   modelName += "Hair/"; materialType = { 3u, 0u }; break;
        case MOD_GLASSES:               modelName += "Glasses/"; materialType = { 4u, 1u }; break;
        case MOD_HAT:                   modelName += "Hats/"; materialType = { 0u, 3u }; break;
        case MOD_COAT: materialType = { 0u, 4u }; [[fallthrough]];
        case MOD_SHIRT: case MOD_PANTS: case MOD_SHOES: modelName += "Clothes/"; break;
        }
        const StringVector modelStrings{ attire[key].GetString().Split(' ') };
        const String name{ modelStrings.Front() };
        modelName += name + ".mdl";
        models_[s]->SetModel(RES(Model, modelName));
        models_[s]->SetCastShadows(true);

        const PODVector<int> colVals{ DecodeOctal(modelStrings.At(1u)) };
        MemoryBuffer colBuffer{ colVals.Buffer(), static_cast<unsigned>(sizeof(int) * colVals.Size()) };

        if (name == "Boots")
            materialType = { 3u };
        else if (name == "FlipFlops")
            materialType = { 0u, 1u };

        for (unsigned m{ 0u }; m < models_[s]->GetNumGeometries(); ++m)
        {
            int mt{ 0 };
            if (!materialType.IsEmpty())
                mt = materialType.At(Min(materialType.Size() - 1, m));

            switch (mt)
            {
            default: case 0: // Cloth
                models_[s]->SetMaterial(m, materials->Cloth({ colBuffer.ReadIntVector3() }));
            break;
            case 1: // Paint
                models_[s]->SetMaterial(m, materials->Paint({ colBuffer.ReadIntVector3() }));
            break;
            case 2: // Skin
                models_[s]->SetMaterial(m, materials->Skin({ colBuffer.ReadIntVector2() }));
            break;
            case 3: // Hair
                models_[s]->SetMaterial(m, materials->Hair(Tint{ colBuffer.ReadIntVector2() }));
            break;
            case 4: // Unique
                if (s == MOD_COAT)
                {
                    models_[s]->SetMaterial(m, RES(Material, "Materials/Metal.xml"));
                }
                else if (s == MOD_GLASSES)
                {
                    Material* material{ colBuffer.ReadInt() == 1
                                ? models_[MOD_BODY]->GetMaterial(MAT_CORNEA)
                                : RES(Material, "Materials/Shade.xml") };
                    models_[s]->SetMaterial(m, material);
                }
            break;
            }
        }

        if (modelStrings.Size() > 2u)
        {
            const PODVector<int> morphVals{ DecodeOctal(modelStrings.At(2u)) };
            for (unsigned m{ 0u }; m < models_[s]->GetNumMorphs(); ++m)
                models_[s]->SetMorphWeight(m, MorphToFloat(morphVals.At(m)));
        }
    }

    float sideScale{ (1.f + 2.f * personalia.finalLength_) / 300 };
    graphicsNode_->SetScale({ sideScale, personalia.finalLength_ * .01f, sideScale });

    for (SharedPtr<Component> c: graphicsNode_->GetComponents())
    {
        if (c->IsInstanceOf<Drawable>())
            c->Cast<Drawable>()->SetViewMask(LAYER(LAYER_CHARACTER));
    }

    SubscribeToEvent(E_POSTRENDERUPDATE, DRY_HANDLER(Character, HandlePostRenderUpdate));
    SubscribeToEvent(E_PATHREMOVED, DRY_HANDLER(Character, HandlePathRemoved));

    return true;
}

void Character::MarkNetworkUpdate() { LogicComponent::MarkNetworkUpdate(); }
void Character::GetDependencyNodes(PODVector<Node*>& dest) {}
