/* LucKey Park
// Copyright (C) 2024 LucKey Productions (https://luckey.games)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef ANIMATRON_H
#define ANIMATRON_H

#include "../mastercontrol.h"

struct Tendon
{
    enum Transformation{ ROT_X = 0, ROT_Y, ROT_Z,
                         SCA_X, SCA_Y, SCA_Z,
                         POS_X, POS_Y, POS_Z };

    Tendon(): chain_{}, limits_{} {}
    Tendon(const Vector<unsigned>& chain):
        chain_{ chain },
        limits_{}
    {
    }

//    void AddNode(Node* node)

    Vector<unsigned> chain_; // Node IDs
    HashMap<unsigned, HashMap<char, Vector2> > limits_; // Node ID: TransformType: Min/Max
};

class Animatron: public Component
{
    DRY_OBJECT(Animatron, Component);

public:
    Animatron(Context* context);

    Tendon& CreateTendon(StringHash name, const Vector<unsigned>& chain)
    {
        tendons_.Insert({ name, { chain } });

        return tendons_[name];
    }

    void SetTension(StringHash tendon, float tension = 0.f)
    {
        if (Equals(tension, 0.f))
            tension_.Erase(tendon);
        else if (tendons_.Keys().Contains(tendon))
            tension_[tendon] = tension;
    }

    void OnSetEnabled() override;

    bool Save(Serializer& dest) const override;
    bool SaveXML(XMLElement& dest) const override;
    bool SaveJSON(JSONValue& dest) const override;
    void MarkNetworkUpdate() override;
    void GetDependencyNodes(PODVector<Node*>& dest) override;
    void DrawDebugGeometry(DebugRenderer* debug, bool depthTest) override;

protected:
    void OnNodeSet(Node* node) override;
    void OnSceneSet(Scene* scene) override;
    void OnMarkedDirty(Node* node) override;
    void OnNodeSetEnabled(Node* node) override;


private:
    HashMap<StringHash, Tendon> tendons_;
    HashMap<StringHash, float> tension_;
};

#endif // ANIMATRON_H
