/* LucKey Park
// Copyright (C) 2024 LucKey Productions (https://luckey.games)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "animatron.h"

Animatron::Animatron(Context* context): Component(context),
    tendons_{},
    tension_{}
{
}

void Animatron::OnSetEnabled() { Component::OnSetEnabled(); }
void Animatron::OnNodeSet(Node* node) {}
void Animatron::OnSceneSet(Scene* scene) { Component::OnSceneSet(scene); }
void Animatron::OnMarkedDirty(Node* node) {}
void Animatron::OnNodeSetEnabled(Node* node) {}
bool Animatron::Save(Serializer& dest) const { return Component::Save(dest); }
bool Animatron::SaveXML(XMLElement& dest) const { return Component::SaveXML(dest); }
bool Animatron::SaveJSON(JSONValue& dest) const { return Component::SaveJSON(dest); }
void Animatron::MarkNetworkUpdate() { Component::MarkNetworkUpdate(); }
void Animatron::GetDependencyNodes(PODVector<Node*>& dest) {}
void Animatron::DrawDebugGeometry(DebugRenderer* debug, bool depthTest) {}
