/* LucKey Park
// Copyright (C) 2024 LucKey Productions (https://luckey.games)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef CHARACTER_H
#define CHARACTER_H

#include "../mastercontrol.h"
#include "../pathfinder.h"
#include "identity.h"
#include "animatron.h"

class Person;

class Character: public LogicComponent
{
    DRY_OBJECT(Character, LogicComponent);

public:
    Character(Context* context);

    Person* GetPerson() const { return person_; }
    void SetPerson(Person* person) { person_ = person; }

    void SetVelocity(const Vector2& v) { velocity_ = v; }
    void SetDirection(const Vector2& d)
    {
        if (!Equals(d.LengthSquared(), 0.f))
            node_->LookAt(node_->GetWorldPosition() + Vector3{ d.x_, 0.f, d.y_ });
        else
            node_->SetWorldRotation(Quaternion::IDENTITY);
    }

    void OnSetEnabled() override;
    void Start() override;
    void DelayedStart() override;
    void Stop() override;

    void Update(float timeStep) override;
    void PostUpdate(float timeStep) override;
    void FixedUpdate(float timeStep) override;
    void FixedPostUpdate(float timeStep) override;

    bool SaveJSON(JSONValue& dest) const override;
    bool LoadJSON(const JSONValue& source) override;

    void MarkNetworkUpdate() override;
    void GetDependencyNodes(PODVector<Node*>& dest) override;
    void DrawDebugGeometry(DebugRenderer* debug, bool depthTest) override;

    void Initialize(Person* person);

    void GoTo(const IntVector2& goal);

protected:
    void OnNodeSet(Node* node) override;

    void OnSceneSet(Scene* scene) override;
    void OnMarkedDirty(Node* node) override;
    void OnNodeSetEnabled(Node* node) override;

private:
    void HandlePostRenderUpdate(StringHash eventType, VariantMap& eventData);
    void HandlePathRemoved(StringHash eventType, VariantMap& eventData);
    Vector3 FrontFoot() const;
    float Step() const;
    void PutOnGround(Vector3 v, float timeStep);
    bool Moving() const { return mobile_ > 0.f; }
    float Leapt() const;
    void Releap();

    static int MorphToInt(float weight)
    {
        return Clamp(RoundToInt(63 * (weight + .5f) / 1.5f), 0, 63);
    }
    static int MorphToFloat(int val)
    {
        return (1.5f * val / 63.f) - .5f;
    }

    enum MaterialSlot{ MAT_UNDERPANTS = 0, MAT_SKIN, MAT_LIPS, MAT_HAIR, MAT_GUMS, MAT_TEETH, MAT_EYEWHITE, MAT_IRIS, MAT_PUPIL, MAT_CORNEA, MAT_BROW };
    enum ModelSlot{ MOD_BODY = 0, MOD_HAIR, MOD_FACE, MOD_GLASSES, MOD_HAT, MOD_SHIRT, MOD_COAT, MOD_PANTS, MOD_SHOES, MOD_ALL };

    Person* person_;
    PathFinder* pathFinder_;
    Vector<IntVector2> route_;
    Vector2 velocity_;
    float mobile_;
    float leap_;

    Node* graphicsNode_;
    HashMap<unsigned, AnimatedModel*> models_;
    AnimationController* animCtrl_;
    Animatron* animatron_;
    float blink_;
    float ahead_;
    float randomizer_;

    Vector<Vector3> been_;
    void AssignBodyMaterials();
};

DRY_EVENT(E_CHARACTERAWAY, CharacterAway)
{
    DRY_PARAM(P_CHARACTER, Character); // Character pointer
    DRY_PARAM(P_TOLEFT, ToLeft);           // Bool
}

#endif // CHARACTER_H
