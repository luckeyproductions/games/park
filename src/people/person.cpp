/* LucKey Park
// Copyright (C) 2024 LucKey Productions (https://luckey.games)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "person.h"

#include "../ui/gui.h"
#include "../world.h"

unsigned Person::nextId_{ 1u };

void Person::RegisterObject(Context* context)
{
    context->RegisterFactory<Person>();
    context->RegisterFactory<Character>();
    context->RegisterSubsystem<Personnel>();
}

Person::Person(Context* context): Object(context),
    personalia_{},
    character_{ nullptr },
    present_{ false },
    passed_{ false },
    visited_{ false }
{
    personalia_.id_ = nextId_++;
}

void Person::SetID(unsigned id)
{
    personalia_.id_ = id;
    nextId_ = Max(nextId_, id + 1u);
}

bool Person::SaveJSON(JSONValue& dest) const
{
    JSONValue person{};
    personalia_.SaveJSON(person);

    if (character_)
        character_->SaveJSON(person);

    dest.Set(String{ personalia_.id_ }, person);

    return true;
}

bool Person::LoadJSON(const JSONValue& source)
{
    personalia_.LoadJSON(source);

    if (source.GetObject().Keys().Contains("randomizer"))
    {
        Node* characterNode{ GetSubsystem<World>()->GetScene()->CreateChild(personalia_.GetName()) };
        character_ = characterNode->CreateComponent<Character>();
        character_->SetPerson(this);
        character_->LoadJSON(source);
        if (character_->GetNode()->IsEnabled())
            present_ = true;

        SubscribeToEvent(character_, E_CHARACTERAWAY, DRY_HANDLER(Person, HandleCharacterAway));
    }

    return true;
}

void Person::Appear(const Vector3& position)
{
    if (!character_)
    {
        Node* characterNode{ GetSubsystem<World>()->GetScene()->CreateChild(personalia_.GetName()) };
        character_ = characterNode->CreateComponent<Character>();
        character_->Initialize(this);

        SubscribeToEvent(character_, E_CHARACTERAWAY, DRY_HANDLER(Person, HandleCharacterAway));
    }

    Node* characterNode{ character_->GetNode() };
    Vector3 pos{ position };

    if (!present_)
    {
        bool fromLeft{ (LivesLeft() ^ passed_) != 0 };

        World* world{ GetSubsystem<World>() };
        if (pos.x_ == M_INFINITY)
            pos = { world->CoordinatesToWorldPosition({ fromLeft ? -1 : world->GetWidth(), 0, SIDEWALK_Y - !LivesNorth() * 3 }) +
                    Random(-GRID, GRID) * Vector3::FORWARD * 0.25f };

        characterNode->SetPosition(pos);
        characterNode->ResetDeepEnabled();

        const Vector2 v{ (.9f + .1f * Random(8)) * (fromLeft ? Vector2::RIGHT : Vector2::LEFT) };
        character_->SetVelocity(v);
        character_->SetDirection(v.Normalized());
        character_->GoTo(Random(world->GetWalkable(false)));

        present_ = true;

        Dash* dash{ GetSubsystem<GUI>()->GetDash() };
        if (dash->IsVisible())
            dash->GetTicker()->AddText(personalia_.GetName() + " appeared"); /// TRANSLATE
    }
    else
    {
        characterNode->SetPosition(pos);
        characterNode->ResetDeepEnabled();
    }
}

bool Person::WorksHere() const
{
    return GetSubsystem<Personnel>()->IsEmployed(const_cast<Person*>(this));
}

void Person::HandleCharacterAway(StringHash /*eventType*/, VariantMap& eventData)
{
    present_ = false;
    passed_ = eventData[CharacterAway::P_TOLEFT] != LivesLeft();

    Dash* dash{ GetSubsystem<GUI>()->GetDash() };
    if (dash->IsVisible())
        dash->GetTicker()->AddText(personalia_.GetName() + (passed_ ? " passed by" : " went home")); /// TRANSLATE
}
