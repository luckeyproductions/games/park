/* LucKey Park
// Copyright (C) 2024 LucKey Productions (https://luckey.games)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "../pirmit.h"

#include "identity.h"

bool Personalia::SaveJSON(JSONValue& dest) const
{
    dest.Set("human", human_);
    dest.Set("male", gender_ == 1);
    dest.Set("name", name_);
    dest.Set("family", family_);
    dest.Set("born", born_);
    dest.Set("length", finalLength_);
    dest.Set("home", home_.ToString());

    String colorString{};
    colorString.Append(EncodeOctal(skinTone_.Data(), 2u));
    colorString.Append(EncodeOctal(hairTint_.Data(), 2u));
    colorString.Append(EncodeOctal(eyeColor_.Data(), 3u));
    colorString.Append(EncodeOctal(teethColor_.Data(), 3u, true));
    dest.Set("bodycolors", colorString);

    JSONValue stats{};
    stats_.SaveJSON(stats);
    dest.Set("stats", stats);

    return true;
}

bool Personalia::LoadJSON(const JSONValue& source)
{
    human_       = source.Get("human").GetBool();
    gender_      = source.Get("male").GetBool() ? G_MALE : G_FEMALE;
    name_        = source.Get("name").GetString();
    family_      = source.Get("family").GetString();
    born_        = source.Get("born").GetUInt();
    finalLength_ = source.Get("length").GetUInt();
    home_        = FromString<IntVector2>(source.Get("home").GetString());

    PODVector<int> cols{ DecodeOctal(source.Get("bodycolors").GetString()) };
    skinTone_ = Tone{ cols.At(0u), cols.At(1u) };
    hairTint_ = Tint{ cols.At(2u), cols.At(3u) };
    eyeColor_ = Shade{ cols.At(4u), cols.At(5u), cols.At(6u) };
    teethColor_ = Shade{ cols.At(7u), cols.At(8u), cols.At(9u) };

    stats_.LoadJSON(source.Get("stats"));

    return true;
}

void PersonStats::SaveJSON(JSONValue& dest) const
{
    dest.Set("mood", mood_);
    dest.Set("credit", EncodeOctal(wallet_, false) + EncodeOctal(bank_, true));
}

void PersonStats::LoadJSON(const JSONValue& source)
{
    mood_ = source.Get("mood").GetFloat();
    PODVector<int> credit{ DecodeOctal(source.Get("credit").GetString()) };
    wallet_ = credit.Front();
    bank_ = credit.Back();
}
