﻿/* LucKey Park
// Copyright (C) 2024 LucKey Productions (https://luckey.games)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef IDENTITY_H
#define IDENTITY_H

#include "../materials.h"

enum Gender{ G_NEUTER = 0, G_MALE, G_FEMALE };
enum TransportType{ T_FOOT, T_BIKE, T_CAR, T_BUS, T_TAXI, T_PLANE };

const Vector<String> namesNeuter{
    "Alex",
    "Steph",
    "Jesse",
    "Jayne",
    "Kai",
    "Jean"
};
const Vector<String> namesMale{
    "Abe",
    "Allen",
    "Ben",
    "Carlos",
    "Dennis",
    "Ed",
    "Fela",
    "Francois",
    "Geoff",
    "Harry",
    "Hurl",
    "Jim",
    "Josh",
    "Luke",
    "Manuel",
    "Marc",
    "Mario",
    "Mello",
    "Mike",
    "Morris",
    "Pete",
    "Thomas",
    "Tim",
    "Timothy",
    "Tom",
    "Tommy",
    "Urho",
    "Will"
};
const Vector<String> namesFemale{
    "Ada",
    "Anne",
    "Babette",
    "Barbara",
    "Belle",
    "Caroline",
    "Catya",
    "Chloe",
    "Dizzy",
    "Dolly",
    "Germaine",
    "Iris",
    "Jill",
    "Lilly",
    "Lucy",
    "Manuela",
    "Marie",
    "Olive",
    "River",
    "Stella",
    "Tak",
    "Tina"
};
const Vector<String> namesLast{
    "Antwarden",
    "Archer",
    "Bargebilder",
    "Bedman",
    "Beercake",
    "Bolan",
    "Bonnet",
    "Brahm",
    "Budipal",
    "Byrd",
    "Clambrook",
    "Deltid",
    "Everkeen",
    "Fenderbake",
    "Fulu",
    "Huntfelt",
    "Karelen",
    "Lanwade",
    "Longyard",
    "Lupino",
    "Magmaroc",
    "Mailer",
    "Mikronova",
    "Milltone",
    "Mingemini",
    "Morgana",
    "Mountainhill",
    "Orsini",
    "Raker",
    "Riddle",
    "Ringer",
    "Root",
    "Silverthorn",
    "Soloman",
    "Spark",
    "Spells",
    "Spoke",
    "Statman",
    "Supper",
    "Thalion",
    "Titbit",
    "Uluru",
    "Vased",
    "Virgo",
    "Webu",
    "Yellow"
};

struct PersonStats
{
    float mood_{ RandomOffCenter(1.f) };
    int wallet_{ DiceRoll(5, 7) };
    int bank_{ DiceRoll(10, 42) };

    void SaveJSON(JSONValue& dest) const;
    void LoadJSON(const JSONValue& source);
};

struct Personalia
{
    static String RandomName(Gender gender = G_NEUTER)
    {
        Vector<String> names{ namesNeuter };
        if (gender != G_FEMALE)
            names += namesMale;
        if (gender != G_MALE)
            names += namesFemale;

        return names.At(Random(static_cast<int>(names.Size())));
    }

    bool SaveJSON(JSONValue& dest) const;
    bool LoadJSON(const JSONValue& source);

    unsigned id_;
    bool human_{ true };
    Gender gender_{ (RandomBool() ? G_MALE : G_FEMALE) };
    String name_{ RandomName(gender_) };
    String family_{ namesLast.At(Random(static_cast<int>(namesLast.Size()))) };
    unsigned born_{ 0 }; // 4 months, 2 weeks, 8 days, 4 hours
    unsigned finalLength_{ 1u * RoundToInt(100 * RandomNormal(1.85f + .05f * (gender_ == G_MALE), .01f - .002f * gender_)) }; // Full grown in centimeters
    Tone skinTone_{ Tone::Pick() };
    Tint hairTint_{ Tint::Pick(skinTone_) };
    Shade eyeColor_{ skinTone_.Eyes() };
    Shade teethColor_{ 4, Random(1, 3), 7 };
    IntVector2 home_{ Random(100), Random(360) }; // distance, direction

    PersonStats stats_{};

    String GetName(bool full = true, bool reverse = false) const
    {
        if (!full)
        {
            if (!reverse)
                return name_;
            else
                return family_;
        }
        else
        {
            if (!reverse)
                return name_ + " " + family_;
            else
                return family_ + ", " + name_;
        }
    }
};


#endif // IDENTITY_H
