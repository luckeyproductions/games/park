/* LucKey Park
// Copyright (C) 2024 LucKey Productions (https://luckey.games)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef PATHFINDER_H
#define PATHFINDER_H

#include "mastercontrol.h"


class PathFinder: public Component
{
    DRY_OBJECT(PathFinder, Component);

    struct NodeData
    {
        float fromStart_{ M_INFINITY };
        IntVector2 previous_{ M_MAX_INT, M_MAX_INT };
        float penalty_{ 1.f };
    };

    class Graph: public HashMap<IntVector2, NodeData>
    {
        public:
            bool Add(const IntVector2& coordinate)
            {
                if (Contains(coordinate))
                    return false;

                Insert({ coordinate, NodeData{} });
                return true;
            }
            void Add(const PODVector<IntVector2>& coordinates)
            {
                for (const IntVector2& coordinate: coordinates)
                    Insert({ coordinate, NodeData{} });
            }

            Vector<IntVector2> Neighbors(const IntVector2& coord, bool onlyOrth = false) const;
            IntVector2 Nearest(const HashSet<IntVector2>& frontier, const IntVector2& goal) const;
    };

public:
    PathFinder(Context* context);

    Vector<IntVector2> FindPathDiagonal(IntVector2 to) const;
    Vector<IntVector2> FindPathOrthogonal(IntVector2 to) const;

private:
    IntVector2 Nearest(const Vector<IntVector2>& nodes, const IntVector2& pos) const;
    Vector<IntVector2> Reachable(const Vector<IntVector2>& nodes, const IntVector2& from) const;
};

#endif // PATHFINDER_H
