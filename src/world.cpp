/* LucKey Park
// Copyright (C) 2024 LucKey Productions (https://luckey.games)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "world.h"

#include "landscape/ground.h"
#include "people/person.h"
#include "pathfinder.h"
#include "transport/airplane.h"
#include "structures/drawingtable.h"
#include "effect/dust.h"
#include "ui/dash/clock.h"
#include "effectmaster.h"
#include "pirmit.h"

World::World(Context* context): Object(context),
    parkInfo_{},
    scene_{ context->CreateObject<Scene>() },
    rootNode_{ nullptr },
    ground_{ nullptr },
    people_{},
    path_{},
    size_{ 55, 11, 66 },
    postScarcity_{ true },
    sun_{ nullptr },
    skyLight_{ nullptr },
    skyMat_{ nullptr },
    fogMat_{ nullptr }
{
    Person::RegisterObject(context_);
    context_->RegisterFactory<PathFinder>();
    DrawingTable::RegisterObject(context_);
    context_->RegisterFactory<Ground>();
    Vehicle::RegisterObject(context_);
    context_->RegisterFactory<Dust>();

//    scene_->SetTimeScale(10);
    scene_->CreateComponent<Octree>();
    scene_->CreateComponent<DebugRenderer>()->SetLineAntiAlias(true);
    scene_->CreateComponent<PhysicsWorld>()->SetFps(60);
    rootNode_ = scene_->CreateChild("World");

    context_->RegisterSubsystem(scene_->CreateChild("Jib")->CreateComponent<Jib>());
    context_->RegisterSubsystem<Estate>();

    SubscribeToEvent(E_SCENEPOSTUPDATE, DRY_HANDLER(World, HandleSceneUpdate));
    SubscribeToEvent(E_PERIODCHANGED, DRY_HANDLER(World, HandlePeriodChanged));

    // Airplanes on display
    for (int j{ 0 }; j < 8; ++j)
    {
        Node* jetNode{ scene_->CreateChild("Jet") };
        jetNode->LookAt({ -1.f, .0f, 1.f });
        jetNode->CreateComponent<Airplane>();
        jetNode->SetPosition(CoordinatesToWorldPosition({ size_.x_ * (j + 4)/8, 0, SIDEWALK_Y - 7 }));
    }
}

void World::Generate(bool initial)
{
    rootNode_->SetPosition(GetOffset());

    if (initial)
    {
        ground_ = rootNode_->CreateChild("Ground")->CreateComponent<Ground>();

        Node* sunNode{ scene_->CreateChild("Sun") };
        sunNode->SetPosition({ 5.f, 7.f, -2.f });
        sunNode->LookAt(Vector3::ZERO);

        sun_ = sunNode->CreateComponent<Light>();
        sun_->SetLightType(LIGHT_DIRECTIONAL);
        sun_->SetCastShadows(true);
        sun_->SetShadowBias({ .000055f, 1.25f });
        sun_->SetShadowCascade({ 100.f, 400.f, 800.f, 900.f, .75f });
        sun_->SetShadowIntensity(.55f);
        sun_->SetBrightness(.8f);
        sun_->SetColor(Color::WHITE.Lerp(Color::YELLOW, .1f));

        Node* skyLightNode{ scene_->CreateChild("SkyLight") };
        skyLightNode->SetDirection(Quaternion(180.f, Vector3::UP) * sunNode->GetDirection().Lerp(Vector3::DOWN, .34f));
        skyLight_ = skyLightNode->CreateComponent<Light>();
        skyLight_->SetLightType(LIGHT_DIRECTIONAL);
        skyLight_->SetBrightness(.5f);
        skyLight_->SetSpecularIntensity(.25f);
        skyLight_->SetPerVertex(true);
        skyLight_->SetColor(Color::CYAN.Lerp(COLOR_ZOMP, .23f).Lerp(Color::WHITE, .42f));

        Skybox* sky{ scene_->CreateComponent<Skybox>() };
        sky->SetModel(RES(Model, "Models/Skybox.mdl"));
        skyMat_ = RES(Material, "Materials/Skybox.xml");
        sky->SetMaterial(0, skyMat_);
        fogMat_ = RES(Material, "Materials/DistantFog.xml");
        fogMat_->SetShaderParameter("MatDiffColor", COLOR_ZOMP);
        sky->SetMaterial(1, fogMat_);

        Node* frontFill{ ground_->GetNode()->CreateChild("Fill") };
        frontFill->SetPosition({ size_.x_ * .5f * GRID, 0.f, -300.f - GRID * 4.5f});
        frontFill->SetScale({ 700.f + 3.f * size_.x_, 0.f, 600.f });

        StaticModel* fillPlane{ frontFill->CreateComponent<StaticModel>() };
        fillPlane->SetModel(RES(Model, "Models/Plane.mdl"));
        fillPlane->SetMaterial(RES(Material, "Materials/Ground.xml"));
    }

    ground_->GenerateTerrain();
    GenerateRoad();
    GeneratePeople();

    Jib* jib{ GetSubsystem<Jib>() };
    jib->Restore();
}

bool World::SaveJSON(JSONValue& dest) const
{
    GetSubsystem<Estate>()->SaveJSON(dest);

    JSONValue people{};
    for (unsigned id: people_.Keys())
        people_[id]->Get()->SaveJSON(people);
    dest.Set("people", people);

    String pathString{};
    for (const IntVector2& coords: path_)
        if (ground_->GetHeightMap().WithinBounds(coords))
            pathString.Append(EncodeOctal(coords.Data(), 2u));
    dest.Set("path", pathString);

    ground_->SaveJSON(dest);

    return true;
}

void World::RemovePeople()
{
    GetSubsystem<Personnel>()->Clear();
    for (Person* person: people_.Values())
    {
        Character* character{ person->GetCharacter() };
        if (character)
            character->GetNode()->Remove();
    }

    people_.Clear();
}

bool World::LoadJSON(const JSONValue& source)
{
    parkInfo_.name_ = source.Get("name").GetString();
    if (!ground_->LoadJSON(source))
        return false;

    ClearPath();
    const PODVector<IntVector2> path{ DecodeOctal2(source.Get("path").GetString()) };
    for (const IntVector2& coords: path)
        SetPath(coords, true);

    GetSubsystem<Estate>()->LoadJSON(source.Get("estate"));

    if (source.Get("people").IsNull()) ///
        return true;

    RemovePeople();
    Person::ResetNextID();

    JSONObject people{ source.Get("people").GetObject() };
    for (const String& key: people.Keys())
    {
        const unsigned id{ FromString<unsigned>(key) };
        JSONValue person{ people[key] };
        SharedPtr<Person> p{ context_->CreateObject<Person>() };
        p->SetID(id);
        p->LoadJSON(person);
        people_.Insert({ id, p });
    }

    return true;
}

bool World::SetPath(const IntVector2& coords, bool path)
{
    if (!ground_->GetHeightMap().WithinBounds(coords))
        return false;

    if (path)
    {
        const HeightProfile profile{ ground_->GetHeightMap().GetHeightProfileAt(coords) };
        if ((!profile.IsFlat() && !profile.IsSlope()) || path_.Contains(coords))
            return false;

        path_.Insert(coords);
    }
    else
    {
        if (!path_.Contains(coords))
            return false;

        path_.Erase(coords);
    }

    ground_->UpdatePath(coords);
    return true;
}

void World::RemovePath(const Vector<IntVector2>& coords)
{
    VectorBuffer buffer{};
    for (const IntVector2& coord: coords)
    {
        if (!ground_->GetHeightMap().WithinBounds(coord))
            continue;;

        SetPath(coord, false);
        buffer.WriteIntVector2(coord);
    }

    VariantMap eventData{};
    eventData[PathRemoved::P_COORDINATES] = buffer;
    SendEvent(E_PATHREMOVED, eventData);
}

void World::ClearPath()
{
    for (const IntVector2& coords: path_)
        SetPath(coords, false);
}

void World::GenerateRoad()
{
    ClearPath();

    const int sidewalk{ SIDEWALK_Y };

    StaticModelGroup* slabGroup{ rootNode_->CreateComponent<StaticModelGroup>() };
    slabGroup->SetModel(RES(Model, "Models/Sidewalk.mdl"));
    slabGroup->SetMaterial(0, RES(Material, "Materials/Sidewalk.xml"));
    slabGroup->SetCastShadows(true);
    slabGroup->SetViewMask(LAYER(LAYER_GROUND));

    StaticModelGroup* roadGroup{ rootNode_->CreateComponent<StaticModelGroup>() };
    roadGroup->SetModel(RES(Model, "Models/Road.mdl"));
    roadGroup->SetMaterial(RES(Material, "Materials/Road.xml"));
    roadGroup->SetViewMask(LAYER(LAYER_GROUND));

    StaticModelGroup* zebraGroup{ rootNode_->CreateComponent<StaticModelGroup>() };
    zebraGroup->SetModel(RES(Model, "Models/Zebra.mdl"));
    zebraGroup->SetMaterial(RES(Material, "Materials/VCol.xml"));
    zebraGroup->SetViewMask(LAYER(LAYER_GROUND));

    for (int x{ -111 }; x < GetWidth() + 111; ++x)
    {
        for (int z{ sidewalk }; (z - sidewalk) <= (Abs(GetWidth() / 2 - x) <= 1 ? 5 : 0); ++z)
        {
            if (z == sidewalk)
            {
                for (int m{ 0 }; m < 2; ++m)
                {
                    const IntVector3 slabCoords{ x, 0, z - m * 3 };
                    Node* slabNode{ rootNode_->CreateChild("Slab") };
                    slabNode->SetPosition(CoordinatesToPosition(slabCoords) + Vector3::UP * 1.e-2f * (z != sidewalk));
                    slabNode->Yaw((m == 0 ? 90.f : -90.f));
                    slabGroup->AddInstanceNode(slabNode);

                    if (x >= 0 && x < GetWidth())
                        path_.Insert({ slabCoords.x_, slabCoords.z_ });

                    const IntVector3 roadCoords{ x, 0, z - 1 - m };
                    const Vector3 roadPosition{ CoordinatesToPosition(roadCoords) };

                    Node* roadNode{ rootNode_->CreateChild("Road") };
                    roadNode->SetPosition(roadPosition);
                    roadNode->Yaw((m == 0 ? 90.f : -90.f));
                    roadGroup->AddInstanceNode(roadNode);

                    if (Abs(x - GetWidth() / 2) == ZEBRA_X)
                    {
                        zebraGroup->AddInstanceNode(roadNode);
                        path_.Insert({ roadCoords.x_, roadCoords.z_ });
                    }
                }
            }
            else
            {
                // Initial entrance path
                SetPath({ x, z }, true);
            }
        }
    }
}

void World::GeneratePeople()
{
    RemovePeople();

    for (int p{ 0 }; p < 42; ++p)
    {
        SharedPtr<Person> person{ context_->CreateObject<Person>() };
        people_.Insert({ person->GetPersonalia().id_, person });
    }
}

void World::HandleSceneUpdate(StringHash /*eventType*/, VariantMap& /*eventData*/)
{
    Zone* zone{ GetSubsystem<Renderer>()->GetDefaultZone() };
    zone->SetFogColor(fogMat_->GetShaderParameter("MatDiffColor").GetColor());
    zone->SetAmbientColor(Color::BLACK.Lerp(Color::WHITE.Lerp(Color::CYAN, .125f), .5f + .5f * sun_->GetBrightness()));

    if (people_.Size() != 0)
    {
        Vector<SharedPtr<Person> > people{ people_.Values() };
        const int p{ Random(static_cast<int>(people.Size())) };

        Person* person{ people.At(p) };
        if (!person)
            return;

        const IntVector2 home{ person->GetPersonalia().home_ };
        if (!person->IsPresent() && scene_->GetElapsedTime() > ((home.x_ + (home.y_ % 90)) * .5f - 23.f))
            person->Appear();
    }
}

void World::HandlePeriodChanged(StringHash /*eventType*/, VariantMap& eventData)
{
    const Period period{ static_cast<Period>(eventData[PeriodChanged::P_PERIOD].GetInt()) };
    const bool instant{ eventData[PeriodChanged::P_INSTANT].GetBool() };
    Color sunColor;
    float sunBrightness;

    assert(period < TIME_ALL);
    switch (period)
    {
    case TIME_NIGHT:     { sunColor = Color::WHITE.Lerp(Color::CYAN,   .6f); sunBrightness = .23f; } break;
    case TIME_MORNING:   { sunColor = Color::WHITE.Lerp(Color::YELLOW, .3f); sunBrightness = .7f; } break;
    case TIME_AFTERNOON: { sunColor = Color::WHITE.Lerp(Color::YELLOW, .1f); sunBrightness = .8f; } break;
    case TIME_EVENING:   { sunColor = Color::WHITE.Lerp(Color::ORANGE, .5f); sunBrightness = .55f; } break;
    default: return;
    }

    const float targetSpec{ .1f + (period != TIME_NIGHT) * .9f };
    const float brightScale{ Sqrt(sunBrightness * 1.25f) };
    const float shadowIntensity{ .8f - sunBrightness * .45f };
    const Color skyColor{ Color::WHITE.Lerp(sunColor, 1.f - brightScale) * brightScale };
    const Color fogColor{ COLOR_ZOMP * 1.25f * sunBrightness };

    if (instant)
    {
        sun_->SetSpecularIntensity(targetSpec);
        sun_->SetColor(sunColor);
        sun_->SetBrightness(sunBrightness);
        sun_->SetShadowIntensity(shadowIntensity);
        skyLight_->SetBrightness(sunBrightness * .5f);

        skyMat_->SetShaderParameter("MatDiffColor", skyColor);
        Zone* zone{ GetSubsystem<Renderer>()->GetDefaultZone() };
        zone->SetAmbientColor(skyColor.Lerp(Color::WHITE.Lerp(Color::CYAN, .125f) * .9f, sunBrightness));
        zone->SetFogColor(fogColor);
        fogMat_->SetShaderParameter("MatDiffColor", fogColor);
    }
    else
    {
        const float shiftDuration{ 16.f };
        sun_->SetAttributeAnimation("Specular Intensity",    EFFECT->Sinusoidal(sun_->GetSpecularIntensity(), targetSpec, shiftDuration), WM_CLAMP);
        sun_->SetAttributeAnimation("Color",                 EFFECT->Sinusoidal(sun_->GetColor(), sunColor, shiftDuration), WM_CLAMP);
        sun_->SetAttributeAnimation("Brightness Multiplier", EFFECT->Sinusoidal(sun_->GetBrightness(), sunBrightness, shiftDuration), WM_CLAMP);
        sun_->SetAttributeAnimation("Shadow Intensity",      EFFECT->Sinusoidal(sun_->GetShadowIntensity(), shadowIntensity, shiftDuration), WM_CLAMP);
        skyLight_->SetAttributeAnimation("Brightness Multiplier", EFFECT->Sinusoidal(skyLight_->GetBrightness(), sunBrightness * .5f, shiftDuration), WM_CLAMP);

        skyMat_->SetShaderParameterAnimation("MatDiffColor", EFFECT->Sinusoidal(skyMat_->GetShaderParameter("MatDiffColor").GetColor(), skyColor, shiftDuration), WM_CLAMP, GetScene()->GetTimeScale());
        fogMat_->SetShaderParameterAnimation("MatDiffColor", EFFECT->Sinusoidal(fogMat_->GetShaderParameter("MatDiffColor").GetColor(), fogColor, shiftDuration), WM_CLAMP, GetScene()->GetTimeScale());
    }
}
