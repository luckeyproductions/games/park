/* LucKey Park
// Copyright (C) 2024 LucKey Productions (https://luckey.games)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef JIB_H
#define JIB_H

#include "mastercontrol.h"

class Jib: public Component
{
    DRY_OBJECT(Jib, Component);

public:
    Jib(Context* context);

    bool SaveJSON(JSONValue& dest) const override;
    bool LoadJSON(const JSONValue& source) override;
    void DrawDebugGeometry(DebugRenderer* debug, bool depthTest) override;

    void Restore();
    void SetPivot(const Vector3& pivot, bool correctOverlook = true);
    void Jump(const Vector3& target);

    void Follow(Node* target);
    void Unfollow();

    void SetPosition(const Vector3& pos);

    Camera* GetActiveCamera() const { return activeCamera_; }

    Ray GetScreenRay(const IntVector2& mousePos) const
    {
        const Vector2 screenPos{ 1.f * mousePos / GetSubsystem<Graphics>()->GetSize() };

        return activeCamera_->GetScreenRay(screenPos);
    }

    IntVector3 ViewToGlobal(const IntVector3& step) const
    {
        const float orthoYaw{ (RoundToInt(node_->GetWorldRotation().YawAngle() / 90)) % 4 * 90.f };
        return VectorRoundToInt(Quaternion{ orthoYaw, Vector3::UP } * Vector3{ step });
    }

    void ToggleOrthographic() { activeCamera_->SetOrthographic(!activeCamera_->IsOrthographic());}

protected:
    void OnNodeSet(Node* node) override;

private:
    void HandlePostUpdate(StringHash eventType, VariantMap& eventData);
    void SendMouseMoveEvent();

    Camera* activeCamera_;
    Node* following_;
    Vector3 lastTargetPos_;
    Vector3 lastTargetVelocity_;
    Vector3 pivot_;
    Vector3 jump_;
    float overlook_;

    Vector3 lastPan_;
    Quaternion lastRot_;

    bool middleMouseDown_;

    void JibDebug(StringHash eventType, VariantMap& eventData);
    void HandleMouseButtonDown(StringHash eventType, VariantMap& eventData);
    void HandleMouseButtonUp(StringHash eventType, VariantMap& eventData);
    void UpdatePivot();
};

#endif // JIB_H
