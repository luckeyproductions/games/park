/* LucKey Park
// Copyright (C) 2024 LucKey Productions (https://luckey.games)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef GAME_H
#define GAME_H

#include "mastercontrol.h"


enum GameStatus{ GS_MAIN, GS_PLAY, GS_PAUSED, GS_MODAL };

class Game: public Object
{
    DRY_OBJECT(Game, Object);

public:
    Game(Context* context);

    void StartNew();
    void Save();
    void Load(const String& filename = "test.json");
    void BackToMain();
    void Exit();

    GameStatus GetStatus() const { return status_; }
    void SetStatus(GameStatus status) { status_ = status; }
    void TogglePause();

    Scene* GetWorldScene() const;


    void Resume();

private:
    GameStatus status_;
};

#endif // GAME_H
