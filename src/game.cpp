/* LucKey Park
// Copyright (C) 2024 LucKey Productions (https://luckey.games)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "game.h"

#include "world.h"
#include "landscape/ground.h"
#include "structures/structure.h"
#include "people/person.h"

#include "jib.h"
#include "ui/gui.h"

Game::Game(Context* context): Object(context),
    status_{ GS_MAIN }
{
    context_->RegisterSubsystem<World>()->Generate(true);
    context_->RegisterSubsystem<GUI>()->UpdateSizes();

    SoundSource* musicSource{ GetWorldScene()->CreateComponent<SoundSource>() };
    SharedPtr<Sound> music{ RES(Sound, "Music/joitrax - Nintendo 64 (loop).ogg") };

    music->SetLooped(true);
    musicSource->SetSoundType(SOUND_MUSIC);
    musicSource->Play(music);
}

void Game::Resume()
{
    status_ = GS_PLAY;
    GetSubsystem<GUI>()->HideMainMenu();
    GetSubsystem<GUI>()->ShowDash();
}

void Game::StartNew()
{
    GetSubsystem<World>()->Generate(false);
    GetSubsystem<Estate>()->Clear();
    GetSubsystem<Clock>()->SetTime((GetSubsystem<Time>()->GetTimeSinceEpoch() % 02000u / 2)
                                   + 2.f * Fract(GetSubsystem<Time>()->GetSystemTime() * .5e-3f));

    Resume();
}

void Game::Save()
{
    SharedPtr<JSONFile> saveGame{ context_->CreateObject<JSONFile>() };
    JSONValue& root{ saveGame->GetRoot() };

    World* world{ GetSubsystem<World>() };
    root.Set("name", world->GetParkName());
    GetSubsystem<Clock>()->SaveJSON(root);
    GetSubsystem<Jib>()->SaveJSON(root);
    world->SaveJSON(root);

    saveGame->SaveFile(GetSubsystem<MasterControl>()->GetSaveGamePath("test.json"));
    GetSubsystem<ResourceCache>()->ReleaseResource<JSONFile>(saveGame->GetName());
}

void Game::Load(const String& filename)
{
    SharedPtr<JSONFile> saveGame{ RES_TEMP(JSONFile, MC->GetSaveGamePath(filename)) };
    if (!saveGame)
        return;

    JSONValue& root{ saveGame->GetRoot() };
    GetSubsystem<Clock>()->LoadJSON(root);
    GetSubsystem<Jib>()->LoadJSON(root);
    GetSubsystem<World>()->LoadJSON(root);

    if (!filename.StartsWith("/"))
    {
        status_ = GS_PLAY;

        GetSubsystem<GUI>()->HideMainMenu();
        GetSubsystem<GUI>()->ShowDash();
    }
}

void Game::BackToMain()
{
    status_ = GS_MAIN;

    GetSubsystem<GUI>()->ShowMainMenu();
    GetSubsystem<GUI>()->HideDash();

    Load("/.menu.json");
}

void Game::Exit()
{
    GetSubsystem<Engine>()->Exit();
}

void Game::TogglePause()
{
    if (status_ == GS_PLAY)
        status_ = GS_PAUSED;
    else if (status_ == GS_PAUSED)
        status_ = GS_PLAY;
}

Scene* Game::GetWorldScene() const
{
    return GetSubsystem<World>()->GetScene();
}

/*

  // Teacups, gravitron, octopus
class Spinner: public Ride

enum liftType{ LIFT_NONE, LIFT_TILT, LIFT_ARMWHOLE, LIFT}
int symmetry{};

struct Cascade

cascades_;
*/
