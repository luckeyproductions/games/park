/* LucKey Park
// Copyright (C) 2024 LucKey Productions (https://luckey.games)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "pendulum.h"

Pendulum::Pendulum(Context* context): Component(context)
{
}

void Pendulum::OnSetEnabled() { Component::OnSetEnabled(); }
void Pendulum::OnNodeSet(Node* node) {}
void Pendulum::OnSceneSet(Scene* scene) { Component::OnSceneSet(scene); }
void Pendulum::OnMarkedDirty(Node* node) {}
void Pendulum::OnNodeSetEnabled(Node* node) {}
bool Pendulum::Save(Serializer& dest) const { return Component::Save(dest); }
bool Pendulum::SaveXML(XMLElement& dest) const { return Component::SaveXML(dest); }
bool Pendulum::SaveJSON(JSONValue& dest) const { return Component::SaveJSON(dest); }
void Pendulum::MarkNetworkUpdate() { Component::MarkNetworkUpdate(); }
void Pendulum::GetDependencyNodes(PODVector<Node*>& dest) {}
void Pendulum::DrawDebugGeometry(DebugRenderer* debug, bool depthTest) {}
