/* LucKey Park
// Copyright (C) 2024 LucKey Productions (https://luckey.games)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "world.h"

#include "pathfinder.h"

PathFinder::PathFinder(Context* context): Component(context)
{
}

Vector<IntVector2> PathFinder::FindPathDiagonal(IntVector2 to) const
{
    World* world{ GetSubsystem<World>() };
    Graph graph{};
    graph.Add(world->GetWalkable(true, true, FindPathOrthogonal(to / GRID_SUBS)));

    Vector<IntVector2> nodes{ graph.Keys() };
    if (nodes.IsEmpty())
        return {};

    IntVector2 from{ world->WorldPositionToCoordinates2D(
                    node_->GetWorldPosition(), true) };

    if (!nodes.Contains(from))
        from = Nearest(nodes, from);

    nodes = Reachable(nodes, from);

    if (!nodes.Contains(to))
        to = Nearest(nodes, to);

    HashSet<IntVector2> frontier{ from };
    graph[from].fromStart_ = 0.f;

    while (!frontier.IsEmpty())
    {
        const IntVector2 current{ graph.Nearest(frontier, to) };

        // Found path
        if (current == to)
        {
            Vector<IntVector2> path{ to };
            while(graph[path.Back()].previous_.x_ != M_MAX_INT)
                path.Push(graph[path.Back()].previous_);

            return path;
        }
        // Continue search
        const Vector<IntVector2> neighbors{ graph.Neighbors(current) };
        for (const IntVector2& neighbor: neighbors)
        {
            const float fromStart{ graph[current].fromStart_ + Vector2{ neighbor - current }.Length() * graph[neighbor].penalty_ };
            if (fromStart < graph[neighbor].fromStart_)
            {
                graph[neighbor].fromStart_ = fromStart;
                graph[neighbor].previous_ = current;

                frontier.Insert(neighbor);
            }
        }

        frontier.Erase(current);
    }

    return {};
}

Vector<IntVector2> PathFinder::FindPathOrthogonal(IntVector2 to) const
{
    World* world{ GetSubsystem<World>() };
    Graph graph{};
    graph.Add(world->GetWalkable(true, false));

    Vector<IntVector2> nodes{ graph.Keys() };
    if (nodes.IsEmpty())
        return {};

    IntVector2 from{ world->WorldPositionToCoordinates2D(
                     node_->GetWorldPosition(), false) };

    if (!nodes.Contains(from))
        from = Nearest(nodes, from);

    nodes = Reachable(nodes, from);

    if (!nodes.Contains(to))
        to = Nearest(nodes, to);

    HashSet<IntVector2> frontier{ from };
    graph[from].fromStart_ = 0.f;

    while (!frontier.IsEmpty())
    {
        const IntVector2 current{ graph.Nearest(frontier, to) };
        // Found path
        if (current == to)
        {
            Vector<IntVector2> path{ to };

            while(graph[path.Back()].previous_.x_ != M_MAX_INT)
                path.Push(graph[path.Back()].previous_);

            return path;
        }
        // Continue search
        const Vector<IntVector2> neighbors{ graph.Neighbors(current, true) };
        for (const IntVector2& neighbor: neighbors)
        {
            const float fromStart{ graph[current].fromStart_ + Vector2{ neighbor - current }.Length() * graph[neighbor].penalty_ };
            if (fromStart < graph[neighbor].fromStart_)
            {
                graph[neighbor].fromStart_ = fromStart;
                graph[neighbor].previous_ = current;

                frontier.Insert(neighbor);
            }
        }

        frontier.Erase(current);
    }

    return {};
}

IntVector2 PathFinder::Nearest(const Vector<IntVector2>& nodes, const IntVector2& pos) const
{
    IntVector2 closestToStart{ M_MAX_INT, M_MAX_INT };
    float distance{ M_INFINITY };

    for (const IntVector2& coords: nodes)
    {
        const float d{ Vector2{ coords - pos }.Length() };
        if (d < distance)
        {
            closestToStart = coords;
            distance = d;

            if (distance < M_1_SQRT2)
                break;
        }
    }
    return closestToStart;
}

Vector<IntVector2> PathFinder::Reachable(const Vector<IntVector2>& nodes, const IntVector2& from) const
{
    if (!nodes.Contains(from))
        return {};

    Vector<IntVector2> reachable{};
    PODVector<IntVector2> frontier{ from };

    while (!frontier.IsEmpty())
    {
        const IntVector2 front{ frontier.Front() };
        for (const IntVector2& n: AllNeighbors(front))
        {
            if (nodes.Contains(n) && !reachable.Contains(n) && !frontier.Contains(n))
                frontier.Push(n);
        }

        reachable.Push(front);
        frontier.Erase(0);
    }

    return reachable;
}

Vector<IntVector2> PathFinder::Graph::Neighbors(const IntVector2& coord, bool onlyOrth) const
{
    const Vector<IntVector2> coords{ Keys() };
    Vector<IntVector2> nigh{};

    for (int dx{ -1 }; dx <= 1; ++dx)
    for (int dy{ -1 }; dy <= 1; ++dy)
    {
        if (dx == 0 && dy == 0)
            continue;

        // No cutting corners
        if (Abs(dx) == Abs(dy))
        {
            if (onlyOrth)
                continue;

            const IntVector2 relevantX{ IntVector2{ dx, 0 } + coord };
            const IntVector2 relevantY{ IntVector2{ 0, dy } + coord };
            if (!coords.Contains(relevantX) || !coords.Contains(relevantY))
                continue;
        }

        const IntVector2 neighbor{ IntVector2{ dx, dy } + coord };
        if (coords.Contains(neighbor))
            nigh.Push(neighbor);
    }

    return nigh;
}

IntVector2 PathFinder::Graph::Nearest(const HashSet<IntVector2>& frontier, const IntVector2& goal) const
{
    float lowest{ M_INFINITY };
    IntVector2 nearest{ M_MAX_INT, M_MAX_INT };

    for (const IntVector2& coord: frontier)
    {

        const NodeData& node{ *operator [](coord) };
        const float toGoal{ Vector2{ coord - goal }.Length() };

        if (node.fromStart_ + toGoal < lowest)
        {
            nearest = coord;
            lowest = node.fromStart_ + toGoal;
        }
    }

    return nearest;
}
