/* LucKey Park
// Copyright (C) 2024 LucKey Productions (https://luckey.games)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef INPUTMASTER_H
#define INPUTMASTER_H

#include "mastercontrol.h"

using ActionSet = HashSet<int>;
enum InputAction: int { IA_NONE = -1, IA_STEPLEFT, IA_STEPRIGHT, IA_STEPUP, IA_STEPDOWN, IA_STEPFORWARD, IA_STEPBACK,
                        IA_CONFIRM, IA_CANCEL, IA_PREVIOUS, IA_NEXT, IA_MODE, IA_INVERT, IA_FAST, IA_ALT,
                        IA_ALL };
#define INTERVAL .23f

class InputMaster: public Object
{
    DRY_OBJECT(InputMaster, Object);

public:
    InputMaster(Context* context);

    void Bind(InputAction action, Key key)                  { boundKeys_   [key]    = action; }
    void Bind(InputAction action, PODVector<Key> keys)      { for (Key k: keys) Bind(action, k); }
    void Bind(InputAction action, ControllerButton button)  { boundButtons_[button] = action; }
    Vector2 GetStickPosition(bool right) const;
    float GetTriggerPosition() const;
    bool GetAction(InputAction action);

private:
    void HandleUpdate(StringHash, VariantMap& eventData);
    void SetPreviousActions(const ActionSet& actions);
    void HandleAction(InputAction action);
    void HandleActionRelease(InputAction action);
//    void HandleAction(InputAction action, float timeStep);

    void HandleKD(StringHash, VariantMap& eventData);
    void HandleKU(StringHash, VariantMap& eventData);
    void HandleJAM(StringHash, VariantMap& eventData);
    void HandleJBD(StringHash, VariantMap& eventData);
    void HandleJBU(StringHash, VariantMap& eventData);

    bool Repeating(InputAction action) const { return previousActions_.Contains(action); }
    ActionSet Repeating(const ActionSet& actions) const;
    float SinceToggled(InputAction action) const;

    HashMap<unsigned, InputAction> boundButtons_;
    HashMap<unsigned, InputAction> boundKeys_;
    HashSet<unsigned> pushedJoystickButtons_;
    HashSet<unsigned> pressedKeys_;
    Pair<Vector2, Vector2> stickPositions_;
    Pair<float, float> triggerPositions_;
    ActionSet previousActions_;
    HashMap<int, float> lastToggled_;
    const PODVector<InputAction> modifierActions_{ IA_INVERT, IA_FAST };
};

#endif // INPUTMASTER_H
