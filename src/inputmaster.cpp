/* LucKey Park
// Copyright (C) 2024 LucKey Productions (https://luckey.games)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "inputmaster.h"

#include "ui/gui.h"
#include "ui/tools/tool.h"
#include "ui/tools/digger.h"

InputMaster::InputMaster(Context* context): Object(context),
    boundButtons_{},
    boundKeys_{},
    pushedJoystickButtons_{},
    pressedKeys_{},
    stickPositions_{},
    triggerPositions_{},
    previousActions_{},
    lastToggled_{}
{
    // Bind keyboard
    Bind(IA_STEPFORWARD,    KEY_UP);
    Bind(IA_STEPBACK,       KEY_DOWN);
    Bind(IA_STEPRIGHT,      KEY_RIGHT);
    Bind(IA_STEPLEFT,       KEY_LEFT);
    Bind(IA_CONFIRM,        { KEY_RETURN, KEY_RETURN2, KEY_KP_ENTER, KEY_SPACE });
    Bind(IA_CANCEL,         KEY_ESCAPE);
    Bind(IA_PREVIOUS,       KEY_PAGEUP);
    Bind(IA_NEXT,           KEY_PAGEDOWN);
    Bind(IA_MODE,           KEY_TAB);
    Bind(IA_FAST,           { KEY_SHIFT, KEY_RSHIFT });
    Bind(IA_INVERT,         { KEY_CTRL,  KEY_RCTRL });
    Bind(IA_ALT,            { KEY_ALT,   KEY_RALT });
    // Bind joystick
    Bind(IA_STEPFORWARD,    CONTROLLER_BUTTON_DPAD_UP);
    Bind(IA_STEPBACK,       CONTROLLER_BUTTON_DPAD_DOWN);
    Bind(IA_STEPRIGHT,      CONTROLLER_BUTTON_DPAD_RIGHT);
    Bind(IA_STEPLEFT,       CONTROLLER_BUTTON_DPAD_LEFT);
    Bind(IA_CONFIRM,        CONTROLLER_BUTTON_A);
    Bind(IA_CANCEL,         CONTROLLER_BUTTON_B);
    Bind(IA_PREVIOUS,       CONTROLLER_BUTTON_LEFTSHOULDER);
    Bind(IA_NEXT,           CONTROLLER_BUTTON_RIGHTSHOULDER);
    Bind(IA_MODE,           CONTROLLER_BUTTON_BACK);
    Bind(IA_FAST,           CONTROLLER_BUTTON_RIGHTSTICK);
    Bind(IA_INVERT,         CONTROLLER_BUTTON_LEFTSTICK);
    Bind(IA_ALT,            CONTROLLER_BUTTON_X);

    SubscribeToEvent(E_JOYSTICKAXISMOVE,    DRY_HANDLER(InputMaster, HandleJAM));
    SubscribeToEvent(E_JOYSTICKBUTTONDOWN,  DRY_HANDLER(InputMaster, HandleJBD));
    SubscribeToEvent(E_JOYSTICKBUTTONUP,    DRY_HANDLER(InputMaster, HandleJBU));
    SubscribeToEvent(E_KEYDOWN,             DRY_HANDLER(InputMaster, HandleKD));
    SubscribeToEvent(E_KEYUP,               DRY_HANDLER(InputMaster, HandleKU));

    SubscribeToEvent(E_UPDATE, DRY_HANDLER(InputMaster, HandleUpdate));
}

Vector2 InputMaster::GetStickPosition(bool right) const
{
    return (right ? stickPositions_.second_
                  : stickPositions_.first_);
}

float InputMaster::GetTriggerPosition() const
{
    return triggerPositions_.second_ - triggerPositions_.first_;
}

bool InputMaster::GetAction(InputAction action)
{
    return previousActions_.Contains(action);
}

void InputMaster::HandleUpdate(StringHash, VariantMap& /*eventData*/)
{
    //    const float timeStep{ eventData[Update::P_TIMESTEP].GetFloat() };
    ActionSet actions{};
    // Gather actions
    for (unsigned b: pushedJoystickButtons_)
        if (boundButtons_.Contains(b))
            actions.Insert(boundButtons_[b]);
    for (unsigned k: pressedKeys_)
        if (boundKeys_.Contains(k))
            actions.Insert(boundKeys_[k]);

    // Trigger repeat
    ActionSet repeating{ Repeating(actions) };
    for (int a: repeating)
    {
        if (a != IA_ALT && a != IA_CONFIRM && a != IA_CANCEL && SinceToggled(InputAction{ a }) > INTERVAL * (GetAction(IA_FAST) ? .5f : 1.f))
            previousActions_.Erase(a);
    }

    // Store toggle
    for (int a{ 0 }; a < IA_ALL; ++a)
    {
        const InputAction action{ a };
        if ((actions.Contains(action) && !previousActions_.Contains(action))
         || (previousActions_.Contains(action) && !actions.Contains(action)))
        {
            lastToggled_[action] = GetSubsystem<Time>()->GetElapsedTime();
        }
    }

    // Handle actions
    for (int a: actions)
    {
        InputAction action{ a };
        if (Equals(SinceToggled(action), 0.f) && !modifierActions_.Contains(action))
            HandleAction(action);
    }

    // Store actions
    SetPreviousActions(actions);
}

void InputMaster::SetPreviousActions(const ActionSet& actions)
{
    for (int a: previousActions_)
    {
        if (!actions.Contains(a))
            HandleActionRelease(InputAction{ a });
    }

    previousActions_ = actions;
}

void InputMaster::HandleAction(InputAction action)
{
    GUI* gui{ GetSubsystem<GUI>() };
    if (gui->GetDash()->IsVisible())
    {
        Cursor3D* sceneCursor{ gui->GetSceneCursor() };
        Tool* tool{ sceneCursor->GetTool() };
        Jib* jib{ GetSubsystem<Jib>() };

        switch (action) {
        case IA_STEPRIGHT:      sceneCursor->Step(jib->ViewToGlobal(IntVector3::RIGHT));    break;
        case IA_STEPLEFT:       sceneCursor->Step(jib->ViewToGlobal(IntVector3::LEFT));     break;
        case IA_STEPFORWARD:    sceneCursor->Step(jib->ViewToGlobal(IntVector3::FORWARD));  break;
        case IA_STEPBACK:       sceneCursor->Step(jib->ViewToGlobal(IntVector3::BACK));     break;
        case IA_STEPUP:         sceneCursor->Step(IntVector3::UP); break;
        case IA_STEPDOWN:       sceneCursor->Step(IntVector3::DOWN); break;
        case IA_CONFIRM:
        {
            if (sceneCursor->IsBehindUI())
                gui->HandleAction(IA_CONFIRM);
            else if (tool)
                tool->Apply(false);
        }
        break;
        case IA_ALT:
        {
            if (tool && !sceneCursor->IsBehindUI())
                tool->Apply(true);
        }
        break;
        case IA_CANCEL:
        {
            if (tool)
                sceneCursor->SetTool(nullptr);
            else
                gui->HandleAction(IA_CANCEL);
        }
        break;
        case IA_PREVIOUS:       Tool::PickPrevious(); break;
        case IA_NEXT:           Tool::PickNext(); break;
        case IA_MODE:           break; /// Toggle UI/scene focus
        default: return;
        }
    }
    else
    {
        gui->HandleAction(action);
    }
}

void InputMaster::HandleActionRelease(InputAction action)
{
    GUI* gui{ GetSubsystem<GUI>() };
    Cursor3D* sceneCursor{ gui->GetSceneCursor() };
    Tool* tool{ sceneCursor->GetTool() };

    if (tool)
    {
        if (tool->GetTypeInfo()->IsTypeOf<Digger>() && action == IA_INVERT)
            static_cast<Digger*>(tool)->CenterBrush();
    }
}

//void InputMaster::HandleAction(InputAction action, float timeStep)
//{

//}

#define KEY static_cast<Key>(eventData[KeyDown::P_KEY].GetUInt())
void InputMaster::HandleKD(StringHash, VariantMap& eventData) { pressedKeys_.Insert(KEY); }
void InputMaster::HandleKU(StringHash, VariantMap& eventData) { pressedKeys_.Erase(KEY);  }

#define JOYSTICK_AXIS eventData[JoystickAxisMove::P_AXIS].GetInt(), eventData[JoystickAxisMove::P_POSITION].GetFloat()
void InputMaster::HandleJAM(StringHash, VariantMap& eventData)
{
    const Pair<int, float> a{ JOYSTICK_AXIS };
    const int axis{ a.first_ };
    const float pos{ a.second_ };

    switch (axis)
    {
    default: return;
    case CONTROLLER_AXIS_LEFTX:  stickPositions_.first_ .x_ = pos; break;
    case CONTROLLER_AXIS_LEFTY:  stickPositions_.first_ .y_ = pos; break;
    case CONTROLLER_AXIS_RIGHTX: stickPositions_.second_.x_ = pos; break;
    case CONTROLLER_AXIS_RIGHTY: stickPositions_.second_.y_ = pos; break;
    case CONTROLLER_AXIS_TRIGGERLEFT: triggerPositions_.first_   = pos; break;
    case CONTROLLER_AXIS_TRIGGERRIGHT: triggerPositions_.second_ = pos; break;
    }
}

#define JOYSTICK_BUTTON static_cast<ControllerButton>(eventData[JoystickButtonDown::P_BUTTON].GetUInt())
void InputMaster::HandleJBD(StringHash, VariantMap& eventData) { pushedJoystickButtons_.Insert(JOYSTICK_BUTTON); }
void InputMaster::HandleJBU(StringHash, VariantMap& eventData) { pushedJoystickButtons_.Erase(JOYSTICK_BUTTON);  }

ActionSet InputMaster::Repeating(const ActionSet& actions) const
{
    ActionSet repeat{};
    for (int a: actions)
        if (Repeating(InputAction{ a }))
            repeat.Insert(a);
    return repeat;
}

float InputMaster::SinceToggled(InputAction action) const
{
    const float time{ GetSubsystem<Time>()->GetElapsedTime() };
    return (lastToggled_.Contains(action) ? time - *lastToggled_[action]
                                          : time);
}
