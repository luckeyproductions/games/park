/* LucKey Park
// Copyright (C) 2024 LucKey Productions (https://luckey.games)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef DUST_H
#define DUST_H

#include "../mastercontrol.h"

class Board
{
public:
    Board(const TypedPolynomial<Vector3>& trajectory):
        trajectory_{ trajectory },
        age_{ 0.f }
    {
    }

    void Update(Billboard* billboard, float timeStep)
    {
        if (!billboard || age_ - timeStep > 2.f)
            return;

        age_ += timeStep;
        billboard->position_ = trajectory_.Solve(age_);
        billboard->size_ = Vector2::ONE * Sqrt(age_ + .75f + billboard->position_.y_ * .5f) * M_SQRT2;
        billboard->color_ = Color::WHITE.Lerp(Color::BLACK, .05f).Transparent(Clamp(1.f - Pow(Abs(age_ - 1.f), 2 * (2 - age_)), 0.f, Max(0.f, (.55f - billboard->position_.y_) * 1.2f)));
        billboard->rotation_ += Sign(billboard->rotation_) * timeStep * (23.f - age_ * 5.f);
        billboard->enabled_ = billboard->color_.a_ > 0.f;
    }

    float GetAge() const { return age_; }

private:
    TypedPolynomial<Vector3> trajectory_;
    float age_;
};

class Dust: public BillboardSet
{
    DRY_OBJECT(Dust, BillboardSet);

public:
    Dust(Context* context);
    ~Dust();
    void HandleUpdate(StringHash eventType, VariantMap& eventData);
    void AddCloud(const Vector3& position);

protected:
    void OnNodeSet(Node* node) override;

private:
    Vector<Board*> boards_;
};

#endif // DUST_H
