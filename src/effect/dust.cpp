/* LucKey Park
// Copyright (C) 2024 LucKey Productions (https://luckey.games)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "dust.h"

Dust::Dust(Context* context): BillboardSet(context),
    boards_{}
{
    SetViewMask(LAYER(LAYER_DECO));
    SubscribeToEvent(E_UPDATE, DRY_HANDLER(Dust, HandleUpdate));
}

Dust::~Dust()
{
    for (Board* b: boards_)
        delete b;
}

void Dust::OnNodeSet(Node* node)
{
    if (!node)
        return;

    SetSorted(true);
    SetMaterial(RES(Material, "Materials/Dust.xml"));
}

void Dust::HandleUpdate(StringHash /*eventType*/, VariantMap& eventData)
{
    if (GetNumBillboards() != boards_.Size())
        SetNumBillboards(boards_.Size());

    const float timeStep{ eventData[Update::P_TIMESTEP].GetFloat() };

    for (unsigned b{ 0u }; b < boards_.Size(); ++b)
    {
        Board* board{ boards_.At(b) };
        board->Update(GetBillboard(b), timeStep);
        if (board->GetAge() > 2.f)
        {
            Remove();
            return;
        }
    }

    Commit();
}

void Dust::AddCloud(const Vector3& position)
{
    const Vector3 flatPos{ position.ProjectOntoPlane(Vector3::UP) };
    const Vector3 offCenter{ .4f, .0f, .4f };
    TypedPolynomial<Vector3> min{ { position - offCenter * 4.2f, -offCenter * 2.f + flatPos * .1f,  offCenter * 3.f - flatPos * .02f + Vector3{ -.2f, 5.f / (1.f + flatPos.Length()), -.2f }, (Sin(flatPos.Length() * 90.f) * 23.f * flatPos.NormalizedOrDefault().CrossProduct(Vector3::UP) + Vector3::DOWN * 2.f) * .02f } };
    TypedPolynomial<Vector3> max{ { position + offCenter * 4.2f,  offCenter * 2.f + flatPos * .1f, -offCenter * 3.f - flatPos * .03f + Vector3{ .8f, .2f * Cbrt(flatPos.Length()), .8f },     (Sin(flatPos.Length() * 90.f) * 23.f * flatPos.NormalizedOrDefault().CrossProduct(Vector3::UP) + Vector3::DOWN)       * .03f } };
    TypedBipolynomial<Vector3> variance{ min, max };
    const int numBoards{ 13 };
    SetNumBillboards(GetNumBillboards() + numBoards);
    for (unsigned b{ 0u }; b < numBoards; ++b)
    {
        Billboard* bb{ GetBillboard(GetNumBillboards() - (b + 1u)) };
        bb->enabled_ = true;
        bb->rotation_ = RandomOffCenter(M_EPSILON, 180.f);
        Board* board{ new Board{ variance.ExtractRandom() } };
        boards_.Push(board);
    }
}


