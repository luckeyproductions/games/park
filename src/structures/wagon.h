/* LucKey Park
// Copyright (C) 2024 LucKey Productions (https://luckey.games)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef WAGON_H
#define WAGON_H

#include "../transport/vehicle.h"
#include "trackdefs.h"

#define LIFT 2.f
#define LAUNCH 3.f

class Structure;
class Train;

struct WagonPhysics
{
    float mass_{ 1.f };
    float speed_{ 0.f };
    float alongCurve_{ .5f };
    Vector3 position_{};
    Quaternion rotation_{};
    Vector3 gForces_{ Vector3::DOWN };
    Vector3 angularVelocity_{};
};

class Wagon: public Vehicle
{
    friend class Train;

    DRY_OBJECT(Wagon, Vehicle);

public:
    static void ResetNextID() { nextId_ = 1u; }

    Wagon(Context* context);

    void SetTransform(const Vector3& position, const Quaternion& rotation);

    Train* GetTrain() const { return train_; }
    Track* GetTrack() const;

    bool LoadJSON(const JSONValue& source) override;
    bool SaveJSON(JSONValue& dest) const override;

protected:
    void OnNodeSet(Node* node) override;

private:
    void SetTrain(Train* train);
    void SetElement(TrackElement* element);
    void Reverse();

    void HandleFixedUpdate(StringHash eventType, VariantMap& eventData);

    static unsigned nextId_;
    unsigned wagonID_;
    Structure* structure_;
    Train* train_;
    TrackElement* element_;
    Witch::Spell curve_;

    WagonPhysics physics_;
    float sincePhysicsStep_;
    Vector3 fromPos_;

    SmoothedTransform* smoothedTransform_;
};

#endif // WAGON_H
