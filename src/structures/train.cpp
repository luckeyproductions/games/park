/* LucKey Park
// Copyright (C) 2024 LucKey Productions (https://luckey.games)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "train.h"

unsigned Train::nextId_{ 1u };

Train::Train(Context* context): Component(context),
    trainID_{},
    wagons_{},
    track_{ nullptr },
    wagonGroup_{ nullptr },
    wagonDistance_{ 4/3.f },
    traveled_{ wagonDistance_ * .5f }
{
}

void Train::OnNodeSet(Node* node)
{
    if (!node)
        return;

    node_->AddTag("CamTarget");

    wagonGroup_ = node_->CreateComponent<StaticModelGroup>();
    wagonGroup_->SetModel(RES(Model, "Models/Wagon.mdl"));
    wagonGroup_->SetMaterial(0, GetSubsystem<Materials>()->Paint(3));
    wagonGroup_->SetMaterial(1, GetSubsystem<Materials>()->Paint(1));
    wagonGroup_->SetMaterial(2, GetSubsystem<Materials>()->Skin({ DiceRoll(2, 2), Random(-8, 8) }));
    wagonGroup_->SetCastShadows(true);
    wagonGroup_->SetViewMask(LAYER(LAYER_STRUCTURE));

    SubscribeToEvent(GetFixedUpdateSource(), E_PHYSICSPOSTSTEP, DRY_HANDLER(Train, HandleFixedPostUpdate));
    SubscribeToEvent(E_POSTUPDATE, DRY_HANDLER(Train, HandlePostUpdate));
}

void Train::AddWagon(const JSONValue& wagonData)
{
    Node* wagonNode{ node_->CreateChild("Wagon") };
    Wagon* wagon{ wagonNode->CreateComponent<Wagon>() };
    wagon->SetTrain(this);

    if (!wagonData.IsNull())
    {
        wagon->LoadJSON(wagonData);
    }
    else
    {
        wagon->wagonID_ = Wagon::nextId_++;

        const Ray stationRay{ GetTrack()->GetStationRay() };
        const Vector3 position{ stationRay.origin_ + (wagons_.Size() + 1u) * wagonDistance_ * stationRay.direction_ };
        Quaternion rotation;
        rotation.FromLookRotation(node_->GetParent()->GetParent()->GetRotation() * -stationRay.direction_);
        wagon->SetTransform(position, rotation);

        auto elemAlong{ track_->FindElement(position) };
        wagon->SetElement(elemAlong.first_);
        wagon->physics_.alongCurve_ = elemAlong.second_;
    }

    if (track_ && IsKarts())
    {
        wagonNode = wagonNode->CreateChild("Kart");
        wagonNode->SetScale(.75f);
        wagonNode->Translate({ (wagons_.Size() % 3) - 1.f, -.3f, .0f } );
    }

    wagonGroup_->AddInstanceNode(wagonNode);
    wagons_.Push(wagon);

    if (wagonData.IsNull() && GetNumWagons() < MaxLength())
        AddWagon();
}

Node* Train::GetNearestWagon(const Vector3& position) const
{
    Node* nearest{ nullptr };
    float dist{ M_INFINITY };

    for (unsigned w{ 0 }; w < wagons_.Size(); ++w)
    {
        Node* wagonNode{ wagons_.At(w)->GetNode() };
        const float d{ wagonNode->GetWorldPosition().DistanceSquaredToPoint(position) };

        if (d < dist)
        {
            nearest = wagonNode;
            dist = d;
        }
    }

    return nearest;
}

Wagon* Train::GetNextWagon(Wagon* wagon, bool wrap)
{
    const unsigned i{ wagons_.IndexOf(wagon) };

    if (i == wagons_.Size())
        return nullptr;
    else if (i == 0u)
        return wrap ? wagons_.Back() : nullptr;
    else
        return wagons_.At(i - 1);
}

Wagon* Train::GetPreviousWagon(Wagon* wagon, bool wrap)
{
    const unsigned i{ wagons_.IndexOf(wagon) };

    if (i == wagons_.Size())
        return nullptr;
    else if (i == wagons_.Size() - 1)
        return wrap ? wagons_.Front() : nullptr;
    else
        return wagons_.At(i + 1);
}

void Train::SetTrack(Track* track)
{
    if (track_ == track)
        return;

    track_ = track;

    for (Wagon* w: wagons_)
        w->SetTrain(this);

    if (track_->GetTrackType() == TRACKS_ROAD)
        wagonGroup_->SetModel(RES(Model, "Models/Kart.mdl"));
}

float Train::Mass()
{
    float totalMass{ 0.f };
    for (unsigned w{ 0u }; w < wagons_.Size(); ++w)
    {
        Wagon* wagon{ wagons_.At(w) };
        totalMass += wagon->physics_.mass_;
    }

    return totalMass;
}

void Train::HandlePostUpdate(StringHash eventType, VariantMap& eventData)
{
    if (IsKarts())
        return;

    TrackElement* element{ wagons_.Front()->element_ };
    float along{ wagons_.Front()->physics_.alongCurve_ };

    for (unsigned w{ 1u }; w < wagons_.Size(); ++w)
    {
        along -= wagonDistance_;

        if (along < 0.f)
        {
            element = element->Previous();
            along += element->GetLength();
        }

        Wagon* wagon{ wagons_.At(w) };
        if (wagon->element_ != element)
            wagon->SetElement(element);

        wagon->physics_.alongCurve_ = along;
    }
}

void Train::HandleFixedPostUpdate(StringHash eventType, VariantMap& eventData)
{
    if (!track_ || !wagons_.Size())
        return;

    float trainMass{ Mass() };
    float weightedSpeed{};
    Vector3 averagePosition{};

    for (Wagon* w: wagons_)
    {
        weightedSpeed += w->physics_.speed_ * w->physics_.mass_;
        averagePosition += w->physics_.position_;
    }

    weightedSpeed /= trainMass;
    averagePosition /= GetNumWagons();

    if (!IsKarts())
        for (Wagon* w: wagons_)
            w->physics_.speed_ = weightedSpeed;

    traveled_ += wagons_.Front()->fromPos_.DistanceToPoint(wagons_.Front()->physics_.position_);

//    if (wagons_.Size() < MaxLength() && (traveled_ - wagons_.Size() * wagonDistance_) > wagonDistance_)
//        AddWagon();

    // Distance to StaticModelGroup determines LOD
    node_->SetPosition(averagePosition);
}

void Train::OnSetEnabled() { Component::OnSetEnabled(); }
void Train::OnSceneSet(Scene* scene) { Component::OnSceneSet(scene); }
void Train::OnMarkedDirty(Node* node) {}
void Train::OnNodeSetEnabled(Node* node) {}
bool Train::Save(Serializer& dest) const { return Component::Save(dest); }
bool Train::SaveXML(XMLElement& dest) const { return Component::SaveXML(dest); }

bool Train::SaveJSON(JSONValue& dest) const
{
    dest.Set("id", trainID_);
    JSONArray wagons{};
    for (Wagon* w: wagons_)
    {
        JSONValue wagon{};
        w->SaveJSON(wagon);
        wagons.Push(wagon);
    }
    dest.Set("wagons", wagons);

    return true;
}

void Train::MarkNetworkUpdate() { Component::MarkNetworkUpdate(); }
void Train::GetDependencyNodes(PODVector<Node*>& dest) {}
void Train::DrawDebugGeometry(DebugRenderer* debug, bool depthTest) {}
