/* LucKey Park
// Copyright (C) 2024 LucKey Productions (https://luckey.games)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef TRACK_H
#define TRACK_H

#include "trackdefs.h"

class Structure;

class Track: public Component
{
    DRY_OBJECT(Track, Component);

public:
    static TrackType TrackTypeFromString(const String& str);;
    static void RegisterObject(Context* context);

    static Witch::Spell PathFromProfile(const TrackProfile& profile, unsigned detail = 12);
    static IntVector3 RotatedStep(const IntVector3& step, int rotation)
    {
        int cs{ RoundToInt(Cos(rotation * 90.f)) };
        int sn{ RoundToInt(Sin(rotation * 90.f)) };

        const IntVector3 turnedStep{ step.x_ * cs + step.z_ * sn,
                                     step.y_,
                                     step.x_ * -sn + step.z_ * cs };
        return turnedStep;
    }

    Track(Context* context);

    IntRect GetCoordBounds() const { return coordBounds_; }

    void SetStructure(Structure* structure, bool addStation);
    void SetTrainsData(const JSONArray& data) { trainsData_ = data; }

    Structure* GetStructure() const { return structure_; }
    void OnSetEnabled() override;

    bool Save(Serializer& dest) const override;
    bool SaveXML(XMLElement& dest) const override;
    bool SaveJSON(JSONValue& dest) const override;
    void MarkNetworkUpdate() override;
    void GetDependencyNodes(PODVector<Node*>& dest) override;
    void DrawDebugGeometry(DebugRenderer* debug, bool depthTest) override;

    Model* GenerateElement(const TrackProfile& profile);
    Material* GetPrimaryMaterial() { return primaryMat_; }

    void SetTrackType(TrackType type);
    TrackType GetTrackType() const { return trackType_; }

    unsigned GetNumElements() const { return elements_.Size(); }
    Vector<TrackElement*> GetElements() const { return elements_; }
    TrackElement* Head() const;
    TrackElement* Tail() const;
    TrackElement* FirstElement() const;

    int GetStationLength() const;
    TrackElement* GetStationStart() const;
    TrackElement* GetStationEnd() const;
    Ray GetStationRay() const;

    TrackElement* Append(const TrackStep& step);
    TrackElement* Snip(bool front = true);
    void Mend();
    bool IsCircuit() const;

    void Randomize();
    TrackElement* AddElement(const TrackProfile& profile, int rotation, const IntVector3& coords);
    void RemoveElement(TrackElement* element);

    Pair<TrackElement*, float> FindElement(const Vector3& position);

protected:
    void OnNodeSet(Node* node) override;
    void OnSceneSet(Scene* scene) override;
    void OnMarkedDirty(Node* node) override;
    void OnNodeSetEnabled(Node* node) override;

    Vector<TrackElement*> elements_;
    HashMap<TrackProfile, SharedPtr<Model> > elementSet_; /// Should be reused by all tracks of same type
    HashMap<TrackType, Witch::Spell> crossSections_;      /// Should be reused by all tracks of same type
    SharedPtr<Material> primaryMat_;
    SharedPtr<Material> secondaryMat_;

private:
    void UpdateCoordBounds();
    void AddSupports(TrackElement* element);
    void CreateTrain();
    void CreateStation();

    Structure* structure_;
    StaticModelGroup* supportGroup_;

    TrackType trackType_;
    bool suspended_;
    float trackWidthScale_; // .5-1.5

    IntRect coordBounds_;
    JSONArray trainsData_;
};

#endif // TRACK_H
