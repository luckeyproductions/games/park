/* LucKey Park
// Copyright (C) 2024 LucKey Productions (https://luckey.games)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "ferriswheel.h"

#include "../materials.h"

FerrisWheel::FerrisWheel(Context* context): Structure(context)
{
}

void FerrisWheel::Construct()
{
    Structure::Construct();

    Node* wheel{ node_->GetChild("Graphics") };
    wheel->Translate(Vector3::UP * (1.f + 4/3.f * info_.size_.x_));
    wheel->RemoveAllComponents();
    wheel->SetScale({ 1.f, 1.f, info_.size_.z_ * 1.f });

    Materials* mat{ GetSubsystem<Materials>() };

    AnimatedModel* wheelModel{ wheel->CreateComponent<AnimatedModel>() };
    wheelModel->SetModel(RES(Model, "Models/Structures/Wheel.mdl"));
    wheelModel->SetMaterial(mat->Paint(Shade::VCOL));
    wheelModel->SetMorphWeight(0, (info_.size_.x_ - 2) * .125f);
    wheelModel->SetCastShadows(true);
    wheelModel->SetViewMask(LAYER(LAYER_STRUCTURE));

    const int num{ RoundToInt(info_.size_.x_ / 2.3f) * 6 };
    for (int w{ 0 }; w < num; ++w)
    {
        Node* hinge{ wheel->CreateChild("Gondola") };
        hinge->SetPosition(Quaternion{ w * 360.f / num, Vector3::FORWARD } * Vector3::UP * (1.333f * info_.size_.x_));
        hinge->SetRotation(Quaternion{ -90.f, Vector3::UP });

        Node* gondola{ hinge->CreateChild("Gondola") };
        gondola->Translate(Vector3::DOWN);
        StaticModel* gondolaModel{ gondola->CreateComponent<StaticModel>() };
        gondolaModel->SetModel(RES(Model, "Models/Wagon.mdl"));
        gondolaModel->SetCastShadows(true);
        gondolaModel->SetViewMask(LAYER(LAYER_STRUCTURE));
    }

    SubscribeToEvent(E_SCENEUPDATE, DRY_HANDLER(FerrisWheel, HandleSceneUpdate));
}

void FerrisWheel::HandleSceneUpdate(StringHash /*eventType*/, VariantMap& eventData)
{
    Node* wheel{ node_->GetChild("Graphics") };
    wheel->Roll((10.f - Sqrt(info_.size_.x_)) * eventData[SceneUpdate::P_TIMESTEP].GetFloat());
    for (Node* hinge: wheel->GetChildren())
        hinge->SetRotation(wheel->GetRotation().Inverse() * Quaternion{ -90.f, Vector3::UP });
}
