/* LucKey Park
// Copyright (C) 2024 LucKey Productions (https://luckey.games)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef COASTER_H
#define COASTER_H

#include "structure.h"
#include "track.h"

class Coaster: public Structure
{
    DRY_OBJECT(Coaster, Structure);

public:
    static void RegisterObject(Context* context);

    Coaster(Context* context);

    bool SaveJSON(JSONValue& dest) const override;
    bool LoadJSON(const JSONValue& source) override;

    IntRect GetCoordBounds() const override;
    HashSet<IntVector2> GetOccupied() const override;

protected:
    void Construct() override;

private:
    void HandlePostRenderUpdate(StringHash eventType, VariantMap& eventData);

    String trackString_;
    JSONArray trainsData_;
};

#endif // COASTER_H
