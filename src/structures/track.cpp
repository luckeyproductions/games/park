/* LucKey Park
// Copyright (C) 2024 LucKey Productions (https://luckey.games)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "cyberplasm.h"
#include "../materials.h"
#include "../world.h"
#include "structure.h"
#include "train.h"
#include "../pirmit.h"

#include "track.h"

TrackType Track::TrackTypeFromString(const String& str)
{
    if (str.Contains("triskel", false))
        return TRACKS_TRISKEL;
    if (str.Contains("square", false))
        return TRACKS_SQUASKEL;
    if (str.Contains("flat", false))
        return TRACKS_FLAT;
    if (str.Contains("box", false))
        return TRACKS_BOX;
    if (str.Contains("wood", false))
        return TRACKS_WOOD;
    if (str.Contains("road", false))
        return TRACKS_ROAD;

    return TRACKS_UNDEFINED;
}

void Track::RegisterObject(Context* context)
{
    context->RegisterFactory<Track>();
    context->RegisterFactory<TrackElement>();
    context->RegisterFactory<Train>();
    context->RegisterFactory<Wagon>();
}

Track::Track(Context* context): Component(context),
    elements_{},
    elementSet_{},
    /// Should inherit colour from structure
    primaryMat_{ GetSubsystem<Materials>()->Paint({ Random(64), Random(2, 8), Random(4, 8) })/*RES(Material, "Materials/GlowWire.xml")*/ },
    secondaryMat_{ GetSubsystem<Materials>()->Paint({ Random(64), Random(2, 8), Random(4, 8) }) },
    structure_{ nullptr },
    supportGroup_{ nullptr },
    trackType_{ TRACKS_TRISKEL },
    suspended_{ false },
    trackWidthScale_{ 1.f },
    coordBounds_( IntRect::ZERO ),
    trainsData_{}
{
}

void Track::OnNodeSet(Node* node)
{
    if (!node)
        return;

    supportGroup_ = node_->CreateComponent<StaticModelGroup>();
    supportGroup_->SetModel(RES(Model, "Models/Cylinder.mdl"));
    supportGroup_->SetMaterial(secondaryMat_);
    supportGroup_->SetCastShadows(true);
    supportGroup_->SetViewMask(LAYER(LAYER_STRUCTURE));

//    Randomize();
}


void Track::SetStructure(Structure* structure, bool addStation)
{
    structure_ = structure;

    if (addStation)
    {
        Node* elementNode{ node_->CreateChild("TrackElement")};
        TrackElement* element{ elementNode->CreateComponent<TrackElement>() };
        element->Initialize({}, 0, {});
        element->SetStation(true);
        elements_.Push(element);
        UpdateCoordBounds();

        const float startSlope{ 0.f };
        Vector3 trackPosition{ elementNode->GetPosition() - elementNode->GetDirection() * (GRID * .5f - Sin(startSlope) * .5f)};
        Node* supportNode{ node_->CreateChild("Support") };
        const float height{ element->GetCoords().y_ * HEIGHTSTEP };
        supportNode->SetPosition(trackPosition + (height + .1f) * Vector3::DOWN * .5f);
        supportNode->SetScale({ .23f, height, .23f });
        supportGroup_->AddInstanceNode(supportNode);

        while (elements_.Size() < structure_->GetInfo().size_.x_ * 1u)
            Append({});
    }
}

void Track::UpdateCoordBounds()
{
    IntRect bounds{ IntRect::ZERO };
    if (elements_.IsEmpty())
    {
        coordBounds_ = bounds;
        return;
    }

    for (TrackElement* e: elements_)
        bounds.Merge(e->GetBounds());

    coordBounds_ = bounds;
}

void Track::AddSupports(TrackElement* element)
{
    Node* elementNode{ element->GetNode() };
    const TrackProfile& profile{ element->GetProfile() };

    int startBank{ Abs(profile.bank_.first_) };
    if (startBank % 16 <= 4 || startBank % 16 >= 12)
    {
        const int startSlope{ profile.slope_.first_ };
        Vector3 supportPosition{ -Vector3::FORWARD * (GRID * .5f - Sin(startSlope * 11.f) * .5f) + Vector3::RIGHT * -Sin(profile.bank_.first_ * 11.f) * .75f };
        Node* supportNode{ elementNode->CreateChild("Support") };
        const float height{ element->GetCoords().y_ * HEIGHTSTEP };
        supportNode->SetPosition(supportPosition + (height + .1f) * Vector3::DOWN * .5f);
        supportNode->SetScale({ .23f, height, .23f });
        supportGroup_->AddInstanceNode(supportNode);
    }
}

TrackElement* Track::Append(const TrackStep& step)
{
    TrackElement* head{ Head() };

    if (!head || step.step_ == IntVector3::ZERO)
        return nullptr;

    const TrackProfile headProfile{ head->GetProfile() };
    TrackProfile profile{ headProfile.Appendage(step) };

    Node* elementNode{ node_->CreateChild("TrackElement")};
    TrackElement* element{ elementNode->CreateComponent<TrackElement>() };
    element->Initialize(profile, head->GetStepRotation() + headProfile.turn_, head->GetCoords() + head->GetRotatedStep());

    // Close loop
    const bool closing { Tail()->GetCoords() == element->GetCoords() + element->GetRotatedStep() };

    if (closing)
    {
        const TrackProfile& tailProfile{ Tail()->GetProfile() };

        if (Abs(tailProfile.bank_.first_) % 16 != Abs(profile.bank_.second_) % 16
         || tailProfile.slope_.first_ != profile.slope_.second_
         || Tail()->GetStepRotation() != (element->GetStepRotation() + profile.turn_) % 4) /// Cancel side connections
        {
            elementNode->Remove();
            return nullptr;
        }


        element->ConnectEnd(Tail());
        Tail()->ConnectStart(element);

        TrackElement* tallest{ Head() };
        for (TrackElement* e: elements_)
            if (e->GetCoords().y_ + e->GetProfile().step_.y_ > tallest->GetCoords().y_ + tallest->GetProfile().step_.y_)
                tallest = e;

        while (tallest && tallest->GetProfile().step_.y_ > 0)
        {
            tallest->SetLift(true);
            tallest = tallest->Previous();
        }
    }

    head->ConnectEnd(element);
    element->ConnectStart(head);

    elements_.Push(element);
    AddSupports(element);
    UpdateCoordBounds();

    if (closing)
    {
        CreateStation();
        CreateTrain();
    }

    return element;
}

TrackElement* Track::Snip(bool front)
{
    if (elements_.IsEmpty())
        return nullptr;

    TrackElement* other{ nullptr };

    if (front)
    {
        other = elements_.Back()->Previous();
        RemoveElement(elements_.Back());
    }
    else
    {
        other = elements_.Back()->Next();
        RemoveElement(elements_.Front());
    }

    return other;
}

TrackElement* Track::AddElement(const TrackProfile& profile, int rotation, const IntVector3& coords)
{
    Node* elementNode{ node_->CreateChild("TrackElement")};
    TrackElement* element{ elementNode->CreateComponent<TrackElement>() };
    element->Initialize(profile, rotation, coords);

    elements_.Push(element);
    AddSupports(element);
    UpdateCoordBounds();

    return element;
}

void Track::RemoveElement(TrackElement* element)
{
    if (!element)
        return;

    for (TrackElement* e: elements_)
    {
        if (e->Next() == element)
            e->ConnectEnd(nullptr);
        if (e->Previous() == element)
            e->ConnectStart(nullptr);
    }

    element->GetNode()->Remove();
    elements_.Remove(element);
    UpdateCoordBounds();
}

Pair<TrackElement*, float> Track::FindElement(const Vector3& position)
{
    float nearest{ M_INFINITY };
    TrackElement* element{ nullptr };
    unsigned nearestIndex{ 0u };
    for (TrackElement* elem: elements_)
    {
        const Vector3 localPos{ elem->GetNode()->GetTransform().Inverse() * position };
        const Witch::Spell& curve{ elem->GetCurve() };
        for (unsigned p{ 0 }; p < curve.Size() - 1; ++p)
        {
            const Pair<Vector3, Vector3> segment{ curve.Position(p), curve.Position(p + 1u) };
            const Vector3 projected{ localPos.ProjectOntoLine(segment.first_, segment.second_) };
            const float distanceToProjected{ projected.DistanceToPoint(localPos) };
            if (distanceToProjected > elem->GetLength())
            {
                break;
            }
            else if (distanceToProjected < nearest)
            {
                nearest = distanceToProjected;
                nearestIndex = p;
                element = elem;
            }
            else if (nearest < segment.first_.DistanceToPoint(segment.second_))
            {
                break;
            }
        }
    }

    float along{ 0.f };
    const Vector3 localPos{ element->GetNode()->GetTransform().Inverse() * position };
    const Witch::Spell& curve{ element->GetCurve() };
    for (unsigned p{ 0 }; p <= nearestIndex; ++p)
    {
        if (p != nearestIndex)
        {
            along += curve.Position(p).DistanceToPoint(curve.Position(p + 1u));
        }
        else
        {
            Pair<Vector3, Vector3> segment{ curve.Position(p), curve.Position(p + 1u) };
            const Vector3 projected{ localPos.ProjectOntoLine(segment.first_, segment.second_) };
            along += segment.first_.DistanceToPoint(projected);
        }
    }

    return { element, along };
}

void Track::CreateStation()
{
    TrackElement* station{ GetStationStart() };
    if (!station)
        return;

    while (station)
    {
        station->SetStation(true);
        if (station->Next()->GetProfile().IsStraight())
            station = station->Next();
        else
            station = nullptr;
    }

    if (trackType_ != TRACKS_ROAD)
    {
        Node* stationNode{ node_->CreateChild("Station") };
        stationNode->SetScale(Vector3{ GRID, 2.f * HEIGHTSTEP + .05f, GRID * GetStationLength() });
        stationNode->SetWorldPosition(GetStationStart()->GetNode()->GetWorldPosition());
        stationNode->Translate(Vector3::FORWARD * (GetStationLength() - 1) * .5f * GRID
                               + Vector3::DOWN * (HEIGHTSTEP - .025f));
        StaticModel* stationModel{ stationNode->CreateComponent<StaticModel>() };
        stationModel->SetModel(RES(Model, "Models/Box.mdl"));
        stationModel->SetMaterial(GetSubsystem<Materials>()->Paint({ Random(64), Random(2, 7), Random(1, 5) }, 0));
        stationModel->SetCastShadows(true);
        stationModel->SetViewMask(LAYER(LAYER_STRUCTURE));
    }
    else
    {
        // Create finish line
//            Text3D* startPosition{};
    }
}

void Track::CreateTrain()
{
    if (trainsData_.IsEmpty())
    {
        Node* trainNode{ node_->CreateChild("Train") };
        Train* train{ trainNode->CreateComponent<Train>() };
        train->SetTrack(this);
        train->AddWagon();
    }
    else
    {
        for (const JSONValue& trainData: trainsData_)
        {
            Node* trainNode{ node_->CreateChild("Train") };
            Train* train{ trainNode->CreateComponent<Train>() };
            train->SetTrack(this);
//            train->SetTrainID(trainData.Get("id").GetUInt());
            const JSONArray& wagonsData{ trainData.Get("wagons").GetArray() };
            for (const JSONValue& wagonData: wagonsData)
                train->AddWagon(wagonData);
        }
    }
}

void Track::Mend()
{
    for (TrackElement* elemA: elements_)
    {
        for (TrackElement* elemB: elements_)
        {
            if (elemA->Fits(elemB))
            {
                elemA->ConnectEnd(elemB);
                elemB->ConnectStart(elemA);
            }
        }
    }

    if (IsCircuit())
    {
        CreateStation();
        CreateTrain();
    }
}

bool Track::IsCircuit() const
{
    if (elements_.IsEmpty())
        return false;

    for (TrackElement* elem: elements_)
    {
        if (elem->IsOpenEnded())
            return false;
    }

    return true;
}

void Track::Randomize()
{
    /*
    for (int i{0}; i < 0; ++i)
    {
        TrackStep step{};
        step.turn_ = Random( -1, 1 );
        step.step_ = { Random(-1, 1) + step.turn_ * 2, Random(0, 3), Random(2, 6) };
        Append(step);
    }
    return;
    */

    const int startY{ Random(10) };
    IntVector3 head{ IntVector3::UP * startY };
    int direction{ 0 };
    TrackElement* previousElement{ nullptr };
    const int width{ DiceRoll(3, 6) };
    const int depth{ DiceRoll(2, 4) };
    Vector<int> straights{ width / 2, depth, width, depth, width / 2 };
//    Vector<int> straights{ width / 2, depth/2, width, -depth/2, -width/2, -depth };

    for (unsigned e{ 0 }; e < straights.Size(); ++e)
    {
        TrackProfile profile{};

        int previousSlope{ (previousElement ? previousElement->GetProfile().slope_.second_ : 0)};

        bool last{ e == straights.Size() - 1 };
        profile.step_ = { last ? -head.x_ : Sign(straights.At(e)) * Random(2, 3),
                          (!last ? Random(-head.y_, (8 - e - head.y_ / 2) * (1 + 3 * (e == 0))) : startY - head.y_),
                          (last ? -head.z_ : straights.At(e) - Random(1, 2)) };
        profile.turn_ = { Sign(straights.At(e)) * !last };
        profile.slope_ = { previousSlope, (last ? 0 : RandomSign() * Min((head.y_ + profile.step_.y_) / 2, DiceRoll(2, 3 - profile.step_.y_ / 3)))};

        Node* elementNode{ node_->CreateChild("TrackElement")};
        TrackElement* element{ elementNode->CreateComponent<TrackElement>() };

        element->Initialize(profile, direction, head);

        if (previousElement)
        {
            previousElement->ConnectEnd(element);
            element->ConnectStart(previousElement);
        }
        else
        {
            element->SetLift(true);
        }

        if (last)
        {
            TrackElement* first{ elements_.At(0) };
            element->ConnectEnd(first);
            first->ConnectStart(element);
        }

        // Supports
        Vector3 trackPosition{ elementNode->GetPosition() - elementNode->GetDirection() * (GRID * .5f - Sin(previousSlope * 11.f) * .5f)};
        Node* supportNode{ node_->CreateChild("Support") };
        const float height{ head.y_ * HEIGHTSTEP };
        supportNode->SetPosition(trackPosition + (height + .1f) * Vector3::DOWN * .5f);
        supportNode->SetScale({ .23f, height, .23f });
        supportGroup_->AddInstanceNode(supportNode);

        // Proceed
        elements_.Push(element);
        previousElement = element;

        head += element->GetRotatedStep();
        direction += profile.turn_;
    }

    node_->Rotate(World::StepRotationToQuaternion(Random(4)));
}

void Track::OnSetEnabled() { Component::OnSetEnabled(); }
void Track::OnSceneSet(Scene* scene) { Component::OnSceneSet(scene); }
void Track::OnMarkedDirty(Node* node) {}
void Track::OnNodeSetEnabled(Node* node) {}

Model* Track::GenerateElement(const TrackProfile& profile)
{
    if (profile.step_ == IntVector3::ZERO)
    {
        // Bumper block
        return nullptr;
    }

    if (elementSet_.Contains(profile))
        return elementSet_[profile];

    const unsigned detail{ 1u / Max(1, (trackType_ == TRACKS_ROAD) * 2) };

    SharedPtr<Model> elementModel{ context_->CreateObject<Model>() };
    Witch::Spell path{ PathFromProfile(profile, detail) };

    const Vector3 boxUp{ .5f * HEIGHTSTEP * Vector3::UP };
    const Vector3 gridBlock{ GRID_BLOCK };
    BoundingBox bb{ -.5f * gridBlock + boxUp,
                     .5f * gridBlock + boxUp };
    for (const Witch::Rune& r: path)
        bb.Merge(r.first_);

    elementModel->SetBoundingBox(bb);
    elementModel->SetNumGeometries(0);

    /// Butchery
    ///

    Vector<Vector2> rails{ Vector2::RIGHT * HEIGHTSTEP * .5f };

    if (trackType_ != TRACKS_ROAD)
    {
        rails.Push(Vector2::RIGHT * HEIGHTSTEP * .3f + Vector2::DOWN * HEIGHTSTEP * .5f);
        rails.Push(Vector2::RIGHT * HEIGHTSTEP * .3f + Vector2::UP   * HEIGHTSTEP * .5f);
    }

    Vector3 startDir{ profile.GetStartDirection() };
    Vector3 endDir{ profile.GetEndDirection() };

    for (const Vector2 offset: rails)
    {

        const unsigned g0{ elementModel->GetNumGeometries() };

        Witch::Spell crossSection{{}, Witch::Spell::CURVE};
        if (trackType_ == TRACKS_ROAD)
        {
            crossSection.Push(Witch::Rune{ Vector3{ offset } + 1/3.f * GRID * Vector3::UP * trackWidthScale_, Vector3::UP });
            crossSection.Push(Witch::Rune{ Vector3{ offset } + .05f * Vector3::LEFT + .3f * GRID * Vector3::UP   * trackWidthScale_, Vector3::LEFT });
            crossSection.Push(Witch::Rune{ Vector3{ offset } + .05f * Vector3::LEFT + .3f * GRID * Vector3::DOWN * trackWidthScale_, Vector3::LEFT } );
            crossSection.Push(Witch::Rune{ Vector3{ offset } + 1/3.f * GRID * Vector3::DOWN * trackWidthScale_, Vector3::DOWN } );
        }
        else
        {
            crossSection = Witch::Spell{ Geonomicon::Ellipse(Vector2::ONE * (.12f + .1f * (g0 == 0)) * trackWidthScale_, 4 - (detail != 0), offset * trackWidthScale_ + Vector2{ -.15f * HEIGHTSTEP, 0.f}) };
        }


        SharedPtr<Model> rail{ Grimoire::Summon(Geonomicon::Callis(path, { crossSection }, startDir, endDir), detail) };
        elementModel->SetNumGeometries(elementModel->GetNumGeometries() + rail->GetNumGeometries());

        for (unsigned i{ 0 }; i < rail->GetNumGeometries(); ++i)
        {
            const unsigned g{ g0 + i };
            elementModel->SetGeometry(g, 0, rail->GetGeometry(i, 0));
        }
    }


    elementSet_.Insert({ profile, elementModel });
    return elementModel;
}

int Track::GetStationLength() const
{
    int res{};

    TrackElement* element{ GetStationStart() };
    if (!element)
        return 0;

    while (element && element->IsStation())
    {
        res += element->GetProfile().step_.z_;
        element = element->Next();
    }

    return res;
}

TrackElement* Track::GetStationStart() const
{
    TrackElement* element{ nullptr };
    for (TrackElement* e: elements_)
    {
        if (e->IsStation())
        {
            element = e;
            break;
        }
    }

    if (!element)
        return nullptr;

    while (element->Previous() && element->Previous()->GetProfile().IsStraight())
        element = element->Previous();

    return element;
}

TrackElement* Track::GetStationEnd() const
{
    TrackElement* element{ nullptr };
    for (TrackElement* e: elements_)
    {
        if (e->IsStation())
        {
            element = e;
            break;
        }
    }

    if (!element)
        return nullptr;

    while (element->Next() && element->Next()->IsStation())
        element = element->Next();

    return element;
}

Ray Track::GetStationRay() const
{
    TrackElement* stationEnd{ GetStationEnd() };
    if (!stationEnd)
        return {};

    const Matrix3x4 stationTransform{ stationEnd->GetNode()->GetTransform() };
    const Vector3 origin{ stationEnd->GetCurve().Back().Transformed(stationTransform).Position() };
    const Vector3 direction{ stationTransform.Rotation() * Vector3::BACK };

    return { origin, direction };
}

void Track::SetTrackType(TrackType type)
{
    trackType_ = type;

    if (trackType_ == TRACKS_ROAD)
    {
        primaryMat_ = RES(Material, "Materials/Asphalt.xml");
        trackWidthScale_ = 1.5f;
    }
}

TrackElement* Track::Head() const
{
    TrackElement* first{ nullptr };

    for (TrackElement* e: elements_)
    {
        if (!first)
            first = e;
        else if (e == first)
            break;

        if (!e->Next())
            return e;
    }

    return nullptr;
}

TrackElement* Track::Tail() const
{
    TrackElement* first{ nullptr };

    for (TrackElement* e: elements_)
    {
        if (!first)
            first = e;
        else if (e == first)
            break;

        if (!e->Previous())
            return e;
    }

    return nullptr;
}

TrackElement* Track::FirstElement() const
{
    if (elements_.IsEmpty())
        return nullptr;
    else
        return elements_.Front();
}

Witch::Spell Track::PathFromProfile(const TrackProfile& profile, unsigned detail)
{
    const Vector3 gridBlock{ GRID_BLOCK };
    const Vector3 beginPos{ Vector3{ 0.f, .5f, -.5f} * gridBlock};
    const Vector3 turnOffset{ Vector3{ 1.f * Sign(profile.turn_) * (Abs(profile.turn_) % 2),
                    0.f,
                    -1.f * (Abs(profile.turn_) % 3) }
                              * gridBlock * .5f};
    const Vector3 endPos{ beginPos + profile.step_ * gridBlock - turnOffset };

    Witch::Wand bend
    {
        { beginPos.ProjectOntoPlane(Vector3::UP),
                    Vector3::RIGHT },
        { endPos.ProjectOntoPlane(Vector3::UP),
                    Quaternion( 90.f * profile.turn_, Vector3::UP) * Vector3::RIGHT }
    };

    const float dia{ Vector2(endPos.x_ - beginPos.x_, endPos.z_ - beginPos.z_).Length() };
    const unsigned subs{ ((3u + detail * 2u + Abs(profile.turn_)) * RoundToInt(Sqrt(dia)) + Abs(profile.step_.y_) / 2u) * 3u/4u };
    bool curves{ !profile.IsStraight() };

    if (!curves)
    {
        bend.first_.first_  += Vector3::UP * beginPos.y_;
        bend.second_.first_ += Vector3::UP * beginPos.y_;
    }

    Witch::Spell path{ bend.Plot((curves ? subs : 0), Vector3::UP), Witch::Spell::CURVE };

    if (curves)
    {
        const float inSlope{  profile.slope_.first_ * 90/8.f };
        const float outSlope{ profile.slope_.second_ * 90/8.f };
        Witch::Wand raise
        {
            { Vector3::UP * beginPos.y_,
                        Quaternion(inSlope, Vector3::LEFT) * Vector3::UP },
            { Vector3::UP * endPos.y_ + Vector3::FORWARD * dia,
                        Quaternion(outSlope, Vector3::LEFT) * Vector3::UP }
        };

        Witch::Spell elevation{ raise.Plot(subs, Vector3::UP), Witch::Spell::CURVE };

        // Length for banking
        Vector3 point{ Vector3::ONE * M_INFINITY };
        float length{};

        for (unsigned r{ 0 }; r < path.Size(); ++r)
        {
            const float y{ elevation.At(r).first_.y_ };
            path.At(r).first_ = bend.Plot(elevation.At(r).first_.z_ / dia).first_ + y * Vector3::UP;

            if (point.x_ != M_INFINITY)
                length += (path.At(r).first_ - point).Length();

            point = path.At(r).first_;
        }

        // Bank
        float t{ 0.f };
        Vector3 axis{};
        for (unsigned r{ 0 }; r < path.Size(); ++r)
        {
            const float bankStep{ 360.f / 16.f };
            const float ct{ .5f - Cos(180.f * t) * .5f };
            const float rot{ Lerp(profile.bank_.first_ * bankStep, profile.bank_.second_ * bankStep, ct) };

            Vector3 delta{ path.At(r).first_ - path.At(Min(path.Size() - 1, r + 1)).first_ };

            if (r == 0)
                axis = -profile.GetStartDirection();
            else if (r == path.Size() - 1)
                axis = -profile.GetEndDirection();
            else if (delta.LengthSquared() != 0.f)
                axis = (delta).Normalized();

            path.At(r).second_ = Quaternion{ rot, axis } * path.At(r).second_;

            t += delta.Length() / length;
        }
    }
    else
    {
        // Bank
        for (unsigned r{ 0 }; r < path.Size(); ++r)
        {
            const float bankStep{ 22.5f };
            const float rot{ profile.bank_.first_ * bankStep };

            Vector3 axis{ (path.Back().first_ - path.Front().first_).Normalized() };
            path.At(r).second_ = Quaternion{ rot, axis } * path.At(r).second_;
        }
    }

    return path;
}

bool Track::Save(Serializer& dest) const { return Component::Save(dest); }
bool Track::SaveXML(XMLElement& dest) const { return Component::SaveXML(dest); }
bool Track::SaveJSON(JSONValue& dest) const
{
    String trackString{};
    for (TrackElement* element: elements_)
    {
        const TrackProfile profile{ element->GetProfile() };
        trackString.Append(EncodeOctal(element->GetCoords().Data(), 3u));
        trackString.Append(EncodeOctal(element->GetStepRotation() + element->IsStation() * 4 + element->IsLift() * 8, true));
        trackString.Append(EncodeOctal(profile.step_.Data(), 3u));
        trackString.Append(EncodeOctal(profile.turn_, true));
        trackString.Append(EncodeOctal(IntVector2{ profile.slope_.first_, profile.slope_.second_ }.Data(), 2u));
        trackString.Append(EncodeOctal(IntVector2{ profile.bank_ .first_, profile.bank_ .second_ }.Data(), 2u));
    }

    dest.Set("track", trackString);
    return true;
}

void Track::MarkNetworkUpdate() { Component::MarkNetworkUpdate(); }
void Track::GetDependencyNodes(PODVector<Node*>& dest) {}
void Track::DrawDebugGeometry(DebugRenderer* debug, bool depthTest) {}
