/* LucKey Park
// Copyright (C) 2024 LucKey Productions (https://luckey.games)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef DRAWINGTABLE_H
#define DRAWINGTABLE_H

#include "structure.h"

class DrawingTable: public Object
{
    DRY_OBJECT(DrawingTable, Object);

public:
    static void RegisterObject(Context* context);

    DrawingTable(Context* context);

    Vector<StructureInfo> GetDesigns(StructureCategory category = STRUCT_UNDEFINED);
    StructureInfo GetBlueprint(StringHash identifier) const
    {
        if (!designs_.Contains(identifier))
            return {};
        else
            return designs_.Find(identifier).operator->()->second_;
    }

private:
    void LoadBlueprints(JSONFile* file);

    HashMap<StringHash, StructureInfo> designs_;
    //    Vector<SharedPtr<Structure> > templates_;
};

#endif // DRAWINGTABLE_H
