/* LucKey Park
// Copyright (C) 2024 LucKey Productions (https://luckey.games)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef TRACKDEFS_H
#define TRACKDEFS_H

#include "cyberplasm/include/witch.h"

#include "../mastercontrol.h"

enum TrackType{ TRACKS_UNDEFINED = 0, TRACKS_TRISKEL, TRACKS_SQUASKEL, TRACKS_FLAT, TRACKS_BOX, TRACKS_WOOD, TRACKS_ROAD };

struct TrackStep
{
    IntVector3 step_{ 0, 0, 1 };
    int turn_{ 0 };
    int slope_{ 0 };
    int bank_{ 0 };
};

struct TrackProfile
{
    IntVector3 step_{ 0, 0, 1 };
    int turn_{ 0 }; // Difference between begin and end in quarter turns, min -2 max 2 (double that for diagonals)
    Pair<int, int> slope_{ 0, 0 }; // Begin and end slope, -8 straight down
    Pair<int, int> bank_{ 0, 0 }; // Begin and end banking, 8 == -8, upside down

    unsigned ToHash() const
    {
        return Vector4(step_, turn_ ).ToHash() * (23 * slope_.ToHash() + bank_.ToHash());
    }

    bool operator ==(const TrackProfile& rhs) const
    {
        return step_  == rhs.step_
            && turn_  == rhs.turn_
            && slope_ == rhs.slope_
            && bank_  == rhs.bank_;
    }

    TrackProfile Appendage(const TrackStep& step) const
    {
        TrackProfile nextProfile{};
        nextProfile.step_ = step.step_;
        nextProfile.turn_ = step.turn_;
        nextProfile.slope_ = { slope_.second_, step.slope_ };
        nextProfile.bank_  = { bank_.second_,  step.bank_ };

        return nextProfile;
    }
    TrackStep ForwardStep() const
    {
        TrackStep step{};
        step.step_ = step_;
        step.turn_ = turn_;
        step.slope_ = slope_.second_;
        step.bank_  = bank_.second_;
        return step;
    }
    TrackStep BackStep() const
    {
        TrackStep step{};
        step.step_ = step_; // Reverse?
        step.turn_ = turn_;
        step.slope_ = slope_.first_;
        step.bank_  = bank_.first_;
        return step;
    }

    bool IsStraight() const
    {
        return turn_ == 0
            && bank_.first_ % 16 == 0 && bank_.second_ % 16 == 0 && bank_.first_ == bank_.second_
            && step_.x_ == 0 && step_.y_ == 0
            && slope_.first_ == 0 && slope_.second_ == 0;
    }

    Vector3 GetStartDirection() const
    {
        if (step_ == IntVector3::ZERO)
            return Vector3::ZERO;

        Vector3 startDir{ Vector3::FORWARD };

        if (slope_.first_ != 0)
            startDir = Quaternion{ slope_.first_ * 90/8.f, Vector3::LEFT } * startDir;

        return startDir;
    }
    Vector3 GetEndDirection() const
    {
        if (step_ == IntVector3::ZERO)
            return Vector3::ZERO;

        Vector3 endDir{ Quaternion{ 90.f * turn_, Vector3::UP } * Vector3::FORWARD };

        if (slope_.second_ != 0)
            endDir = Quaternion{ slope_.second_ * 90/8.f, endDir.CrossProduct(Vector3::UP) } * endDir;

        return endDir;
    }
};

class Track;

class TrackElement: public Component
{
    DRY_OBJECT(TrackElement, Component);

public:
    TrackElement(Context* context);

    void Initialize(const TrackProfile& profile, int rotation, const IntVector3& coords = IntVector3::ONE * M_MAX_INT);
    IntVector3 GetRotatedStep() const;

    void ConnectStart(TrackElement* previous) { connected_.first_ = previous; }
    void ConnectEnd(TrackElement* next) { connected_.second_ = next; }
    TrackElement* Next() const { return connected_.second_; }
    TrackElement* Previous() const { return connected_.first_; }
    bool IsOpenEnded() const
    {
        return connected_.first_ == nullptr || connected_.second_ == nullptr;
    }

    const TrackProfile& GetProfile() const { return profile_; }
    void SetProfile(const TrackProfile& profile)
    {
        if (profile_ == profile)
            return;

        Initialize(profile, rotation_, coords_);
    }

    void SetCoords(const IntVector3& coords);
    IntVector3 GetCoords() const { return coords_; }
    float GetLength() const { return length_; }
    const Witch::Spell& GetCurve() const { return curve_; }

    IntRect GetBounds() const;

    void SetStepRotation(int rotation);
    char GetStepRotation() const { return rotation_; }
    Quaternion GetQuaternion(bool tip = false) const;

    void SetStation(bool station) { station_ = station; }
    bool IsStation()  const { return station_; }

    void SetLift(bool lift) { lift_ = lift; }
    bool IsLift()  const { return lift_; }

    bool Fits(TrackElement* elemB);

protected:
    void OnNodeSet(Node* node) override;

private:
    void RecalculateLength();

    Track* track_;
    TrackProfile profile_;
    char rotation_;
    IntVector3 coords_;
    Pair<TrackElement*, TrackElement*> connected_;
    float length_;
    Witch::Spell curve_;

    bool station_;
    bool lift_;
};

#endif // TRACKDEFS_H
