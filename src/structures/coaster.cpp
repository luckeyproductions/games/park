/* LucKey Park
// Copyright (C) 2024 LucKey Productions (https://luckey.games)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "train.h"
#include "../pirmit.h"

#include "coaster.h"

void Coaster::RegisterObject(Context* context)
{
    context->RegisterFactory<Coaster>();
    Track::RegisterObject(context);
}

Coaster::Coaster(Context* context): Structure(context),
    trackString_{},
    trainsData_{}
{
//    SubscribeToEvent(E_POSTRENDERUPDATE, DRY_HANDLER(Coaster, HandlePostRenderUpdate));
}

bool Coaster::SaveJSON(JSONValue& dest) const
{
    if (!Structure::SaveJSON(dest) || !GetComponent<Track>()->SaveJSON(dest))
        return false;

    JSONArray trainArray{};
    PODVector<Train*> trains{};
    node_->GetComponents<Train>(trains, true);
    for (Train* train: trains)
    {
        JSONValue trainValue{};
        train->SaveJSON(trainValue);
        trainArray.Push(trainValue);
    }
    dest.Set("trains", trainArray);

    return true;
}

bool Coaster::LoadJSON(const JSONValue& source)
{
    if (!Structure::LoadJSON(source))
        return false;

    trackString_ = source.Get("track").GetString();
    trainsData_ = source.Get("trains").GetArray();

    return true;
}

IntRect Coaster::GetCoordBounds() const
{
    Track* track{ GetComponent<Track>() };
    if (!track)
        return IntRect::ZERO;
    else
        return LocalToGlobal(track->GetCoordBounds());
}

HashSet<IntVector2> Coaster::GetOccupied() const
{
    Track* track{ GetComponent<Track>() };
    if (!track)
    {
        return {};
    }
    else
    {
        HashSet<IntVector2> occupied{};
        for (TrackElement* e: track->GetElements())
        {
            IntRect bounds{ e->GetBounds() };
            for (int x{ bounds.Min().x_ }; x < bounds.Max().x_; ++x)
            for (int y{ bounds.Min().y_ }; y < bounds.Max().y_; ++y)
                occupied.Insert(LocalToGlobal({ x, y }));
        }

        return occupied;
    }
}

void Coaster::Construct()
{
    if (id_ == 0u)
        id_ = nextId_++;

    SharedPtr<Track> track{ context_->CreateObject<Track>() };
    if (info_.attributes_.Contains("tracks"))
        track->SetTrackType(Track::TrackTypeFromString(info_.attributes_["tracks"].GetString()));
    node_->AddComponent(track, track->GetID(), REPLICATED);

    bool loadTrack{ !trackString_.IsEmpty() };
    track->SetStructure(this, !loadTrack);

    if (loadTrack)
    {
        const PODVector<int> trackData{ DecodeOctal(trackString_) };
        MemoryBuffer buffer{ trackData.Buffer(), static_cast<unsigned>(sizeof(int) * trackData.Size()) };

        while (!buffer.IsEof())
        {
            const IntVector3 coords{ buffer.ReadIntVector3() };
            const int rotStatLift{ buffer.ReadInt() };
            const int rotation{ rotStatLift % 4 };
            const bool station{ (rotStatLift & 4) != 0 };
            const bool lift{ (rotStatLift & 8) != 0 };
            const IntVector3 step{ buffer.ReadIntVector3() };
            const int turn{ buffer.ReadInt() };
            const IntVector2 slope{ buffer.ReadIntVector2() };
            const IntVector2 bank{ buffer.ReadIntVector2() };

            TrackProfile profile{};
            profile.step_ = step;
            profile.turn_ = turn;
            profile.slope_ = { slope.x_, slope.y_ };
            profile.bank_ = { bank.x_, bank.y_ };

            TrackElement* elem{ track->AddElement(profile, rotation, coords) };
            elem->SetStation(station);
            elem->SetLift(lift);

            if (!trainsData_.IsEmpty())
                track->SetTrainsData(trainsData_);

            track->Mend();
        }
    }
}

void Coaster::HandlePostRenderUpdate(StringHash eventType, VariantMap& eventData)
{
    Track* track{ GetComponent<Track>() };
    if (!track || track->GetStationLength() == 0)
        return;

    Ray stationRay{ track->GetStationRay() };
    const Vector3 worldOrigin{ node_->GetTransform() * stationRay.origin_ };
    const Vector3 worldDirection{ node_->GetTransform().Rotation() * stationRay.direction_ };
    GetScene()->GetComponent<DebugRenderer>()->AddLine(worldOrigin, worldOrigin + worldDirection, Color::ORANGE, false);
}
