/* LucKey Park
// Copyright (C) 2024 LucKey Productions (https://luckey.games)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "structure.h"
#include "../world.h"
#include "train.h"

#include "wagon.h"

unsigned Wagon::nextId_{ 1u };

Wagon::Wagon(Context* context): Vehicle(context),
    wagonID_{},
    structure_{ nullptr },
    train_{ nullptr },
    element_{ nullptr },
    physics_{},
    sincePhysicsStep_{ 0.f },
    fromPos_{},
    smoothedTransform_{ nullptr }
{
    capacity_ = 2u;
}

void Wagon::SetTransform(const Vector3& position, const Quaternion& rotation)
{
    node_->SetPosition(position);
    node_->SetWorldRotation(rotation);
    physics_.position_ = position;
    physics_.rotation_ = rotation;
    smoothedTransform_->SetTargetPosition(position);
    smoothedTransform_->SetTargetWorldRotation(rotation);
}


Track* Wagon::GetTrack() const { return train_->GetTrack(); }

void Wagon::OnNodeSet(Node* node)
{
    if (!node)
        return;

    smoothedTransform_ = node_->CreateComponent<SmoothedTransform>();
    SubscribeToEvent(GetFixedUpdateSource(), E_PHYSICSPRESTEP, DRY_HANDLER(Wagon, HandleFixedUpdate));
}

void Wagon::SetTrain(Train* train)
{
    train_ = train;
    if (!train_)
        return;

    Track* track{ GetTrack() };
    if (track)
    {
        structure_ = track->GetStructure();
        SetElement(track->FirstElement());
        node_->SetParent(track->GetNode());
    }
}

void Wagon::SetElement(TrackElement* element)
{
    element_ = element;

    if (!element_)
        return;

    curve_ = { element_->GetCurve().Transformed(element_->GetNode()->GetTransform()) };
}

void Wagon::Reverse() /// Train's job
{
    physics_.speed_ = -physics_.speed_;

//    fromRot_ = Quaternion{ 180.f, node_->GetWorldDirection() } * fromRot_;
//    physics_.rotation_ = Quaternion{ 180.f, node_->GetWorldDirection() } * physics_.rotation_;
}

void Wagon::HandleFixedUpdate(StringHash /*eventType*/, VariantMap& eventData)
{
    if (!element_)
        return;

    float timeStep{ eventData[PhysicsPreStep::P_TIMESTEP].GetFloat() };
    physics_.alongCurve_ += physics_.speed_ * timeStep;
    fromPos_ = physics_.position_;

    if (physics_.alongCurve_ > element_->GetLength())
    {

        if (element_->Next())
        {
            physics_.alongCurve_ -= element_->GetLength();
            SetElement(element_->Next());
        }
        else
        {
            Reverse();
        }
    }

    if (physics_.alongCurve_ < 0.f)
    {
        if (element_->Previous())
        {
            physics_.alongCurve_ += element_->Previous()->GetLength();
            SetElement(element_->Previous());
        }
        else
        {
            Reverse();
        }
    }


    unsigned i{ 0u };
    float rest{ physics_.alongCurve_ };
    float stepSize{ (curve_.At(i + 1).Position() - curve_.At(i).Position()).Length() };

    while (stepSize < rest)
    {
        rest -= stepSize;
        ++i;

        if (i >= curve_.Size() - 2u)
            break;

        stepSize = curve_.At(i + 1).Position().DistanceToPoint(curve_.At(i).Position());
    }

    const Vector3 oldPos{ physics_.position_ };
    const float rt{ rest / stepSize };

    physics_.position_ = curve_.At(i).Position().Lerp(curve_.At(Min(curve_.Size() - 1, i + 1)).Position(), rt);

    const Vector3 delta{ physics_.position_ - oldPos };
    const bool kart{ GetTrack()->GetTrackType() == TRACKS_ROAD };

    if (!kart)
    {
        physics_.speed_ += 11.f * timeStep * (curve_.At(Min(curve_.Size() - 1, i + 1)).Position() - curve_.At(i).Position()).Normalized().DotProduct(Vector3::DOWN);
        physics_.speed_ -= Clamp(Sign(physics_.speed_) * timeStep * .125f, -physics_.speed_, physics_.speed_);

        // Station brake
        if (element_->IsStation() && physics_.speed_ > LAUNCH)
            physics_.speed_ -= 5.f * physics_.speed_ * timeStep;

        const float drive{ (element_->IsLift() ? LIFT : element_->IsStation() ? LAUNCH : 0.f) };
        if (drive > 0.f && physics_.speed_ >= 0.f)
            physics_.speed_ = Max(physics_.speed_, Lerp(physics_.speed_, LIFT, Min(1.f, timeStep * 10.f)));
    }
    else
    {
        const float t{ physics_.alongCurve_ / element_->GetLength() };
        const float topSpeed{ 9.f };
        Node* kartNode{ GetNode()->GetChild("Kart") };
        const TrackProfile currentProfile{ element_->GetProfile() };
        const TrackProfile nextProfile{ element_->Next()->GetProfile() };

        const float bank{ Lerp(static_cast<float>(currentProfile.bank_.first_ ),
                               static_cast<float>(currentProfile.bank_.second_), t) };
        const float turn{ Lerp(static_cast<float>(currentProfile.turn_),
                               static_cast<float>(nextProfile.turn_), t) };

        const float bend{ 2 * (4 * turn - bank - kartNode->GetPosition().x_ * .25f) / (Abs(currentProfile.step_.x_) + Abs(currentProfile.step_.z_)) };

        physics_.speed_ -= (.023f * ((Abs(currentProfile.step_.x_) + currentProfile.step_.y_) / 4
                                   + (currentProfile.slope_.first_ + currentProfile.slope_.second_) / 8)
                          - .023f * topSpeed * Abs(bend))
                           * timeStep * Sqrt(Max(0.f, physics_.speed_ - .5f));
        physics_.speed_ += (topSpeed - physics_.speed_) * timeStep * .1f;
        physics_.speed_ = Clamp(physics_.speed_, 0.f, topSpeed);

        Vector3 kartPos{ kartNode->GetPosition() };
        kartPos.x_ = Lerp(kartPos.x_, fmod(((-physics_.speed_ / topSpeed) + .5f) - turn * .5f, 1.f), 23.f * timeStep * (Sin(5.f * (physics_.alongCurve_ + kartPos.x_)) * .5f + .5f));
//        kartNode->SetPosition(kartPos);
    }

    if (!Equals(physics_.speed_, 0.f))
    {
        Node* trackNode{ GetTrack()->GetNode() };
        Quaternion newRot{};
        newRot.FromLookRotation(trackNode->GetRotation() * (delta * Sign(physics_.speed_)).Normalized(),
                                trackNode->GetRotation() * Quaternion{ Sign(physics_.speed_) < 0.f ? -90.f : 90.f, (delta).Normalized() }
                                * curve_.At(i).Normal().Lerp(curve_.At(Min(curve_.Size() - 1, i + 1)).Normal(), rt).Normalized() );

        physics_.rotation_ = newRot;
    }

    sincePhysicsStep_ = 0.f;

    if (!smoothedTransform_->IsInProgress())
    {
        node_->SetPosition(physics_.position_);
        node_->SetWorldRotation(physics_.rotation_);
    }

    smoothedTransform_->SetTargetPosition(physics_.position_);
    smoothedTransform_->SetTargetWorldRotation(physics_.rotation_);
}

bool Wagon::SaveJSON(JSONValue& dest) const
{
    dest.Set("id", wagonID_);
    dest.Set("speed", physics_.speed_);
    dest.Set("position", physics_.position_.ToString());
    dest.Set("rotation", physics_.rotation_.ToString());

    return true;
}

bool Wagon::LoadJSON(const JSONValue& source)
{
    wagonID_ = source.Get("id").GetUInt();
    physics_.speed_ = source.Get("speed").GetFloat();
    physics_.position_ = FromString<Vector3>(source.Get("position").GetString());
    physics_.rotation_ = FromString<Quaternion>(source.Get("rotation").GetString());
    node_->SetPosition(physics_.position_);
    node_->SetWorldRotation(physics_.rotation_);
    smoothedTransform_->SetTargetPosition(physics_.position_);
    smoothedTransform_->SetTargetWorldRotation(physics_.rotation_);

    Track* track{ GetTrack() };
    const Pair<TrackElement*, float> placement{ track->FindElement(physics_.position_) };
    SetElement(placement.first_);
    physics_.alongCurve_ = placement.second_;

    return true;
}
