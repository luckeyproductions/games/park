/* LucKey Park
// Copyright (C) 2024 LucKey Productions (https://luckey.games)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef TRAIN_H
#define TRAIN_H

#include "wagon.h"
#include "track.h"

class Train: public Component
{
    DRY_OBJECT(Train, Component);

public:
    static void ResetNextID()
    {
        nextId_ = 1u;
        Wagon::ResetNextID();
    }

    Train(Context* context);

    void SetTrainID(unsigned id);
    void OnSetEnabled() override;
    void AddWagon(const JSONValue& wagonData = {});
    Node* GetNearestWagon(const Vector3& position) const;
    Wagon* GetNextWagon(Wagon* wagon, bool wrap = false);
    Wagon* GetPreviousWagon(Wagon* wagon, bool wrap = false);
    void SetTrack(Track* track);
    Track* GetTrack() const { return track_; }

    bool Save(Serializer& dest) const override;
    bool SaveXML(XMLElement& dest) const override;
    bool SaveJSON(JSONValue& dest) const override;
    void MarkNetworkUpdate() override;
    void GetDependencyNodes(PODVector<Node*>& dest) override;
    void DrawDebugGeometry(DebugRenderer* debug, bool depthTest) override;

    unsigned GetNumWagons() const { return wagons_.Size(); }

protected:
    void OnNodeSet(Node* node) override;
    void OnSceneSet(Scene* scene) override;
    void OnMarkedDirty(Node* node) override;
    void OnNodeSetEnabled(Node* node) override;

private:
    float Mass();
    void HandlePostUpdate(StringHash eventType, VariantMap& eventData);
    void HandleFixedPostUpdate(StringHash eventType, VariantMap& eventData);

    bool IsKarts() const { return track_->GetTrackType() == TRACKS_ROAD; }
    unsigned MaxLength() const { return Floor(track_->GetStationLength() * 3 / wagonDistance_); }

    static unsigned nextId_;
    unsigned trainID_;
    Vector<Wagon*> wagons_;
    Track* track_;
    StaticModelGroup* wagonGroup_;
    float wagonDistance_;
    float traveled_;
};

#endif // TRAIN_H
