/* LucKey Park
// Copyright (C) 2024 LucKey Productions (https://luckey.games)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "../world.h"
#include "../effect/dust.h"
#include "train.h"
#include "drawingtable.h"

#include "structure.h"

unsigned Structure::nextId_{ 1u };

Vector3 Structure::CenterOffset(const IntVector3& size)
{
    return VectorMod(VectorMax(IntVector3::ZERO, size - IntVector3::ONE), { 2, 1, 2 }) * .5f * GRID;
}

Structure::Structure(Context* context): Component(context),
    info_{},
    id_{ 0u },
    customName_{ "" },
    rotation_{ 0 },
    coords_{ IntVector3::ONE * M_MAX_INT }
{
}

bool Structure::SaveJSON(JSONValue& dest) const
{
    dest.Set("design", info_.identifier_.ToHash());
    dest.Set("name", info_.name_);
    dest.Set("coords", coords_.ToString());
    dest.Set("rotation", rotation_);
    dest.Set("size", info_.size_.ToString());

    return true;
}

bool Structure::LoadJSON(const JSONValue& source)
{
    info_.name_ = source.Get("name").GetString();
    info_.size_ = VectorClamp(FromString<IntVector3>(source.Get("size").GetString()),
                              info_.minSize_, info_.maxSize_);

    rotation_ = source.Get("rotation").GetInt();
    coords_ = FromString<IntVector3>(source.Get("coords").GetString());

    /// Class-specific reconstruction

    return true;
}

Quaternion Structure::GetQuaternion() const
{
    return World::StepRotationToQuaternion(rotation_);
}

void Structure::DrawBounds(const Color& color, bool laserFence) const
{
    World* world{ GetSubsystem<World>() };
    DebugRenderer* debug{ GetScene()->GetComponent<DebugRenderer>() };

    PODVector<Vector3> points{};
    const IntRect coordBounds{ GetCoordBounds() };
    const Vector3 np{ -Vector3{ GRID * .5f, 0.f, GRID * .5f }};
    points.Push(np + world->CoordinatesToWorldPosition(coordBounds.Min()));
    points.Push(np + world->CoordinatesToWorldPosition({ coordBounds.Max().x_, GetCoordBounds().Min().y_ }));
    points.Push(np + world->CoordinatesToWorldPosition(coordBounds.Max()));
    points.Push(np + world->CoordinatesToWorldPosition({ coordBounds.Min().x_, GetCoordBounds().Max().y_ }));
    for (int v{ 0 }; v < 4; ++v)
    {
        const Vector3 u{ Vector3::UP * (Sin(GetScene()->GetElapsedTime() * 270.f) * .125f + .25f) };
        const Vector3 a{ u + points.At(v) };
        const Vector3 b{ u + points.At((v + 1) % points.Size()) };
        const Vector3 d{ (a - b).Normalized() };
        const Vector3 n{ d.CrossProduct(Vector3::UP) };
        for (int l{ 0 }; l < 3 * laserFence; ++l)
        {
            const Color laser{ color.r_, Min(1.f, color.g_ + l * .23f), Min(1.f, color.b_ + .23f), 1.f - 1/3.f * l };
            debug->AddLine(l * u * .4f + a, l * u * .4f + b, laser, true);
        }
        debug->AddLine(a + n * .2f + u * -.2f, b + n * .2f + u * -.2f, color, false);
        debug->AddLine(b + n * .2f + u * -.2f, b - d * .2f + u * -.2f, color, false);
    }
}

void Structure::PlayBuildSound(int tone)
{
    if (info_.buildSound_.IsEmpty() || !GetScene())
        return;

    SoundSource* buildSound{ GetScene()->CreateChild("BuildSound")->CreateComponent<SoundSource>() };
    buildSound->SetAutoRemoveMode(REMOVE_NODE);
    buildSound->Play(RES(Sound, info_.buildSound_));
    buildSound->SetFrequency(buildSound->GetFrequency() * (.9f + (Random(5) + tone) * .025f));

    if (info_.buildSound_.EndsWith("ChippingHammer.wav"))
        buildSound->SetGain(.55f);
}

void Structure::Collapse()
{
    ValueAnimation* sink{ new ValueAnimation{ context_ } };
    const float sinkTime{ 2.f };
    const int frames{ CeilToInt(23 * sinkTime) };
    const float dt{ sinkTime / frames };
    for (int f{ 0 }; f < frames; ++f)
    {
        const float t{ f * dt };
        sink->SetKeyFrame(t + 1/17.f, node_->GetPosition() + Vector3::DOWN * HEIGHTSTEP * 5e2f * PowN(t, 2));
    }

    sink->SetEventFrame(sinkTime, E_SANK);
    node_->SetAttributeAnimation("Position", sink, WM_CLAMP);
}

IntRect Structure::LocalToGlobal(const IntRect& rect) const
{
    const IntVector2 shift{ -(VectorRoundToInt(Vector2::ONE.Rotated(-90.f * rotation_)) - IntVector2::ONE) / 2 };
    const IntVector2 gMin{ LocalToGlobal(rect.Min()) };
    const IntVector2 gMax{ LocalToGlobal(rect.Max()) };
    return { VectorMin(gMin, gMax) + shift, VectorMax(gMin, gMax) + shift };
}

void Structure::UpdatePosition()
{
    if (node_)
        node_->SetWorldPosition(GetSubsystem<World>()->CoordinatesToWorldPosition(coords_));
}

void Structure::OnNodeSet(Node* node)
{
    if (!node)
        return;

    node_->AddTag("CamTarget");
    node_->SetRotation(GetQuaternion());
    UpdatePosition();

    Construct();
}

void Structure::Construct()
{
    if (id_ == 0u)
        id_ = nextId_++;

    Node* graphicsNode{ node_->CreateChild("Graphics") };
    graphicsNode->SetPosition(CenterOffset(info_.size_));
    StaticModel* building{ graphicsNode->CreateComponent<StaticModel>() };
    building->SetModel(RES(Model, info_.model_));
    building->SetMaterial(RES(Material, "Materials/VCol.xml"));
    building->SetCastShadows(true);
    building->SetViewMask(LAYER(LAYER_STRUCTURE));
}

Estate::Estate(Context* context): Object(context)
{
    SubscribeToEvent(E_SANK, DRY_HANDLER(Estate, HandleSank));
}

bool Estate::SaveJSON(JSONValue& dest) const
{
    JSONValue estate{};
    for (Structure* s: structures_)
    {
        JSONValue structure{};
        s->SaveJSON(structure);
        estate.Set(String{ s->Structure::id_ }, structure);
    }
    dest.Set("estate", estate);

    return true;
}

void Estate::Clear()
{
    Structure::ResetNextID();
    Train::ResetNextID();

    for (Structure* structure: structures_)
    {
        structure->GetNode()->Remove();
    }
    structures_.Clear();
}

bool Estate::LoadJSON(const JSONValue& source)
{
    Clear();

    const Vector<String> ids{ source.GetObject().Keys() };
    for (const String& idString: ids)
    {
        JSONValue structure{ source.Get(idString) };
        unsigned designHash{ structure.Get("design").GetUInt() };
        const StructureInfo design{ GetSubsystem<DrawingTable>()->GetBlueprint(StringHash{ designHash }) };
        SharedPtr<Structure> s{ DynamicCast<Structure>(context_->CreateObject(design.class_)) };
        s->SetInfo(design);
        const unsigned id{ FromString<unsigned>(idString) };
        Structure::nextId_ = Max(Structure::nextId_, (s->Structure::id_ = id) + 1u);
        s->LoadJSON(structure);
        structures_.Push(s);

        Node* structureNode{ GetSubsystem<World>()->GetScene()->CreateChild(s->info_.name_) };
        s->SetNode(structureNode);
    }

    return true;
}

Structure* Estate::Construct(const StructureInfo& cyanotype)
{
    SharedPtr<Structure> structure{ DynamicCast<Structure>(context_->CreateObject(cyanotype.class_)) };
    structure->SetInfo(cyanotype);
    structures_.Push(structure);

    Node* structureNode{ GetSubsystem<World>()->GetScene()->CreateChild(structure->info_.name_) };
    structure->SetNode(structureNode);

    return structure;
}

void Estate::CancelConstruction(Structure* site)
{
    structures_.Remove(SharedPtr<Structure>(site));
    site->GetNode()->Remove();
}

bool Estate::Demolish(Structure* structure)
{
    SharedPtr<Structure> s{ structure };
    if (structure && structures_.Contains(s))
    {
        World* world{ GetSubsystem<World>() };
        Node* dustNode{ s->GetScene()->CreateChild("Poof") };
        HashSet<IntVector2> emptied{ s->GetOccupied() };
        if (!emptied.IsEmpty())
        {
            Vector3 averagePos{};
            for (const IntVector2& coord: emptied)
            {
                const Vector3 pos{ world->CoordinatesToWorldPosition(coord) };
                averagePos += pos;
            }
            averagePos /= emptied.Size();

            Dust* dust{ dustNode->CreateComponent<Dust>() };
            dustNode->SetWorldPosition(averagePos);
            for (const IntVector2& coord: emptied)
            {
                const Vector3 pos{ world->CoordinatesToWorldPosition(coord) - averagePos };
                dust->AddCloud(pos);
            }
        }

        s->Collapse();
        structures_.Remove(s);
        return true;
    }

    return false;
}

void Estate::HandleSank(StringHash, VariantMap&)
{
    static_cast<Node*>(GetEventSender())->Remove();
}
