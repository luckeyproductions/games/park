#include "../world.h"
#include "track.h"

#include "trackdefs.h"

TrackElement::TrackElement(Context* context): Component(context),
    track_{ nullptr },
    profile_{},
    rotation_{ 0 },
    coords_{ IntVector3::ONE * M_MAX_INT },
    connected_{ nullptr, nullptr },
    length_{ GRID },
    station_{ false },
    lift_{ false }
{
}

void TrackElement::OnNodeSet(Node* node)
{
    if (!node)
        return;

    Node* trackNode{ node_->GetParent() };
    if (trackNode)
        track_ = trackNode->GetComponent<Track>();
}

void TrackElement::Initialize(const TrackProfile& profile, int rotation, const IntVector3& coords)
{
    if (!track_)
        return;

    profile_ = profile;

    SetCoords(coords);
    SetStepRotation(rotation);

    StaticModel* model{ node_->GetOrCreateComponent<StaticModel>() };
    model->SetModel(track_->GenerateElement(profile));
    model->SetMaterial(track_->GetPrimaryMaterial());
    model->SetCastShadows(true);
    model->SetViewMask(LAYER(LAYER_STRUCTURE));

    RecalculateLength();
}

IntVector3 TrackElement::GetRotatedStep() const
{
    return Track::RotatedStep(profile_.step_, rotation_);

}

void TrackElement::SetCoords(const IntVector3& coords)
{
    if (coords_ == coords)
        return;

    coords_ = coords;

    node_->SetPosition(World::CoordinatesToPosition(coords_));
}

IntRect TrackElement::GetBounds() const
{
    const IntVector3 elemCoords{ GetCoords() };
    const IntVector2 start{ elemCoords.x_, elemCoords.z_ };
    const IntVector3 step{ Track::RotatedStep(GetProfile().step_ - Track::RotatedStep(IntVector3::FORWARD, GetProfile().turn_), GetStepRotation()) };
    const IntVector2 flatStep{ step.x_, step.z_ };
    const IntVector2 tip{ start + flatStep };

    return { VectorMin(start, tip), VectorMax(start, tip) + IntVector2::ONE };
}

void TrackElement::SetStepRotation(int rotation)
{
    while (rotation < 0)
        rotation += 4;

    rotation = rotation % 4;

    //    if (rotation_ == rotation)
    //        return;

    rotation_ = rotation;
    node_->SetRotation(GetQuaternion());
}

Quaternion TrackElement::GetQuaternion(bool tip) const
{
    return World::StepRotationToQuaternion(rotation_ + tip * profile_.turn_);
}

void TrackElement::RecalculateLength()
{
    length_ = 0.f;

    if (profile_.step_ == IntVector3::ZERO)
        return;

    curve_ = Track::PathFromProfile(profile_, 64);
    for (unsigned p{ 0 }; p < curve_.Size() - 1; ++p)
        length_ += curve_.Position(p).DistanceToPoint(curve_.Position(p + 1));
}

bool TrackElement::Fits(TrackElement* elemB)
{
    if (coords_ + GetRotatedStep() != elemB->GetCoords())
        return false;

    const TrackProfile& profileA{ GetProfile() };
    const TrackProfile& profileB{ elemB->GetProfile() };

    return Abs(profileB.bank_.first_) % 16 == Abs(profileA.bank_.second_) % 16
             && profileB.slope_.first_ == profileA.slope_.second_
             && elemB->GetStepRotation() == (GetStepRotation() + profileA.turn_ + 4) % 4;
}
