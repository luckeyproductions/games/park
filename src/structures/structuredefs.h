/* LucKey Park
// Copyright (C) 2024 LucKey Productions (https://luckey.games)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef STRUCTUREDEFS_H
#define STRUCTUREDEFS_H

#include "../mastercontrol.h"

enum BuildType{ BUILD_NONE = 0, BUILD_PATH, BUILD_MONOLITH, BUILD_BLOCKS, BUILD_TRACK, BUILD_ELEMENT };
enum StructureCategory{ STRUCT_UNDEFINED = 0, STRUCT_TRANSPORT, STRUCT_MONOLITHIC, STRUCT_MODULAR, STRUCT_UTILITY, STRUCT_RESEARCH, STRUCT_ALL };

#define JSON_INTVEC2(val) IntVector2{ val.At(0).GetInt(), val.At(1).GetInt() }
#define JSON_INTVEC3(val) IntVector3{ val.At(0).GetInt(), val.At(1).GetInt(), val.At(2).GetInt() }

struct StructureInfo
{
    StructureInfo():
        identifier_{ StringHash::ZERO },
        name_{ "Structure" },
        category_{ STRUCT_UNDEFINED },
        buildType_{ BUILD_NONE },
        needs_{},
        class_{ StringHash::ZERO },
        model_{ "Models/Cylinder.mdl" },
        buildSound_{ "Samples/ChippingHammer.wav" },
        size_( IntVector3::ONE ),
        minSize_{ IntVector3::ZERO },
        maxSize_{ IntVector3::ZERO },
        subGrid_{ false },
        cost_{ 0 }
    {}

    bool LoadJSON(const JSONValue& value)
    {
        for (auto i{ value.begin() }; i != value.end(); ++i)
        {
            const String name{ i->first_ };
            const JSONArray& array{ i->second_.GetArray() };

            if (name == "name")
                name_ = i->second_.GetString();
            else if (name == "class")
                class_ = i->second_.GetString();
            else if (name == "model")
                model_ = i->second_.GetString();
            else if (name == "sound")
                buildSound_ = i->second_.GetString();
            else if (name == "size")
            {
                if (array.Size() == 3)
                    size_ = JSON_INTVEC3(array);
                else if (array.Size() == 2)
                {
                    const IntVector2 flatSize{ JSON_INTVEC2(array) };
                    size_ = { flatSize.x_, 1, flatSize.y_ };
                }
            }
            else if (name == "maxsize")
            {
                if (array.Size() == 3)
                    maxSize_ = JSON_INTVEC3(array);
                else if (array.Size() == 2)
                {
                    const IntVector2 flatMaxSize{ JSON_INTVEC2(array) };
                    maxSize_ = { flatMaxSize.x_, 0, flatMaxSize.y_ };
                }
            }
            else if (name == "subgrid")
                subGrid_ = i->second_.GetBool();
            else if (name == "needs")
                needs_ = { "True" };
            // Attributes
            else if (name == "tracks")
                attributes_[name] = i->second_.GetString();
        }

        minSize_ = size_;
        maxSize_ = VectorMax(maxSize_, minSize_);

        return true;
    }

    StringHash identifier_;
    String name_;
    StructureCategory category_;
    BuildType buildType_;
    Vector<StringHash> needs_;
    StringHash class_;
    String model_;
    String buildSound_;
    IntVector3 size_;
    IntVector3 minSize_;
    IntVector3 maxSize_;
    bool subGrid_;
    unsigned cost_;
    VariantMap attributes_;
};

#endif // STRUCTUREDEFS_H
