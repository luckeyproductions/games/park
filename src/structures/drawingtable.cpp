/* LucKey Park
// Copyright (C) 2024 LucKey Productions (https://luckey.games)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "coaster.h"
#include "utility.h"
#include "ferriswheel.h"

#include "drawingtable.h"

void DrawingTable::RegisterObject(Context* context)
{
    context->RegisterSubsystem<DrawingTable>();
    context->RegisterFactory<Structure>();
    context->RegisterFactory<Utility>();
    context->RegisterFactory<FerrisWheel>();
    Coaster::RegisterObject(context);
}

DrawingTable::DrawingTable(Context* context): Object(context),
    designs_{}/*,
          templates_{}*/
{
    LoadBlueprints(RES(JSONFile, "JSON/Blueprints.json"));
}

void DrawingTable::LoadBlueprints(JSONFile* file)
{
    const JSONValue& root{ file->GetRoot() };
    JSONValue categoryValue{ JSONValue::EMPTY };
    for (int c{ STRUCT_UNDEFINED + 1 }; c < STRUCT_ALL; ++c)
    {
        const StructureCategory category{ static_cast<StructureCategory>(c) };

        switch (category) {
        default:                categoryValue = JSONValue::EMPTY;       continue;
        case STRUCT_TRANSPORT:  categoryValue = root.Get("transport");  break;
        case STRUCT_MONOLITHIC: categoryValue = root.Get("monolithic"); break;
        case STRUCT_MODULAR:    categoryValue = root.Get("modular");    break;
        case STRUCT_UTILITY:    categoryValue = root.Get("utility");    break;
        case STRUCT_RESEARCH:   categoryValue = root.Get("research");   break;
        }

        for (auto i{ categoryValue.begin() }; i != categoryValue.end(); ++i)
        {
            StructureInfo design{};
            design.identifier_ = i->first_;
            design.category_ = category;
            design.LoadJSON(i->second_);

            if (design.class_ == "Coaster")
                design.buildType_ = BUILD_TRACK;
            else if (design.class_ == "Path")
                design.buildType_ = BUILD_PATH;
            else
            {
                if (design.class_ == StringHash::ZERO)
                    design.class_ = "Structure";
                design.buildType_ = BUILD_MONOLITH;
            }

            designs_[design.identifier_] = design;
        }
    }
}

Vector<StructureInfo> DrawingTable::GetDesigns(StructureCategory category)
{
    if (category == STRUCT_UNDEFINED)
        return designs_.Values();

    Vector<StructureInfo> categoryMatch{};
    for (const StructureInfo& d: designs_.Values())
    {
        if (d.category_ == category)
            categoryMatch.Push(d);
    }

    return categoryMatch;
}
