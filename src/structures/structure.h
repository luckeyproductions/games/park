/* LucKey Park
// Copyright (C) 2024 LucKey Productions (https://luckey.games)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef STRUCTURE_H
#define STRUCTURE_H

#include "structuredefs.h"

class Structure: public Component
{
    DRY_OBJECT(Structure, Component);

public:
    static Vector3 CenterOffset(const IntVector3& size);
    static void ResetNextID() { nextId_ = 1u; }

    friend class Estate;
    using Component::SetNode;
    Structure(Context* context);

    bool SaveJSON(JSONValue& dest) const override;
    bool LoadJSON(const JSONValue& source) override;

    void SetInfo(const StructureInfo& info) { info_ = info; }
    const StructureInfo& GetInfo() const { return info_; }

    void SetCoords(const IntVector3& coords)
    {
        coords_ = coords;

        UpdatePosition();
    }

    IntVector3 GetCoords() const
    {
        return coords_;
    }

    virtual IntRect GetCoordBounds() const
    {
        /// Needs work; rotation etc.
        const IntVector2 flatSize{ info_.size_.x_, info_.size_.z_ };
        const IntVector2 offset{ -(flatSize - IntVector2::ONE) / 2 };
        return LocalToGlobal({ offset, offset + flatSize });
    }

    virtual HashSet<IntVector2> GetOccupied() const
    {
        HashSet<IntVector2> occupied{};
        IntRect bounds{ GetCoordBounds() }; /// Expect global
        for (int x{ bounds.Min().x_ }; x < bounds.Max().x_; ++x)
        for (int y{ bounds.Min().y_ }; y < bounds.Max().y_; ++y)
            occupied.Insert({ x, y });

        return occupied;
    }

    Quaternion GetQuaternion() const;

    char GetStepRotation() const { return rotation_; }
    void SetStepRotation(char rotation)
    {
        if (rotation_ == rotation)
            return;

        rotation_ = rotation;

        if (node_)
            node_->SetRotation(GetQuaternion());

    }
    void Rotate(bool clockwise = true)
    {
        if (clockwise)
        {
            rotation_ = ++rotation_ % 4;
        }
        else
        {
            if (rotation_ != 0)
                --rotation_;
            else
                rotation_ = 3;
        }
    }

    SharedPtr<Structure> Copy() const
    {
        SharedPtr<Structure> copy{ static_cast<Structure*>(context_->CreateObject(GetTypeInfo()->GetType()).Get()) };
        copy->info_ = info_;
        copy->customName_ = customName_;
        copy->coords_ = coords_;
        copy->rotation_ = rotation_;

        return copy;
    }

    void DrawBounds(const Color& color = Color::CYAN, bool laserFence = true) const;
    IntVector2 LocalToGlobal(const IntVector2& coords) const
    {
        IntVector3 globalized{ LocalToGlobal({ coords.x_, 0, coords.y_ }) };
        return { globalized.x_, globalized.z_ };
    }
    IntVector3 LocalToGlobal(const IntVector3& coords) const
    {
        return VectorRoundToInt(GetQuaternion() * coords) + coords_;
    }

    void PlayBuildSound(int tone = 0);

    void Collapse();

protected:
    IntRect LocalToGlobal(const IntRect& rect) const;
    void OnNodeSet(Node* node) override;
    virtual void Construct();

    StructureInfo info_;
    static unsigned nextId_;
    unsigned id_;

private:
    void UpdatePosition();

    String customName_;
    char rotation_;
    IntVector3 coords_;
};

class Estate: public Object
{
    DRY_OBJECT(Estate, Object);

public:
    Estate(Context* context);

    bool SaveJSON(JSONValue& dest) const;
    bool LoadJSON(const JSONValue& source);

    Structure* Construct(const StructureInfo& cyanotype);
    void CancelConstruction(Structure* site);
    bool Demolish(Structure* structure);

    Vector<SharedPtr<Structure> > GetStructures() const { return structures_; }

    void Clear();

private:
    Vector<SharedPtr<Structure> > structures_;
    void HandleSank(StringHash /*eventType*/, VariantMap& /*eventData*/);
    // land_;
};

#endif // STRUCTURE_H
