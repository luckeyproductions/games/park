/* LucKey Park
// Copyright (C) 2024 LucKey Productions (https://luckey.games)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "mastercontrol.h"

#include "inputmaster.h"
#include "effectmaster.h"
#include "ui/main/settingsmenu.h"
#include "people/animatron.h"
#include "cyberplasm.h"
#include "materials.h"
#include "game.h"
#include "jib.h"

DRY_DEFINE_APPLICATION_MAIN(MasterControl);

MasterControl::MasterControl(Context* context): Application(context),
    storagePath_{}
{
}

void MasterControl::Setup()
{
    FileSystem* fs{ GetSubsystem<FileSystem>() };

    engineParameters_[EP_LOG_NAME] = fs->GetAppPreferencesDir("luckey", "logs") + "park.log";
    engineParameters_[EP_WINDOW_TITLE] = "LucKey Park";
    engineParameters_[EP_WINDOW_ICON] = "icon.png";

    FindResourcePath();

    SharedPtr<JSONFile> settingsFile{ context_->CreateObject<JSONFile>() };

    if (fs->FileExists(GetSettingsPath()) && settingsFile->LoadFile(GetSettingsPath()))
    {
        const JSONValue& root{ settingsFile->GetRoot() };
        const JSONArray& resolution{ root.Get("Resolution").GetArray() };
        const bool fullscreen{ root.Get("Fullscreen").GetBool() };

        engineParameters_[EP_WINDOW_WIDTH]  = resolution.At(0).GetInt();
        engineParameters_[EP_WINDOW_HEIGHT] = resolution.At(1).GetInt();
        engineParameters_[EP_FULL_SCREEN] = fullscreen;
        engineParameters_[EP_WINDOW_RESIZABLE] = !fullscreen;
        engineParameters_[EP_VSYNC] = root.Get("V-Sync").GetBool();

        Audio* audio{ GetSubsystem<Audio>() };
        audio->SetMasterGain(SOUND_MUSIC, root.Get("Music").GetBool() ? 1.f : 0.f );
    }
    else
    {
        engineParameters_[EP_VSYNC] = true;

//        engineParameters_[EP_WINDOW_WIDTH]  = 1440;
//        engineParameters_[EP_WINDOW_HEIGHT] =  810;
//        engineParameters_[EP_FULL_SCREEN] = false;
//        engineParameters_[EP_WINDOW_RESIZABLE] = true;
    }
}

void MasterControl::FindResourcePath()
{
    FileSystem* fs{ GetSubsystem<FileSystem>() };

    String resourcePath{ "Resources" };
    if (!fs->DirExists(AddTrailingSlash(fs->GetProgramDir()) + resourcePath))
    {
        const String installedResources{ RemoveTrailingSlash(RESOURCEPATH) };
        if (fs->DirExists(installedResources))
            resourcePath = installedResources;
    }

    if (resourcePath == "Resources")
        storagePath_ = resourcePath;
    else
        storagePath_ = RemoveTrailingSlash(fs->GetAppPreferencesDir("luckey", "luckeypark"));

    engineParameters_[EP_RESOURCE_PATHS] = resourcePath;
}

void MasterControl::Start()
{
    SubscribeToEvent(E_SANK, DRY_HANDLER(MasterControl, HandleSank));
    SetRandomSeed(GetSubsystem<Time>()->GetSystemTime());
    ConfigureDefaultZoneAndRenderPath();

    context_->RegisterFactory<Jib>();
    context_->RegisterFactory<Animatron>();

    context_->RegisterSubsystem(this);
    context_->RegisterSubsystem<InputMaster>();
    context_->RegisterSubsystem<EffectMaster>();
    context_->RegisterSubsystem<Cyberplasm::Vat>();
    context_->RegisterSubsystem<Materials>();
    context_->RegisterSubsystem<Game>()->Load("/.menu.json");
;

//    SubscribeToEvent(E_UPDATE, DRY_HANDLER(MasterControl, HandleUpdate)); /// Debugging
}

void MasterControl::ConfigureDefaultZoneAndRenderPath()
{
    Zone* zone{ GetSubsystem<Renderer>()->GetDefaultZone() };
    zone->SetAmbientColor(Color::WHITE.Lerp(Color::CYAN, .125f) * .9f);
    zone->SetFogColor(COLOR_ZOMP);
    zone->SetFogStart(64.f);
    zone->SetFogEnd(320.f);

    RenderPath* renderPath{ GetSubsystem<Renderer>()->GetDefaultRenderPath() };
    renderPath->Load(RES(XMLFile, "RenderPaths/Deferred.xml"));
    renderPath->Append(RES(XMLFile, "PostProcess/FXAA3.xml"));
    renderPath->Append(RES(XMLFile, "PostProcess/BloomHDR.xml"));
    renderPath->SetShaderParameter("BloomHDRThreshold", 0.9f);
    renderPath->SetShaderParameter("BloomHDRMix", Vector2{ 0.95f, .75f });
    renderPath->SetShaderParameter("BloomHDRBlurRadius", 16.f);
//    renderPath->SetShaderParameter("BloomHDRBlurSigma", 1.5f);

    FileSystem* fs{ GetSubsystem<FileSystem>() };
    SharedPtr<JSONFile> settingsFile{ context_->CreateObject<JSONFile>() };
    const String settingsPath{ GetSettingsPath() };
    if (fs->FileExists(settingsPath) && settingsFile->LoadFile(settingsPath))
    {
        const JSONValue& root{ settingsFile->GetRoot() };
        bool antiAliasing{ root.Get("Anti-aliasing").GetBool() };
        bool bloom{ root.Get("Bloom").GetBool() };

        renderPath->SetEnabled("FXAA3", antiAliasing);
        renderPath->SetEnabled("BloomHDR", bloom);
    }
    else
    {
        renderPath->SetEnabled("FXAA3", false);
        renderPath->SetEnabled("BloomHDR", false);
    }
}


#include "world.h"
void MasterControl::HandleUpdate(StringHash /*eventType*/, VariantMap& /*eventData*/)
{
    Input* input{ GetSubsystem<Input>() };
    GetSubsystem<World>()->GetScene()->SetTimeScale(1.f + 7.f * (input->GetKeyDown(KEY_2) && input->GetQualifierDown(QUAL_SHIFT)));
}

void MasterControl::Stop()
{
    SaveSettings();
}

void MasterControl::SaveSettings() const
{
    SettingsMenu*  settingsMenu{ GetSubsystem<SettingsMenu>() };

    SharedPtr<JSONFile> settingsFile{ context_->CreateObject<JSONFile>() };
    JSONValue& root{ settingsFile->GetRoot() };

    Graphics* graphics{ GetSubsystem<Graphics>() };
    root.Set("Resolution", JSONArray{ graphics->GetWidth(), graphics->GetHeight() });
    root.Set("Restore", JSONArray{ settingsMenu->GetRestoreWidth(), settingsMenu->GetRestoreHeight() });
    root.Set("Fullscreen", graphics->GetFullscreen());
    root.Set("V-Sync",     graphics->GetVSync());

    RenderPath* renderPath{ GetSubsystem<Renderer>()->GetDefaultRenderPath() };
    root.Set("Anti-aliasing", renderPath->IsEnabled("FXAA3"));
    root.Set("Bloom", renderPath->IsEnabled("BloomHDR"));

    Audio* audio{ GetSubsystem<Audio>() };
    root.Set("Music", audio->GetMasterGain(SOUND_MUSIC) != 0.f);

    settingsFile->SaveFile(GetSettingsPath());
}
