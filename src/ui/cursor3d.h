/* LucKey Park
// Copyright (C) 2024 LucKey Productions (https://luckey.games)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef CURSOR3D_H
#define CURSOR3D_H

#include "../mastercontrol.h"

enum SnapMode{ SNAP_NONE = 0, SNAP_CENTER, SNAP_CORNER, SNAP_SUBGRID };
enum DragMode{ DM_NONE = 0, DM_LINE, DM_PLANE };

class Tool;

DRY_EVENT(E_TOOLCHANGED, ToolChanged) {}
DRY_EVENT(E_SCENECURSORSTEP, SceneCursorStep) {}

class Cursor3D: public LogicComponent
{
    DRY_OBJECT(Cursor3D, LogicComponent);

public:
    Cursor3D(Context* context);

    void FreeMove(const Ray& ray);
    void Step(const IntVector3& step = IntVector3::ZERO, bool updateMouse = true);
    IntVector3 GetCoords() const { return mouseWorldCoords_; }
    IntVector2 GetFlatCoords() const { return { mouseWorldCoords_.x_, mouseWorldCoords_.z_ }; }

    void SetSnapMode(SnapMode snap);
    void SetDragMode(DragMode drag, const Vector3& axis = Vector3::UP)
    {
        dragMode_ = drag;
        dragAxis_ = axis;
    }
    void SetDragAxis(const Vector3 axis) { dragAxis_ = axis; }
    void SetCoords(const IntVector3& coords, bool updateCursor = true);

    SnapMode GetSnapMode() const { return snapMode_; }
    DragMode GetDragMode() const { return dragMode_; }

    void SetTool(Tool* tool);
    Tool* GetTool() const { return hand_; }

    IntVector2 GetClickScreenPos() const { return clickScreenPos_; }
    Vector3 GetClickWorldPos() const { return clickWorldPos_; }

    const RayQueryResult& GetFreeMoveResult() const { return freeMoveResult_; }

    float ClickDelta() const
    {
        return (GetSubsystem<UI>()->GetCursorPosition() - GetClickScreenPos()).Length();
    }

    Vector3 DragDelta(const Ray& ray) const
    {
        if (dragMode_ == DM_PLANE)
        {
            const Plane hitPlane{ dragAxis_, clickWorldPos_ };
            const float hitDistance{ ray.HitDistance(hitPlane) };

            if (hitDistance == M_INFINITY)
                return Vector3::ONE * M_INFINITY;

            const Vector3 hitPos{ ray.origin_ + ray.direction_ * hitDistance };
            return hitPos - clickWorldPos_;
        }

        if (dragMode_ == DM_LINE)
        {
            const Ray axisRay{ clickWorldPos_, dragAxis_ };
            return axisRay.ClosestPoint(ray) - clickWorldPos_;
        }

        return Vector3::ZERO;
    }

    bool Dragging() const { return dragMode_ != DM_NONE; }
    bool Clicked() const { return clickWorldPos_.x_ != M_INFINITY; }
    bool IsBehindUI() const;
    void Unclick();

    void OnSetEnabled() override;
    void Start() override;
    void DelayedStart() override;
    void Stop() override;

    void Update(float timeStep) override;
    void PostUpdate(float timeStep) override;
    void FixedUpdate(float timeStep) override;
    void FixedPostUpdate(float timeStep) override;

    void DrawDebugGeometry(DebugRenderer* debug, bool depthTest) override;
    void MouseMoved();

protected:
    void OnNodeSet(Node* node) override;
    void OnSceneSet(Scene* scene) override;
    void OnNodeSetEnabled(Node* node) override;

private:
    void UpdateMouseCursor();
    void HandleMouseMove(StringHash eventType, VariantMap& eventData);
    void HandleMouseButtonDown(StringHash eventType, VariantMap& eventData);
    void HandleMouseButtonUp(StringHash eventType, VariantMap& eventData);

    IntVector3 mouseWorldCoords_;
    Vector3 mouseWorldPos_;
    Vector3 clickWorldPos_;
    IntVector2 clickScreenPos_;
    Vector3 dragAxis_;

    SnapMode snapMode_;
    DragMode dragMode_;
    Tool* hand_;
    RayQueryResult freeMoveResult_;
    bool behindUI_;
};

#endif // CURSOR3D_H
