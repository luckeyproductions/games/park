/* LucKey Park
// Copyright (C) 2024 LucKey Productions (https://luckey.games)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef INDICATOR_H
#define INDICATOR_H

#include "../mastercontrol.h"


class Indicator: public Component
{
    DRY_OBJECT(Indicator, Component);

public:
    Indicator(Context* context);
    void OnSetEnabled() override;

    void SetTarget(unsigned id) { target_ = id; }
    void SetHeight(float height) { height_ = height; }
    void SetScale(float scale) { scale_ = scale; }
    void SetAutoRemove(bool autoRemove) { autoRemove_ = autoRemove; }
    void Hide(bool sudden = false);

    bool Save(Serializer& dest) const override;
    bool SaveXML(XMLElement& dest) const override;
    bool SaveJSON(JSONValue& dest) const override;
    void MarkNetworkUpdate() override;
    void GetDependencyNodes(PODVector<Node*>& dest) override;
    void DrawDebugGeometry(DebugRenderer* debug, bool depthTest) override;

protected:
    void OnNodeSet(Node* node) override;
    void OnSceneSet(Scene* scene) override;
    void OnMarkedDirty(Node* node) override;
    void OnNodeSetEnabled(Node* node) override;

private:
    void HandlePostUpdate(StringHash eventType, VariantMap& eventData);

    static StaticModelGroup* arrowsModelGroup_;

    unsigned target_;
    float height_;
    float scale_;
    bool spin_;
    bool autoRemove_;
};

#endif // INDICATOR_H
