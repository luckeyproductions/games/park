/* LucKey Park
// Copyright (C) 2024 LucKey Productions (https://luckey.games)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "gui.h"

#include "../game.h"
#include "tools/tool.h"

UISizes GUI::sizes_{};

GUI::GUI(Context* context): Object(context)
{
    context_->RegisterFactory<MainMenu>();
    context_->RegisterFactory<Indicator>();
    context_->RegisterFactory<Cursor3D>();
    context_->RegisterFactory<Dash>();
    SettingsMenu::RegisterObject(context_);

    Input* input{ GetSubsystem<Input>() };
    input->SetMouseMode(MM_ABSOLUTE);

    UI* ui{ GetSubsystem<UI>() };
    UIElement* root{ ui->GetRoot() };

    Cursor* c{ root->CreateChild<Cursor>("Cursor") };
    ui->SetCursor(c);
    c->SetStyleAuto(RES(XMLFile, "UI/DefaultStyle.xml"));
    c->DefineShape(CS_NORMAL, RES(Image, "UI/Cursor.png"), { 0, 0, 192, 192 }, { 2, 3 });
    c->SetBlendMode(BLEND_ALPHA);
    c->SetPosition(GetSubsystem<Graphics>()->GetSize() / 2);

    settingsMenu_ = root->CreateChild<SettingsMenu>();
    context_->RegisterSubsystem(settingsMenu_);

    mainMenu_ = root->CreateChild<MainMenu>();
    dash_ = root->CreateChild<Dash>();
    HideDash();

    SubscribeToEvent(E_SCREENMODE, DRY_HANDLER(GUI, HandleScreenMode));
    SubscribeToEvent(GetSubsystem<UI>(), E_UIMOUSECLICKEND, DRY_HANDLER(GUI, HandleClickEnd));

    Scene* scene{ GetScene() };
    Node* cursorNode{ scene->CreateChild("Cursor") };
    cursor3d_ = cursorNode->CreateComponent<Cursor3D>();
    Tool::SetTooltip(GetRoot()->CreateChild<ToolText>());
}

void GUI::HandleAction(InputAction action)
{
    switch (action)
    {
    case IA_STEPLEFT:       StepLeft();  break;
    case IA_STEPRIGHT:      StepRight(); break;
    case IA_STEPUP:         break;
    case IA_STEPDOWN:       break;
    case IA_STEPFORWARD:    StepUp();break;
    case IA_STEPBACK:       StepDown();break;
    case IA_CONFIRM:        HandleConfirm(); break;
    case IA_CANCEL:         HandleCancel();  break;
    case IA_PREVIOUS:       PageUp();      break;
    case IA_NEXT:           PageDown();    break;
    case IA_MODE:           break; /// Switch focus to scene
    default: return;
    }
}

void GUI::Select(UIElement* element)
{
    UI* ui{ GetSubsystem<UI>() };
    ui->SetFocusElement(element);
    ui->GetCursor()->SetPosition(element->GetScreenPosition() + element->GetSize() * 4/5);
}

void GUI::SelectWithTag(const String& tag)
{
    PODVector<UIElement*> taggedElements{ GetSubsystem<UI>()->GetRoot()->GetChildrenWithTag(tag, true) };
    for (UIElement* tagged: taggedElements)
    {
        if (tagged->IsVisibleEffective())
        {
            Select(tagged);
            return;
        }
    }
}

void GUI::SelectNext(UIElement* from, const String& varName)
{
    if (!from)
        return;

    UIElement* next{ static_cast<UIElement*>(from->GetVar(varName).GetPtr()) };
    if (next && next->IsVisibleEffective())
        Select(next);
}

void GUI::SelectFirst()
{
    SelectWithTag("First");
}

void GUI::SelectLast()
{
    SelectWithTag("Last");
}

void GUI::StepLeft()
{
    UIElement* focus{ GetSubsystem<UI>()->GetFocusElement() };
    if (!focus)
    {
        SelectLast();
        return;
    }

    SelectNext(focus, "Left");
}

void GUI::StepRight()
{
    UIElement* focus{ GetSubsystem<UI>()->GetFocusElement() };
    if (!focus)
    {
        SelectFirst();
        return;
    }

    SelectNext(focus, "Right");
}

void GUI::StepUp()
{
    UIElement* focus{ GetSubsystem<UI>()->GetFocusElement() };
    if (!focus)
    {
        SelectLast();
        return;
    }

    SelectNext(focus, "Up");
}

void GUI::StepDown()
{
    UIElement* focus{ GetSubsystem<UI>()->GetFocusElement() };
    if (!focus)
    {
        SelectFirst();
        return;
    }

    SelectNext(focus, "Down");
}

void GUI::HandleConfirm()
{
    UI* ui{ GetSubsystem<UI>() };
//    ui->SetFocusElement(ui->GetElementAt(ui->GetCursorPosition()));
    UIElement* clickedElem{ ui->GetElementAt(ui->GetCursorPosition()) };

    ui->SetFocusElement(clickedElem);

    {
        VariantMap beginData{};
        VariantMap endData{};
        IntVector2 cursorPos{ ui->GetCursorPosition() };
        beginData[UIMouseClick::P_X]             = Variant{ cursorPos.x_ };
        beginData[UIMouseClick::P_Y]             = Variant{ cursorPos.y_ };
        beginData[UIMouseClick::P_ELEMENT]       = Variant{ clickedElem };
        beginData[UIMouseClick::P_BUTTON]        = Variant{ MOUSEB_LEFT };
        endData[UIMouseClickEnd::P_X]            = Variant{ cursorPos.x_ };
        endData[UIMouseClickEnd::P_Y]            = Variant{ cursorPos.y_ };
        endData[UIMouseClickEnd::P_ELEMENT]      = Variant{ clickedElem };
        endData[UIMouseClickEnd::P_BEGINELEMENT] = Variant{ static_cast<UIElement*>(nullptr) };
        endData[UIMouseClickEnd::P_BUTTON]       = Variant{ MOUSEB_LEFT };
        ui->SendEvent(E_UIMOUSECLICK, beginData);
        ui->SendEvent(E_UIMOUSECLICKEND, endData);
    }

    if (clickedElem)
    {
        VariantMap beginData{};
        VariantMap endData{};
        IntVector2 cursorPos{ ui->GetCursorPosition() };
        beginData[Click::P_X]             = Variant{ cursorPos.x_ };
        beginData[Click::P_Y]             = Variant{ cursorPos.y_ };
        beginData[Click::P_ELEMENT]       = Variant{ clickedElem };
        beginData[Click::P_BUTTON]        = Variant{ MOUSEB_LEFT };
        endData[ClickEnd::P_X]            = Variant{ cursorPos.x_ };
        endData[ClickEnd::P_Y]            = Variant{ cursorPos.y_ };
        endData[ClickEnd::P_ELEMENT]      = Variant{ clickedElem };
        endData[ClickEnd::P_BEGINELEMENT] = Variant{ clickedElem };
        endData[ClickEnd::P_BUTTON]       = Variant{ MOUSEB_LEFT };

        clickedElem->SendEvent(E_CLICK, beginData);
        clickedElem->SendEvent(E_CLICKEND, endData);
    }
}

void GUI::HandleCancel()
{
    if (!mainMenu_->IsVisible())
        ShowMainMenu();
    else if (settingsMenu_->IsVisible())
        HideSettingsMenu();
    else if (GetSubsystem<Game>()->GetStatus() != GS_MAIN)
        HideMainMenu();
}

void GUI::ShowMainMenu()
{
    mainMenu_->Show();

    Game* game{ GetSubsystem<Game>() };
    if (game->GetStatus() == GS_PLAY)
    {
        game->SetStatus(GS_MODAL);
        HideDash();
    }
}

void GUI::HideMainMenu()
{
    mainMenu_->SetVisible(false);

    Game* game{ GetSubsystem<Game>() };
    if (game->GetStatus() == GS_MODAL)
    {
        game->SetStatus(GS_PLAY);
        ShowDash();
    }
}

void GUI::HideSettingsMenu()
{
    settingsMenu_->Hide();

    if (GetSubsystem<Game>()->GetStatus() == GS_MAIN)
        ShowMainMenu();
    else
        HideMainMenu();
}

Indicator* GUI::AddIndicator(Node* node)
{
    Indicator* indicator{ node->GetScene()->CreateChild()->CreateComponent<Indicator>() };
    indicator->SetTarget(node->GetID());

    return indicator;
}

Text* GUI::CreateButtonText(UIElement* element, unsigned header)
{
    if (!element)
        return nullptr;

    Text* buttonText{ element->CreateChild<Text>() };
    buttonText->SetTags({ (header == 0 ? "p" : String{"h"}.Append(String{ header })) });
    buttonText->SetAlignment(HA_CENTER, VA_CENTER);
    buttonText->SetOpacity(.9f);

    // All headers
    buttonText->SetFont(element->RES(Font, "Fonts/Pontiff.otf"));
    buttonText->SetFontSize(GUI::sizes_.text_[header]);
    buttonText->SetTextEffect(TE_SHADOW);
    buttonText->SetEffectShadowOffset({ 0, (header == 0 ? 1 : GUI::sizes_.text_[0] / 4) });
    buttonText->SetEffectColor({ Color::BLACK, .4f - (header == 0) * .2f });

    return buttonText;
}

void GUI::UpdateSizes()
{
    UISizes defaultSizes{};

    Graphics* graphics{ GetSubsystem<Graphics>() };
    sizes_.screen_ = graphics->GetSize();
    sizes_.margin_ = Max(1, Floor(VMin(defaultSizes.margin_ * .125f)));
    sizes_.logo_ = Min(Min(VMin(90), 1024), VH(55));
    sizes_.centralColumnWidth_ = Min(320 + VW(10), VW(80));
    sizes_.toolButton_ = RoundToInt(Min(GUI::VH(11), GUI::VW(22)));

    for (int t{ 0 }; t < 7; ++t)
        sizes_.text_[t] = Round(Lerp(defaultSizes.text_[t] * 1.f,
                                VMin(defaultSizes.text_[t] * .15f), .75f));


    Cursor* c{ GetRoot()->GetChildStaticCast<Cursor>("Cursor", false) };
    if (c)
    {
        const float cursorScale{ sizes_.screen_.y_ / 1080.f };
        c->SetSize(IntVector2::ONE * (cursorScale > 1.f ? 96 * RoundToInt(log2(cursorScale + 1))
                                                        : Max(8, 96 / RoundToInt(log2(1 / cursorScale) + 1))));
    }

    for (int i{ 0 }; i <= 6; ++i)
        for (UIElement* elem: GetRoot()->GetChildrenWithTag(i != 0 ? "h" + String{ i } : "p", true))
            static_cast<Text*>(elem)->SetFontSize(sizes_.text_[i]);

    SendEvent(E_UISIZESCHANGED);

    if (!GetSubsystem<UI>()->GetFocusElement())
        SelectFirst();
}

void GUI::HandleScreenMode(StringHash /*eventType*/, VariantMap& /*eventData*/)
{
    UpdateSizes();
}

void GUI::HandleClickEnd(StringHash /*eventType*/, VariantMap& eventData)
{
    CheckBox* box{ static_cast<CheckBox*>(eventData[UIMouseClickEnd::P_ELEMENT].GetPtr()) };
    if (box && eventData[UIMouseClickEnd::P_BEGINELEMENT].GetPtr() == nullptr)
        box->SetChecked(!box->IsChecked());
}

