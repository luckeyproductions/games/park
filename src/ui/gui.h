/* LucKey Park
// Copyright (C) 2024 LucKey Productions (https://luckey.games)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef GUI_H
#define GUI_H

#include "../inputmaster.h"
#include "main/mainmenu.h"
#include "main/settingsmenu.h"
#include "dash/dash.h"
#include "cursor3d.h"
#include "../world.h"

#include "indicator.h"

struct UISizes {
    IntVector2 screen_{};
    int margin_{ 4 };
    int text_[7] = { 12, 38, 34, 23, 17, 16, 14 };
    int logo_{ 512 };
    int centralColumnWidth_{ 512 };
    int toolButton_{ 64 };

    bool operator != (UISizes& rhs)
    {
        if (logo_ != rhs.logo_
         || centralColumnWidth_ != rhs.centralColumnWidth_
         || margin_ != rhs.margin_)
            return true;

        for (int t{ 0 }; t <= 6; ++t)
        {
            if (text_[t] != rhs.text_[t])
                return true;
        }

        return false;
    }
};

class GUI: public Object
{
    DRY_OBJECT(GUI, Object)

public:
    static UISizes sizes_;
    static float VW(float c = 1.f) { return c * sizes_.screen_.x_ * .01f; }
    static float VH(float c = 1.f) { return c * sizes_.screen_.y_ * .01f; }
    static float VMin(float c = 1.f) { return c * Min(VW(), VH()); }
    static float VMax(float c = 1.f) { return c * Max(VW(), VH()); }
    static float ScreenRatio() { return VW() / VH(); }
    static bool Portrait() { return ScreenRatio() < .8f; }

    GUI(Context* context);

    void HandleAction(InputAction action);
    void ShowMainMenu(/*bool home = true*/);
    void HideMainMenu();
    void ShowSettingsMenu() { settingsMenu_->Show(); }
    void HideSettingsMenu();
    void ShowDash() { dash_->SetVisible(true); }
    void HideDash() { dash_->SetVisible(false); }

    void PageUp()
    {
        if (settingsMenu_->IsVisible())
            settingsMenu_->PreviousTab();
    }

    void PageDown()
    {
        if (settingsMenu_->IsVisible())
            settingsMenu_->NextTab();
    }

    Indicator* AddIndicator(Node* node);

    UIElement* GetRoot() const { return GetSubsystem<UI>()->GetRoot(); }
    Cursor3D* GetSceneCursor() const { return cursor3d_; }
    Dash* GetDash() const { return dash_; }
    SettingsMenu* GetSettingsMenu() const { return settingsMenu_; }

    Text* CreateButtonText(UIElement* element, unsigned header = 0);

    void UpdateSizes();

private:
    void StepLeft();
    void StepRight();
    void StepUp();
    void StepDown();
    void HandleConfirm();
    void HandleCancel();

    void Select(UIElement* element);
    void SelectNext(UIElement* from, const String& varName);

    void SelectWithTag(const String& tag);
    void SelectFirst();
    void SelectLast();

    Scene* GetScene() const { return GetSubsystem<World>()->GetScene(); }
    void HandleScreenMode(StringHash eventType, VariantMap& eventData);
    void HandleClickEnd(StringHash eventType, VariantMap& eventData);

    MainMenu* mainMenu_;
    SettingsMenu* settingsMenu_;
    Dash* dash_;
    Cursor3D* cursor3d_;
};

DRY_EVENT(E_UISIZESCHANGED, UISizeChanged)
{
}

#endif // GUI_H
