/* LucKey Park
// Copyright (C) 2024 LucKey Productions (https://luckey.games)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "gui.h"
#include "../landscape/ground.h"
#include "tools/tool.h"

#include "cursor3d.h"

Cursor3D::Cursor3D(Context* context): LogicComponent(context),
    mouseWorldCoords_{ IntVector3::ONE * M_MAX_INT },
    mouseWorldPos_{ Vector3::ONE * M_INFINITY },
    clickWorldPos_{ Vector3::ONE * M_INFINITY },
    clickScreenPos_{ IntVector2::ONE * M_MAX_INT },
    dragAxis_{ Vector3::ONE * M_INFINITY },
    snapMode_{ SNAP_NONE },
    dragMode_{ DM_NONE },
    hand_{ nullptr },
    behindUI_{ false }
{
}

void Cursor3D::Step(const IntVector3& step, bool updateMouse)
{
    if (step == IntVector3::ZERO)
        return;

    Jib* jib{ GetSubsystem<Jib>() };
    World* world{ GetSubsystem<World>() };
    mouseWorldCoords_ += step;

    switch (snapMode_)
    {
    default: case SNAP_NONE:
        FreeMove(jib->GetScreenRay(VectorRoundToInt(jib->GetActiveCamera()->WorldToScreenPos(world->CoordinatesToWorldPosition(mouseWorldCoords_)) * GetSubsystem<Graphics>()->GetSize())));
    break;
    case SNAP_CENTER:
        node_->SetWorldPosition(world->CoordinatesToWorldPosition(mouseWorldCoords_));
    break;
    case SNAP_CORNER:
        node_->SetWorldPosition(world->CoordinatesToWorldPosition(mouseWorldCoords_)
                                - GRID_SQUARE * .5f);
    break;
    case SNAP_SUBGRID:
//        node_->SetWorldPosition(mouseWorldPos_ + step * Vector3{ SUBGRID, HEIGHTSTEP, SUBGRID });
    break;
    }

    if (updateMouse)
        UpdateMouseCursor();

    SendEvent(E_SCENECURSORSTEP);

    if (hand_ != nullptr)
        hand_->HandleCursorStep(step);
}

void Cursor3D::OnNodeSet(Node* node)
{
    if (!node)
        return;

    SubscribeToEvent(E_MOUSEMOVE, DRY_HANDLER(Cursor3D, HandleMouseMove));
    SubscribeToEvent(E_MOUSEBUTTONDOWN, DRY_HANDLER(Cursor3D, HandleMouseButtonDown));
    SubscribeToEvent(E_MOUSEBUTTONUP, DRY_HANDLER(Cursor3D, HandleMouseButtonUp));

    FreeMove(GetSubsystem<Jib>()->GetScreenRay(GetSubsystem<UI>()->GetCursorPosition()));
}

void Cursor3D::SetSnapMode(SnapMode snap)
{
    if (snapMode_ == snap)
        return;

    snapMode_ = snap;

    MouseMoved();
}

void Cursor3D::SetCoords(const IntVector3& coords, bool updateCursor)
{
    Step(coords - mouseWorldCoords_, updateCursor);
}

void Cursor3D::SetTool(Tool* tool)
{
    if (hand_ == tool)
        return;

    if (hand_)
    {
        hand_->Cancel();
        hand_->EndDelegation();
        hand_->UnsubscribeFromAllEvents();
    }

    hand_ = tool;
    GetSubsystem<GUI>()->GetDash()->SetToolInfoVisible(false);

    if (hand_ == nullptr)
    {
        Tool::ClearPreview();
        SetSnapMode(SNAP_NONE);
    }
    else
    {
        SetSnapMode(hand_->GetSnapMode());
    }

    SendEvent(E_TOOLCHANGED);
}

bool Cursor3D::IsBehindUI() const
{
    UI* ui{ GetSubsystem<UI>() };
    IntVector2 mousePos{ ui->GetCursorPosition() };
    return ui->GetElementAt(mousePos, false) != nullptr;
}

void Cursor3D::Unclick()
{
    if (hand_)
        hand_->HandleClickEnd();

    clickScreenPos_ = IntVector2::ONE * M_MAX_INT;
    clickWorldPos_ = dragAxis_ = Vector3::ONE * M_INFINITY;
    dragMode_ = DM_NONE;
}

void Cursor3D::UpdateMouseCursor()
{
    GetSubsystem<UI>()->GetCursor()->SetPosition(VectorRoundToInt(
    GetSubsystem<Graphics>()->GetSize() * GetSubsystem<Jib>()->GetActiveCamera()->WorldToScreenPos(node_->GetWorldPosition())));
}

void Cursor3D::HandleMouseMove(StringHash /*eventType*/, VariantMap& eventData)
{
    MouseMoved();
}

void Cursor3D::MouseMoved()
{
    Ray ray{ GetSubsystem<Jib>()->GetScreenRay(GetSubsystem<UI>()->GetCursorPosition()) };

    if (hand_ && Clicked())
    {
        hand_->HandleDrag(ray);
    }
    else
    {
        FreeMove(ray);
    }

    if (behindUI_ != IsBehindUI())
    {
        behindUI_ = IsBehindUI();
        if (hand_)
        {
            hand_->HandleMouseMoved();

            if (!behindUI_)
                hand_->HandleCursorStep(IntVector3::ZERO);
        }
    }
}

void Cursor3D::FreeMove(const Ray& ray)
{
    Octree* octree{ GetScene()->GetComponent<Octree>() };
    PODVector<RayQueryResult> result{};
    const unsigned viewMask{ (hand_ ? hand_->GetViewMask() : (M_MAX_UNSIGNED - LAYER(LAYER_UI))) };
    RayOctreeQuery query{ result, ray, RAY_TRIANGLE, M_INFINITY, DRAWABLE_GEOMETRY, viewMask };
    octree->RaycastSingle(query);
    const IntVector3 oldCoords{ mouseWorldCoords_ };

    if (query.result_.Size())
    {
        freeMoveResult_ = query.result_.Front();
        mouseWorldPos_ = freeMoveResult_.position_;
    }
    else
    {
        const float toPlane{ ray.HitDistance(Plane{ Vector3::UP, Vector3::ZERO }) };
        if (toPlane < M_INFINITY)
            mouseWorldPos_ = ray.origin_ + ray.direction_ * toPlane;
    }

    World* world{ GetSubsystem<World>() };

    const bool sub{ snapMode_ == SNAP_SUBGRID };
    const Vector3 gridBlock{ (sub ? SUBGRID_BLOCK : GRID_BLOCK) };
    const HeightMap& heightMap{ world->GetGround()->GetHeightMap() };

    switch (snapMode_)
    {
    case SNAP_NONE: default:
    {
        node_->SetWorldPosition(mouseWorldPos_);
        mouseWorldCoords_ = world->WorldPositionToCoordinates(mouseWorldPos_);
    }
    break;
    case SNAP_CENTER: case SNAP_SUBGRID:
    {
        const IntVector3 centerCoords{ VectorRoundToInt((mouseWorldPos_ - world->GetOffset()) / gridBlock) };
        const IntVector2 flatCoords{ centerCoords.x_, centerCoords.z_ };
        const HeightProfile profile{ heightMap.GetHeightProfileAt(flatCoords) };
        const int elevation{ profile.Max() };

        mouseWorldCoords_ = { flatCoords.x_, elevation, flatCoords.y_ };
        const Vector3 nearestCenter{ world->CoordinatesToWorldPosition(mouseWorldCoords_, sub) };
        node_->SetWorldPosition(nearestCenter);
    }
    break;
    case SNAP_CORNER:
    {
        const Vector3 halfGrid{ GRID * .5f, 0.f, GRID * .5f };
        const IntVector3 cornerCoords{ VectorRoundToInt((mouseWorldPos_ + halfGrid - world->GetOffset()) / gridBlock) };
        const IntVector2 flatCoords{ cornerCoords.x_, cornerCoords.z_ };
        const int elevation{ heightMap.GetHeightAt(flatCoords) };

        mouseWorldCoords_ = { flatCoords.x_, elevation, flatCoords.y_ };
        const Vector3 nearestCorner{ world->CoordinatesToWorldPosition(mouseWorldCoords_) - halfGrid};
        node_->SetWorldPosition(nearestCorner);
    }
    break;
    }

    if (mouseWorldCoords_ != oldCoords)
    {
        SendEvent(E_SCENECURSORSTEP);

        if (hand_)
            hand_->HandleCursorStep(mouseWorldCoords_ - oldCoords);
    }
}

void Cursor3D::HandleMouseButtonDown(StringHash /*eventType*/, VariantMap& eventData)
{
    const int button{ eventData[MouseButtonDown::P_BUTTON].GetInt() };
    if (button == MOUSEB_MIDDLE)
        return;

    UI* ui{ GetSubsystem<UI>() };
    const IntVector2 cursorPos{ ui->GetCursorPosition() };

    if (!ui->GetElementAt(cursorPos))
    {
        clickScreenPos_ = cursorPos;
        clickWorldPos_ = mouseWorldPos_;
    }

    if (hand_)
        hand_->HandleClickBegin(button);
}

void Cursor3D::HandleMouseButtonUp(StringHash /*eventType*/, VariantMap& eventData)
{
    const int button{ eventData[MouseButtonDown::P_BUTTON].GetInt() };
    if (button == MOUSEB_MIDDLE)
        return;

    if (hand_ && Clicked())
    {

        if (dragMode_ == DM_NONE)
            hand_->Apply(button == MOUSEB_RIGHT);
        else
            hand_->HandleDragEnd();
    }

    Unclick();
}

void Cursor3D::OnSetEnabled() { LogicComponent::OnSetEnabled(); }
void Cursor3D::Start() {}
void Cursor3D::DelayedStart() {}
void Cursor3D::Stop() {}

void Cursor3D::Update(float timeStep)
{
    InputMaster* im{ GetSubsystem<InputMaster>() };
    Vector2 leftStick{ im->GetStickPosition(false) };
    leftStick = VectorSign(leftStick) * Vector2{ PowN(leftStick.x_, 2), PowN(leftStick.y_, 2) };

    UI* ui{ GetSubsystem<UI>() };
    const IntVector2 stickDelta{ VectorRoundToInt(leftStick * 8.f) };
    if (stickDelta != IntVector2::ZERO)
    {
        ui->GetCursor()->SetPosition(ui->GetCursorPosition() + stickDelta);
        MouseMoved();
    }
}

void Cursor3D::PostUpdate(float timeStep) {}
void Cursor3D::FixedUpdate(float timeStep) {}
void Cursor3D::FixedPostUpdate(float timeStep) {}
void Cursor3D::OnSceneSet(Scene* scene) { LogicComponent::OnSceneSet(scene); }
void Cursor3D::OnNodeSetEnabled(Node* node) {}
void Cursor3D::DrawDebugGeometry(DebugRenderer* debug, bool depthTest) {}

