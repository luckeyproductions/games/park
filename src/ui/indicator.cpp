/* LucKey Park
// Copyright (C) 2024 LucKey Productions (https://luckey.games)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "indicator.h"

StaticModelGroup* Indicator::arrowsModelGroup_{ nullptr };

Indicator::Indicator(Context* context): Component(context),
    target_{},
    height_{ 2.f },
    scale_{ 1.f },
    spin_{ false },
    autoRemove_{ true }
{
    SubscribeToEvent(E_POSTUPDATE, DRY_HANDLER(Indicator, HandlePostUpdate));
}

void Indicator::HandlePostUpdate(StringHash eventType, VariantMap& eventData)
{
    if (!GetScene() || target_ == node_->GetID())
        return;

    Node* targetNode{ GetScene()->GetNode(target_) };
    bool gone{ node_->GetScale().LengthSquared() == 0.f };

    if (!targetNode || gone)
    {
        if (autoRemove_ || gone)
        {
            node_->Remove();
        }
        else
        {
            target_ = node_->GetID();
            Hide();
        }

        return;
    }

    node_->SetWorldPosition(targetNode->GetWorldPosition() + Vector3::UP * height_);


    Renderer* renderer{ GetSubsystem<Renderer>() };
    Vector3 camPos{};
    if (renderer && renderer->GetNumViewports() != 0)
        camPos = renderer->GetViewport(0)->GetCamera()->GetNode()->GetWorldPosition();

    Vector3 up;
    if (spin_)
        up = Quaternion(GetScene()->GetElapsedTime() * 90.f, Vector3::UP) * Vector3::FORWARD;
    else
        up = (camPos - node_->GetWorldPosition()).ProjectOntoPlane(Vector3::UP).Normalized();

    node_->SetScale(scale_ * (.5f + node_->GetWorldPosition().DistanceToPoint(camPos) * .023f));

    node_->LookAt(node_->GetWorldPosition() + Vector3::DOWN, up, TS_WORLD);
}

void Indicator::Hide(bool sudden)
{
    if (sudden)
        node_->SetEnabled(false);
    else
        node_->SetScale(0.f); /// Animate
}

void Indicator::OnSetEnabled() { Component::OnSetEnabled(); }

void Indicator::OnNodeSet(Node* node)
{
    if (!node)
        return;

    if (!arrowsModelGroup_)
    {
        arrowsModelGroup_ =  node_->GetScene()->CreateComponent<StaticModelGroup>();
        arrowsModelGroup_->SetModel(RES(Model, "Models/UI/Arrow.mdl"));
        arrowsModelGroup_->SetMaterial(RES(Material, "Materials/Glow.xml"));
        arrowsModelGroup_->SetCastShadows(true);
        arrowsModelGroup_->SetViewMask(LAYER(LAYER_UI));
    }

    arrowsModelGroup_->AddInstanceNode(node_);

    target_ = node_->GetID();
}

void Indicator::OnSceneSet(Scene* scene) { Component::OnSceneSet(scene); }
void Indicator::OnMarkedDirty(Node* node) {}
void Indicator::OnNodeSetEnabled(Node* node) {}
bool Indicator::Save(Serializer& dest) const { return Component::Save(dest); }
bool Indicator::SaveXML(XMLElement& dest) const { return Component::SaveXML(dest); }
bool Indicator::SaveJSON(JSONValue& dest) const { return Component::SaveJSON(dest); }
void Indicator::MarkNetworkUpdate() { Component::MarkNetworkUpdate(); }
void Indicator::GetDependencyNodes(PODVector<Node*>& dest) {}
void Indicator::DrawDebugGeometry(DebugRenderer* debug, bool depthTest) {}
