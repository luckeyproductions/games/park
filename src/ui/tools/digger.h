/* LucKey Park
// Copyright (C) 2024 LucKey Productions (https://luckey.games)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef DIGGER_H
#define DIGGER_H

#include "../../landscape/ground.h"
#include "tool.h"

#define MAXSLOPE 8

class Digger: public Tool
{
    DRY_OBJECT(Digger, Tool);

public:
    Digger(Context* context);

    void Pick() override;
    void ResetBrush();
    IntVector2 CenterBrush();

    void Cancel() override
    {
        Tool::Cancel();

        offset_ = Vector3::ZERO;
        change_ = 0;

        UpdateShovelElevation();
    }


    void HandleClickBegin(int button) override;
    void HandleMouseMoved() override;
    void HandleCursorStep(const IntVector3& step) override;

    void HandleDrag(const Ray& ray) override;
    void HandleDragEnd() override
    {
        Tool::HandleDragEnd();

        if (change_ != 0)
            Apply(false);

        change_ = 0;
        UpdateShovelElevation();
    }

    ToolRestriction Apply(bool rightClick = false) override;

    void ToggleShovel(const IntVector2& at);
    void UpdateShovelElevation();

    IntVector2 GetBrushCenter() const
    {
        return (BrushRect().Min() + BrushRect().Max() - VectorMod(GetBrushSize(), { 2, 2 })) / 2;
    }

    IntVector2 GetBrushSize() const
    {
        return BrushRect().Size();
    }

    Vector<IntVector2> GetBrush() const
    {
        return shovels_.Keys();
    }

    void SetBrush(const HashSet<IntVector2>& brush);

    void GrowBrush();
    void ShrinkBrush();
    int CountNeighbors(const Vector<IntVector2>& brush, const IntVector2& coords, bool diagonal = false) const
    {
        int n{};

        for (const IntVector2& d: Neighbors(coords, diagonal))
        {
            if (brush.Contains(d))
                ++n;
        }

        return n;
    }

    void RotateBrush(bool clockwise);

private:
    using DigLimit = Pair<int, int>;

    void UpdateShovelVisibility(const IntVector2& coords);
    void UpdateLimits();
    void ClearBrush();
    IntRect BrushRect() const
    {
        IntRect bounds{ centralCoords_,
                        centralCoords_ };

        for (const IntVector2& shovel: shovels_.Keys())
            bounds.Merge({ shovel, shovel + IntVector2::ONE });

        return bounds;
    }
    HashSet<IntVector2> ShrunkBrush(const Vector<IntVector2>& brush);

    void HandleKeyDown(StringHash eventType, VariantMap& eventData);
    void HandleKeyUp(StringHash eventType, VariantMap& eventData);

    StaticModelGroup* shovelGroup_;
    HashMap<IntVector2, Node*> shovels_;
    HashMap<IntVector2, DigLimit> limits_;
    IntVector2 centralCoords_;
    int change_;
    bool ctrlDown_;
};

#endif // DIGGER_H
