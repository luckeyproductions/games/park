/* LucKey Park
// Copyright (C) 2024 LucKey Productions (https://luckey.games)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "../dash/pages/diggerpage.h"
#include "digger.h"

/// TODO:
/// - Mass shovel toggel area drag (Ctrl + Shift)
/// - Clamp to current high/low, digging from the extremes (Shift)
/// - Changes should be cullected during drag to calculate costs
/// - Change indicators: Arrow from current elevation to shovel during drag.
/// - Solve nested limits bug
/// - Properly center brush

Digger::Digger(Context* context): Tool(context),
    shovelGroup_{ nullptr },
    shovels_{},
    limits_{},
    centralCoords_{ M_MAX_INT * IntVector2::ONE },
    change_{ 0 },
    ctrlDown_{ false }
{
    name_ = "Dig";
    snap_ = SNAP_CORNER;
    viewMask_ = LAYER(LAYER_GROUND);

    type_ = TOOL_DIG;
    tools_[type_] = this;
}

void Digger::Pick()
{
    SubscribeToEvent(E_KEYDOWN, DRY_HANDLER(Digger, HandleKeyDown));
    SubscribeToEvent(E_KEYUP, DRY_HANDLER(Digger, HandleKeyUp));
    ctrlDown_ = GetSubsystem<Input>()->GetQualifierDown(QUAL_CTRL);

    shovels_.Clear();
    centralCoords_ = GetSceneCursor()->GetFlatCoords();

    Tool::Pick();

    shovelGroup_ = previewNode_->GetScene()->CreateComponent<StaticModelGroup>();
    shovelGroup_->SetModel(RES(Model, "Models/UI/Shovel.mdl"));
    shovelGroup_->SetMaterial(RES(Material, "Materials/VCol.xml"));
    shovelGroup_->SetCastShadows(true);
    shovelGroup_->SetViewMask(LAYER(LAYER_UI));

    ResetBrush();
    UpdateShovelElevation();

    ToolInfo* toolInfo{ GetToolInfo() };
    toolInfo->SetPreferredRatio(4/3.f);
    HandleMouseMoved();
}

void Digger::ClearBrush()
{
    shovelGroup_->RemoveAllInstanceNodes();
    for (Node* node: shovels_.Values())
        node->Remove();
    shovels_.Clear();
}

void Digger::ResetBrush()
{
    ClearBrush();

    for (int x{ 0 }; x <= 1; ++x)
    for (int y{ 0 }; y <= 1; ++y)
        ToggleShovel(IntVector2{ x, y });

    GetToolInfo()->GetCurrentPage()->UpdateContents();
}

void Digger::HandleMouseMoved()
{
    for (const IntVector2& coords: shovels_.Keys())
        UpdateShovelVisibility(coords);
}

void Digger::HandleDrag(const Ray& ray)
{
    Cursor3D* cursor{ GetSceneCursor() };

    if (cursor->GetDragMode() == DM_NONE)
    {
        if (!GetSubsystem<Input>()->GetMouseButtonDown(MOUSEB_LEFT) || ctrlDown_)
            return;

        if (cursor->ClickDelta() > GUI::sizes_.margin_ * 2)
            cursor->SetDragMode(DM_LINE);
        else
            return;
    }

    const Vector3 delta{ cursor->DragDelta(ray) };
    const int change{ RoundToInt(delta.y_ / HEIGHTSTEP) };

    if (change_ != change)
    {
        change_ = change;
        UpdateShovelElevation();
    }
}

void Digger::HandleClickBegin(int button)
{
    if (ctrlDown_)
        return;

    if (GetSceneCursor()->Dragging())
        Cancel();

    UpdateLimits();
}

void Digger::HandleCursorStep(const IntVector3& step)
{
    Cursor3D* cursor{ GetSceneCursor() };

    if (ctrlDown_ || GetSubsystem<InputMaster>()->GetAction(IA_INVERT))
    {
        IntVector2 shift{ centralCoords_ - cursor->GetFlatCoords() };
        offset_ = { shift.x_ * GRID, 0, shift.y_ * GRID };
    }
    else
    {
        centralCoords_ = cursor->GetFlatCoords();
        offset_ = Vector3::ZERO;
        change_ = 0;
    }

    UpdateLimits();
    HandleMouseMoved();
    if (step != IntVector3::ZERO)
        UpdateShovelElevation();
}

void Digger::UpdateLimits()
{
    const HeightMap& heightMap{ GetSubsystem<World>()->GetGround()->GetHeightMap() };

    limits_.Clear();
    Vector<IntVector2> rest{ shovels_.Keys() };
    for (const IntVector2& at: rest)
    {
        if (!shovels_[at]->IsEnabled())
        {
            limits_[at] = { 0, 0 };
            rest.Remove(at);
        }
    }

    while (rest.Size())
    {
        const HashSet<IntVector2> shrunkRest{ ShrunkBrush(rest) };

        for (const IntVector2& at: rest)
        {
            if (shrunkRest.Contains(at))
                continue;

            int n{ 0 };
            for (const IntVector2& neighbor: AllNeighbors(at))
            {
                if (shrunkRest.Contains(neighbor))
                    n += 2;
                else if (rest.Contains(neighbor))
                    n += 1;
            }

            if (n > 6)
                continue;

            const IntVector2 coords{ centralCoords_ + at };
            const int elevation{ heightMap.GetHeightAt(coords) };

            int min{ M_MIN_INT };
            int max{ M_MAX_INT };

            for (const IntVector2& neighbor: AllNeighbors(coords))
            {
                const IntVector2 att{ neighbor - centralCoords_ };
                if (rest.Contains(att))
                    continue;

                const int slope{ heightMap.GetHeightAt(neighbor) - elevation};

                if (shovels_.Keys().Contains(att))
                {
                    min = Max(min, limits_[att].first_  - MAXSLOPE + slope);
                    max = Min(max, limits_[att].second_ + MAXSLOPE + slope);
                }
                else
                {
                    min = Max(min, slope - MAXSLOPE);
                    max = Min(max, slope + MAXSLOPE);
                }
            }

            if (elevation > max && elevation < min)
                min = max = 0;

            if (min != M_MIN_INT && max != M_MAX_INT)
            {
                limits_[at] = DigLimit{ min, max };
                rest.Remove(at);
            }
        }
    }
}

ToolRestriction Digger::Apply(bool rightClick)
{
    Cursor3D* cursor{ GetSceneCursor() };

    if (!ctrlDown_ && !GetSubsystem<InputMaster>()->GetAction(IA_INVERT))
    {
        ToolRestriction restriction{ CheckRestriction() };

        if (restriction == TR_NONE)
        {
            UpdateLimits();
            bool single{ !cursor->Dragging() };
            Ground* ground{ GetSubsystem<World>()->GetGround() };

            if (single)
                change_ = (rightClick ? -1 : 1);

            Vector<IntVector2> dirtyTiles{};

            for (const IntVector2& at: shovels_.Keys())
            {
                if (!shovels_[at]->IsEnabled())
                    continue;

                int localChange{ change_ };
                if (limits_.Contains(at))
                {
                    DigLimit limit{ limits_[at] };
                    localChange = Clamp(localChange, limit.first_, limit.second_ );
                }

                ground->GetHeightMap().Raise(centralCoords_ + at, localChange);

                for (int x{ -1 }; x < 1; ++x)
                for (int y{ -1 }; y < 1; ++y)
                {
                    const IntVector2 tile{ centralCoords_ + at + IntVector2{ x, y } };

                    if (!dirtyTiles.Contains(tile))
                        dirtyTiles.Push(tile);
                }

                shovels_[at]->Translate(HEIGHTSTEP * localChange * Vector3::UP);
            }

            ground->UpdateTiles(dirtyTiles);
            GetSubsystem<World>()->RemovePath(dirtyTiles);

            if (!single)
                UpdateShovelElevation();
        }

        cursor->Unclick();

        return restriction;
    }
    else
    {
        ToggleShovel(cursor->GetFlatCoords() - centralCoords_);
        GetToolInfo()->GetCurrentPage()->UpdateContents();

        return TR_NONE;
    }
}

void Digger::ToggleShovel(const IntVector2& at)
{
    if (shovels_.Contains(at))
    {
        shovelGroup_->RemoveInstanceNode(shovels_[at]);
        shovels_[at]->Remove();
        shovels_.Erase(at);
    }
    else
    {
        Node* shovelNode{ previewNode_->CreateChild("Shovel") };
        shovelNode->SetPosition(Vector3{ at.x_ * GRID, 0.f, at.y_ * GRID } + offset_);
        shovels_[at] = shovelNode;
        shovelGroup_->AddInstanceNode(shovelNode);
        UpdateShovelElevation();
    }
}

void Digger::UpdateShovelElevation()
{
    if (!shovelGroup_ || shovels_.IsEmpty())
        return;

    World* world{ GetSubsystem<World>() };
    Cursor3D* cursor{ GetSceneCursor() };
    const HeightMap& heightMap{ world->GetGround()->GetHeightMap() };
    const IntVector2 flatCursorCoords{ cursor->GetFlatCoords() };
    const int cursorHeight{ heightMap.GetHeightAt(centralCoords_) };
    cursor->SetCoords({ flatCursorCoords.x_, cursorHeight, flatCursorCoords.y_ }, !ctrlDown_);
    offset_.y_ = HEIGHTSTEP * (heightMap.GetHeightAt(centralCoords_) - cursor->GetCoords().y_);

    for (const IntVector2& at: shovels_.Keys())
    {
        Node* shovel{ shovels_[at] };
        UpdateShovelVisibility(at);
        if (!shovel->IsEnabled())
            continue;

        const IntVector2 coords{ centralCoords_ + at };
        int elevation{ heightMap.GetHeightAt(coords) - heightMap.GetHeightAt(centralCoords_) };

        if (change_ != 0 && limits_.Contains(at))
            elevation += Clamp(change_, limits_[at].first_, limits_[at].second_);

        shovel->SetPosition(world->CoordinatesToPosition({ at.x_, elevation, at.y_}) + offset_);
    }
}

void Digger::UpdateShovelVisibility(const IntVector2& coords)
{
    Node* shovel{ shovels_[coords] };
    if (!shovel)
        return;

    World* world{ GetSubsystem<World>() };
    Cursor3D* cursor{ GetSceneCursor() };
    const HeightMap& heightMap{ world->GetGround()->GetHeightMap() };
    bool inBounds{ heightMap.WithinBounds(centralCoords_ + coords, 1) };
    shovel->SetEnabled(inBounds && !cursor->IsBehindUI());
}

void Digger::SetBrush(const HashSet<IntVector2>& brush)
{
    ClearBrush();

    for (const IntVector2& coords: brush)
        ToggleShovel(coords);

    CenterBrush();
    GetToolInfo()->GetCurrentPage()->UpdateContents();
}

void Digger::GrowBrush()
{
    const Vector<IntVector2> oldBrush{ GetBrush() };
    HashSet<IntVector2> newBrush{};
    for (const IntVector2& coords: oldBrush)
    {
        newBrush.Insert(coords);
        for (const IntVector2& c: Neighbors(coords, false))
            newBrush.Insert(c);

        if (CountNeighbors(oldBrush, coords, false) == 2 && CountNeighbors(oldBrush, coords, true) == 1)
        {
            for (const IntVector2& c: Neighbors(coords, true))
                newBrush.Insert(c);
        }
    }

    SetBrush(newBrush);
}

HashSet<IntVector2> Digger::ShrunkBrush(const Vector<IntVector2>& brush)
{
    HashSet<IntVector2> shrunk{};
    for (const IntVector2& coords: brush)
    {
        if (CountNeighbors(brush, coords, false) == 4)
            shrunk.Insert(coords);
    }

    return shrunk;
}

void Digger::ShrinkBrush()
{
    const IntVector2 oldSize{ GetBrushSize() };
    if (oldSize.x_ <= 2 || oldSize.y_ <= 2)
    {
        SetBrush({ IntVector2::ZERO });
    }
    else
    {
        const HashSet<IntVector2> shrunkBrush{ ShrunkBrush(GetBrush()) };

        if (shrunkBrush.IsEmpty())
            SetBrush({ IntVector2::ZERO });
        else
            SetBrush(shrunkBrush);
    }
}

void Digger::RotateBrush(bool clockwise)
{
    CenterBrush();
    const Vector<IntVector2> oldBrush{ GetBrush() };
    HashSet<IntVector2> newBrush{};
    for (const IntVector2& coords: oldBrush)
    {
        newBrush.Insert(coords.Transposed() * IntVector2{ 1 - 2 * !clockwise, 1 - 2 * clockwise });
    }

    SetBrush(newBrush);
}

IntVector2 Digger::CenterBrush()
{
    const IntVector2& offCenter{ GetBrushCenter() };

    if (offCenter != IntVector2::ZERO)
    {
        HashMap<IntVector2, Node*> shovels{};

        for (const IntVector2& at: shovels_.Keys())
        {
            const IntVector2& nat{ at - offCenter };
            shovels[nat] = shovels_[at];
        }

        shovels_ = shovels;
        offset_ = Vector3::ZERO;
    }

    GetToolInfo()->GetCurrentPage()->UpdateContents();
    UpdateShovelElevation();

    return offCenter;
}

void Digger::HandleKeyDown(StringHash /*eventType*/, VariantMap& eventData)
{
    if (eventData[KeyDown::P_KEY] == KEY_CTRL && !GetSceneCursor()->Clicked())
        ctrlDown_ = true;
}

void Digger::HandleKeyUp(StringHash /*eventType*/, VariantMap& eventData)
{
    if (eventData[KeyUp::P_KEY] == KEY_CTRL)
    {
        if (shovels_.IsEmpty())
            ToggleShovel(IntVector2::ZERO);

        ctrlDown_ = false;
        CenterBrush();
    }
}
