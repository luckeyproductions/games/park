/* LucKey Park
// Copyright (C) 2024 LucKey Productions (https://luckey.games)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef DOZER_H
#define DOZER_H

#include "tool.h"

class Dozer: public Tool
{
    DRY_OBJECT(Dozer, Tool);

public:
    Dozer(Context* context);

    void Pick() override;
    ToolRestriction Apply(bool rightClick = false) override;

    void HandleCursorStep(const IntVector3& step) override;

private:
    void HandlePostRenderUpdate(StringHash eventType, VariantMap& eventData);

    bool subGrid_;
};

#endif // DOZER_H
