﻿/* LucKey Park
// Copyright (C) 2024 LucKey Productions (https://luckey.games)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef TOOLS_H
#define TOOLS_H

#include "../gui.h"
#include "tooltext.h"
#include "../dash/toolinfo.h"

enum ToolType{ TOOL_NONE = -1, TOOL_BUILD, TOOL_DOZE, TOOL_DIG, TOOL_INFO, TOOL_ALL };
const StringVector toolNames{ "Builder", "Dozer", "Digger", "Looker" }; /// TRANSLATE
enum ToolRestriction{ TR_NONE = 0, TR_FUNDS, TR_SPACE, TR_OTHER };

class Delegate;

class Tool: public Object
{
    DRY_OBJECT(Tool, Object);

public:
    static Tool* Get(ToolType tool) { return tools_[tool]; }
    static Tool* GetCurrent() { Cursor3D* c{ GetSceneCursor() }; return ( c ? c->GetTool() : nullptr); }
    static Node* GetPreviewNode() { return previewNode_; }
    static void ClearPreview()
    {
        if (previewNode_)
        {
            previewNode_->RemoveAllChildren();
            previewNode_->RemoveAllComponents();
            previewNode_->ResetToDefault();
        }
    }

    Tool(Context* context);

    virtual void HandleMouseMoved()
    {
        if (previewNode_)
            previewNode_->SetEnabled(!GetSceneCursor()->IsBehindUI());
    }

    virtual void HandleClickBegin(int button) {}
    virtual void HandleClickEnd() {}
    virtual void HandleCursorStep(const IntVector3& step);

    virtual void HandleDrag(const Ray& ray) {}
    virtual void HandleDragEnd() {}

    virtual ToolRestriction Apply(bool rightClick = false) { return CheckRestriction(); }
    virtual ToolRestriction CheckRestriction() { return TR_NONE; }

    virtual void Cancel()
    {
        GetSceneCursor()->Unclick();
    }

    virtual void Pick();

    static void PickNext();
    static void PickPrevious();

    void SetDelegate(Delegate* delegate);
    virtual void EndDelegation();

    String GetName() const { return name_; }
    SnapMode GetSnapMode() const { return snap_; }
    Vector3 GetOffset() const { return offset_; }
    unsigned GetViewMask() const { return viewMask_; }

    static Cursor3D* GetSceneCursor() { return tools_.Values().Front()->GetSubsystem<GUI>()->GetSceneCursor(); }
    static void SetTooltip(ToolText* tooltip) { tooltip_ = tooltip; }

protected:
    void PlaceCursorOnGround();
    ToolInfo* GetToolInfo() const;

    static HashMap<unsigned, SharedPtr<Tool> > tools_;
    static Node* previewNode_;
    static ToolText* tooltip_;

    ToolType type_;
    String name_;
    SnapMode snap_;
    Vector3 offset_;
    unsigned viewMask_;
    Delegate* delegate_;
};

class Mover: public Tool
{
    DRY_OBJECT(Mover, Tool);
public:
    Mover(Context* context): Tool(context) {}
};

#endif // TOOLS_H
