
#ifndef TRACKEDITOR_H
#define TRACKEDITOR_H

#include "../../structures/coaster.h"

#include "delegate.h"

class TrackEditor: public Delegate
{
    DRY_OBJECT(TrackEditor, Delegate);

public:
    TrackEditor(Context* context);

    ToolRestriction Apply(bool rightClick) override;
    void UpdatePreview() override;

    void HandleCursorStep(const IntVector3& step = IntVector3::ZERO) override;
    void HandleCursorTurned(char r) override;

    void SetActiveElement(TrackElement* element);

protected:
    void OnNodeSet(Node* node) override;

private:
    void SetProfile(const TrackProfile& profile);
    void UpdateHighlight();

    void HandleKeyDown(StringHash eventType, VariantMap& eventData);
    void HandleKeyUp(StringHash eventType, VariantMap& eventData);

    Track* track_;
    Node* elementPreviewNode_;
    StaticModel* previewModel_;
    StaticModel* highlightModel_;

    SharedPtr<Material> signalMat_;
    SharedPtr<Material> highlightMat_;
    TrackElement* activeElement_;
    TrackProfile profile_;
    Node* previewArrowNode_;
};

#endif // TRACKEDITOR_H
