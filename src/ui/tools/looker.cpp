/* LucKey Park
// Copyright (C) 2024 LucKey Productions (https://luckey.games)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "../../structures/train.h"
#include "../dash/pages/lookerpage.h"

#include "looker.h"

Looker::Looker(Context* context): Tool(context),
    targetNode_{ nullptr },
    rideCamNode_{ nullptr }
{
    name_ = "Look";
    viewMask_ = LAYER(LAYER_GROUND) + LAYER(LAYER_CHARACTER) + LAYER(LAYER_STRUCTURE);

    type_ = TOOL_INFO;
    tools_[type_] = this;
}

void Looker::Pick()
{
    Tool::Pick();

    targetNode_ = nullptr;
    ToolInfo* toolInfo{ GetToolInfo() };
    toolInfo->SetPreferredRatio(3/2.f);
    toolInfo->SetMinimized(true);
}

void Looker::Unfollow()
{
    targetNode_ = nullptr;
    GetSubsystem<Jib>()->Unfollow();
    ToolInfo* info{ GetToolInfo() };
    info->GetCurrentPage()->UpdateContents();
    info->SetMinimized(true);
}

ToolRestriction Looker::Apply(bool rightClick)
{
    if (!rightClick && rideCamNode_)
        return TR_NONE;


    if (rightClick)
    {
        if (rideCamNode_)
            Unride();
        else
            Unfollow();

        return TR_NONE;
    }

    Jib* jib{ GetSubsystem<Jib>() };
    Cursor3D* cursor{ GetSceneCursor() };
    const RayQueryResult& cursorResult{ cursor->GetFreeMoveResult() };
    Node* hitNode{ cursorResult.node_ };
    Node* target{ (hitNode->HasTag(TAG_CAMTARGET)
                 ? hitNode
                 : hitNode->GetParentWithTag(TAG_CAMTARGET, true)) };

    targetNode_ = target;

    if (target)
    {
        if (target->HasComponent<Train>())
        {
            target = target->GetComponent<Train>()->GetNearestWagon(cursorResult.position_);

            if (target)
            {
                jib->Unfollow();
                Ride(target);
                return TR_NONE;
            }
        }

        jib->Follow(target);
        ToolInfo* toolInfo{ GetToolInfo() };
        toolInfo->SetMinimized(false);
        toolInfo->GetCurrentPage()->UpdateContents();
    }
    else
    {
        jib->Unfollow();
        jib->Jump(cursor->GetClickWorldPos());
        GetToolInfo()->SetMinimized(true);
    }

    return TR_NONE;
}

void Looker::Cancel()
{
    Tool::Cancel();
    Unride();
    GetSubsystem<Jib>()->Unfollow();
}

void Looker::Ride(Node* node)
{
    if (rideCamNode_ == node || node == nullptr)
        return;

    const bool kart{ node->GetComponent<Wagon>()->GetTrack()->GetTrackType() == TRACKS_ROAD };
    if (kart)
        node = node->GetChild("Kart");

    rideCamNode_ = node->CreateChild("Camera");
    rideCamNode_->Translate({ 0.f, .75f, -.2f });
    Camera* cam{ rideCamNode_->CreateComponent<Camera>() };
    cam->SetNearClip(.025f);
    Renderer* renderer{ GetSubsystem<Renderer>() };
    renderer->GetViewport(0)->SetCamera(cam);

    UI* ui{ GetSubsystem<UI>() };
    ui->GetRoot()->SetVisible(false);
    ui->GetCursor()->SetVisible(false);

    SubscribeToEvent(E_KEYDOWN, DRY_HANDLER(Looker, HandleKeyDown));
    SubscribeToEvent(E_MOUSEMOVE, DRY_HANDLER(Looker, HandleMouseMove));
}

void Looker::Unride()
{
    if (!rideCamNode_)
        return;

    Camera* cam{ GetSubsystem<Jib>()->GetActiveCamera() };
    Renderer* renderer{ GetSubsystem<Renderer>() };
    renderer->GetViewport(0)->SetCamera(cam);
    rideCamNode_->Remove();
    rideCamNode_ = nullptr;

    UI* ui{ GetSubsystem<UI>() };
    ui->GetRoot()->SetVisible(true);
    ui->GetCursor()->SetVisible(true);

    UnsubscribeFromEvent(E_KEYDOWN);
    UnsubscribeFromEvent(E_MOUSEMOVE);

    targetNode_ = nullptr;

    ToolInfo* info{ GetToolInfo() };
    info->GetCurrentPage()->UpdateContents();
    info->SetMinimized(true);
}

void Looker::HandleDrag(const Ray& ray)
{
    Cursor3D* cursor{ GetSceneCursor() };

    if (cursor->GetDragMode() == DM_NONE)
    {
        if (!GetSubsystem<Input>()->GetMouseButtonDown(MOUSEB_LEFT))
            return;

        if (cursor->ClickDelta() > GUI::sizes_.margin_ * 2)
            cursor->SetDragMode(DM_PLANE);
        else
            return;
    }

//    const Vector3 delta{ cursor->DragDelta(ray) };
//    if (delta.x_ != M_INFINITY)
    //        GetSubsystem<Jib>()->GetNode()->Translate(delta, TS_WORLD);
}

void Looker::HandleKeyDown(StringHash /*eventType*/, VariantMap& eventData)
{
    const Key k{ static_cast<Key>(eventData[KeyDown::P_KEY].GetUInt()) };
    const bool repeat{ eventData[KeyDown::P_REPEAT].GetBool() };

    if (!repeat)
    {
        Wagon* wagon{ rideCamNode_->GetParentComponent<Wagon>(true) };
        Train* train{ wagon->GetTrain() };

        if (!train)
            return;

        if (k == KEY_HOME)
        {
            Unride();
        }
        else if (k == KEY_UP)
        {
            Wagon* nextWagon{ train->GetNextWagon(wagon) };
            if (nextWagon)
            {
                Unride();
                Ride(nextWagon->GetNode());
            }
        }
        else if (k == KEY_DOWN)
        {
            Wagon* prevWagon{ train->GetPreviousWagon(wagon) };
            if (prevWagon)
            {
                Unride();
                Ride(prevWagon->GetNode());
            }
        }
    }
}

void Looker::HandleMouseMove(StringHash /*eventType*/, VariantMap& eventData)
{
    if (!rideCamNode_)
        return;

    const IntVector2 delta{
        eventData[MouseMove::P_DX].GetInt(),
        eventData[MouseMove::P_DY].GetInt()
    };

    Quaternion oldRot{ rideCamNode_->GetRotation() };
    Quaternion newRot{ Clamp(oldRot.PitchAngle() + delta.y_ * .1f, -80.f, 45.f),
                       Clamp(oldRot.YawAngle()   + delta.x_ * .1f, -110.f, 110.f),
                       0.f
                     };

    rideCamNode_->SetRotation(newRot);
}
