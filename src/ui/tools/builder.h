/* LucKey Park
// Copyright (C) 2024 LucKey Productions (https://luckey.games)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef BUILDER_H
#define BUILDER_H

#include "../../structures/structure.h"
#include "tool.h"

class Structure;

class Builder: public Tool
{
    DRY_OBJECT(Builder, Tool);
public:
    static void RegisterObject(Context* context);

    Builder(Context* context);

    void Pick() override;
    void HandleClickBegin(int button) override;
    void HandleClickEnd() override;

    void HandleMouseMoved() override;
    void HandleCursorStep(const IntVector3& step) override;
    void HandleDrag(const Ray& ray) override;

    void HandleDragEnd() override;

    ToolRestriction Apply(bool rightClick = false) override;
    ToolRestriction CheckRestriction() override;

    void EndDelegation() override;

    void SetCyanotype(const StructureInfo& blueprint);
    const StructureInfo& GetCyanotype() const { return cyanotype_; }
    void PlaceStructure();

private:
    void SetBuildMode(BuildType buildMode);
    void UpdatePreview();
    void SetPreviewMaterial(Material* mat);

    StructureInfo cyanotype_;
    Structure* construction_;
    BuildType buildMode_;
    MouseButton beginButton_;
    void UpdateWireColor();
};

#endif // BUILDER_H
