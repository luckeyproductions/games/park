/* LucKey Park
// Copyright (C) 2024 LucKey Productions (https://luckey.games)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef LOOKER_H
#define LOOKER_H

#include "tool.h"

class Looker: public Tool
{
    DRY_OBJECT(Looker, Tool);

public:
    Looker(Context* context);

    void Pick() override;
    ToolRestriction Apply(bool rightClick = false) override;
    void Cancel() override;

    Node* GetTarget() const { return targetNode_; }

    void Ride(Node* node);
    void Unride();

    void HandleDrag(const Ray& ray) override;

    /// Left click: Go to and follow
    /// Drag: Pan
    /// Right click: Overview or unfollow
private:
    void Unfollow();
    void HandleKeyDown(StringHash eventType, VariantMap& eventData);
    void HandleMouseMove(StringHash eventType, VariantMap& eventData);

    Node* targetNode_;
    Node* rideCamNode_;
};

#endif // LOOKER_H
