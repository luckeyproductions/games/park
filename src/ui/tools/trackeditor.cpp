
#include "cyberplasm.h"
#include "../../structures/coaster.h"

#include "trackeditor.h"

TrackEditor::TrackEditor(Context* context): Delegate(context),
    track_{ nullptr },
    elementPreviewNode_{ nullptr },
    previewModel_{ nullptr },
    highlightModel_{ nullptr },
    signalMat_{ RES(Material, "Materials/Glow.xml")->Clone() },
    highlightMat_{ RES(Material, "Materials/Glow.xml")->Clone() },
    activeElement_{ nullptr },
    profile_{},
    previewArrowNode_{}
{
    profile_.step_ = IntVector3::ZERO;

    master_ = Tool::Get(TOOL_BUILD);

    SubscribeToEvent(E_KEYDOWN, DRY_HANDLER(TrackEditor, HandleKeyDown));
    SubscribeToEvent(E_KEYUP, DRY_HANDLER(TrackEditor, HandleKeyUp));
}

void TrackEditor::HandleKeyDown(StringHash /*eventType*/, VariantMap& eventData)
{
    const int key{ eventData[KeyDown::P_KEY].GetInt() };
    const unsigned qualifiers{ eventData[KeyDown::P_QUALIFIERS].GetUInt() };
    const bool ctrl{ (qualifiers & QUAL_CTRL) != 0u };
    const bool alt{  (qualifiers & QUAL_ALT ) != 0u };
    const bool both{ ctrl && alt };
    bool hit{ false };

    switch (key)
    {
    default: break;
    case KEY_R:
        profile_.bank_.second_ = 0;  /// Should pick shortest twist to flat from first
        profile_.slope_.second_ = 0;

        hit = true;
    break;
    case KEY_9:
        if (!both)
        {
            if (ctrl)
                profile_.slope_.second_ -= (profile_.slope_.second_ % 4 == 0 ? 4 : profile_.slope_.second_ % 4);
            else
                --profile_.slope_.second_;

            if (profile_.slope_.second_ < -8)
                profile_.slope_.second_ = -8;

            hit = true;
        }
    break;
    case KEY_0:
        if (!both)
        {
            if (ctrl)
                profile_.slope_.second_ += 4 - (profile_.slope_.second_ % 4);
            else
                ++profile_.slope_.second_;

            if (profile_.slope_.second_ > 8)
                profile_.slope_.second_ = 8;

            hit = true;
        }
    break;
    case KEY_LEFTBRACKET:
        if (!both)
        {
            if (ctrl)
                profile_.bank_.second_ -= (profile_.bank_.second_ % 4 == 0 ? 4 : profile_.bank_.second_ % 4);
            else
                --profile_.bank_.second_;

            hit = true;
        }
    break;
    case KEY_RIGHTBRACKET:
        if (!both)
        {
            if (ctrl)
                profile_.bank_.second_ += 4 - (profile_.bank_.second_ % 4);
            else
                ++profile_.bank_.second_;

            hit = true;
        }
    break;
    case KEY_EQUALS:
        if (!both)
        {
            if (ctrl)
                profile_.step_.y_ += 4 - (profile_.step_.y_ % 4);
            else
                ++profile_.step_.y_;
            hit = true;
        }
    break;
    case KEY_MINUS:
        if (!both)
        {
            if (ctrl)
                profile_.step_.y_ -= (profile_.step_.y_ % 4 == 0 ? 4 : profile_.step_.y_ % 4);
            else
                --profile_.step_.y_;
            hit = true;
        }
    break;
    }

    if (hit)
        UpdatePreview();
}

void TrackEditor::HandleKeyUp(StringHash /*eventType*/, VariantMap& eventData)
{
    int key{ eventData[KeyDown::P_KEY].GetInt() };
    Input* input{ GetSubsystem<Input>() };

    if ((key == KEY_SHIFT || key == KEY_CTRL)
        && (!input->GetQualifierDown(QUAL_CTRL) || !input->GetQualifierDown(QUAL_SHIFT)))
        HandleCursorStep();
}

void TrackEditor::OnNodeSet(Node* node)
{
    if (!node)
    {
        if (highlightModel_)
            highlightModel_->Remove();

        return;
    }

    if (!node_->GetParent()->GetComponent<Track>())
    {
        master_->EndDelegation();
        return;
    }

    node_->SetTemporary(true);
    track_ = node_->GetParent()->GetComponent<Track>();

    elementPreviewNode_ = node_->CreateChild("Preview");
    elementPreviewNode_->SetTemporary(true);
    previewModel_ = elementPreviewNode_->CreateComponent<StaticModel>();
    previewModel_->SetModel(RES(Model, "Models/Box.mdl"));
    previewModel_->SetViewMask(LAYER(LAYER_UI));

    previewArrowNode_ = elementPreviewNode_->CreateChild("Arrow");
    previewArrowNode_->SetScale(.55f);
    StaticModel* arrow{ previewArrowNode_->CreateComponent<StaticModel>() };
    arrow->SetModel(RES(Model, "Models/UI/TrackArrow.mdl"));
    arrow->SetMaterial(0, signalMat_);
    arrow->SetMaterial(1, RES(Material, "Materials/Darkness.xml"));
    arrow->SetViewMask(LAYER(LAYER_UI));

    if (track_->GetNumElements())
        SetActiveElement(track_->Head());
}

void TrackEditor::SetActiveElement(TrackElement* element)
{
    if (activeElement_ == element)
        return;

    activeElement_ = element;

    if (activeElement_)
    {
        UpdateHighlight();

        const TrackProfile activeProfile{ activeElement_->GetProfile() };
        const int slope{ activeProfile.slope_.second_ };

        profile_.bank_.first_ = activeProfile.bank_.second_ % 16;
        profile_.slope_.first_ = slope;
        profile_.step_.y_ = slope + slope / 4 + slope / 6 * (slope - 4 + 6 * (slope > 6));

        Vector3 previewPos{ World::CoordinatesToPosition(activeElement_->GetCoords() + activeElement_->GetRotatedStep()) };
        elementPreviewNode_->SetEnabled(true);
        elementPreviewNode_->SetPosition(previewPos);
        elementPreviewNode_->SetRotation(activeElement_->GetQuaternion(true));

        Node* masterPreviewNode{ Tool::GetPreviewNode() };
        masterPreviewNode->SetRotation(track_->GetNode()->GetWorldRotation() * World::StepRotationToQuaternion(activeElement_->GetStepRotation() + activeElement_->GetProfile().turn_));
        HandleCursorTurned(World::RotationFromDirection(masterPreviewNode->GetWorldDirection()));
    }
    else
    {
        elementPreviewNode_->SetEnabled(false);
    }
}

void TrackEditor::UpdateHighlight()
{
    if (highlightModel_)
        highlightModel_->Remove();

    if (!activeElement_ || !activeElement_->GetNode())
        return;

    highlightModel_ = activeElement_->GetNode()->CreateComponent<StaticModel>(); /// !
    highlightModel_->SetViewMask(LAYER(LAYER_UI));

    highlightModel_->SetModel(Grimoire::Summon({ Track::PathFromProfile(activeElement_->GetProfile()) }, 0));
    highlightModel_->SetMaterial(highlightMat_);
}

void TrackEditor::SetProfile(const TrackProfile& profile)
{
    if (profile_ == profile)
        return;

    profile_ = profile;
    UpdatePreview();

}

void TrackEditor::HandleCursorStep(const IntVector3& step)
{
    if (!track_)
        return;

    Input* input{ GetSubsystem<Input>() };
    Structure* structure{ track_->GetStructure() };

    if (!structure || !activeElement_
     || input->GetQualifierDown(QUAL_CTRL) || input->GetQualifierDown(QUAL_SHIFT))
        return;

    IntVector3 previewCoords{ activeElement_->GetCoords() + activeElement_->GetRotatedStep() };

    const int oldY{ profile_.step_.y_ };

    profile_.step_ = Track::RotatedStep(Track::RotatedStep(master_->GetSceneCursor()->GetCoords() - structure->GetCoords(), -structure->GetStepRotation()) - previewCoords, -activeElement_->GetProfile().turn_ - activeElement_->GetStepRotation());

    const int oldTurn{ profile_.turn_ };

    if (/*profile_.step_.y_ == 0 &&*/ profile_.step_.z_ < 0)
        profile_.step_.z_ = 0;

    if (profile_.step_.x_ == 0 || Sign(profile_.step_.x_) != Sign(profile_.turn_))
        profile_.turn_ = 0;

    if (Abs(profile_.turn_) == 2)
        profile_.turn_ = Sign(profile_.step_.x_);

    if (profile_.step_.z_ == 0)
    {
        profile_.turn_ = Sign(profile_.step_.x_);

        if (profile_.step_.x_ == 0)
            profile_.step_.z_ = 1;
    }

    master_->GetPreviewNode()->SetRotation(World::StepRotationToQuaternion(activeElement_->GetStepRotation() + activeElement_->GetProfile().turn_ + profile_.turn_ + structure->GetStepRotation()));

    profile_.step_.y_ = oldY;

    UpdatePreview();
}

void TrackEditor::HandleCursorTurned(char r)
{
    if (!track_)
        return;

    r = r - track_->GetStructure()->GetStepRotation() - activeElement_->GetStepRotation() - activeElement_->GetProfile().turn_;
    while (r < -2 || r > 2)
    {
        if (r < -2)
            r += 4;
        else
            r -= 4;
    }

    profile_.turn_ = r;

    HandleCursorStep(IntVector3::ZERO);
}

ToolRestriction TrackEditor::Apply(bool rightClick)
{
    if (!track_)
        return TR_NONE;

    if (!rightClick)
    {
        TrackElement* element{ track_->Append(profile_.ForwardStep()) };

        if (element)
        {
            const int tone{ track_->GetTrackType() < TRACKS_WOOD
                        ? -Min(12, FloorToInt(profile_.step_.Length() * M_1_SQRT2))
                        : 0 };
            track_->GetStructure()->PlayBuildSound(tone);

            SetActiveElement(element);

            if (!element->IsOpenEnded())
                master_->GetSceneCursor()->SetTool(nullptr);

            return TR_NONE;
        }
        else
        {
            return TR_OTHER;
        }
    }
    else
    {
        if (track_->GetNumElements() > 1)
        {
            const TrackProfile profile{ activeElement_->GetProfile() };

            SetActiveElement(track_->Snip());
            SetProfile(profile);
        }
        else
        {
            GetSubsystem<Estate>()->CancelConstruction(track_->GetStructure());
            master_->EndDelegation();
        }

        return TR_NONE;
    }
}

void TrackEditor::UpdatePreview()
{
    previewModel_->SetModel(Grimoire::Summon({ Track::PathFromProfile(profile_) }, 0));
    previewModel_->SetMaterial(signalMat_);

    Vector3 arrowPosition{ (profile_.step_) * GRID_BLOCK
                         + Vector3{ GRID * -.5f * profile_.turn_, HEIGHTSTEP * .5f, GRID * -.5f * ((Abs(profile_.turn_) + 1) % 2) } };
    previewArrowNode_->SetPosition(arrowPosition);
    previewArrowNode_->SetRotation({ -profile_.slope_.second_ * 90.f/8, profile_.turn_ * 90.f, -profile_.bank_.second_ * 180.f/8 });
}
