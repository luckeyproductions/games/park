/* LucKey Park
// Copyright (C) 2024 LucKey Productions (https://luckey.games)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "../../structures/structure.h"
#include "../dash/pages/dozerpage.h"
#include "../../effect/dust.h"

#include "dozer.h"

Dozer::Dozer(Context* context): Tool(context),
    subGrid_{ false }
{
    name_ = "Doze";
    snap_ = SNAP_CENTER;
    viewMask_ = LAYER(LAYER_GROUND) + LAYER(LAYER_STRUCTURE);

    type_ = TOOL_DOZE;
    tools_[type_] = this;
}

void Dozer::Pick()
{
    Tool::Pick();

    ToolInfo* toolInfo{ GetToolInfo() };
    toolInfo->SetPreferredRatio(3.f);
    toolInfo->SetMinimized(true);

    StaticModel* tractorModel{ previewNode_->CreateComponent<StaticModel>() };
    tractorModel->SetModel(RES(Model, "Models/UI/Tractor.mdl"));
    tractorModel->SetMaterial(RES(Material, "Materials/VCol.xml"));
    tractorModel->SetCastShadows(true);
    tractorModel->SetViewMask(LAYER(LAYER_UI));

    StaticModel* toolModel{ previewNode_->CreateComponent<StaticModel>() };
    toolModel->SetModel(RES(Model, "Models/UI/Dozer.mdl"));
    toolModel->SetMaterial(RES(Material, "Materials/VCol.xml"));
    toolModel->SetCastShadows(true);
    toolModel->SetViewMask(LAYER(LAYER_UI));

    previewNode_->SetScale(subGrid_ ? SUBGRID : GRID);
    previewNode_->SetRotation(Quaternion::IDENTITY);

    SubscribeToEvent(E_POSTRENDERUPDATE, DRY_HANDLER(Dozer, HandlePostRenderUpdate));
    HandleMouseMoved();
}

void Dozer::HandlePostRenderUpdate(StringHash /*eventType*/, VariantMap& /*eventData*/)
{
    if (GetSceneCursor()->GetTool() != this || !previewNode_->IsEnabled())
        return;

    Estate* estate{ GetSubsystem<Estate>() };
    const IntVector2 cursorCoords{ FLAT(GetSceneCursor()->GetCoords()) };
    for (Structure* s: estate->GetStructures())
    {
        if (s->GetCoordBounds().IsInside(cursorCoords))
            s->DrawBounds(Color::RED);
    }
}

void Dozer::HandleCursorStep(const IntVector3& step)
{
    if (!previewNode_ || (step.x_ == 0 && step.z_ == 0))
        return;

    Vector3 axile{ (Abs(step.x_) >  Abs(step.z_) ? Sign(step.x_) : 0.f),
                0.f,
                (Abs(step.z_) >= Abs(step.x_) ? Sign(step.z_) : 0.f) };

    previewNode_->SetDirection(axile);

    PlaceCursorOnGround();
    HandleMouseMoved();
}

ToolRestriction Dozer::Apply(bool rightClick)
{
    if (rightClick)
        return TR_NONE;

    ToolRestriction restriction{ CheckRestriction() };
    if (restriction == TR_NONE)
    {
        const IntVector2 cursorCoords{ GetSceneCursor()->GetFlatCoords() };
        World* world{ GetSubsystem<World>() };
        Scene* scene{ world->GetScene() };

        if (cursorCoords.y_ >= 0 && world->IsPath(cursorCoords))
        {
            world->RemovePath({ cursorCoords });

            Node* dustNode{ scene->CreateChild("Poof") };
            Dust* dust{ dustNode->CreateComponent<Dust>() };
            const Vector3 pos{ world->CoordinatesToWorldPosition(cursorCoords) };
            dustNode->SetWorldPosition(pos);
            dust->AddCloud({});

            SoundSource* demolishSound{ scene->CreateChild("DemolishSound")->CreateComponent<SoundSource>() };
            demolishSound->SetAutoRemoveMode(REMOVE_NODE);
            demolishSound->Play(RES(Sound, "Samples/Demolish.wav"));
            demolishSound->SetFrequency(demolishSound->GetFrequency() * 1.3f + (Random(3) * .025f));
        }

        Estate* estate{ GetSubsystem<Estate>() };
        for (Structure* s: estate->GetStructures())
        {
            const IntRect bounds{ s->GetCoordBounds() };
            if (bounds.IsInside(cursorCoords))
            {
                const int tone{ 1 - FloorToInt(Sqrt(bounds.Width() + bounds.Height())) };
                SoundSource* demolishSound{ scene->CreateChild("DemolishSound")->CreateComponent<SoundSource>() };
                demolishSound->SetAutoRemoveMode(REMOVE_NODE);
                demolishSound->Play(RES(Sound, "Samples/Demolish.wav"));
                demolishSound->SetFrequency(demolishSound->GetFrequency() * (.9f + (Random(5) + tone) * .025f));

                estate->Demolish(s);
            }
        }
    }

    return restriction;
}
