/* LucKey Park
// Copyright (C) 2024 LucKey Productions (https://luckey.games)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "tooltext.h"

#include "tool.h"
#include "../gui.h"

ToolText::ToolText(Context* context): Text(context)
{
    SetTags({ "h4" });
    SetOpacity(.9f);

    SetFont(RES(Font, "Fonts/Pontiff.otf"));
    SetTextEffect(TE_SHADOW);
    SetColor(Color::YELLOW);
    SetEffectShadowOffset({ 0, 2 });
    SetEffectColor({ Color::BLACK, .3f });
}

void ToolText::Update(float timeStep)
{
    UIElement::Update(timeStep);

    UI* ui{ GetSubsystem<UI>() };
    const bool overUi{ ui->GetElementAt(ui->GetCursorPosition()) != nullptr };
    Cursor3D* cursor{ GetSubsystem<GUI>()->GetSceneCursor() };

    if (overUi || !cursor || cursor->GetTool() != Tool::Get(TOOL_INFO))
    {
        SetText("");
        return;
    }

    const RayQueryResult& cursorResult{ cursor->GetFreeMoveResult() };
    Node* hitNode{ cursorResult.node_ };
    if (!hitNode)
    {
        SetText("");
        return;
    }

    Node* target{ (hitNode->HasTag(TAG_CAMTARGET)
                ? hitNode
                : hitNode->GetParentWithTag(TAG_CAMTARGET, true)) };

    if (target)
        SetText(target->GetName());
    else
        SetText("");

    IntVector2 pos{ GetSubsystem<UI>()->GetCursorPosition() - IntVector2{ 0, 8 + RoundToInt(GetFontSize()) } };
    pos.x_ = Max(0, Min(GRAPHICS->GetWidth()  - GetWidth()  - GetFontSize() / 4, pos.x_));
    pos.y_ = Max(0, Min(GRAPHICS->GetHeight() - GetHeight() - GetFontSize() / 4, pos.y_));
    SetPosition(pos);
}
