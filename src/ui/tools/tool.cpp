/* LucKey Park
// Copyright (C) 2024 LucKey Productions (https://luckey.games)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "../dash/pages/toolpage.h"
#include "../../landscape/ground.h"
#include "delegate.h"

#include "tool.h"

HashMap<unsigned, SharedPtr<Tool> > Tool::tools_{};
Node* Tool::previewNode_{ nullptr };
ToolText* Tool::tooltip_{ nullptr };

Tool::Tool(Context* context): Object(context),
    type_{ TOOL_NONE },
    name_{},
    snap_{ SNAP_NONE },
    offset_{},
    viewMask_{ M_MAX_UNSIGNED },
    delegate_{ nullptr }
{
}

void Tool::HandleCursorStep(const IntVector3& step)
{
    if (delegate_)
        delegate_->HandleCursorStep(step);
}

void Tool::PlaceCursorOnGround()
{
    World* world{ GetSubsystem<World>() };
    Cursor3D* cursor{ GetSceneCursor() };
    const HeightMap& heightMap{ world->GetGround()->GetHeightMap() };
    const IntVector2 flatCoords{ cursor->GetFlatCoords() };
    const int cursorHeight{ heightMap.GetHeightProfileAt(flatCoords).Max() };
    cursor->SetCoords({ flatCoords.x_, cursorHeight, flatCoords.y_ });
}

void Tool::Pick()
{
    GetSceneCursor()->SetTool(this);

    if (!previewNode_)
    {
        previewNode_ = GetSceneCursor()->GetNode()->CreateChild("Preview");
        previewNode_->SetTemporary(true);
    }

    ClearPreview();
    offset_ = Vector3::ZERO;

    ToolInfo* toolInfo{ GetToolInfo() };
    toolInfo->SetVisible(true);
    toolInfo->SetMinimized(false);
    toolInfo->SetVisiblePage(type_);
    toolInfo->GetCurrentPage()->UpdateContents();
}

void Tool::PickNext()
{
    SharedPtr<Tool> currentTool{ GetCurrent() };
    if (!currentTool || !tools_.Values().Contains(currentTool))
        tools_.Values().Front()->Pick();

    const unsigned currentIndex{ tools_.Values().IndexOf(currentTool) };
    tools_.Values().At((currentIndex + 1u) % tools_.Size())->Pick();
}

void Tool::PickPrevious()
{
    SharedPtr<Tool> currentTool{ GetCurrent() };
    if (!currentTool || !tools_.Values().Contains(currentTool))
        tools_.Values().Back()->Pick();

    const unsigned currentIndex{ tools_.Values().IndexOf(currentTool) };
    tools_.Values().At(currentIndex == 0u ? tools_.Size() - 1u : currentIndex - 1u)->Pick();
}

void Tool::SetDelegate(Delegate* delegate)
{
    if (delegate_ == delegate)
        return;

    if (delegate_)
        EndDelegation();

    delegate_ = delegate;
}

void Tool::EndDelegation()
{
    if (!delegate_)
        return;

    if (delegate_->GetNode())
        delegate_->GetNode()->Remove();

    delegate_ = nullptr;
}

ToolInfo* Tool::GetToolInfo() const
{
    return GetSubsystem<GUI>()->GetDash()->GetToolBox()->GetToolInfo();
}
