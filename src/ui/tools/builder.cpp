/* LucKey Park
// Copyright (C) 2024 LucKey Productions (https://luckey.games)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "../dash/pages/builderpage.h"
#include "../../landscape/ground.h"
#include "trackeditor.h"

#include "builder.h"

void Builder::RegisterObject(Context* context)
{
    context->RegisterFactory<Builder>();
    context->RegisterFactory<TrackEditor>();
}

Builder::Builder(Context* context): Tool(context),
    cyanotype_{},
    construction_{ nullptr },
    buildMode_{ BUILD_NONE },
    beginButton_{ MOUSEB_NONE }
{
    name_ = "Build";
    snap_ = SNAP_CENTER;
    viewMask_ = LAYER(LAYER_GROUND);

    type_ = TOOL_BUILD;
    tools_[type_] = this;
}

void Builder::Pick()
{
    Tool::Pick();
    GetToolInfo()->SetPreferredRatio(4/5.f);
    SetCyanotype({});

    buildMode_ = BUILD_NONE;
    GetSubsystem<World>()->GetGround()->ClearPathPreview();
    HandleMouseMoved();
}

void Builder::HandleClickBegin(int button)
{
    UI* ui{ GetSubsystem<UI>() };
    if (ui->GetElementAt(ui->GetCursorPosition()))
        return;

    beginButton_ = static_cast<MouseButton>(button);
    if (buildMode_ == BUILD_PATH && beginButton_ == MOUSEB_LEFT)
        Apply(false);
}

void Builder::HandleClickEnd()
{
    beginButton_ = MOUSEB_NONE;

    if (buildMode_ == BUILD_PATH)
        GetSubsystem<World>()->GetGround()->ClearPathPreview();
}

void Builder::HandleMouseMoved()
{
    const bool behindUi{ GetSceneCursor()->IsBehindUI() };
    if (buildMode_ == BUILD_PATH)
    {
        if (behindUi)
            GetSubsystem<World>()->GetGround()->ClearPathPreview();
    }
    else
    {
        Tool::HandleMouseMoved();
    }
}

ToolRestriction Builder::Apply(bool rightClick)
{
    if (delegate_)
        return delegate_->Apply(rightClick);

    ToolRestriction restriction{ CheckRestriction() };
    if (cyanotype_.class_ != StringHash::ZERO)
    {
        if (rightClick)
        {
            previewNode_->SetRotation(World::StepRotationToQuaternion(
                                          World::RotationFromDirection(previewNode_->GetWorldDirection()) + 1));
        }
        else if (restriction == TR_NONE)
        {
            if (cyanotype_.buildType_ == BUILD_PATH)
            {
                World* world{ GetSubsystem<World>() };
                world->GetGround()->ClearPathPreview();
                world->SetPath(GetSceneCursor()->GetFlatCoords(), true);
            }
            else
            {
                PlaceStructure();
            }
        }
    }

    return restriction;
}

ToolRestriction Builder::CheckRestriction()
{
    const IntVector2 cursorCoords{ GetSceneCursor()->GetFlatCoords() };
    const HeightMap& heightMap{ GetSubsystem<World>()->GetGround()->GetHeightMap() };
    switch (buildMode_)
    {
    default: return TR_NONE;
    case BUILD_PATH:
    {
        const HeightProfile profileAtCursor{ heightMap.GetHeightProfileAt(cursorCoords) };
        if (!(profileAtCursor.IsFlat() || profileAtCursor.IsSlope()))
            return TR_SPACE;
        else
            return TR_NONE;
    } break;
    case BUILD_MONOLITH:
    {
        /// Assumes rectangle, needs further offset correction
        for (int dx{ 0 }; dx < cyanotype_.size_.x_; ++dx)
        for (int dy{ 0 }; dy < cyanotype_.size_.z_; ++dy)
        {
            const IntVector2 coordOffset{ VectorRoundToInt(Vector2{ dx - (cyanotype_.size_.x_ - 1) / 2 * 1.f, dy - (cyanotype_.size_.z_ - 1) / 2 * 1.f }.
                                                           Rotated(previewNode_->GetWorldRotation().YawAngle())) };

            const HeightProfile profile{ heightMap.GetHeightProfileAt(cursorCoords + coordOffset) };

            if (!profile.IsFlat())
                return TR_SPACE;
        }

        return TR_NONE;
    } break;
    }
}

void Builder::HandleCursorStep(const IntVector3& step)
{
    PlaceCursorOnGround();

    if (buildMode_ == BUILD_PATH)
    {
        Cursor3D* cursor{ GetSceneCursor() };
        if (beginButton_ == MOUSEB_LEFT)
        {
            if (cursor->Clicked())
                Apply(false);
        }
        else
        {
            Ground* ground{ GetSubsystem<World>()->GetGround() };
            if (!cursor->IsBehindUI() && CheckRestriction() == TR_NONE)
                ground->PreviewPath(cursor->GetFlatCoords());
            else
                ground->ClearPathPreview();
        }
    }
    else
    {
        // Keep pencil leaning right
        if (buildMode_ == BUILD_NONE)
        {
            const Vector3 jibDir{ GetSubsystem<Jib>()->GetNode()->GetDirection() };
            if (previewNode_)
                previewNode_->SetRotation(World::StepRotationToQuaternion(World::RotationFromDirection(-jibDir)));
        }

        Tool::HandleCursorStep(step);
    }

    UpdateWireColor();
    HandleMouseMoved();
}

void Builder::UpdateWireColor()
{
    if (CheckRestriction() == TR_NONE)
        SetPreviewMaterial(RES(Material, "Materials/GlowWire.xml"));
    else
        SetPreviewMaterial(RES(Material, "Materials/RedWire.xml"));
}

void Builder::HandleDrag(const Ray& ray)
{
    Cursor3D* cursor{ GetSceneCursor() };
    if (buildMode_ == BUILD_PATH || buildMode_ == BUILD_NONE)
    {
        cursor->FreeMove(ray);
        return;
    }

    const Vector2 cursorPos{ GetSubsystem<UI>()->GetCursorPosition() };

    if (cursor->GetDragMode() == DM_NONE)
    {
        if ((cursorPos - GetSceneCursor()->GetClickScreenPos()).Length() > GUI::sizes_.margin_ * 2)
        {
            cursor->SetDragMode(DM_PLANE);
            previewNode_->SetPosition(offset_ + Vector3::UP * .25f * HEIGHTSTEP);
        }
        else
        {
            return;
        }
    }

    const Vector3 delta{ cursor->DragDelta(ray) };
    if (delta.x_ == M_INFINITY)
        return;

    char d{ World::RotationFromDirection(delta) };
    previewNode_->SetRotation(World::StepRotationToQuaternion(d));

//    offset_ = previewNode_->GetRotation().Inverse() * Structure::CenterOffset(cyanotype_.size_);
    offset_ = {};//previewNode_->GetRotation().Inverse() * Structure::CenterOffset(cyanotype_.size_);

    if (delegate_)
    {
        delegate_->HandleCursorTurned(d);
        UpdatePreview();

        return;
    }

    if (buildMode_ == BUILD_MONOLITH && cyanotype_.minSize_ != cyanotype_.maxSize_)
    {
        const IntVector3 dragStep{ VectorRoundToInt(delta / GRID_BLOCK + VectorSign(delta) * .5f) };
        IntVector3 size{ VectorAbs(dragStep) };
        if (size.z_ > size.x_)
            Swap(size.x_, size.z_);
        size = VectorMin(cyanotype_.maxSize_, VectorMax(cyanotype_.minSize_, size));

        cyanotype_.size_ = size;

        if (cyanotype_.buildType_ != BUILD_MONOLITH)
            previewNode_->SetRotation(World::StepRotationToQuaternion(d));
        else
            previewNode_->SetRotation(World::StepRotationToQuaternion(d - 1));

        UpdatePreview();
    }
}

void Builder::HandleDragEnd()
{
    GetSceneCursor()->Unclick();
    UpdatePreview();
    GetToolInfo()->GetCurrentPage()->UpdateContents();
    GetSceneCursor()->MouseMoved();
}

void Builder::EndDelegation()
{
    Tool::EndDelegation();

    SetCyanotype(cyanotype_);
}

void Builder::SetCyanotype(const StructureInfo& blueprint)
{
    if (blueprint.identifier_ == StringHash::ZERO)
    {
        cyanotype_ = {};
        SetBuildMode(BUILD_NONE);
    }
    else
    {
        cyanotype_ = blueprint;

        const BuildType buildType{ cyanotype_.buildType_ };
        if (buildType == BUILD_NONE)
        {
            SetCyanotype({});
            return;
        }

        offset_ = Structure::CenterOffset(cyanotype_.size_);
        if (buildType == BUILD_PATH)
            SetBuildMode(BUILD_PATH);
        else
            SetBuildMode(BUILD_MONOLITH);
    }

    UpdatePreview();

    const Vector3 jibDir{ GetSubsystem<Jib>()->GetNode()->GetDirection() };
    previewNode_->SetRotation(World::StepRotationToQuaternion(World::RotationFromDirection(-jibDir)));
//    offset_ = previewNode_->GetRotation() * Structure::CenterOffset(cyanotype_.size_);
//    previewNode_->SetPosition(offset_);
}

void Builder::PlaceStructure()
{
    Estate* estate{ GetSubsystem<Estate>() };
    construction_ = estate->Construct(cyanotype_);
    construction_->SetCoords(GetSceneCursor()->GetCoords());
    construction_->SetStepRotation(World::RotationFromDirection(previewNode_->GetWorldDirection()));
    construction_->PlayBuildSound();

    SetBuildMode(cyanotype_.buildType_);
}

void Builder::SetBuildMode(BuildType buildMode)
{
    if (buildMode_ == buildMode)
        return;

    buildMode_ = buildMode;

    switch (buildMode_) {
    default: {
        EndDelegation();
        ClearPreview();
    } break;
    case BUILD_TRACK:
    {
        if (!construction_)
            return;

        SetDelegate(construction_->GetNode()->CreateChild("Editor")->CreateComponent<TrackEditor>());

        if (delegate_)
        {
            previewNode_->RemoveAllChildren();
            StaticModel* preview{ previewNode_->GetOrCreateComponent<StaticModel>() };

            preview->SetModel(RES(Model, "Models/UI/Arrow.mdl"));
            preview->SetMaterial(RES(Material, "Materials/GlowWire.xml"));
            preview->SetViewMask(LAYER(LAYER_UI));
            previewNode_->SetScale(.5f * GRID_BLOCK);
        }
    } break;
    }

    ToolInfo* toolInfo{ GetToolInfo() };
    toolInfo->SetPreferredRatio((cyanotype_.identifier_ ? 3/2.f : 4/5.f));
    toolInfo->GetCurrentPage()->UpdateContents();
}

void Builder::UpdatePreview()
{
    if (delegate_)
    {
        delegate_->UpdatePreview();
        return;
    }

    previewNode_->SetPosition(offset_);
    StaticModel* preview{ previewNode_->GetOrCreateComponent<StaticModel>() };

    if (buildMode_ == BUILD_NONE)
    {
        preview->SetModel(RES(Model, "Models/UI/Pencil.mdl"));
        preview->SetViewMask(LAYER(LAYER_UI));
    }
    else
     {

        if (cyanotype_.class_ != StringHash::ZERO)
        {
            const Quaternion rot{ previewNode_->GetRotation() };
            ClearPreview();
            SharedPtr<Structure> structure{ DynamicCast<Structure>(context_->CreateObject(cyanotype_.class_)) };
            if (structure)
            {
                structure->SetInfo(cyanotype_);
                structure->SetNode(previewNode_->CreateChild());
                structure->GetNode()->SetPosition({});
                structure->SetCoords(GetSceneCursor()->GetCoords() + GetSceneCursor()->Clicked() * (cyanotype_.buildType_ == BUILD_MONOLITH) * (cyanotype_.size_ - IntVector3::ONE) / 2 );
            }
            previewNode_->SetRotation(rot);
        }
        else
        {
            previewNode_->RemoveAllChildren();
            preview->SetModel(RES(Model, cyanotype_.model_));
        }
    }

    UpdateWireColor();
}

void Builder::SetPreviewMaterial(Material* mat)
{
    if (!previewNode_)
        return;

    PODVector<StaticModel*> models{};
    previewNode_->GetDerivedComponents<StaticModel>(models, true);
    for (StaticModel* sm: models)
    {
        sm->SetMaterial(mat);
        sm->SetViewMask(LAYER(LAYER_UI));
        sm->SetCastShadows(false);
    }
}
