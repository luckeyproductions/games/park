/* LucKey Park
// Copyright (C) 2024 LucKey Productions (https://luckey.games)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef OPTIONSMENU_H
#define OPTIONSMENU_H

#include "../../mastercontrol.h"

class Setting: public Object
{
    DRY_OBJECT(Setting, Object);

public:
    Setting(Context* context): Object(context),
        element_{ nullptr },
        name_{},
        value_{}
    {}

    void SetElement(UIElement* element)
    {
        element_ = element;
    }

    UIElement* GetPanel() { return element_->GetParent(); }
    UIElement* GetValueElement() const { return valueElement_; }

    void Initialize(const String& name, const Variant& value);
    void HandleToggled(StringHash eventType, VariantMap& eventData);
    void UpdateSize();

private:
    UIElement* element_;
    UIElement* valueElement_;
    String name_;
    Variant value_;
};

struct Tab: public Object
{
    DRY_OBJECT(Tab, Object);

public:
    Tab(Context* context): Object(context),
        settings_{},
        button_{ nullptr },
        panel_{ nullptr }
    {
    }

    void SetButton(Button* button);
    Button* GetButton() const { return button_; }
    void AddSetting(Setting* setting) { settings_.Push(setting); }
    const PODVector<Setting*>& GetSettings() const { return settings_; }
    void CreatePanel();
    Window* GetPanel() const { return panel_; }
    void SetPanelVisible(bool visible)
    {
        if (!panel_)
            return;

        panel_->SetVisible(visible);

        if (visible)
            button_->SetFocus(true);
    }

private:
    PODVector<Setting*> settings_;
    Button* button_;
    Window* panel_;
};

class SettingsMenu: public UIElement
{
    enum SettingsCategory{ SC_GRAPHICS = 0, SC_AUDIO, SC_KEYBOARD, SC_MOUSE, SC_GAMEPAD };

    DRY_OBJECT(SettingsMenu, UIElement);

public:
    static void RegisterObject(Context* context);

    SettingsMenu(Context* context);

    void Show();
    void Hide();

    void SettingChanged(const String& name, const Variant& value);

    int GetRestoreWidth() const { return windowSize_.x_; }
    int GetRestoreHeight() const { return windowSize_.y_; }

    void SetCurrentTabIndex(unsigned index)
    {
        index = Clamp(index, 0u, tabs_.Size() - 1u);

        for (unsigned t{ 0u }; t < tabs_.Size(); ++t)
            tabs_.At(t)->SetPanelVisible(t == index);
    }

    unsigned GetCurrentTabIndex() const
    {
        for (unsigned t{ 0u }; t < tabs_.Size(); ++t)
        {
            if (tabs_.At(t)->GetPanel()->IsVisible())
                return t;
        }

        return 0u;
    }

    void NextTab()
    {
        SetCurrentTabIndex((GetCurrentTabIndex() + 1) % tabs_.Size());
    }

    void PreviousTab()
    {
        unsigned index{ GetCurrentTabIndex() };
        SetCurrentTabIndex(index == 0u ? tabs_.Size() - 1 : index - 1);
    }

private:
    void CreateTab(const String& text);
    void AddSetting(Tab* tab, const String& name, const Variant& value);
    void HandleTabClicked(StringHash eventType, VariantMap& eventData);
    void HandleSizesChanged(StringHash eventType, VariantMap& eventData);
    void UpdateTabSize();

    const Vector<String> tabNames_;
    Vector<SharedPtr<Tab>> tabs_;
    Vector<SharedPtr<Setting>> settings_;
    IntVector2 windowSize_;
};

#endif // OPTIONSMENU_H
