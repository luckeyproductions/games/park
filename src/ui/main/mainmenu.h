/* LucKey Park
// Copyright (C) 2024 LucKey Productions (https://luckey.games)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef MAINMENU_H
#define MAINMENU_H

#include "../../mastercontrol.h"


class MainMenu: public UIElement
{
    DRY_OBJECT(MainMenu, UIElement)

public:
    MainMenu(Context* context);

    void Show();

private:
    void CreateLogo();
    void CreateButton(const String& text);
    void ConnectButtons();
    Text* GetButtonText(StringHash button) { return buttons_[button]->GetChildDynamicCast<Text>(0); }

    void HandleButtonNewClicked(StringHash eventType, VariantMap& eventData);
    void HandleButtonLoadClicked(StringHash eventType, VariantMap& eventData);
    void HandleButtonSettingsClicked(StringHash, VariantMap&);
    void HandleButtonExitClicked(StringHash eventType, VariantMap& eventData);
    void HandleSizesChanged(StringHash eventType, VariantMap& eventData);

    UIElement* home_;

    Sprite* logo_;
    HashMap<StringHash, Button*> buttons_;
    void UpdateLogoSize();
    void UpdateButtonSize();

/*

Post-scarcity tooltips:
"What is money?"
"Not buying it"
"Freedom knows no paywalls"
"Everything for nothing"
"Fun rules"

*/
};

#endif // MAINMENU_H
