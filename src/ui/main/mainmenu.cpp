/* LucKey Park
// Copyright (C) 2024 LucKey Productions (https://luckey.games)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "../gui.h"

#include "../../game.h"

MainMenu::MainMenu(Context* context): UIElement(context),
    home_{ nullptr },
    logo_{ nullptr },
    buttons_{}
{
    SetDefaultStyle(RES(XMLFile, "UI/DefaultStyle.xml"));

    home_ = CreateChild<UIElement>("Home");
    home_->SetPosition( GetSubsystem<Graphics>()->GetWidth() / 2, 0);

    CreateLogo();

    for (String text: { "New", "Load", "Settings", "Exit" })
        CreateButton(text);

    Vector<Button*> buttons{ buttons_.Values() };
    for (unsigned b{ 0u }; b < buttons.Size(); ++b)
    {
        Button* button{ buttons.At(b) };
        if (b == 0u)
        {
            button->SetVar("Up", buttons.At(buttons.Size() - 1u));
            button->AddTag("First");
        }
        else
        {
            button->SetVar("Up", buttons.At(b - 1u));

            if (b == buttons.Size() - 1u)
                button->AddTag("Last");
        }

        button->SetVar("Down", buttons.At((b + 1u) % buttons.Size()));
    }

    ConnectButtons();
    UpdateButtonSize();

    SubscribeToEvent(E_UISIZESCHANGED, DRY_HANDLER(MainMenu, HandleSizesChanged));
}

void MainMenu::Show()
{
    for (Button* b: buttons_.Values())
        b->SetVisible(true);

    const bool inMain{ GetSubsystem<Game>()->GetStatus() == GS_MAIN };
    GetButtonText("New") ->SetText(inMain ? "New"  : "Resume");    /// TRANSLATE
    GetButtonText("Load")->SetText(inMain ? "Load" : "Save");      /// TRANSLATE
    GetButtonText("Exit")->SetText(inMain ? "Exit" : "Main menu"); /// TRANSLATE
    SetVisible(true);
}

void MainMenu::CreateLogo()
{
    logo_ = home_->CreateChild<Sprite>("Logo");
    logo_->SetTexture(RES(Texture2D, "UI/Logo.png"));
    logo_->SetBlendMode(BLEND_ALPHA);

    UpdateLogoSize();
}

void MainMenu::CreateButton(const String& text)
{
    UpdateButtonSize();

    Button* button{ home_->CreateChild<Button>() };
    button->SetName(text + "Button");
    button->SetStyleAuto();
    Text* buttonText{ GetSubsystem<GUI>()->CreateButtonText(button, 1) };
    buttonText->SetText(text); /// TRANSLATE
    buttons_.Insert({ text, button });
}

void MainMenu::ConnectButtons()
{
    SubscribeToEvent(buttons_["New"], E_CLICKEND, DRY_HANDLER(MainMenu, HandleButtonNewClicked));
    SubscribeToEvent(buttons_["Load"], E_CLICKEND, DRY_HANDLER(MainMenu, HandleButtonLoadClicked));
    SubscribeToEvent(buttons_["Settings"], E_CLICKEND, DRY_HANDLER(MainMenu, HandleButtonSettingsClicked));
    SubscribeToEvent(buttons_["Exit"], E_CLICKEND, DRY_HANDLER(MainMenu, HandleButtonExitClicked));
}

void MainMenu::HandleButtonNewClicked(StringHash /*eventType*/, VariantMap& /*eventData*/)
{
    const bool inMain{ GetSubsystem<Game>()->GetStatus() == GS_MAIN };

    if (inMain)
        GetSubsystem<Game>()->StartNew();
    else
        GetSubsystem<Game>()->Resume();
}

void MainMenu::HandleButtonLoadClicked(StringHash /*eventType*/, VariantMap& /*eventData*/)
{
    const bool inMain{ GetSubsystem<Game>()->GetStatus() == GS_MAIN };
    if (inMain)
        GetSubsystem<Game>()->Load();
    else
        GetSubsystem<Game>()->Save();
}

void MainMenu::HandleButtonSettingsClicked(StringHash /*eventType*/, VariantMap& /*eventData*/)
{
    GetSubsystem<GUI>()->ShowSettingsMenu();

    for (Button* b: buttons_.Values())
        b->SetVisible(false);
}

void MainMenu::HandleButtonExitClicked(StringHash /*eventType*/, VariantMap& /*eventData*/)
{
    Game* game{ GetSubsystem<Game>() };
    if (game->GetStatus() == GS_MAIN)
        game->Exit();
    else
        game->BackToMain();
}

void MainMenu::HandleSizesChanged(StringHash /*eventType*/, VariantMap& /*eventData*/)
{
    UpdateLogoSize();
    UpdateButtonSize();

    home_->SetPosition( GetSubsystem<Graphics>()->GetWidth() / 2, 0);
}

void MainMenu::UpdateLogoSize()
{
    logo_->SetSize(GUI::sizes_.logo_ * IntVector2::ONE);
    logo_->SetPosition(-GUI::sizes_.logo_ / 96 -GUI::sizes_.logo_ / 2, -GUI::sizes_.logo_ / 16);
}

void MainMenu::UpdateButtonSize()
{
    const int buttonWidth{ GUI::sizes_.centralColumnWidth_ };
    const int buttonHeight{ GUI::sizes_.text_[1] + 5 * GUI::sizes_.margin_ };

    Vector<Button*> buttons{ buttons_.Values() };
    for (unsigned b{ 0u }; b < buttons.Size(); ++b)
    {
        Button* button{ buttons.At(b) };
        button->SetWidth(buttonWidth);
        button->SetHeight(buttonHeight);
        button->SetPosition(-buttonWidth / 2, GUI::sizes_.logo_ * 13/15 + (buttonHeight + 4 * GUI::sizes_.margin_) * b);
    }
}
