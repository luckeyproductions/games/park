/* LucKey Park
// Copyright (C) 2024 LucKey Productions (https://luckey.games)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "../gui.h"
#include "../../game.h"

#include "settingsmenu.h"

void SettingsMenu::RegisterObject(Context* context)
{
    context->RegisterFactory<SettingsMenu>();
    context->RegisterFactory<Setting>();
    context->RegisterFactory<Tab>();
}

SettingsMenu::SettingsMenu(Context* context): UIElement(context),
    tabNames_{ "Graphics", "Audio", "Keyboard", "Mouse", "Gamepad" },
    tabs_{},
    settings_{},
    windowSize_{ 640, 480 }
{
    FileSystem* fs{ GetSubsystem<FileSystem>() };
    SharedPtr<JSONFile> settingsFile{ context_->CreateObject<JSONFile>() };
    const String settingsPath{ MC->GetSettingsPath() };
    if (fs->FileExists(settingsPath) && settingsFile->LoadFile(settingsPath))
    {
        const JSONValue& root{ settingsFile->GetRoot() };
        const JSONArray& restoreSize{ root.Get("Restore").GetArray() };

        windowSize_ = { restoreSize.At(0).GetInt(), restoreSize.At(1).GetInt() };
    }

    SetDefaultStyle(RES(XMLFile, "UI/DefaultStyle.xml"));

    for (String text: tabNames_)
        CreateTab(text);

    for (unsigned t{ 0u }; t < tabs_.Size(); ++t)
    {
        Tab* tab{ tabs_.At(t) };
        Button* tabButton{ tab->GetButton() };

        tabButton->SetVar("Left",  tabs_.At(t == 0u ? tabs_.Size() - 1u : t - 1u)->GetButton());
        tabButton->SetVar("Right", tabs_.At((t + 1u) % tabs_.Size())->GetButton());

        const PODVector<Setting*> settings{ tab->GetSettings() };
        if (!settings.IsEmpty())
        {
            tabButton->SetVar("Down", settings.Front()->GetValueElement());
            tabButton->SetVar("Up",   settings.Back()->GetValueElement());
        }
    }

    SetCurrentTabIndex(0u);

    UpdateTabSize();
    SetVisible(false);

    SubscribeToEvent(E_UISIZESCHANGED, DRY_HANDLER(SettingsMenu, HandleSizesChanged));
}

void SettingsMenu::CreateTab(const String& text)
{
    SharedPtr<Tab> tab{ context_->CreateObject<Tab>() };
    Button* tabButton{ CreateChild<Button>() };
    tab->SetButton(tabButton);
    SubscribeToEvent(tabButton, E_CLICKEND, DRY_HANDLER(SettingsMenu, HandleTabClicked));

    Text* buttonText{ GetSubsystem<GUI>()->CreateButtonText(tabButton, 5) };
    buttonText->SetText(text); /// TRANSLATE
    tab->CreatePanel();

    switch (tabs_.Size())
    {
    case SC_GRAPHICS:
    {
        Graphics* graphics{ GetSubsystem<Graphics>() };
        RenderPath* renderPath{ GetSubsystem<Renderer>()->GetDefaultRenderPath() };

        AddSetting(tab, "Fullscreen", { graphics->GetFullscreen() });
        AddSetting(tab, "V-Sync", { graphics->GetVSync() });
        AddSetting(tab, "Anti-aliasing", { renderPath->IsEnabled("FXAA3") });
        AddSetting(tab, "Bloom", { renderPath->IsEnabled("BloomHDR") });
        settings_.Back()->GetValueElement()->SetVar("Down", tab->GetButton());
    }
    break;
    case SC_AUDIO:
    {
        Audio* audio{ GetSubsystem<Audio>() };

        AddSetting(tab, "Music", { audio->GetMasterGain(SOUND_MUSIC) != 0.f });
        settings_.Back()->GetValueElement()->SetVar("Down", tab->GetButton());
    }
    default:
    break;
    }

    tabs_.Push(tab);
}

void SettingsMenu::AddSetting(Tab* tab, const String& name, const Variant& value)
{
    SharedPtr<Setting> setting{ context_->CreateObject<Setting>() };
    setting->SetElement(tab->GetPanel()->CreateChild<UIElement>());
    setting->Initialize(name, value);

    Setting* previousSetting{ (settings_.IsEmpty() ? nullptr : settings_.Back()) };
    if (previousSetting && previousSetting->GetPanel() == tab->GetPanel())
    {
        setting->GetValueElement()->SetVar("Up", previousSetting->GetValueElement());
        previousSetting->GetValueElement()->SetVar("Down", setting->GetValueElement());
    }
    else
    {
        setting->GetValueElement()->SetVar("Up", tab->GetButton());
    }

    tab->AddSetting(setting);
    settings_.Push(setting);
}

void SettingsMenu::SettingChanged(const String& name, const Variant& value)
{
    if (name == "Music")
    {
        GetSubsystem<Audio>()->SetMasterGain(SOUND_MUSIC, value.GetBool());
    }
    else if (name == "Fullscreen")
    {
        Graphics* graphics{ GetSubsystem<Graphics>() };
        IntVector3 resolution{ graphics->GetResolutions(0).Front() };

        if (!value.GetBool())
        {
            graphics->SetMode(windowSize_.x_, windowSize_.y_, false, graphics->GetBorderless(), true, graphics->GetHighDPI(), value.GetBool(), graphics->GetTripleBuffer(), graphics->GetMultiSample(), graphics->GetMonitor(), graphics->GetRefreshRate());
        }
        else
        {
            windowSize_ = graphics->GetSize();

            graphics->SetMode(resolution.x_, resolution.y_, true, graphics->GetBorderless(), false, graphics->GetHighDPI(), value.GetBool(), graphics->GetTripleBuffer(), graphics->GetMultiSample(), graphics->GetMonitor(), graphics->GetRefreshRate());
        }

    }
    else if (name == "V-Sync")
    {
        Graphics* graphics{ GetSubsystem<Graphics>() };

        graphics->SetMode(graphics->GetWidth(), graphics->GetHeight(), graphics->GetFullscreen(), graphics->GetBorderless(), graphics->GetResizable(), graphics->GetHighDPI(), value.GetBool(), graphics->GetTripleBuffer(), graphics->GetMultiSample(), graphics->GetMonitor(), graphics->GetRefreshRate());
    }
    else if (name == "Anti-aliasing")
    {
        Renderer* renderer{ GetSubsystem<Renderer>() };

        RenderPath* renderPath{ renderer->GetViewport(0)->GetRenderPath() };
        renderPath->SetEnabled("FXAA3", value.GetBool());
    }
    else if (name == "Bloom")
    {
        Renderer* renderer{ GetSubsystem<Renderer>() };

        RenderPath* renderPath{ renderer->GetViewport(0)->GetRenderPath() };
        renderPath->SetEnabled("BloomHDR", value.GetBool());
    }
}

void SettingsMenu::Show()
{
    SetVisible(true);
    SetCurrentTabIndex(GetCurrentTabIndex());
}

void SettingsMenu::Hide()
{
    SetVisible(false);
}

void SettingsMenu::HandleTabClicked(StringHash /*eventType*/, VariantMap& eventData)
{
    for (SharedPtr<Tab> tab: tabs_)
        tab->SetPanelVisible(tab->GetButton() == eventData[ClickEnd::P_ELEMENT].GetPtr());
}

void SettingsMenu::HandleSizesChanged(StringHash /*eventType*/, VariantMap& /*eventData*/)
{
    UpdateTabSize();
}

void SettingsMenu::UpdateTabSize()
{
    const int margin{ GUI::sizes_.margin_ };
    const int tabWidth{ static_cast<int>((GUI::sizes_.screen_.x_ - margin * (tabs_.Size() + 1)) / tabs_.Size()) };
    const int tabHeight{ GUI::sizes_.text_[5] + 4 * margin };
    const int fromTop{ GUI::sizes_.logo_ * 6/7 + GUI::sizes_.text_[5] + 5 * GUI::sizes_.margin_ };

    for (unsigned t{ 0 }; t < tabs_.Size(); ++t)
    {
        Button* button{ tabs_.At(t)->GetButton() };
        Window* panel{ tabs_.At(t)->GetPanel() };

        if (!button || !panel)
            continue;

        button->SetWidth(tabWidth);
        button->SetHeight(tabHeight);
        button->SetPosition(margin + (tabWidth + margin) * t, GUI::sizes_.logo_ * 6/7);

        panel->SetSize(GUI::sizes_.screen_.x_ - GUI::sizes_.margin_ * 2,
                       GUI::sizes_.screen_.y_ - fromTop - GUI::sizes_.margin_);
        panel->SetPosition(GUI::sizes_.margin_, fromTop);
    }

    for (SharedPtr<Setting> setting: settings_)
    {
        setting->UpdateSize();
    }
}

void Tab::SetButton(Button* button)
{
    button_ = button;
    button_->SetStyleAuto();
}

void Tab::CreatePanel()
{
    panel_ = button_->GetParent()->CreateChild<Window>();
    panel_->SetStyleAuto();
    panel_->SetColor(Color::WHITE.Transparent(.75f));
    panel_->SetVisible(false);
}

void Setting::Initialize(const String& name, const Variant& value)
{
    name_ = name;
    value_ = value;

    if (!element_)
        return;

    Text* text{ GetSubsystem<GUI>()->CreateButtonText(element_, 5) };
    text->SetText(name); /// TRANSLATE
    text->SetAlignment(HA_LEFT, VA_CENTER);

    switch (value_.GetType()) {
    case VAR_NONE: default: break;
    case VAR_BOOL:
    {
        CheckBox* box{ element_->CreateChild<CheckBox>() };
        box->SetName("CheckBox");
        box->SetChecked(value_.GetBool());
        box->SetStyleAuto();
        valueElement_ = box;

        SubscribeToEvent(box, E_TOGGLED, DRY_HANDLER(Setting, HandleToggled));
    }
    break;
    }

    element_->SetWidth(GUI::sizes_.screen_.x_ / 3);
    element_->SetLayout(LM_HORIZONTAL, GUI::sizes_.margin_ * 2);

    UpdateSize();
}

void Setting::HandleToggled(StringHash eventType, VariantMap& eventData)
{
    const bool value{ static_cast<CheckBox*>(eventData[Toggled::P_ELEMENT].GetPtr())->IsChecked() };

    if (value_ == value)
        return;

    value_ = value;
    GetSubsystem<SettingsMenu>()->SettingChanged(name_, value_);
}

void Setting::UpdateSize()
{
    UIElement* parent{ element_->GetParent() };
    const int panelWidth{  parent->GetWidth()  - GUI::sizes_.margin_ * 2 };
    const int panelHeight{ parent->GetHeight() - (GUI::sizes_.text_[5] + GUI::sizes_.margin_ * 4) };
    const int row{ static_cast<int>(parent->GetChildren().IndexOf(SharedPtr<UIElement>{ element_ })) };
    const int rowHeight{ Max(17, GUI::sizes_.text_[5] + GUI::sizes_.margin_) };
    int rows{ Max(1, panelHeight / rowHeight) };
    const int columns{ Max(Max(1, element_->GetParent()->GetNumChildren() / rows), panelWidth / (320 + GUI::sizes_.margin_ * 2)) };
    if (columns == 1)
        rows = M_MAX_INT;
    const int column{ row / rows };

    element_->SetPosition(GUI::sizes_.text_[5] + GUI::sizes_.margin_ * 2 + panelWidth / columns * column, rowHeight * (row % rows) + GUI::sizes_.text_[5] / 2);
    element_->SetWidth((panelWidth - columns * GUI::sizes_.text_[5] * 2) / columns);
    element_->UpdateLayout();
}
