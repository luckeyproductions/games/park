/* LucKey Park
// Copyright (C) 2024 LucKey Productions (https://luckey.games)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef TABLE_H
#define TABLE_H

#include "../mastercontrol.h"


class Table: public UIElement
{
    DRY_OBJECT(Table, UIElement);

public:
    Table(Context* context);

    void AddElement(UIElement* elem, int row, int col)
    {
        if (!elem)
            return;

        AddChild(elem);
        elem->AddTag("CELL");
        elem->SetVar("ROW", row);
        elem->SetVar("COLUMN", col);
    }

    void UpdateContents()
    {

    }
};

#endif // TABLE_H
