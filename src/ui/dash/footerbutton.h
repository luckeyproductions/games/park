/* LucKey Park
// Copyright (C) 2024 LucKey Productions (https://luckey.games)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef FOOTERBUTTON_H
#define FOOTERBUTTON_H

#include "../../mastercontrol.h"

class FooterButton: public Button
{
    DRY_OBJECT(FooterButton, Button);

public:
    FooterButton(Context* context);

    void SetIcon(Texture2D* icon) { icon_->SetTexture(icon); }
    void SetIconColor(const Color& color) { icon_->SetColor(color); }

    void SetIndex(int id)
    {
        const int dx{ 256 * id };

        icon_->SetImageRect({ dx, 0, 256 + dx, 256 });
        SetVar("ID", id);
    }

    void Hover()
    {
        SetColor(Color::YELLOW.Transparent(.42f));
//        icon_->SetPosition(GUI::sizes_.margin_, -GUI::sizes_.margin_);
    }

    void Unhover()
    {
        SetColor(Color::WHITE.Transparent(.23f));
//        icon_->SetPosition(GUI::sizes_.margin_, GUI::sizes_.margin_);
    }

    void UpdateSize();

private:
    void HandleClicked(StringHash eventType, VariantMap& eventData);

    Sprite* icon_;
};

#endif // FOOTERBUTTON_H
