/* LucKey Park
// Copyright (C) 2024 LucKey Productions (https://luckey.games)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "../gui.h"
#include "pages/builderpage.h"
#include "pages/dozerpage.h"
#include "pages/diggerpage.h"
#include "pages/lookerpage.h"

#include "toolinfo.h"

void ToolInfo::RegisterObject(Context* context)
{
    context->RegisterFactory<ToolInfo>();
    BuilderPage::RegisterObject(context);
    context->RegisterFactory<DozerPage>();
    DiggerPage::RegisterObject(context);
    context->RegisterFactory<LookerPage>();
}

ToolInfo::ToolInfo(Context* context): Window(context),
    ratio_{ 1.f },
    snapped_{ true },
    minimized_{ false },
    snapPosition_{},
    minimizeButton_{ nullptr },
    pages_{}
{
    SetTexture(RES(Texture2D, "UI/UI.png"));
    SetImageRect({ 48, 0, 64, 16 });
    SetBorder({ 4, 4, 4, 4 });
    SetResizeBorder({ 8, 8, 8, 8 });
    SetColor(Color::CHARTREUSE.Transparent(.55f));
    SetVisible(false);

    SetAlignment(HA_LEFT, VA_TOP);

    for (int t{ TOOL_BUILD }; t < TOOL_ALL; ++t)
        AddPage(ToolPage::CreatePage(static_cast<ToolType>(t), this));

    CreateMinimizeButton();

    SubscribeToEvent(this, E_CLICK,         DRY_HANDLER(ToolInfo, HandleClick));
    SubscribeToEvent(this, E_DRAGMOVE,      DRY_HANDLER(ToolInfo, HandleDragMove));
    SubscribeToEvent(this, E_DRAGBEGIN,     DRY_HANDLER(ToolInfo, HandleDragBegin));
    SubscribeToEvent(this, E_DRAGCANCEL,    DRY_HANDLER(ToolInfo, HandleDragCancel));
}


void ToolInfo::CreateMinimizeButton()
{
    minimizeButton_ = CreateChild<Button>();
    minimizeButton_->SetTexture(RES(Texture2D, "UI/Header.png"));
    minimizeButton_->SetImageRect({ 256, 0, 512, 256 });
    minimizeButton_->SetAlignment(HA_RIGHT, VA_TOP);
    minimizeButton_->SetBlendMode(BLEND_ADDALPHA);
    minimizeButton_->SetColor(Color::CHARTREUSE.Lerp(Color::GREEN, 1/3.f).Transparent(1/3.f));
    SubscribeToEvent(minimizeButton_, E_CLICKEND, DRY_HANDLER(ToolInfo, HandleMinimizeButtonClicked));
}

void ToolInfo::UpdateSize(int width)
{
    GUI* gui{ GetSubsystem<GUI>() };
    ToolBox* toolBox{ (gui ? gui->GetDash()->GetToolBox() : nullptr) };
    const int m{ GUI::sizes_.margin_ };
    const int header{ GUI::sizes_.text_[3] + m * 2 };
    const int fh{ (GetCurrentPage() ? GetCurrentPage()->GetFooterHeight(minimized_) : 0) };
    const int height{ (!minimized_ ? RoundToInt(width / (ratio_ * (1 + GUI::Portrait())))
                                   : (header + fh)) };
    const int xPos{ (toolBox ? toolBox->GetPosition().x_ - (GUI::Portrait() * width / 2)
                             : GetPosition().x_) };
    const int yPos{ (toolBox ? toolBox->GetPosition().y_ + 2 - height
                             : GetPosition().y_) };
    SetSize(width, height);

    snapPosition_ = { xPos, yPos };
    if (snapped_)
        SetPosition(snapPosition_);

    for (ToolPage* p: pages_)
        p->UpdateSize(minimized_);

    minimizeButton_->SetPosition(-2 * m, 0);
    minimizeButton_->SetSize(header, header);
}

void ToolInfo::SetPreferredRatio(float ratio)
{
    ratio_ = ratio;
    UpdateSize(GetWidth());
}

void ToolInfo::SetMinimized(bool minimized)
{
    minimized_ = minimized;
    UpdateSize(GetWidth());

    ToolPage* currentPage{ GetCurrentPage() };
    if (currentPage)
        currentPage->UpdateSize(minimized_);
}

void ToolInfo::SetVisiblePage(unsigned page)
{
    for (unsigned p{ 0u }; p < pages_.Size(); ++p)
    {
        bool current{ p == page };

        if (current)
        {
            pages_.At(p)->UpdateSize(minimized_);
            pages_.At(p)->SetVisible(true);
        }
        else
        {
            pages_.At(p)->SetVisible(false);
        }
    }
}

ToolPage* ToolInfo::GetCurrentPage() const
{
    for (ToolPage* p: pages_)
        if (p->IsVisible())
            return p;

    return nullptr;
}

void ToolInfo::HandleMinimizeButtonClicked(StringHash /*eventType*/, VariantMap& eventData)
{
    if (eventData[ClickEnd::P_BEGINELEMENT].GetPtr() != eventData[ClickEnd::P_ELEMENT].GetPtr())
        return;

    minimized_ = !minimized_;

    UpdateSize(GetWidth());
}

void ToolInfo::HandleClick(StringHash /*eventType*/, VariantMap& eventData)
{
    Button* element{ (Button*)eventData[Click::P_ELEMENT].GetVoidPtr() };

    element->BringToFront();
}

void ToolInfo::HandleDragBegin(StringHash /*eventType*/, VariantMap& eventData)
{
    using namespace DragBegin;

    Button* element{ (Button*)eventData[P_ELEMENT].GetVoidPtr() };

    const int lx{ eventData[P_X].GetInt() };
    const int ly{ eventData[P_Y].GetInt() };
    const IntVector2 p{ element->GetPosition() };

    element->SetVar("START", p);
    element->SetVar("DELTA", IntVector2(p.x_ - lx, p.y_ - ly));

    const int buttons{ eventData[P_BUTTONS].GetInt() };
    element->SetVar("BUTTONS", buttons);
}

void ToolInfo::HandleDragMove(StringHash /*eventType*/, VariantMap& eventData)
{
    using namespace DragBegin;

    Button* element{ (Button*)eventData[P_ELEMENT].GetVoidPtr() };
    const int buttons{ eventData[P_BUTTONS].GetInt() };
    const IntVector2 d{ element->GetVar("DELTA").GetIntVector2() };
    int X{ eventData[P_X].GetInt() + d.x_ };
    const int Y{ eventData[P_Y].GetInt() + d.y_ };
    const int BUTTONS{ element->GetVar("BUTTONS").GetInt() };

    if (buttons == BUTTONS)
    {
        const float snapMargin{ GUI::sizes_.margin_ * 4.f };
        const IntVector2 pos{ X, Y };
        if ((pos - snapPosition_).Length() <= snapMargin)
        {
            element->SetPosition(snapPosition_);
            snapped_ = true;
        }
        else
        {
            if (pos.Length() <= snapMargin)
                element->SetPosition({});
            else
                element->SetPosition(pos);

            snapped_ = false;
        }
    }
}

void ToolInfo::HandleDragCancel(StringHash eventType, VariantMap& eventData)
{
    using namespace DragBegin;

    Button* element{ (Button*)eventData[P_ELEMENT].GetVoidPtr() };
    const IntVector2 P{ element->GetVar("START").GetIntVector2() };

    element->SetPosition(P);
}
