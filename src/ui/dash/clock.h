/* LucKey Park
// Copyright (C) 2024 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef HEALTHINDICATOR_H
#define HEALTHINDICATOR_H

#include "../../mastercontrol.h"

#define NUM_HANDS 3

enum Period{ TIME_NIGHT = 0u, TIME_MORNING, TIME_AFTERNOON, TIME_EVENING, TIME_ALL };
static const StringVector periodNames{
    "Night",
    "Morning",
    "Afternoon",
    "Evening"
};

class Clock: public Sprite
{
    DRY_OBJECT(Clock, Sprite);

public:
    static void RegisterObject(Context* context);
    Clock(Context* context);

    bool SaveJSON(JSONValue& dest) const override
    {
        dest.Set("time", time_ + .5f * sec_);
        return true;
    }

    bool LoadJSON(const JSONValue& source) override
    {
        SetTime(source.Get("time").GetFloat());
        return true;
    }

    void Tick(StringHash eventType, VariantMap& eventData) { Tick(); }
    void Tick() { SetTime(time_ + 1u); }
    void SetTime(float time);
    void SetTime(unsigned time);

    Period GetCurrentPeriod() const { return TimeToPeriod(time_); }

private:
    void HandleSceneUpdate(StringHash eventType, VariantMap& eventData);
    float TimeToRotation(int hand, unsigned hp) const;
    unsigned TimeToStep(int hand, unsigned time) const;

    void HandleSizesChanged(StringHash /*eventType*/, VariantMap& /*eventData*/) { UpdateSizes(); }
    void UpdateSizes();

    Period TimeToPeriod(unsigned time) const
    {
        return static_cast<Period>((TimeToStep(0, time) / 4u) % 4u);
    }

    PODVector<Sprite*> hands_;
    unsigned time_;
    float sec_;
};

DRY_EVENT(E_PERIODCHANGED, PeriodChanged)
{
    DRY_PARAM(P_PERIOD, Period);    // int
    DRY_PARAM(P_INSTANT, Instant);  // bool
}

#endif // HEALTHINDICATOR_H
