/* LucKey Park
// Copyright (C) 2024 LucKey Productions (https://luckey.games)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef TOOLBOX_H
#define TOOLBOX_H

#include "../../mastercontrol.h"

class ToolButton;
class ToolInfo;

class ToolBox: public Window
{
    DRY_OBJECT(ToolBox, Window);

public:
    static void RegisterObject(Context* context);

    ToolBox(Context* context);
    void CreateToolInfo();

    ToolInfo* GetToolInfo() const { return toolInfo_; }

private:
    void CreateButtons();
    void UpdateSize();

    void HandleSizesChanged(StringHash eventType, VariantMap& eventData);
    void HandleKeyDown(StringHash eventType, VariantMap& eventData);

    HashMap<StringHash, ToolButton*> toolButtons_;
    ToolInfo* toolInfo_;
};

#endif // TOOLBOX_H
