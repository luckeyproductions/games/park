/* LucKey Park
// Copyright (C) 2024 LucKey Productions (https://luckey.games)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "../gui.h"

#include "footerbutton.h"

FooterButton::FooterButton(Context* context): Button(context)
{
    SetTexture(RES(Texture2D, "Textures/Spot.png"));
    SetBlendMode(BLEND_ADDALPHA);
    SetAlignment(HA_CENTER, VA_TOP);

    icon_ = CreateChild<Sprite>();
    icon_->SetAlignment(HA_CENTER, VA_TOP);
    icon_->SetBlendMode(BLEND_ALPHA);
    icon_->SetColor(Color::GREEN.Lerp(Color::CHARTREUSE, .7f).Lerp(Color::GRAY, .3f));

    Unhover();
}

void FooterButton::HandleClicked(StringHash /*eventType*/, VariantMap& /*eventData*/)
{
}

void FooterButton::UpdateSize()
{
    const int buttonSize{ GetWidth() };

    const int iconSize{ buttonSize - GUI::sizes_.margin_ };
    icon_->SetSize(iconSize, iconSize);
    icon_->SetPosition(-iconSize / 2, GUI::sizes_.margin_ / 2);
}
