/* LucKey Park
// Copyright (C) 2024 LucKey Productions (https://luckey.games)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "toolbutton.h"

ToolButton::ToolButton(Context* context): Button(context),
    tool_{ nullptr }
{
    SetTexture(RES(Texture2D, "Textures/Spot.png"));
    SetBlendMode(BLEND_ADDALPHA);

    icon_ = CreateChild<Sprite>();
    icon_->SetAlignment(HA_LEFT, VA_TOP);
    icon_->SetBlendMode(BLEND_ALPHA);
    Deselect();

    SubscribeToEvent(this, E_CLICKEND, DRY_HANDLER(ToolButton, HandleClicked));
    SubscribeToEvent(E_TOOLCHANGED, DRY_HANDLER(ToolButton, HandleToolChanged));
}

void ToolButton::HandleClicked(StringHash /*eventType*/, VariantMap& eventData)
{
    if (!tool_ ||
        eventData[ClickEnd::P_BEGINELEMENT].GetPtr() != eventData[ClickEnd::P_ELEMENT].GetPtr())
        return;

    Cursor3D* cursor3d{ GetSubsystem<GUI>()->GetSceneCursor() };
    Tool* currentTool{ cursor3d->GetTool() };

    if (tool_ == currentTool)
        cursor3d->SetTool(nullptr);
    else
        tool_->Pick();
}

void ToolButton::HandleToolChanged(StringHash /*eventType*/, VariantMap& /*eventData*/)
{
    Cursor3D* cursor3d{ GetSubsystem<GUI>()->GetSceneCursor() };
    Tool* currentTool{ cursor3d->GetTool() };

    if (tool_ == currentTool)
        Select();
    else
        Deselect();
}

void ToolButton::UpdateSize()
{
    const int buttonSize{ GUI::sizes_.toolButton_ };

    SetMaxSize(buttonSize, buttonSize);
    SetSize(buttonSize, buttonSize);

    const int iconSize{ buttonSize - GUI::sizes_.margin_ * 2 };
    icon_->SetSize(iconSize, iconSize);
    icon_->SetPosition(GUI::sizes_.margin_, GUI::sizes_.margin_);

    GUI* gui{ GetSubsystem<GUI>() };
    if (tool_ && gui && gui->GetSceneCursor()->GetTool() == tool_)
        Select();
}
