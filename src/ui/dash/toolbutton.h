/* LucKey Park
// Copyright (C) 2024 LucKey Productions (https://luckey.games)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef TOOLBUTTON_H
#define TOOLBUTTON_H

#include "../tools/tool.h"


class ToolButton: public Button
{
    DRY_OBJECT(ToolButton, Button);

public:
    ToolButton(Context* context);

    void SetTool(Tool* tool)
    {
        tool_ = tool;

        if (tool_)
            icon_->SetTexture(RES(Texture2D, String{ "UI/" } + tool_->GetName() + ".png"));
    }

    void Select()
    {
        SetColor(Color::YELLOW.Transparent(.42f));
        icon_->SetPosition(GUI::sizes_.margin_, -GUI::sizes_.margin_);
    }

    void Deselect()
    {
        SetColor(Color::WHITE.Transparent(.23f));
        icon_->SetPosition(GUI::sizes_.margin_, GUI::sizes_.margin_);
    }

    void UpdateSize();

private:
    void HandleClicked(StringHash eventType, VariantMap& eventData);
    void HandleToolChanged(StringHash eventType, VariantMap& eventData);

    Sprite* icon_;
    Tool* tool_;
};

#endif // TOOLBUTTON_H
