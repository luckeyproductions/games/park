/* LucKey Park
// Copyright (C) 2024 LucKey Productions (https://luckey.games)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef TICKER_H
#define TICKER_H

#include "../../mastercontrol.h"

#define TEXT_WIDTH(t) static_cast<int>((t->GetText().Length() + 2) * t->GetCharSize(0).x_)

class Ticker: public UIElement
{
    DRY_OBJECT(Ticker, UIElement);

public:
    static int Height();
    Ticker(Context* context);

    void Update(float timeStep) override;

    void AddText(const String& text) { queue_.Push(text); }

private:
    Text* CreateTickerText(String text);
    SharedPtr<ValueAnimation> Animate(Text* tickerText);
    void HandleTextGone(StringHash eventType, VariantMap&eventData);
    void HandleSizesChanged(StringHash, VariantMap&);

    void UpdateSizes();
    void UpdateTextSize();

    BorderImage* tape_;
    StringVector queue_;
};

DRY_EVENT(E_TICKERTEXTGONE, TickerTextGone)
{
    DRY_PARAM(P_TEXT, Text);  // float
}

#endif // TICKER_H
