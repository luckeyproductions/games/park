/* LucKey Park
// Copyright (C) 2024 LucKey Productions (https://luckey.games)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef TOOLINFO_H
#define TOOLINFO_H

#include "../../mastercontrol.h"

class ToolPage;

class ToolInfo: public Window
{
    DRY_OBJECT(ToolInfo, Window);

public:
    static void RegisterObject(Context* context);

    ToolInfo(Context* context);

    void UpdateSize(int width);
    void SetPreferredRatio(float ratio);
    bool IsSnapped() const { return snapped_; }
    void SetMinimized(bool minimized);
    bool IsMinimized() const { return minimized_; }

    void AddPage(ToolPage* page) { pages_.Push(page); }
    void SetVisiblePage(unsigned page);
    ToolPage* GetCurrentPage() const;

private:
    void CreateMinimizeButton();
    void HandleMinimizeButtonClicked(StringHash eventType, VariantMap& eventData);

    void HandleClick(StringHash eventType, VariantMap& eventData);
    void HandleDragCancel(StringHash eventType, VariantMap& eventData);
    void HandleDragMove(StringHash eventType, VariantMap& eventData);
    void HandleDragBegin(StringHash eventType, VariantMap& eventData);

    float ratio_;
    bool snapped_;
    bool minimized_;
    IntVector2 snapPosition_;

    Button* minimizeButton_;
    PODVector<ToolPage*> pages_;
};

#endif // TOOLINFO_H
