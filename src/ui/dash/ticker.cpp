/* LucKey Park
// Copyright (C) 2024 LucKey Productions (https://luckey.games)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "../gui.h"

#include "ticker.h"

int Ticker::Height() { return GUI::VMin(2) + GUI::sizes_.text_[3]; }

Ticker::Ticker(Context* context): UIElement(context),
    tape_{ CreateChild<BorderImage>() },
    queue_{}
{
    tape_->SetTexture(RES(Texture2D, "UI/UI.png"));

    tape_->SetAlignment(HA_LEFT, VA_BOTTOM);
    tape_->SetBlendMode(BLEND_ALPHA);
    tape_->SetFocusMode(FM_NOTFOCUSABLE);
    tape_->SetClipChildren(true);
    tape_->SetColor(Color::BLACK.Lerp(Color::GREEN, .0675f));
    tape_->SetOpacity(.875f);

    SubscribeToEvent(E_UISIZESCHANGED, DRY_HANDLER(Ticker, HandleSizesChanged));
    SubscribeToEvent(E_TICKERTEXTGONE, DRY_HANDLER(Ticker, HandleTextGone));

    AddText("Welcome to LucKey Park!!");
}

void Ticker::Update(float timeStep)
{
    UIElement::Update(timeStep);

    if (!GetParent()->IsVisible())
        return;

    Text* lastText{ tape_->GetChildDynamicCast<Text>(tape_->GetNumChildren() - 1) };
    if (!queue_.IsEmpty() && (tape_->GetNumChildren() == 0u ||
        lastText->GetPosition().x_ < tape_->GetWidth() - TEXT_WIDTH(lastText)))
    {
        CreateTickerText(queue_.Front());
        queue_.Erase(0);
    }

    if (queue_.IsEmpty() && tape_->GetNumChildren() == 0u)
        AddText("Welcome to LucKey Park!!");
}

Text* Ticker::CreateTickerText(String text)
{
//    if (tape_->GetNumChildren() != 0)
//        text = String{ " | " }.Append(text);
    if (!text.StartsWith(" "))
        text = String{" "}.Append(text);
    if (!text.EndsWith(".") && !text.EndsWith("!") && !text.EndsWith("?"))
        text = text.Append('.');

    Text* tickerText{ tape_->CreateChild<Text>() };
    tickerText->SetAlignment(HA_LEFT, VA_CENTER);
    tickerText->SetFont(RES(Font, "Fonts/WhiteRabbitReloaded.ttf"));
    tickerText->SetColor(Color::GREEN);
    tickerText->SetOpacity(.17f);
    tickerText->SetFontSize(GUI::sizes_.text_[3]);
    tickerText->SetTextEffect(TE_STROKE);
    tickerText->SetEffectStrokeThickness(GUI::sizes_.text_[0] / 7);
    tickerText->SetEffectColor({ Color::CHARTREUSE, .23f });
    tickerText->SetUseDerivedOpacity(false);
    tickerText->SetText(text);
    Animate(tickerText);

    return tickerText;
}

SharedPtr<ValueAnimation> Ticker::Animate(Text* tickerText)
{
    SharedPtr<ValueAnimation> scroll{ context_->CreateObject<ValueAnimation>() };

    const int textWidth{ TEXT_WIDTH(tickerText) };
    const IntVector2 startPos{ tape_->GetWidth() * IntVector2::RIGHT };
    tickerText->SetPosition(startPos);
    scroll->SetKeyFrame(0.f, startPos);
    scroll->SetKeyFrame((tape_->GetWidth() + textWidth) / tickerText->GetCharSize(0).x_ * .23f, textWidth * IntVector2::LEFT);
    scroll->SetEventFrame(scroll->GetEndTime(), E_TICKERTEXTGONE, { { TickerTextGone::P_TEXT, tickerText } });
    tickerText->SetAttributeAnimation("Position", scroll, WM_ONCE);

    return scroll;
}

void Ticker::HandleTextGone(StringHash /*eventType*/, VariantMap& eventData)
{
    Text* t{ dynamic_cast<Text*>(eventData[TickerTextGone::P_TEXT].GetPtr()) };
    t->Remove();
}

void Ticker::HandleSizesChanged(StringHash /*eventType*/, VariantMap& /*eventData*/)
{
    UpdateSizes();
}

void Ticker::UpdateSizes()
{
    Graphics* g{ GetSubsystem<Graphics>() };

    const UIElement* toolBox{ GetRoot()->GetChild("Toolbox", true) };
    if (!GUI::Portrait())
    {
        const int tapeWidth{ g->GetWidth() - toolBox->GetWidth() };
        tape_->SetWidth(tapeWidth);
        tape_->SetPosition(g->GetWidth() - tape_->GetWidth(), g->GetHeight());
    }
    else
    {
        const int tapeWidth{ g->GetWidth() };
        tape_->SetWidth(tapeWidth);
        tape_->SetPosition(0, g->GetHeight());
    }

    tape_->SetHeight(Height());
    tape_->SetImageRect({ 48 + 4 * GUI::Portrait(), 0, 60, 12 });
    tape_->SetImageBorder({ 4 * !GUI::Portrait(), 4, 0, 0 });
    tape_->SetBorder({ 4 * !GUI::Portrait(), 4, 0, 0 });

    UpdateTextSize();
}

void Ticker::UpdateTextSize()
{
    for (UIElement* e: tape_->GetChildren())
    {
        Text* te{ static_cast<Text*>(e) };

        if (!te)
            continue;

        te->SetFontSize(GUI::sizes_.text_[3]);
        te->SetEffectStrokeThickness(GUI::sizes_.text_[0] / 8);

        if (te->GetAttributeAnimation("Position"))
            Animate(te);
    }
}
