/* LucKey Park
// Copyright (C) 2024 LucKey Productions (https://luckey.games)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "../../tools/looker.h"
#include "../../../people/person.h"
#include "../../../structures/structure.h"

#include "lookerpage.h"

LookerPage::LookerPage(Context* context): ToolPage(context),
    target_{ nullptr },
    rendition_{ nullptr }
{
}

void LookerPage::UpdateContents(bool onlySize)
{
    Node* targetNode{ dynamic_cast<Looker*>(GetTool())->GetTarget() };

    if (target_ != targetNode)
    {
        TargetType oldType{ GetTargetType() };
        target_ = targetNode;

        if (GetTargetType() != oldType)
        {
            centralElem_->RemoveAllChildren();
            GeneratePage();
        }
    }

    switch (GetTargetType()) {
    default: case TARGET_NONE: centralElem_->GetChild(0)->SetWidth(GetWidth()); break;
    case TARGET_PERSON:     UpdatePersonPage(onlySize); break;
    case TARGET_STRUCTURE:  UpdateStructurePage(onlySize); break;
    }
}

void LookerPage::GeneratePage()
{
    switch (GetTargetType()) {
    default: case TARGET_NONE: CreateEmptyPage(); break;
    case TARGET_PERSON:     CreatePersonPage(); break;
    case TARGET_STRUCTURE:  CreateStructurePage(); break;
    }
}

void LookerPage::CreateEmptyPage()
{
    Text* label{ GetSubsystem<GUI>()->CreateButtonText(centralElem_, 5) };
    label->SetAlignment(HA_CENTER, VA_TOP);
    label->SetTextAlignment(HA_CENTER);
    label->SetWordwrap(true);
    label->SetText("Nothing to see here");
    label->SetPosition(0, 23);
    label->SetOpacity(.42f);
}

void LookerPage::CreatePersonPage()
{
    rendition_ = centralElem_->CreateChild<BorderImage>();
    rendition_->SetTexture(RES(Texture2D, "icon.png"));
    rendition_->SetFullImageRect();

    int l{ 0 };
    for (const String& valueName: { "Name", "Mood", "Going" })
    {
        Text* label{ GetSubsystem<GUI>()->CreateButtonText(centralElem_, 0) };
        label->SetAlignment(HA_RIGHT, VA_TOP);
        label->SetName(valueName + "Label" );
        label->SetText(valueName);
        label->AddTag("CELL");
        label->SetVar("ROW", l);
        label->SetVar("COLUMN", 0);

        Text* value{ GetSubsystem<GUI>()->CreateButtonText(centralElem_, 0) };
        value->SetName(valueName);
        value->SetAlignment(HA_LEFT, VA_TOP);
        value->AddTag("CELL");
        value->SetVar("ROW", l);
        value->SetVar("COLUMN", 1);
        value->SetWordwrap(true);

        ++l;
    }
}


void LookerPage::CreateStructurePage()
{
    rendition_ = centralElem_->CreateChild<BorderImage>();
    rendition_->SetTexture(RES(Texture2D, "icon.png"));
    rendition_->SetFullImageRect();
}

void LookerPage::UpdatePersonPage(bool onlySize)
{
    if (!onlySize)
    {
        const Personalia& personalia{ target_->GetComponent<Character>()->GetPerson()->GetPersonalia() };
        GetValueTextElement("Name")->SetText(personalia.GetName());
        GetValueTextElement("Mood")->SetText("Happy");
        GetValueTextElement("Going")->SetText("Nowhere");
    }

    const int m{ GUI::sizes_.margin_ };
    const int s{ Min(centralElem_->GetWidth() / 3, centralElem_->GetHeight() - m) };
    rendition_->SetPosition(2 * m, m);
    rendition_->SetSize(s, s);

    const int h{ 3 * GUI::sizes_.text_[0] / (GUI::Portrait() ? 2 : 3) };
    const int d{ rendition_->GetWidth() + h * 5 };
    int dy{};

    for (UIElement* e: centralElem_->GetChildrenWithTag("CELL"))
    {
        Text* t{ dynamic_cast<Text*>(e) };
        t->SetFontSize(h);
        const int c{ e->GetVar("COLUMN").GetInt() };
        const int r{ e->GetVar("ROW").GetInt() * 2 + c % 2 };
        const int x{ (c == 0 ? -centralElem_->GetWidth() + d : d + m * 2 - 3 * h) };
        if (r % 2 != 0)
            t->SetWidth(centralElem_->GetWidth() - x - m);

        const int y{ m + r * (h + m) + dy };
        dy += (h + m) * Max(0, t->GetNumRows() - 1);

        e->SetPosition(x, y);
    }
}

void LookerPage::UpdateStructurePage(bool onlySize)
{
    const int m{ GUI::sizes_.margin_ };
    rendition_->SetPosition(2 * m, m);
    const int s{ Min(128, centralElem_->GetHeight() - m) };
    rendition_->SetSize(s, s);
}

TargetType LookerPage::GetTargetType() const
{
    if (target_)
    {
        if (target_->HasComponent<Character>())
            return TARGET_PERSON;
        else if (target_->GetDerivedComponent<Structure>())
            return TARGET_STRUCTURE;
    }

    return TARGET_NONE;
}
