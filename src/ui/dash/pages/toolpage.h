/* LucKey Park
// Copyright (C) 2024 LucKey Productions (https://luckey.games)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef TOOLPAGE_H
#define TOOLPAGE_H

#include "../footerbutton.h"

#include "../../tools/tool.h"

class ToolPage: public UIElement
{
    DRY_OBJECT(ToolPage, UIElement);

public:
    static ToolPage* CreatePage(ToolType tool, UIElement* parent);

    ToolPage(Context* context);

    ToolPage* SetTool(ToolType tool);

    virtual void UpdateSize(bool minimized);
    virtual void UpdateContents(bool onlySize = false) {}

    int GetFooterHeight(bool minimized = false) const
    {
        if (!hasFooter_ || !footer_)
            return 0;

        const int m{ GUI::sizes_.margin_ };
        if (GUI::Portrait() && !minimized)
            return 2 * (GUI::sizes_.toolButton_ / 3 + 3 * m);
        else
            return GUI::sizes_.toolButton_ / 3 + 4 * m;
    }

protected:
    virtual void GeneratePage() = 0;
    Tool* GetTool() const { return Tool::Get(tool_); }
    bool PortraitFooter() const
    {
        if (!footer_ || !hasFooter_)
            return false;
        else
            return footer_->GetWidth() < footer_->GetHeight();
    }
    void LayoutFooterGrid(PODVector<FooterButton*> buttons, const IntVector2& numCells);

    Window* centralElem_;
    Window* footer_;
    bool hasFooter_;

private:
    Text* title_;
    ToolType tool_;
};

#endif // TOOLPAGE_H
