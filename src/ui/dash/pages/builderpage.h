/* LucKey Park
// Copyright (C) 2024 LucKey Productions (https://luckey.games)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef BUILDERPAGE_H
#define BUILDERPAGE_H

#include "../../../structures/structuredefs.h"
#include "../footerbutton.h"
#include "../../tools/builder.h"

#include "toolpage.h"

class StructureItem: public BorderImage
{
    DRY_OBJECT(StructureItem, BorderImage);

public:
    static bool Sort(StructureItem* lhs, StructureItem* rhs)
    {
        if (lhs->IsLocked() == rhs->IsLocked())
            return lhs->GetStructureName() < rhs->GetStructureName();

        return rhs->IsLocked();
    }

    StructureItem(Context* context): BorderImage(context),
        blueprint_{ StringHash::ZERO },
        category_{ STRUCT_UNDEFINED },
        locked_{ false },
        title_{ CreateChild<Text>() },
        rendition_{ CreateChild<BorderImage>() }
    {
        SetUseDerivedOpacity(false);
        SetTexture(RES(Texture2D, "UI/UI.png"));
        SetImageRect({ 112, 0, 128, 16 });
        SetHoverOffset({ 0, 16 });
        SetBorder({ 4, 4, 4, 4 });
        SetColor(Color::CHARTREUSE.Transparent(2/3.f));
        SetFocusMode(FM_FOCUSABLE);
        title_->SetFont(RES(Font, "Fonts/Pontiff.otf"));
        title_->SetFontSize(GUI::sizes_.text_[3]);
        title_->SetPosition(GUI::sizes_.margin_ * 2, 0);
        rendition_->SetTexture(RES(Texture2D, "icon.png"));
        rendition_->SetFullImageRect();
        rendition_->SetFixedSize(64, 64);
        rendition_->SetAlignment(HA_RIGHT, VA_BOTTOM);
        rendition_->SetUseDerivedOpacity(false);

        SetFixedHeight(title_->GetHeight() + rendition_->GetHeight());
    }

    void SetStructure(const StructureInfo& structure);
    bool IsCategory(StructureCategory category) const { return category_ == category; }
    StringHash GetBlueprintIdentifier() const { return blueprint_; }
    String GetStructureName() const { return title_->GetText(); }

    void SetLocked(bool locked)
    {
        if (locked_ == locked)
            return;

        locked_ = locked;

        const float o{ (2 - locked) * .5f };
        title_->SetOpacity(o);
        rendition_->SetOpacity(o);
    }

    bool IsLocked() const { return locked_; }

private:
    StringHash blueprint_;
    StructureCategory category_;
    bool locked_;
    Text* title_;
    BorderImage* rendition_;
};

class BuilderPage: public ToolPage
{
    DRY_OBJECT(BuilderPage, ToolPage);

public:
    static void RegisterObject(Context* context);
    BuilderPage(Context* context);

    void UpdateContents(bool onlySize = false) override;

protected:
    void GeneratePage() override;

private:
    void AddCategoryButton(const StructureCategory category);
    void SortListView();
    void StyleScrollArea();
    void CreateStructureItem(const StructureInfo& design);
    void CreateStructurePage();
    void UpdateStructurePage(bool onlySize = false);

    void ShowCategory(StructureCategory category);
    void HandleCategoryButtonClicked(StringHash eventType, VariantMap& eventData);
    void HandleStructureItemClicked(StringHash, VariantMap& eventData);

    Text* GetValueTextElement(const String& name) const
    {
        return blueprintEditor_->GetChildDynamicCast<Text>(name);
    }

    ListView* listView_;
    PODVector<StructureItem*> structureItems_;
    PODVector<FooterButton*> categoryButtons_;
    StructureCategory currentCategory_;

    UIElement* blueprintEditor_;
    Node* target_;
    BorderImage* rendition_;
};


#endif // BUILDERPAGE_H
