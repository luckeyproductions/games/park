/* LucKey Park
// Copyright (C) 2024 LucKey Productions (https://luckey.games)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "builderpage.h"
#include "dozerpage.h"
#include "diggerpage.h"
#include "lookerpage.h"

#include "toolpage.h"

ToolPage* ToolPage::CreatePage(ToolType tool, UIElement* parent)
{
    switch (tool) {
    default: return nullptr;
    case TOOL_BUILD:    return parent->CreateChild<BuilderPage>()->SetTool(tool);
    case TOOL_DOZE:     return parent->CreateChild<DozerPage>()  ->SetTool(tool);
    case TOOL_DIG:      return parent->CreateChild<DiggerPage>() ->SetTool(tool);
    case TOOL_INFO:     return parent->CreateChild<LookerPage>() ->SetTool(tool);
    }
}

ToolPage::ToolPage(Context* context): UIElement(context),
    centralElem_{ nullptr },
    footer_{ nullptr },
    hasFooter_{ false },
    title_{ nullptr },
    tool_{ TOOL_NONE }
{
    SetAlignment(HA_CENTER, VA_TOP);
}

ToolPage* ToolPage::SetTool(ToolType tool)
{
    assert(tool_ == TOOL_NONE);
    tool_ = tool;

    title_ = GetSubsystem<GUI>()->CreateButtonText(this, 3);
    title_->SetAlignment(HA_CENTER, VA_TOP);
    title_->SetTextEffect(TE_STROKE);
    title_->SetOpacity(.9f);
    title_->SetColor(Color::WHITE);
    title_->SetEffectColor(Color::BLACK.Transparent(.2f));
    title_->SetText(toolNames.At(tool));

    centralElem_ = CreateChild<Window>();
    centralElem_->SetAlignment(HA_CENTER, VA_TOP);
    centralElem_->SetTexture(RES(Texture2D, "UI/UI.png"));
    centralElem_->SetImageRect({ 48, 0, 64, 16 });
    centralElem_->SetBorder({ 4, 4, 4, 4 });
    centralElem_->SetColor(Color::CHARTREUSE.Lerp(Color::BLACK, .75f).Transparent(.5f));

    footer_ = CreateChild<Window>();
    footer_->SetAlignment(HA_CENTER, VA_TOP);
    footer_->SetTexture(RES(Texture2D, "UI/UI.png"));
    footer_->SetImageRect({ 48, 0, 64, 12 });
    footer_->SetBorder({ 4, 4, 4, 4 });
    footer_->SetColor(Color::CHARTREUSE.Lerp(Color::GRAY, .5f).Transparent(.5f));
    footer_->SetVisible(hasFooter_);

    GeneratePage();
    UpdateSize(false);
    return this;
}

void ToolPage::UpdateSize(bool minimized)
{
    const int m{ GUI::sizes_.margin_ };
    const int mt{ title_->GetHeight() + m };

    SetSize(GetParent()->GetSize());
    title_->SetEffectStrokeThickness(m / 2);

    footer_->SetVisible(hasFooter_);
    centralElem_->SetVisible(!minimized);
    if (GUI::Portrait() && !minimized)
    {
        const int fh{ GetFooterHeight(minimized) };
        footer_->SetSize(fh, GetHeight() - mt);
        footer_->SetPosition((GetWidth() - fh) / 2, mt);
        centralElem_->SetSize(GetSize() - IntVector2{ m * 2 + (fh - m) * hasFooter_, mt });
        centralElem_->SetPosition(((-fh + m) / 2) * hasFooter_, mt);
    }
    else
    {
        const int fh{ GetFooterHeight(minimized) };
        footer_->SetSize(GetWidth(), fh);
        footer_->SetPosition(0, GetHeight() - fh);

        centralElem_->SetSize(GetSize() - IntVector2{ m * 2, mt + fh * hasFooter_ });
        centralElem_->SetPosition(0, mt);
    }

    UpdateContents(true);
}

void ToolPage::LayoutFooterGrid(PODVector<FooterButton*> buttons, const IntVector2& numCells)
{
    const int& w{ Abs(numCells.x_) };
    const int& h{ Abs(numCells.y_) };
    const unsigned a{ static_cast<unsigned>(w * h) };
    if (a == 0)
        return;

    const int s{ Sign(numCells.y_) };
    const int m{ GUI::sizes_.margin_};
    const int bbs{ Min(((footer_->GetHeight() - m * h) / h), (footer_->GetWidth() - m * w) / w) };
    const int th{ h * bbs };
    for (unsigned b{ 0 }; b < buttons.Size() && b < a; ++b)
    {
        FooterButton* button{ buttons.At(b) };
        const int r{ static_cast<int>(b / w) * s + (h - 1) * (s < 0) };
        const IntVector2 pos{ static_cast<int>(bbs * ((numCells.x_ - 1) / -2.f + b % numCells.x_)),
                              static_cast<int>(bbs * r) + (footer_->GetHeight() - th) / 2 };
        button->SetSize(bbs, bbs);
        button->SetPosition(pos);
        button->UpdateSize();
    }
}
