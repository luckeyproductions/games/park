/* LucKey Park
// Copyright (C) 2024 LucKey Productions (https://luckey.games)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "../../tools/digger.h"

#include "diggerpage.h"

void DiggerPage::RegisterObject(Context* context)
{
    context->RegisterFactory<DiggerPage>();
    context->RegisterFactory<FooterButton>();
}

DiggerPage::DiggerPage(Context* context): ToolPage(context),
    gridSize_{},
    boxes_{}
{
    hasFooter_ = true;
}

void DiggerPage::UpdateContents(bool onlySize)
{
    Digger* digger{ dynamic_cast<Digger*>(GetTool()) };

    if (!onlySize)
    {
        UnsubscribeFromEvent(E_TOGGLED);
        for (CheckBox* b: boxes_.Values())
            b->SetChecked(false);

        SetGridSize(digger->GetBrushSize() + 2 * IntVector2::ONE);

        const Vector<IntVector2> brush{ digger->GetBrush() };
        for (const IntVector2& coords: brush)
        {
            const IntVector2 cell{ ToCell(coords) };
            if (boxes_.Contains(cell))
                boxes_[cell]->SetChecked(true);
        }

        for (CheckBox* b: boxes_.Values())
            SubscribeToEvent(b, E_TOGGLED, DRY_HANDLER(DiggerPage, HandleToggled));
    }

    const IntVector2 parentSize{ centralElem_->GetSize() };
    const int s{ Min(parentSize.x_ / gridSize_.x_,
                     parentSize.y_ * 4/3 / gridSize_.y_) };
    const IntVector2 boxSize{ s, s * 3/4 };
    const IntVector2 offset{ (parentSize - gridSize_ * boxSize) / 2 };

    for (const IntVector2& cell: boxes_.Keys())
    {
        CheckBox* box{ boxes_[cell] };
        if (!box)
            continue;

        box->SetSize(boxSize);
        box->SetPosition(IntVector2{ cell.x_, gridSize_.y_ - cell.y_ - 1 } * boxSize + offset);
    }

    if (PortraitFooter())
        LayoutFooterGrid(brushButtons_, { 2, 4 });
    else
        LayoutFooterGrid(brushButtons_, { 8, 1 });
}

void DiggerPage::GeneratePage()
{
    SetGridSize({ 3, 3 });

    for (int b{ 0 }; b < 8; ++b)
        CreateBrushButton(b);
}

void DiggerPage::CreateBrushButton(int index)
{
    FooterButton* brushButton{ footer_->CreateChild<FooterButton>() };
    brushButton->SetIndex(index);
    brushButton->SetIcon(RES(Texture2D, "UI/Brush.png"));
    brushButton->SetIconColor(Color::GREEN.Lerp(Color::CHARTREUSE, .7f).Lerp(Color::WHITE, .3f + .6f * (index >= 4)));
    SubscribeToEvent(brushButton, E_CLICKEND, DRY_HANDLER(DiggerPage, HandleBrushButtonClicked));
    brushButtons_.Push(brushButton);
}

CheckBox* DiggerPage::CreateCheckBox()
{
    CheckBox* box{ centralElem_->CreateChild<CheckBox>() };
    box->SetBlendMode(BLEND_ALPHA);
    box->SetTexture(RES(Texture2D, "UI/Shovel.png"));
    box->SetImageRect({ 0, 64, 256, 256 });
    box->SetHoverOffset(IntVector2::RIGHT * 256);
    box->SetCheckedOffset(IntVector2::UP * 256);
    box->SetFocusMode(FM_NOTFOCUSABLE);

    return box;
}

void DiggerPage::SetGridSize(const IntVector2& size)
{
    for (const IntVector2& cell: boxes_.Keys())
    {
        if (cell.x_ >= size.x_ || cell.y_ >= size.y_)
        {
            boxes_[cell]->Remove();
            boxes_.Erase(cell);
        }
    }

    for (int x{ 0 }; x < size.x_; ++x)
    for (int y{ 0 }; y < size.y_; ++y)
    {
        const IntVector2 cell{ x, y };
        if (!boxes_.Contains(cell))
        {
            boxes_[cell] = CreateCheckBox();
            boxes_[cell]->SetVar("CELL", cell);
        }
    }

    gridSize_ = size;
}

IntVector2 DiggerPage::ToCoords(const IntVector2& cell) const
{
    const IntVector2 coords{ cell - (ToCell(cell) - cell) };
    return coords;
}

IntVector2 DiggerPage::ToCell(const IntVector2& coords) const
{
    Digger* digger{ dynamic_cast<Digger*>(GetTool()) };
    const IntVector2 brushSize{ digger->GetBrushSize() };
    const IntVector2 cell{ coords - digger->GetBrushCenter() + brushSize / 2 + IntVector2::ONE };
    return cell;
}

void DiggerPage::HandleToggled(StringHash /*eventType*/, VariantMap& /*eventData*/)
{
    Digger* digger{ dynamic_cast<Digger*>(GetTool()) };
    CheckBox* box{ dynamic_cast<CheckBox*>(GetEventSender()) };
    const bool checked{ box->IsChecked() };
    const IntVector2 coords{ ToCoords(box->GetVar("CELL").GetIntVector2()) };

    UI* ui{ GetSubsystem<UI>() };
    Vector2 localMousePos{ (ui->GetCursorPosition() - box->GetScreenPosition()) * 1.f
                           / box->GetSize() };

    if (digger->GetBrush().Contains(coords) != checked)
        digger->ToggleShovel({ coords });
    if (digger->GetBrush().IsEmpty())
        digger->ToggleShovel(IntVector2::ZERO);

    const IntVector2 newCoords{ ToCell(coords - digger->CenterBrush()) };
    if (newCoords != coords && boxes_.Keys().Contains(newCoords))
    {
        box = boxes_[newCoords];
        if (box)
            ui->GetCursor()->SetPosition(VectorRoundToInt(box->GetScreenPosition() + box->GetSize() * localMousePos));
    }

}

void DiggerPage::HandleBrushButtonClicked(StringHash /*eventType*/, VariantMap& eventData)
{
    if (eventData[ClickEnd::P_BEGINELEMENT].GetPtr() != eventData[ClickEnd::P_ELEMENT].GetPtr())
        return;

    using IV2 = IntVector2;

    Digger* digger{ dynamic_cast<Digger*>(GetTool()) };
    const int id{ dynamic_cast<UIElement*>(eventData[ClickEnd::P_ELEMENT].GetPtr())->GetVar("ID").GetInt() };
    switch (id) {
    default: break;
    case 0: digger->SetBrush({ IV2::ZERO }); break;

    case 1: digger->SetBrush({ IV2{ 0, 0 }, IV2{ 1, 0 },
                               IV2{ 0, 1 }, IV2{ 1, 1 }
                             });
    break;
    case 2: digger->SetBrush({                IV2{ 0, -1 },
                               IV2{ -1,  0 }, IV2{ 0,  0 }, IV2{ 1, 0 },
                                              IV2{ 0,  1 }
                             });
    break;
    case 3: digger->SetBrush({ IV2{ -1, -1 },               IV2{ 1, -1 },
                                              IV2{ 0,  0 },
                               IV2{ -1,  1 },               IV2{ 1,  1 }
                             });
    break;
    case 4: digger->GrowBrush(); break;
    case 5: digger->ShrinkBrush(); break;
    case 6: digger->RotateBrush(false); break;
    case 7: digger->RotateBrush(true); break;
    }
}
