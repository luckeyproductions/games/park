/* LucKey Park
// Copyright (C) 2024 LucKey Productions (https://luckey.games)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef DIGGERPAGE_H
#define DIGGERPAGE_H

#include "toolpage.h"

class DiggerPage: public ToolPage
{
    DRY_OBJECT(DiggerPage, ToolPage);

public:
    static void RegisterObject(Context* context);
    DiggerPage(Context* context);

    void UpdateContents(bool onlySize = false) override;

protected:
    void GeneratePage() override;

private:
    void CreateBrushButton(int index);
    CheckBox* CreateCheckBox();
    void SetGridSize(const IntVector2& size);
    IntVector2 ToCoords(const IntVector2& cell) const;
    IntVector2 ToCell(const IntVector2& coords) const;

    void HandleToggled(StringHash eventType, VariantMap& eventData);
    void HandleBrushButtonClicked(StringHash eventType, VariantMap& eventData);

    IntVector2 gridSize_;
    HashMap<IntVector2, CheckBox*> boxes_;
    PODVector<FooterButton*> brushButtons_;
};

#endif // DIGGERPAGE_H
