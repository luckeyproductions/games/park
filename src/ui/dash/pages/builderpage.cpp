/* LucKey Park
// Copyright (C) 2024 LucKey Productions (https://luckey.games)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "../../../structures/drawingtable.h"

#include "builderpage.h"

void BuilderPage::RegisterObject(Context* context)
{
    context->RegisterFactory<BuilderPage>();
    context->RegisterFactory<StructureItem>();
}


BuilderPage::BuilderPage(Context* context): ToolPage(context),
    listView_{ nullptr },
    structureItems_{},
    categoryButtons_{},
    currentCategory_{ STRUCT_UNDEFINED },
    blueprintEditor_{ nullptr },
    target_{ nullptr },
    rendition_{ nullptr }
{
    hasFooter_ = true;
}

void BuilderPage::UpdateContents(bool onlySize)
{
    Builder* builder{ dynamic_cast<Builder*>(GetTool()) };
    StructureInfo cyanotype{ builder->GetCyanotype() };
    const bool constructing{ cyanotype.identifier_ != StringHash::ZERO };
    listView_->SetVisible(!constructing);
    blueprintEditor_->SetVisible(constructing);

    for (FooterButton* fb: categoryButtons_)
        fb->SetVisible(!constructing || fb == categoryButtons_.Front());

    if (constructing)
    {
        UpdateStructurePage(onlySize);
    }
    else
    {
        listView_->SetSize(centralElem_->GetSize());
        listView_->GetVerticalScrollBar()->SetSize({ GUI::sizes_.toolButton_ / 3, listView_->GetHeight()});

        ScrollBar* bar{ listView_->GetVerticalScrollBar() };
        for (bool back: { false, true })
        {
            Button* b{ (back ? bar->GetBackButton() : bar->GetForwardButton()) };
            b->GetChild(0)->SetSize(b->GetSize());
        }

        if (PortraitFooter())
            LayoutFooterGrid(categoryButtons_, { 1, -6 });
        else
            LayoutFooterGrid(categoryButtons_, { 6, 1 });
    }
}

void BuilderPage::GeneratePage()
{
    for (int c{ STRUCT_UNDEFINED }; c < STRUCT_ALL; ++c)
        AddCategoryButton(static_cast<StructureCategory>(c));

    listView_ = centralElem_->CreateChild<ListView>();
    listView_->SetStyleAuto(RES(XMLFile, "UI/DefaultStyle.xml"));
    listView_->SetVerticalScrollBarVisible(true);
    listView_->SetHorizontalScrollBarVisible(false);
    StyleScrollArea();

    for (const StructureInfo& design: GetSubsystem<DrawingTable>()->GetDesigns())
        CreateStructureItem(design);

    SortListView();

    CreateStructurePage();
}

void BuilderPage::SortListView()
{
    Sort(structureItems_.Begin(), structureItems_.End(), StructureItem::Sort);

    Vector<SharedPtr<UIElement>> keep{};
    for (auto i: listView_->GetItems())
    {
        keep.Push(SharedPtr<UIElement>(i));
        listView_->RemoveItem(i);
    }

    for (unsigned s{ 0 }; s < structureItems_.Size(); ++s)
        listView_->AddItem(structureItems_.At(s));
}

void BuilderPage::CreateStructurePage()
{
    if (!blueprintEditor_)
        blueprintEditor_ = centralElem_->CreateChild<UIElement>();
    blueprintEditor_->RemoveAllChildren();
    blueprintEditor_->SetAlignment(HA_CENTER, VA_TOP);

    rendition_ = blueprintEditor_->CreateChild<BorderImage>();
    rendition_->SetTexture(RES(Texture2D, "icon.png"));
    rendition_->SetFullImageRect();

    int l{ 0 };
    for (const String& valueName: { "Name", "Size" })
    {
        Text* label{ GetSubsystem<GUI>()->CreateButtonText(blueprintEditor_, 0) };
        label->SetAlignment(HA_RIGHT, VA_TOP);
        label->SetName(valueName + "Label" );
        label->SetText(valueName);
        label->AddTag("CELL");
        label->SetVar("ROW", l);
        label->SetVar("COLUMN", 0);

        Text* value{ GetSubsystem<GUI>()->CreateButtonText(blueprintEditor_, 0) };
        value->SetName(valueName);
        value->SetAlignment(HA_LEFT, VA_TOP);
        value->AddTag("CELL");
        value->SetVar("ROW", l);
        value->SetVar("COLUMN", 1);
        value->SetWordwrap(true);

        ++l;
    }
}

void BuilderPage::UpdateStructurePage(bool onlySize)
{
    if (!onlySize)
    {
        CreateStructurePage();

        const StructureInfo info{ dynamic_cast<Builder*>(GetTool())->GetCyanotype() };
        if (info.identifier_ != StringHash::ZERO)
        {
            GetValueTextElement("Name")->SetText(info.name_);
            GetValueTextElement("Size")->SetText(info.size_.ToString());
        }
    }

    blueprintEditor_->SetSize(centralElem_->GetSize());

    const int m{ GUI::sizes_.margin_ };
    const int s{ Min(centralElem_->GetWidth() / 3, centralElem_->GetHeight() - m) };
    rendition_->SetPosition(2 * m, m);
    rendition_->SetSize(s, s);

    const int h{ 3 * GUI::sizes_.text_[0] / (GUI::Portrait() ? 2 : 3) };
    const int d{ rendition_->GetWidth() + h * 5 };
    int dy{};

    for (UIElement* e: blueprintEditor_->GetChildrenWithTag("CELL"))
    {
        Text* t{ dynamic_cast<Text*>(e) };
        t->SetFontSize(h);
        const int c{ e->GetVar("COLUMN").GetInt() };
        const int r{ e->GetVar("ROW").GetInt() * 2 + c % 2 };
        const int x{ (c == 0 ? -centralElem_->GetWidth() + d : d + m * 4 - 2 * h) };
        if (r % 2 != 0)
            t->SetWidth(centralElem_->GetWidth() - x - m);

        const int y{ m + r * (h + m) + dy };
        dy += (h + m) * Max(0, t->GetNumRows() - 1);

        e->SetPosition(x, y);
    }
}


void BuilderPage::AddCategoryButton(const StructureCategory category)
{
    FooterButton* categoryButton{ footer_->CreateChild<FooterButton>() };
    categoryButton->SetVar("CATEGORY", category);
    categoryButton->SetIndex(category);
    categoryButton->SetIcon(RES(Texture2D, "UI/Categories.png"));
    categoryButton->SetIconColor(Color::GREEN.Lerp(Color::CHARTREUSE, .7f).Lerp(Color::WHITE, .3f + .6f * (category == 0)));
    SubscribeToEvent(categoryButton, E_CLICKEND, DRY_HANDLER(BuilderPage, HandleCategoryButtonClicked));
    categoryButtons_.Push(categoryButton);
}

void BuilderPage::StyleScrollArea()
{
    ScrollBar* bar{ listView_->GetVerticalScrollBar() };
    bar->SetOpacity(1/3.f);
    const PODVector<BorderImage*> barElements{ bar, bar->GetSlider(),
                                               bar->GetForwardButton(), bar->GetBackButton() };
    for (BorderImage* be: barElements)
    {
        be->SetTexture(RES(Texture2D, "UI/UI.png"));
        be->SetImageRect({ 208, 0, 224, 16 });
        be->SetBorder({ 4, 4, 4, 4 });
        be->SetColor(Color::CHARTREUSE);
    }

    for (bool back: { false, true })
    {
        Button* b{ (back ? bar->GetBackButton() : bar->GetForwardButton()) };
        b->SetPressedChildOffset({});
        BorderImage* arrow{ b->CreateChild<BorderImage>() };
        arrow->SetAlignment(HA_CENTER, VA_CENTER);
        arrow->SetTexture(RES(Texture2D, "UI/Header.png"));
        arrow->SetImageRect({ back * 256, 256, (1 + back) * 256, 512 });
        arrow->SetBlendMode(BLEND_ADDALPHA);
        arrow->SetColor(Color::CHARTREUSE.Lerp(Color::GREEN, 1/3.f));
    }

    BorderImage* panel{ listView_->GetScrollPanel() };
    panel->SetOpacity(0.f);
}

void BuilderPage::HandleCategoryButtonClicked(StringHash /*eventType*/, VariantMap& eventData)
{
    FooterButton* clickedElem{ dynamic_cast<FooterButton*>(eventData[ClickEnd::P_ELEMENT].GetPtr()) };
    if (!clickedElem || eventData[ClickEnd::P_BEGINELEMENT].GetPtr() != clickedElem)
        return;

    const StructureCategory clickedCategory{ static_cast<StructureCategory>(clickedElem->GetVar("CATEGORY").GetInt()) };
    if (clickedElem != categoryButtons_.At(STRUCT_UNDEFINED))
    {
        ToolInfo* toolInfo{ dynamic_cast<ToolInfo*>(GetParent()) };
        if (currentCategory_ == clickedCategory && !toolInfo->IsMinimized())
            ShowCategory(STRUCT_ALL);
        else
            ShowCategory(clickedCategory);

        if (toolInfo->IsMinimized())
            toolInfo->SetMinimized(false);
    }
    else
    {
        dynamic_cast<Builder*>(GetTool())->SetCyanotype({});
    }
}

void BuilderPage::ShowCategory(StructureCategory category)
{
    currentCategory_ = category;

    for (UIElement* item: listView_->GetItems())
    {
        StructureItem* structureItem{ dynamic_cast<StructureItem*>(item) };
        if (category == STRUCT_ALL)
            structureItem->SetVisible(true);
        else
            structureItem->SetVisible(structureItem->IsCategory(category));
    }
}

void BuilderPage::CreateStructureItem(const StructureInfo& design)
{
    SharedPtr<StructureItem> structureItem{ context_->CreateObject<StructureItem>() };
    listView_->AddItem(structureItem);
    structureItem->SetStructure(design);
    structureItems_.Push(structureItem);

    SubscribeToEvent(structureItem, E_CLICKEND, DRY_HANDLER(BuilderPage, HandleStructureItemClicked));
}

void StructureItem::SetStructure(const StructureInfo& structure)
{
    blueprint_ = structure.identifier_;
    category_ = structure.category_;
    title_->SetText(structure.name_);
    SetLocked(!structure.needs_.IsEmpty());
}

void BuilderPage::HandleStructureItemClicked(StringHash /*eventType*/, VariantMap& eventData)
{
    StructureItem* clickedItem{ dynamic_cast<StructureItem*>(eventData[ClickEnd::P_ELEMENT].GetPtr()) };
    if (!clickedItem || eventData[ClickEnd::P_BEGINELEMENT].GetPtr() != clickedItem
        || clickedItem->IsLocked())
        return;

    const StructureInfo blueprint{ GetSubsystem<DrawingTable>()->GetBlueprint(clickedItem->GetBlueprintIdentifier()) };
    dynamic_cast<Builder*>(GetTool())->SetCyanotype(blueprint);
}
