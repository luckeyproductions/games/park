/* LucKey Park
// Copyright (C) 2024 LucKey Productions (https://luckey.games)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef LOOKERPAGE_H
#define LOOKERPAGE_H

#include "toolpage.h"

enum TargetType{ TARGET_NONE = 0, TARGET_PERSON, TARGET_STRUCTURE };

class LookerPage: public ToolPage
{
    DRY_OBJECT(LookerPage, ToolPage);

public:
    LookerPage(Context* context);

    void UpdateContents(bool onlySize = false) override;

protected:
    void GeneratePage() override;

private:
    void CreateEmptyPage();
    void CreatePersonPage();
    void UpdatePersonPage(bool onlySize = false);
    void CreateStructurePage();
    void UpdateStructurePage(bool onlySize = false);

    TargetType GetTargetType() const;
    Text* GetValueTextElement(const String& name) const
    {
        return centralElem_->GetChildDynamicCast<Text>(name);
    }

    Node* target_;
    BorderImage* rendition_;
};

#endif // LOOKERPAGE_H
