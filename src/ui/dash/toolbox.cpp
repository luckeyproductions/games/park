/* LucKey Park
// Copyright (C) 2024 LucKey Productions (https://luckey.games)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "../../game.h"
#include "../gui.h"
#include "../tools/builder.h"
#include "../tools/dozer.h"
#include "../tools/digger.h"
#include "../tools/looker.h"
#include "toolbutton.h"
#include "toolinfo.h"
#include "pages/toolpage.h"

#include "toolbox.h"


void ToolBox::RegisterObject(Context* context)
{
    context->RegisterFactory<ToolBox>();
    context->RegisterFactory<ToolButton>();
    context->RegisterFactory<ToolText>();
    ToolInfo::RegisterObject(context);

    Builder::RegisterObject(context);
    context->RegisterFactory<Dozer>();
    context->RegisterFactory<Digger>();
    context->RegisterFactory<Looker>();
}

ToolBox::ToolBox(Context* context): Window(context),
    toolInfo_{ nullptr }
{
    SetTexture(RES(Texture2D, "UI/UI.png"));
    SetImageRect({ 48, 0, 64, 16 });
    SetBorder({ 4, 4, 4, 4 });
    SetResizeBorder({ 8, 8, 8, 8 });
    SetColor(Color::CHARTREUSE.Transparent(.55f));

    CreateButtons();
    UpdateSize();

    SubscribeToEvent(E_UISIZESCHANGED, DRY_HANDLER(ToolBox, HandleSizesChanged));
    SubscribeToEvent(E_KEYDOWN, DRY_HANDLER(ToolBox, HandleKeyDown));
}

void ToolBox::CreateButtons()
{
    for (StringHash tool: toolNames)
    {
        ToolButton* button{ CreateChild<ToolButton>() };
        button->SetTool(static_cast<Tool*>(context_->CreateObject(tool).Get()));

        toolButtons_.Insert({ tool, button });
    }
}

void ToolBox::CreateToolInfo()
{
    toolInfo_ = GetParent()->CreateChild<ToolInfo>();
    toolInfo_->UpdateSize(GetWidth());
}

void ToolBox::UpdateSize()
{
    for (ToolButton* button: toolButtons_.Values())
        button->UpdateSize();

    const int toolMargin{ GUI::sizes_.margin_ * 2 };
    SetLayout(LM_HORIZONTAL, toolMargin, IntRect{{ toolMargin * 2, toolMargin}, { toolMargin * 2, toolMargin}});
    const int h{ GUI::sizes_.toolButton_ + toolMargin * 2 };
    SetSize(512, h);
    UpdateLayout();

    if (!GUI::Portrait())
    {
        SetHorizontalAlignment(HA_LEFT);
        SetPosition(-GUI::sizes_.margin_ * 2, GUI::VH(100) - h + toolMargin);
    }
    else
    {
        SetHorizontalAlignment(HA_CENTER);
        SetPosition(GUI::sizes_.screen_.x_ / 2, GUI::VH(100) - h + toolMargin - Ticker::Height());
    }

    if (toolInfo_)
    {
        if (toolInfo_->IsSnapped())
            toolInfo_->SetPosition(GetPosition());

        toolInfo_->UpdateSize(GetWidth());
    }
}

void ToolBox::HandleSizesChanged(StringHash /*eventType*/, VariantMap& /*eventData*/)
{
    UpdateSize();
}

void ToolBox::HandleKeyDown(StringHash /*eventType*/, VariantMap& eventData)
{
    if (GetSubsystem<Game>()->GetStatus() != GS_PLAY || eventData[KeyDown::P_REPEAT].GetBool())
        return;

    Cursor3D* cursor3d{ GetSubsystem<GUI>()->GetSceneCursor() };
    Tool* currentTool{ cursor3d->GetTool() };
    Tool* pickedTool{ nullptr };

    switch (eventData[KeyDown::P_KEY].GetInt()) {
    default: break;
    case KEY_V: pickedTool = Tool::Get(TOOL_BUILD); break;
    case KEY_B: pickedTool = Tool::Get(TOOL_DOZE);  break;
    case KEY_N: pickedTool = Tool::Get(TOOL_DIG);   break;
    case KEY_M: pickedTool = Tool::Get(TOOL_INFO);  break;
    }

    if (pickedTool == nullptr)
        return;

    if (currentTool == pickedTool)
        cursor3d->SetTool(nullptr);
    else
        pickedTool->Pick();
}
