/* LucKey Park
// Copyright (C) 2024 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "../gui.h"
#include "../../effectmaster.h"

#include "clock.h"

void Clock::RegisterObject(Context* context)
{
    context->RegisterFactory<Clock>();

    DRY_COPY_BASE_ATTRIBUTES(Sprite);
}

Clock::Clock(Context* context): Sprite(context),
    hands_{},
    time_{ 0 },
    sec_{ 0.f }
{
    context_->RegisterSubsystem(this);
    SharedPtr<Texture2D> faceTexture { RES(Texture2D, "UI/ClockFace.png")  };
    SetTexture(faceTexture);
    SetImageRect({ 0, 0, 512, 512 });
    SetSize(512, 512);
    SetPosition(Vector2::ZERO);
    SetBlendMode(BLEND_ALPHA);

    SharedPtr<Texture2D> handsTexture{ RES(Texture2D, "UI/ClockHands.png") };

    for (int h{ 0 }; h < NUM_HANDS; ++h)
    {
        Sprite* hand{ CreateChild<Sprite>() };
        hand->SetTexture(handsTexture);
        hand->SetImageRect({ 160 - h * 64, 0, 224 - h * 64, 256 });
        hand->SetSize(hand->GetImageRect().Size());
        hand->SetPosition(Vector2{ GetSize() / 2 });
        hand->SetBlendMode(BLEND_ALPHA);
        hand->SetHotSpot(VectorRoundToInt(Vector2{ .5f, .9f } * hand->GetSize()));

        hands_.Push(hand);
        hand->SetRotation(TimeToRotation(h, time_));
    }

    SubscribeToEvent(E_UISIZESCHANGED, DRY_HANDLER(Clock, HandleSizesChanged));
    SubscribeToEvent(GetSubsystem<World>()->GetScene(), E_SCENEUPDATE, DRY_HANDLER(Clock, HandleSceneUpdate));
    UpdateSizes();

    VariantMap eventData{};
    eventData[PeriodChanged::P_PERIOD] = GetCurrentPeriod();
    eventData[PeriodChanged::P_INSTANT] = true;
    SendEvent(E_PERIODCHANGED, eventData);
}

void Clock::HandleSceneUpdate(StringHash /*eventType*/, VariantMap& eventData)
{
    const float s{ 2.f };
    sec_ += eventData[SceneUpdate::P_TIMESTEP].GetFloat();
    if (sec_ >= s)
    {
        sec_ -= s;
        Tick();
    }
}

void Clock::SetTime(float time)
{
    sec_ = 2.f * Fract(time);
    SetTime(static_cast<unsigned>(FloorToInt(time)));
}

void Clock::SetTime(unsigned time)
{
    const bool instant{ Abs(time - time_) > 1u };
    for (int h{ 0 }; h < NUM_HANDS; ++h)
    {
        Sprite* hand{ hands_.At(h) };
        const float from{ hand->GetRotation() };
        const float to{ TimeToRotation(h, time) };
        if (to == from)
            continue;

        const float f{ Cbrt(1.f * NUM_HANDS - h) };
        if (instant)
        {
            hand->RemoveAttributeAnimation("Rotation");
            hand->SetRotation(to);
        }
        else
        {
            SharedPtr<ValueAnimation> rotate{ EFFECT->Spring(from, to, .23f * f, 9.f / f) };
            hand->SetAttributeAnimation("Rotation", rotate, WM_CLAMP);
        }
    }

    const bool periodChanged{ TimeToPeriod(time) != TimeToPeriod(time_) };
    time_ = time;

    if (periodChanged)
    {
        VariantMap eventData{};
        eventData[PeriodChanged::P_PERIOD] = GetCurrentPeriod();
        eventData[PeriodChanged::P_INSTANT] = instant;
        SendEvent(E_PERIODCHANGED, eventData);
    }
}

float Clock::TimeToRotation(int hand, unsigned time) const
{
    return TimeToStep(hand, time) * 22.5f;
}

unsigned Clock::TimeToStep(int hand, unsigned time) const
{
    switch (hand) {
    default: return 0u;
    case 0: return time / 0200u * 4u;
    case 1: return time / 020u  * 2u;
    case 2: return time;
    }
}

void Clock::UpdateSizes()
{
    const IntVector2 screenSize{ GUI::sizes_.screen_ };
    const float scale{ 1/3.f * screenSize.y_ / 1080.f };
    const int clockSize{ GetWidth() };
    SetScale(scale);
//    SetPosition(screenSize - Vector2::ONE * scale * clockSize); ?
    SetPosition(screenSize.x_ - scale * clockSize, 0);
}
