/* LucKey Park
// Copyright (C) 2024 LucKey Productions (https://luckey.games)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef DASH_H
#define DASH_H

#include "../../mastercontrol.h"

#include "toolbox.h"
#include "ticker.h"
#include "clock.h"

class Dash: public UIElement
{
    DRY_OBJECT(Dash, UIElement);

public:
    Dash(Context* context);

    ToolBox* GetToolBox() const { return toolBox_; }
    Ticker* GetTicker() const { return ticker_; }
    void SetToolInfoVisible(bool visible);

private:
    ToolBox* toolBox_;
    Ticker* ticker_;
    Clock* clock_;
};

#endif // DASH_H
