/* LucKey Park
// Copyright (C) 2024 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef EFFECTMASTER_H
#define EFFECTMASTER_H

#include "mastercontrol.h"

#define EFFECT GetSubsystem<EffectMaster>()

class EffectMaster: public Object
{
    DRY_OBJECT(EffectMaster, Object);

public:
    EffectMaster(Context* context);

    template <class T> ValueAnimation* Spring(const T& from, const T& to, float duration = 1.f, float frequency = 1.f)
    {
        ValueAnimation* spring{ new ValueAnimation{ context_ } };
        spring->SetInterpolationMethod(IM_SPLINE);
        const float dt{ .023f };
        for (int i{ 0 }; i * dt < duration; ++i)
        {
            const float t{ i * dt };
            const float factor{ PowN(1.f - t / duration, 3) };
            const T val{ Lerp(from, to, 1.f - factor * Cos(t * 360 * frequency)) };
            spring->SetKeyFrame(t, val);
        }
        spring->SetKeyFrame(duration, to);
        return spring;
    }

    template <class T> ValueAnimation* Sinusoidal(const T& from, const T& to, float duration = 1.f)
    {
        ValueAnimation* sinusoidal{ new ValueAnimation{ context_ } };
        sinusoidal->SetInterpolationMethod(IM_SINUSOIDAL);
        sinusoidal->SetKeyFrame(0.f, from);
        sinusoidal->SetKeyFrame(duration, to);
        return sinusoidal;
    }
};

#endif // EFFECTMASTER_H
