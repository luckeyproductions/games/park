/* LucKey Park
// Copyright (C) 2024 LucKey Productions (https://luckey.games)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef WORLD_H
#define WORLD_H

#include "jib.h"

#define GRID 3.f
#define HEIGHTSTEP .7f
#define GRID_SUBS 6
#define SUBGRID (GRID / GRID_SUBS)
#define SIDEWALK_Y -1
#define ZEBRA_X 5
#define GRID_SQUARE Vector3{ GRID, 0, GRID }
#define GRID_BLOCK Vector3{ GRID, HEIGHTSTEP, GRID }
#define SUBGRID_BLOCK Vector3{ SUBGRID, HEIGHTSTEP, SUBGRID }

class Person;
class Ground;


// List of names to pick from at random on new game
const Vector<String> parkNames{
    "LucKey World",
    "Big Whoop"
};

struct ParkInfo
{
    String name_{ Random(parkNames) };
};

DRY_EVENT(E_PATHREMOVED, PathRemoved)
{
    DRY_PARAM(P_COORDINATES, Coordinates); // Buffer
};

class World: public Object
{
    DRY_OBJECT(World, Object);

public:
    World(Context* context);

    void Generate(bool initial);

    bool SaveJSON(JSONValue& dest) const;
    bool LoadJSON(const JSONValue& source);

    String GetParkName() const { return parkInfo_.name_; }
    Scene* GetScene() const { return scene_; }
    Ground* GetGround() const { return ground_; }
    IntVector2 FlatSize() const { return { size_.x_, size_.z_ }; }
    int GetWidth() const { return size_.x_; }

    static char RotationFromDirection(const Vector3& delta)
    {
        int r;

        if (Abs(delta.x_) > Abs(delta.z_))
        {
            if (delta.x_ > 0.f)
                r = 1;
            else
                r = 3;
        }
        else
        {
            if (delta.z_ > 0.f)
                r = 0;
            else
                r = 2;
        }

        return r;
    }
    static Quaternion StepRotationToQuaternion(int rotation)
    {
        return Quaternion{ rotation * 90.f, Vector3::UP };
    }
    static Vector3 CoordinatesToPosition(const IntVector3& coords, bool sub = false)
    {
        const float size{ (sub ? SUBGRID : GRID ) };

        return coords * Vector3{ size, HEIGHTSTEP, size };
    }
    static Vector3 CoordinatesToPosition(const IntVector2& coords, bool sub = false)
    {
        return CoordinatesToPosition({ coords.x_, 0, coords.y_ }, sub);
    }

    Vector3 CoordinatesToWorldPosition(const IntVector3& coords, bool sub = false)
    {
        return CoordinatesToPosition(coords, sub) + GetOffset(sub);
    }
    Vector3 CoordinatesToWorldPosition(const IntVector2& coords, bool sub = false)
    {
        return CoordinatesToWorldPosition({ coords.x_, 0, coords.y_ }, sub);
    }

    Vector3 GetOffset(bool sub = false) const
    {
        const float x{ GRID * (FlatSize().x_ - 1) * -.5f + sub * (SUBGRID - GRID) * .5f };
        const float z{ GRID * (FlatSize().y_ - 1) * -.5f + sub * (SUBGRID - GRID) * .5f };

        return { x, 0.f, z };
    }

    IntVector2 WorldPositionToCoordinates2D(const Vector3& position, bool sub = false)
    {
        const IntVector3 coordinates{ WorldPositionToCoordinates(position, sub) };
        return { coordinates.x_, coordinates.z_ };
    }

    IntVector3 WorldPositionToCoordinates(const Vector3& position, bool sub = false)
    {
        const Vector3 offset{ GetOffset() + sub * Vector3{ (SUBGRID - GRID) * .5f, .0f, (SUBGRID - GRID) * .5f } };
        const Vector3 gridPos{ position - offset};
        const float grid{ (sub ? SUBGRID : GRID) };
        const IntVector3 coordinates{ RoundToInt(gridPos.x_ / grid),
                                      RoundToInt(gridPos.y_ / HEIGHTSTEP),
                                      RoundToInt(gridPos.z_ / grid) };

        return coordinates;
    }

    PODVector<IntVector2> GetWalkable(bool unstoppable = true, bool sub = true, const Vector<IntVector2> filter = {}) const
    {
        PODVector<IntVector2> walkable{};

        for (const IntVector2& coords: path_)
        {
            if (filter.Size() && !filter.Contains(coords))
                continue;

            //Skip zebra
            if (!unstoppable && coords.y_ < SIDEWALK_Y && coords.y_ > SIDEWALK_Y - 3)
                continue;

            if (sub)
            {
                const IntVector2 origin{ coords * GRID_SUBS };
                const int farEdge{ GRID_SUBS - 1 };

                for (int x{ 0 }; x < GRID_SUBS; ++x)
                for (int y{ 0 }; y < GRID_SUBS; ++y)
                {
                    // Skip roadside
                    if ((   x == 0                       && !path_.Contains(coords + IntVector2::LEFT)    )
                        || (x == farEdge                 && !path_.Contains(coords + IntVector2::RIGHT)   )
                        || (y == 0                       && !path_.Contains(coords + IntVector2::DOWN)    )
                        || (y == farEdge                 && !path_.Contains(coords + IntVector2::UP)      )
                        || (x == 0       && y == 0       && !path_.Contains(coords + IntVector2{ -1, -1 }))
                        || (x == farEdge && y == 0       && !path_.Contains(coords + IntVector2{  1, -1 }))
                        || (x == 0       && y == farEdge && !path_.Contains(coords + IntVector2{ -1,  1 }))
                        || (x == farEdge && y == farEdge && !path_.Contains(coords + IntVector2{  1,  1 })))
                        continue;

                    walkable.Push(origin + IntVector2{ x, y });
                }
            }
            else
            {
                walkable.Push(coords);
            }
        }

        return walkable;
    }

    bool SetPath(const IntVector2& coords, bool path);
    bool IsPath(const IntVector2& coords) const { return path_.Contains(coords); }
    void RemovePath(const Vector<IntVector2>& coords);

private:
    void ClearPath();
    void GenerateRoad();
    void GeneratePeople();
    void RemovePeople();

    void HandleSceneUpdate(StringHash eventType, VariantMap& eventData);
    void HandlePeriodChanged(StringHash eventType, VariantMap& eventData);

    ParkInfo parkInfo_;
    SharedPtr<Scene> scene_;
    Node* rootNode_;
    Ground* ground_;
    HashMap<unsigned, SharedPtr<Person> > people_;
    HashSet<IntVector2> path_;

    IntVector3 size_;
    bool postScarcity_;

    Light* sun_;
    Light* skyLight_;
    SharedPtr<Material> skyMat_;
    SharedPtr<Material> fogMat_;
};

#endif // WORLD_H
