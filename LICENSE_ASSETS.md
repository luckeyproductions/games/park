## Used licenses

_[CC0](https://creativecommons.org/publicdomain/zero/1.0/), [CC-BY 3.0](https://creativecommons.org/licenses/by/3.0/)/[4.0](https://creativecommons.org/licenses/by/4.0/), [CC-BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/)_

## Assets by type, author and license

### Various
##### Author irrelevant
- CC0
    + Docs/Ideas.md
    + Docs/screenshot.jpg
    + Screenshots/*
    + Resources/Materials/*
    + Resources/UI/DefaultStyle.xml 
    + Resources/Shaders/*
    + Resources/Techniques/*
    + Resources/RenderPaths/*
    + Resources/Animations/*
    + Resources/PostProcess/*
    + Resources/Textures/Ramp.*
    + Resources/Textures/Spot.*
    + Resources/Models/Sedan.txt
    + Resources/UI/*.xml
    + Resources/JSON/Blueprints.json
    
##### Unturf/fxhp
- CC0
    + Docs/fedora-build-and-debug.md

### Music
##### joitrax (edited by Modanung)
- CC-BY 3.0
    + Resources/Music/joitrax - Nintendo 64 (loop).ogg [[Source]](https://archive.org/details/soundcloud-883698169)
    
### Samples
#### dermotte (edited by Modanung)
- CC-BY 4.0
    + Resources/Samples/Ting.wav [[Source]](https://freesound.org/people/dermotte/sounds/160672/)

#### ldezem
- CC0
    + Resources/Samples/ChippingHammer.wav [[Source]](https://freesound.org/people/ldezem/sounds/386239/)
    
#### Independent.nu (edited by Modanung)
- CC0
    + Resources/Samples/Demolish.wav [[Source]](https://opengameart.org/content/35-wooden-crackshitsdestructions) (impactwood03)
        
### 2D   
  
#### PoppyNull (@Poppy_Null)
- CC0
    + Resources/Fonts/AugmentedFunk.ttf [[Source]](https://www.dafont.com/augmented-funk.font)
    
#### Jovanny Lemonad 
- CC0
    + Resources/Fonts/Philospher.ttf [[Source]](https://www.dafont.com/philosopher.font)

    
#### Jehoo Creative 
- CC0
    + Resources/Fonts/Pontiff.otf [[Source]](https://www.dafont.com/pontiff-wide.font)

#### Matthew Welch (modified by Modanung)
- CC0
    + Resources/Fonts/WhiteRabbitReloaded.ttf [[Source]](https://www.dafont.com/white-rabbit.font)
    
#### Khurasan 
- CC0
    + Resources/Fonts/Xomai.ttf [[Source]](https://www.dafont.com/xomai.font)
        
##### Modanung
- CC-BY-SA 4.0
    + Raw/*
    + park.svg
    + Resources/icon.png
    + Resources/UI/Logo.png
    + Docs/Logo.png
    + Resources/UI/Header.png
    + Resources/UI/Categories.png
    + Resources/UI/Brush.png
    + Resources/UI/Shovel.png
    + Resources/UI/Cursor.png
    + Resources/UI/ClockFace.png
    + Resources/UI/ClockHands.png
    + Resources/UI/Look.png
    + Resources/UI/Doze.png
    + Resources/UI/Dig.png
    + Resources/UI/Build.png
- CC0
    + Resources/UI/UI.png
    + Resources/Textures/SunflowerSky*.png [[Source HDRI]](https://polyhaven.com/a/sunflowers)

### 3D
##### Modanung
- CC-BY-SA 4.0
    + Blends/*
    + Resources/Models/\*.mdl
    + Resources/Models/\*/\*.mdl