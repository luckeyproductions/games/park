#ifndef CYBERPLASM_H
#define CYBERPLASM_H

#include <Dry/Graphics/StaticModel.h>

#include "grimoire.h"
#include "geonomicon.h"

namespace Cyberplasm
{

class Vortex: public Pair<Vector3, Quaternion> // Replaces Rune
{
public:
    Vortex(): Vortex(Vector3::ZERO) {}

    Vortex(const Vector3& position, const Quaternion& spin = Quaternion::IDENTITY):
        Pair<Vector3, Quaternion>(position, spin)
    {}

    Vortex Lerp(const Vortex& rhs, float t) const
    {
        return { first_.Lerp(rhs.first_, t), second_.Slerp(rhs.second_, t) };
    }
};

class Worm: public Pair<Vortex, Vortex> // Replaces Wand
{
public:
    Worm(): Pair<Vortex, Vortex>() {}
    Vortex At(float t) const { return first_.Lerp(second_, t); }
};

class Formula: public Object, public Vector<Vortex> // Replaces Spell
{
    DRY_OBJECT(Formula, Object);

public:
    Formula(Context* context): Object(context), Vector<Vortex>() {}
};

class Vat: public Object
{
    using Complex = Vector<Formula>; // Replaces Sect

    DRY_OBJECT(Vat, Object)

public:
    Vat(Context* context);

//    SharedPtr<Model> Generate(const Formula& form, unsigned resolution = 9)
//    {
//        return Generate({ form }, resolution);
//    }
//    SharedPtr<Model> Generate(const Complex& form, unsigned resolution = 9);

    SharedPtr<Model> Generate(Witch::Sect sect, unsigned resolution = 9);
    SharedPtr<Geometry> GeometryFromData(const PODVector<float>& vertexData, const PODVector<unsigned short>& indexData, PrimitiveType type);

};

}

#endif // CYBERPLASM_H
