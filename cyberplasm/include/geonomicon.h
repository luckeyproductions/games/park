#ifndef GEONOMICON_H
#define GEONOMICON_H

#include <Dry/Math/Polyhedron.h>
#include "grimoire.h"

struct Geonomicon
{
    static Witch::Spell Rectangle(const Vector2& size = Vector2::ONE, const Vector3& normal = Vector2::ZERO, const Vector2& offset = Vector2::ZERO);
    static Witch::Spell Ellipse(const Vector2& size = Vector2::ONE, unsigned n = 4, const Vector2& offset = Vector2::ZERO);

    static Witch::Sect Block(const Vector3& size, float radius);
    static Witch::Sect Torus(float rIn, float rOut, unsigned n = 3, unsigned m = 6);
    static Witch::Sect Polyhedrune(const Polyhedron& position, const Polyhedron& normal = {});
    static Witch::Sect Callis(const Witch::Spell& path, const Witch::Sect& pattern,
                              const Witch::Rune& start = { Vector3::ONE * M_INFINITY }, const Witch::Rune& end = { Vector3::ONE * M_INFINITY });

    static Witch::Sect Callis(const Witch::Spell& path, const Witch::Sect& pattern, const Vector3& startDir, const Vector3& endDir);
};

#endif // GEONOMICON_H
