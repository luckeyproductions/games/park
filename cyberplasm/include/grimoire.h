#ifndef GRIMOIRE_H
#define GRIMOIRE_H

#include "luckey.h"
#include "witch.h"

class Grimoire: public Object
{
    DRY_OBJECT(Grimoire, Object)
public:
    static Context* MagicContext() { return magicContext_; }
    static SharedPtr<Geometry> Conjure(const PODVector<float> vertexData, const PODVector<unsigned short> indexData, PrimitiveType type = TRIANGLE_LIST);
    static SharedPtr<Model> Summon(const Witch::Sect& sect, unsigned resolution);

    Grimoire(Context* context): Object(context) { magicContext_ = context; }

    static Witch::Sect Curve(const Witch::Spell& spell);
    static Witch::Sect Tris( const Witch::Spell& spell);
    static Witch::Sect Quads(const Witch::Spell& spell);
    static Witch::Sect Rings(const Witch::Spell& spell, unsigned n);
    static Witch::Sect Sheet(const Witch::Spell& spell, unsigned width);
//    static Sect spindle(const Spell& spell) { using namespace Witch; return {}; }

    static Witch::Sect Weave( const Witch::Sect& weft);
private:
    static class Context* magicContext_;
};

#endif // GRIMOIRE_H
