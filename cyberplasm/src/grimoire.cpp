#include <Dry/Graphics/IndexBuffer.h>

#include "grimoire.h"

using namespace Witch;

Context* Grimoire::magicContext_{ nullptr };

SharedPtr<Geometry> Grimoire::Conjure(const PODVector<float> vertexData, const PODVector<unsigned short> indexData, PrimitiveType type)
{
    const unsigned numVertices{ indexData.Size() };

    SharedPtr<VertexBuffer> vb{ new VertexBuffer{ magicContext_ } };
    SharedPtr<IndexBuffer> ib{ new IndexBuffer{ magicContext_ } };

    // We could use the "legacy" element bitmask to define elements for more compact code, but let's demonstrate
    // defining the vertex elements explicitly to allow any element types and order
    PODVector<VertexElement> elements;
    elements.Push(VertexElement{ TYPE_VECTOR3, SEM_POSITION });
    elements.Push(VertexElement{ TYPE_VECTOR3, SEM_NORMAL });

    vb->SetSize(numVertices, elements);
    vb->SetShadowed(true);

    ib->SetSize(numVertices, false);
    ib->SetShadowed(true);

    vb->Lock(0, numVertices);
    vb->SetData(vertexData.Buffer());

    ib->Lock(0, numVertices);
    ib->SetData(indexData.Buffer());

    vb->Unlock();
    ib->Unlock();

    SharedPtr<Geometry> vat{ new Geometry{ magicContext_ } };
    vat->SetVertexBuffer(0, vb);
    vat->SetIndexBuffer(ib);
    vat->SetDrawRange(type, 0, numVertices);

    return vat;
}

SharedPtr<Model> Grimoire::Summon(const Sect& sect, unsigned resolution)
{
    SharedPtr<Model> model{ magicContext_->CreateObject<Model>() };

    BoundingBox bb{};
    for (unsigned s{ 0u }; s < sect.Size(); ++s)
    {
        for (unsigned t{ 0 }; t < sect.At(s).Size(); ++t)
            bb.Merge(sect.At(s).At(t).Position());
    }
///*
    model->SetNumGeometries(sect.Size());
    Vector<SharedPtr<VertexBuffer> > vertexBuffers;
    Vector<SharedPtr<IndexBuffer> > indexBuffers;

    for (unsigned s{ 0u }; s < sect.Size(); ++s)
    {
        SharedPtr<Geometry> g{ sect.At(s).Cast(resolution) };
        model->SetGeometry(s, 0, g);
        vertexBuffers.Push(SharedPtr<VertexBuffer>(g->GetVertexBuffer(0)));
        indexBuffers.Push(SharedPtr<IndexBuffer>(g->GetIndexBuffer()));
    }

    model->SetBoundingBox(bb);

    // Though not necessary to render, the vertex & index buffers must be listed in the model so that it can be saved properly
    // Morph ranges could also be not defined. Here we simply define a zero range (no morphing) for the vertex buffer
    PODVector<unsigned> morphRangeStarts{ 0 };
    PODVector<unsigned> morphRangeCounts{ 0 };
    model->SetVertexBuffers(vertexBuffers, morphRangeStarts, morphRangeCounts);
    model->SetIndexBuffers(indexBuffers);

    return model;
//*/
/* TODO: Single geometry

    model->SetNumGeometries(1);
    SharedPtr<Geometry> geom{ magicContext_->CreateObject<Geometry>() };
    SharedPtr<VertexBuffer> vertexBuffer{ model->GetVertexBuffers().Front() };
    SharedPtr<IndexBuffer> indexBuffer{ model->GetIndexBuffers().Front() };

    for (unsigned s{ 0u }; s < sect.Size(); ++s)
    {
        SharedPtr<Geometry> g{ sect.At(s).Cast(resolution) };
//        model->SetGeometry(s, 0, g);

        PODVector<VertexElement> veA{ vertexBuffer->GetElements() };
        PODVector<VertexElement> veB{ g->GetVertexBuffer(0)->GetElements() };
        vertexBuffer->SetSize((veA.Size() + veB.Size()) * vertexBuffer->GetVertexSize(), veA + veB);

//        indexBuffer.Push(SharedPtr<IndexBuffer>(g->GetIndexBuffer()));

        IndexBuffer* ib{ g->GetIndexBuffer() };
        indexBuffer->Lock(indexBuffer->GetIndexCount(), ib->GetIndexCount());
        indexBuffer->SetDataRange(ib->Lock(0, ib->GetIndexSize()), indexBuffer->GetIndexCount(), ib->GetIndexCount());
        indexBuffer->Unlock();
    }

    model->SetGeometry(0, 0, geom);
    model->SetBoundingBox(bb);

    PODVector<unsigned> morphRangeStarts;
    PODVector<unsigned> morphRangeCounts;
    morphRangeStarts.Push(0);
    morphRangeCounts.Push(0);
//    model->SetVertexBuffers(vertexBuffers, morphRangeStarts, morphRangeCounts);
//    model->SetIndexBuffers(indexBuffers);
    model->SetVertexBuffers({ vertexBuffer }, morphRangeStarts, morphRangeCounts);
    model->SetIndexBuffers({ indexBuffer });

    return model;
    */
}

Sect Grimoire::Curve(const Spell& spell)
{
    return { { spell, Spell::CURVE } };
}

Sect Grimoire::Tris(const Spell& spell)
{
    Sect sect{};
    if (spell.IsEmpty())
        return sect;

    for (unsigned i{ 0 }; i <= spell.Size() - 3; i += 3)
        sect.Push(Spell{ { spell.At(i),
                           spell.At(i + 1),
                           spell.At(i + 2) },
                         Spell::TRI });

    return sect;
}

Sect Grimoire::Quads(const Spell& spell)
{
    Sect sect{};
    if (spell.IsEmpty())
        return sect;

    for (unsigned i{ 0 }; i <= spell.Size() - 4; i += 4)
        sect.Push(Spell{ { spell.At(i),
                           spell.At(i + 1),
                           spell.At(i + 2),
                           spell.At(i + 3) },
                         Spell::QUAD });

    return sect;
}

Sect Grimoire::Rings(const Spell& spell, unsigned n)
{
    Sect sect{};
    if (spell.IsEmpty())
        return sect;

    for (unsigned i{ 0 }; i <= spell.Size() - n; i += n)
    {
        Spell ring{};

        for (unsigned j{ 0 }; j < n; ++j)
            ring.Push(spell.At(i + j));

        sect.Push({ ring, Spell::RING });
    }

    return sect;
}

Sect Grimoire::Sheet(const Spell& spell, unsigned width)
{
    Sect fabric{};
    if (spell.Size() == 0 || width == 0)
        return {};

    for (unsigned i{ 0 }; i < spell.Size() / width; ++i)
    {
        Spell filament{ {}, Spell::CURVE };

        for (unsigned j{ 0 }; j < width; ++j)
        {
            filament.Push(spell.At(i * width + j));
        }

        fabric.Push(filament);
    }

    return Weave(fabric);
}

Sect Grimoire::Weave(const Sect& weft)
{
    Sect fabric{};
    if (!weft.Size())
        return fabric;

    for (unsigned yarn{ 0 }; yarn < weft.Size() - 1; ++yarn)
    {
        Spell yarin{  weft.At(yarn) };
        Spell yarout{ weft.At(yarn + 1) };

        if (yarin.Size() == 0 || yarout.Size() == 0)
            continue;

        Sect warp{};

        if ((yarin.Type() == Spell::POINTS && yarout.Type() == Spell::POINTS)
         || (yarin.Size() == 1 && yarout.Size() == 1))
        {
                warp.Push(Curve({ yarin.Front(), yarout.Front() }));
        }
        else if ((yarin.Type()  == Spell::POINTS &&
                 (yarout.Type() == Spell::RING || yarout.Type() == Spell::CURVE))
             || ((yarin.Type()  == Spell::RING || yarin.Type()  == Spell::CURVE) &&
                  yarout.Type() == Spell::POINTS))
        {
            if (yarin.Type() == Spell::RING)
                yarin.Push(yarin.Front());
            if (yarout.Type() == Spell::RING)
                yarout.Push(yarout.Front());

            Spell tris{};

            if (yarout.Type() == Spell::POINTS)
            {
                Rune share{ yarout.Front() };

                for (unsigned i{ 0 }; i < yarin.Size() - 1u; ++i)
                {
                    tris.Push({ yarin.At(i), yarin.At(i + 1u), share });
                }
            }
            else
            {
                Rune share{ yarin.Front() };

                for (unsigned i{ 0 }; i < yarout.Size() - 1u; ++i)
                {
                    tris.Push({ yarout.At(i + 1u), yarout.At(i), share });
                }
            }

            if (tris.Size())
                warp.Push(Tris(tris));
        }
        else if ((yarin.Type()  == Spell::RING || yarin.Type()  == Spell::CURVE) &&
                 (yarout.Type() == Spell::RING || yarout.Type() == Spell::CURVE))
        {
            if (yarin.Type() == Spell::RING)
                yarin.Push(yarin.Front());
            if (yarout.Type() == Spell::RING)
                yarout.Push(yarout.Front());

            Spell fill{};

            const unsigned reach{ Min(yarin.Size(), yarout.Size()) };
            const unsigned rest{  Max(yarin.Size(), yarout.Size()) - reach };

            for (unsigned i{ 0u }; i < reach - 1u; ++i)
            {
                fill.Push({ yarin.At(i), yarin.At(i + 1), yarout.At(i + 1), yarout.At(i) });
            }

            if (fill.Size())
                warp.Push(Quads(fill));

            if (rest)
            {
                fill.Clear();

                bool restin{ yarin.Size() > yarout.Size() };

                for (unsigned i{ reach }; i - reach < rest; ++i)
                {
                    if (restin)
                        fill.Push({ yarin.At(i - 1), yarin.At(i), yarout.Back() });
                    else
                        fill.Push({ yarin.Back(), yarout.At(i - 1), yarout.At(i) });
                }

                if (fill.Size())
                    warp.Push(Tris(fill));
            }
        }

        if (warp.Size())
            fabric.Push(warp);
    }

    return fabric;
}
