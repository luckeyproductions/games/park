#include <Dry/Graphics/Geometry.h>
#include <Dry/Graphics/IndexBuffer.h>
#include <Dry/Graphics/VertexBuffer.h>

#include "cyberplasm.h"

using namespace Witch;
using namespace Cyberplasm;

Vat::Vat(Context* context): Object(context)
{
    new Grimoire(context_); /// Demystify the ancient
}

SharedPtr<Model> Vat::Generate(Sect sect, unsigned resolution)
{
    SharedPtr<Model> model{ context_->CreateObject<Model>() };

    BoundingBox bb{};
    for (unsigned s{ 0u }; s < sect.Size(); ++s)
    {
        for (unsigned t{ 0 }; t < sect.At(s).Size(); ++t)
            bb.Merge(sect.At(s).At(t).Position());
    }

//    model->SetNumGeometries(sect.Size());
//    Vector<SharedPtr<VertexBuffer> > vertexBuffers;
//    Vector<SharedPtr<IndexBuffer> > indexBuffers;

    /// Broken
    model->SetNumGeometries(1);
    SharedPtr<Geometry> geom{ model->GetGeometry(0, 0) };
    SharedPtr<VertexBuffer> vertexBuffer;
    SharedPtr<IndexBuffer> indexBuffer;

    for (unsigned s{ 0u }; s < sect.Size(); ++s)
    {
        SharedPtr<Geometry> g{ sect.At(s).Cast(resolution) };
//        model->SetGeometry(s, 0, g);

        PODVector<VertexElement> veA{ vertexBuffer->GetElements() };
        PODVector<VertexElement> veB{ g->GetVertexBuffer(0)->GetElements() };
        vertexBuffer->SetSize(veA.Size() + veB.Size(), veA + veB);

//        indexBuffer.Push(SharedPtr<IndexBuffer>(g->GetIndexBuffer()));

        IndexBuffer* ib{ g->GetIndexBuffer() };
        indexBuffer->Lock(indexBuffer->GetIndexCount(), ib->GetIndexCount());
        indexBuffer->SetDataRange(ib->Lock(0, ib->GetIndexSize()), indexBuffer->GetIndexCount(), ib->GetIndexCount());
        indexBuffer->Unlock();
    }

    model->SetGeometry(0, 0, geom);
    model->SetBoundingBox(bb);

    PODVector<unsigned> morphRangeStarts;
    PODVector<unsigned> morphRangeCounts;
    morphRangeStarts.Push(0);
    morphRangeCounts.Push(0);
//    model->SetVertexBuffers(vertexBuffers, morphRangeStarts, morphRangeCounts);
//    model->SetIndexBuffers(indexBuffers);
    model->SetVertexBuffers({ vertexBuffer }, morphRangeStarts, morphRangeCounts);
    model->SetIndexBuffers({ indexBuffer });

    return model;
}

SharedPtr<Geometry> Vat::GeometryFromData(const PODVector<float>& vertexData, const PODVector<unsigned short>& indexData, PrimitiveType type)
{
    const unsigned numVertices{ indexData.Size() };

    SharedPtr<VertexBuffer> vb{ new VertexBuffer{ context_ } };
    SharedPtr<IndexBuffer> ib{ new IndexBuffer{ context_ } };

    // Shadowed buffer needed for raycasts to work, and so that data can be automatically restored on device loss
    vb->SetShadowed(true);
    // We could use the "legacy" element bitmask to define elements for more compact code, but let's demonstrate
    // defining the vertex elements explicitly to allow any element types and order
    PODVector<VertexElement> elements;
    elements.Push(VertexElement{ TYPE_VECTOR3, SEM_POSITION });
    elements.Push(VertexElement{ TYPE_VECTOR3, SEM_NORMAL });
    vb->SetSize(numVertices, elements);
    vb->SetData(vertexData.Buffer());

    ib->SetShadowed(true);
    ib->SetSize(numVertices, false);
    ib->SetData(indexData.Buffer());

    SharedPtr<Geometry> vat{ new Geometry{ context_ } };
    vat->SetVertexBuffer(0, vb);
    vat->SetIndexBuffer(ib);
    vat->SetDrawRange(type, 0, numVertices);

    return vat;
}
