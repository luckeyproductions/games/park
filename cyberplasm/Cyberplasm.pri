INCLUDEPATH += \
    $$PWD/include

HEADERS += \
    $$PWD/include/cyberplasm.h \
    $$PWD/include/geonomicon.h \
    $$PWD/include/grimoire.h \
    $$PWD/include/luckey.h \
    $$PWD/include/witch.h

SOURCES += \
    $$PWD/src/cyberplasm.cpp \
    $$PWD/src/geonomicon.cpp \
    $$PWD/src/grimoire.cpp \
    $$PWD/src/luckey.cpp \
    $$PWD/src/witch.cpp
