include (src/Park.pri)
include (cyberplasm/Cyberplasm.pri)

TARGET = luckeypark

LIBS += \
    $$(DRY_HOME)/lib/libDry.a \
    -lpthread \
    -ldl \
    -lGL

QMAKE_CXXFLAGS += -std=c++17 -O2

INCLUDEPATH += \
    $$(DRY_HOME)/include \
    $$(DRY_HOME)/include/Dry/ThirdParty \

TEMPLATE = app
CONFIG -= app_bundle
CONFIG -= qt

unix
{
    isEmpty(DATADIR) DATADIR = $$(XDG_DATA_HOME)
    isEmpty(DATADIR) DATADIR = $$(HOME)/.local/share

    target.path = /usr/games/
    INSTALLS += target

    resources.path = $$DATADIR/luckey/luckeypark/
    resources.files = Resources/*
    INSTALLS += resources

    icon.path = $$DATADIR/icons/
    icon.files = luckeypark.svg
    INSTALLS += icon

    desktop.path = $$DATADIR/applications/
    desktop.files = luckeypark.desktop
    INSTALLS += desktop

    DEFINES += RESOURCEPATH=\\\"$${resources.path}\\\"
}
