# ![LucKey Park](Docs/Logo.png)

## How to compile

- [Building Dry and LucKey Park from source on Fedora Linux](Docs/fedora-build-and-debug.md)

## Controls

### Keyboard

#### Camera
|Key|Action|
|:---:|---|
**W** | Move forward
**A** | Move left
**S** | Move back
**D** | Move right
**Q** / **Keypad +** | Move down
**E** / **Keypad -** | Move up
**I** / **Keypad 8** | Rotate up
**J** / **Keypad 4** | Rotate left
**K** / **Keypad 2** | Rotate down
**L** / **Keypad 6** | Rotate right
**Shift**| Move faster

#### Tools
|Key|Action|
|:---:|---|
**V** | Builder
**B** | Dozer
**N** | Digger
**M** | Looker

##### Builder
|Key|Action|
|:---:|---|

###### Coaster construction
|Key|Action|
|:---:|---|
**R** | Reset slope and bank
**)** | Slope up
**(** | Slope down
**[** | Bank left
**]** | Bank right
**=** ( + ) | Increase height
**-** ( _ ) | Decrease height
**Ctrl** | Slope/Bank/Height step x4

## Media

### Screenshots
![Screenshot](Docs/screenshot.jpg)
### GIFs
- [**Coaster 42**](https://luckey.games/images/coaster42.gif)



