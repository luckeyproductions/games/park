#!/bin/sh

sudo chown -R $USER ~/.local/share/luckey/luckeypark/
sudo chown $USER ~/.local/share/icons/luckeypark.svg
update-icon-caches ~/.local/share/icons/
